package com.weyr_associates.animaltrakkerfarmmobile

import android.app.Application
import android.util.Log

class AnimalTrakkerApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        if (!AppDirectories.ensureAppDirectoriesExist(this)) {
            Log.w(TAG, "Failed to ensure the existence of the application directory structure.")
        }
    }

    companion object {
        private val TAG = AnimalTrakkerApplication::class.java.simpleName
    }
}
