package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.time.LocalDate
import java.time.temporal.ChronoUnit

@Parcelize
data class AnimalBasicInfo(
    override val id: Int,
    override val name: String,
    val alert: String?,
    val flockPrefix: String?,
    val ownerName: String?,
    val ids: List<IdBasicInfo> = emptyList(),
    val speciesId: Int,
    val speciesCommonName: String,
    val breedId: Int,
    val breedName: String,
    val breedAbbreviation: String,
    val sexId: Int,
    val sexName: String,
    val sexAbbreviation: String,
    val sexStandardName: String,
    val sexStandardAbbreviation: String,
    val birthDate: LocalDate
) : Parcelable, HasIdentity<Int>, HasName {

    fun ageInDays(): Long {
        return ChronoUnit.DAYS.between(birthDate, LocalDate.now())
    }

    fun ageInYears(): Long {
        return ChronoUnit.YEARS.between(birthDate, LocalDate.now())
    }
}
