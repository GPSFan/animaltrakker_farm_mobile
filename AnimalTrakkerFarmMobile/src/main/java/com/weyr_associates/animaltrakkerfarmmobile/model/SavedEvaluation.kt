package com.weyr_associates.animaltrakkerfarmmobile.model

data class SavedEvaluation(
    val id: Int,
    val name: String,
    val userId: Int,
    val userType: UserType,
    val trait01: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait02: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait03: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait04: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait05: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait06: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait07: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait08: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait09: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait10: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait11: UnitsEvalTrait = UnitsEvalTrait.EMPTY,
    val trait12: UnitsEvalTrait = UnitsEvalTrait.EMPTY,
    val trait13: UnitsEvalTrait = UnitsEvalTrait.EMPTY,
    val trait14: UnitsEvalTrait = UnitsEvalTrait.EMPTY,
    val trait15: UnitsEvalTrait = UnitsEvalTrait.EMPTY,
    val trait16: CustomEvalTrait = CustomEvalTrait.EMPTY,
    val trait17: CustomEvalTrait = CustomEvalTrait.EMPTY,
    val trait18: CustomEvalTrait = CustomEvalTrait.EMPTY,
    val trait19: CustomEvalTrait = CustomEvalTrait.EMPTY,
    val trait20: CustomEvalTrait = CustomEvalTrait.EMPTY,
) {
    companion object {
        const val ID_OPTIMAL_AG_RAM_TEST = 4
    }
}
