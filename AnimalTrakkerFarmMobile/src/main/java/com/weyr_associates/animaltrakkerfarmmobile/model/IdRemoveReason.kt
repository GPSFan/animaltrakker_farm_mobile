package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class IdRemoveReason(
    override val id: Int,
    val text: String,
    val order: Int
) : Parcelable, HasIdentity<Int> {
    companion object {
        const val ID_CORRECT_TAG_DATA = 8
    }
}
