package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Contact(
    val id: Int,
    val firstName: String,
    val middleName: String,
    val lastName: String,
    val titleId: Int
) : Parcelable {
    val firstAndLastName: String
        get() = "$firstName $lastName"
}
