package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * Owner is not uniquely identified by its id field,
 * but rather its id and type fields.
 */
@Parcelize
data class Owner(
    val id: Int,
    val type: Type,
    override val name: String,
    val order: Int
) : Parcelable, HasName {
    enum class Type(val id: Int) {
        CONTACT(0),
        COMPANY(1);

        companion object {
            fun fromId(id: Int): Type = when(id) {
                CONTACT.id -> CONTACT
                COMPANY.id -> COMPANY
                else -> throw IllegalArgumentException(
                    "$id is not a valid code for ${Type::class.simpleName}."
                )
            }
        }
        val isContact: Boolean
            get() = this == CONTACT

        val isCompany: Boolean
            get() = this == COMPANY
    }
}
