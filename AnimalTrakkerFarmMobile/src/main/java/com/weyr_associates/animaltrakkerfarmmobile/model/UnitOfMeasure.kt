package com.weyr_associates.animaltrakkerfarmmobile.model

data class UnitOfMeasure(
    val id: Int,
    val name: String,
    val abbreviation: String,
    val type: Type,
    val order: Int = Int.MAX_VALUE
) {
    companion object {
        const val SALE_PRICE_US_DOLLARS = 8

        val NONE = UnitOfMeasure(
            id = 0,
            name = "None",
            abbreviation = "None",
            type = Type.NONE,
            order = Int.MAX_VALUE
        )
    }

    data class Type(
        val id: Int,
        val name: String,
        val order: Int = Int.MAX_VALUE
    ) {
        companion object {
            val NONE = Type(
                id = 0,
                name = "None",
                order = Int.MAX_VALUE
            )
        }
    }
}
