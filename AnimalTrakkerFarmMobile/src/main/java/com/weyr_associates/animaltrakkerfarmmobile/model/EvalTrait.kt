package com.weyr_associates.animaltrakkerfarmmobile.model

sealed interface EvalTrait {
    val id: Int
    val name: String
    val typeId: Int
    val order: Int
    val isEmpty: Boolean
    val isOptional: Boolean
    val isDeferred: Boolean

    companion object {
        const val UNIT_TRAIT_ID_SCROTAL_CIRCUMFERENCE = 15
        const val UNIT_TRAIT_ID_BODY_CONDITION_SCORE = 51
    }
}

data class BasicEvalTrait(
    override val id: Int,
    override val name: String,
    override val typeId: Int,
    override val isOptional: Boolean,
    override val isDeferred: Boolean,
    override val isEmpty: Boolean = false,
    override val order: Int = Int.MAX_VALUE,
) : EvalTrait {
    companion object {
        val EMPTY = BasicEvalTrait(
            id = 0,
            name = "N/A",
            typeId = EvalTraitType.ID_SCORED_1_TO_5,
            isEmpty = true,
            isOptional = false,
            isDeferred = false
        )
    }
}

data class UnitsEvalTrait(
    override val id: Int,
    override val name: String,
    override val typeId: Int,
    override val isOptional: Boolean,
    override val isDeferred: Boolean,
    val units: UnitOfMeasure,
    override val isEmpty: Boolean = false,
    override val order: Int = Int.MAX_VALUE,
) : EvalTrait {
    companion object {
        val EMPTY = UnitsEvalTrait(
            id = 0,
            name = "N/A",
            typeId = EvalTraitType.ID_SCORED_1_TO_5,
            isEmpty = true,
            isOptional = false,
            isDeferred = false,
            units = UnitOfMeasure.NONE
        )
    }
}

data class CustomEvalTrait(
    override val id: Int,
    override val name: String,
    override val typeId: Int,
    override val isOptional: Boolean,
    override val isDeferred: Boolean,
    val options: List<EvalTraitOption>,
    override val isEmpty: Boolean = false,
    override val order: Int = Int.MAX_VALUE
) : EvalTrait {
    companion object {
        val EMPTY = CustomEvalTrait(
            id = 0,
            name = "N/A",
            typeId = EvalTraitType.ID_SCORED_1_TO_5,
            isEmpty = true,
            isOptional = false,
            isDeferred = false,
            options = emptyList()
        )
    }
}
