package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class EvalTraitOption(
    override val id: Int,
    val traitId: Int,
    override val name: String,
    val order: Int = Int.MAX_VALUE
) : Parcelable, HasIdentity<Int>, HasName
