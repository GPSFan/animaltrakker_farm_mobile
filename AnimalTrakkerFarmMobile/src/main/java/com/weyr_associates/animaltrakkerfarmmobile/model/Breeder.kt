package com.weyr_associates.animaltrakkerfarmmobile.model

object Breeder {
    enum class Type(val id: Int) {
        CONTACT(0),
        COMPANY(1);

        companion object {
            fun fromId(id: Int): Type = when(id) {
                CONTACT.id -> CONTACT
                COMPANY.id -> COMPANY
                else -> throw IllegalArgumentException(
                    "$id is not a valid code for ${Type::class.simpleName}."
                )
            }
        }
    }
}
