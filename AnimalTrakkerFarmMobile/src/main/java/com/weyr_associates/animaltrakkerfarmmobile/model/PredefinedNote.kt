package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PredefinedNote(
    override val id: Int,
    val text: String,
    val userId: Int,
    val userType: UserType,
    val order: Int
): Parcelable, HasIdentity<Int>
