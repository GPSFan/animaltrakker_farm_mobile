package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Laboratory(
    override val id: Int,
    val companyId: Int,
    override val name: String,
    val licenseNumber: String,
    val order: Int
) : Parcelable, HasIdentity<Int>, HasName {
    companion object {
        const val LAB_COMPANY_ID_OPTIMAL_LIVESTOCK = 660
    }
}
