package com.weyr_associates.animaltrakkerfarmmobile.repository.database

import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.database.IdColorTable
import com.weyr_associates.animaltrakkerfarmmobile.database.readAllItems
import com.weyr_associates.animaltrakkerfarmmobile.database.readFirstItem
import com.weyr_associates.animaltrakkerfarmmobile.model.IdColor
import com.weyr_associates.animaltrakkerfarmmobile.repository.IdColorRepository

class IdColorRepositoryImpl(private val databaseHandler: DatabaseHandler) : IdColorRepository {

    companion object {
        private const val SQL_QUERY_ID_COLORS =
            """SELECT * FROM ${IdColorTable.NAME}
                ORDER BY ${IdColorTable.Columns.ORDER}"""

        private const val SQL_QUERY_ID_COLOR_BY_ID =
            """SELECT * FROM ${IdColorTable.NAME}
                WHERE ${IdColorTable.Columns.ID} = ?"""
    }

    override fun queryIdColors(): List<IdColor> {
        return databaseHandler.readableDatabase.rawQuery(SQL_QUERY_ID_COLORS, null)
            ?.use { it.readAllItems(IdColorTable::idColorFromCursor) } ?: emptyList()
    }

    override fun queryIdColor(id: Int): IdColor? {
        return databaseHandler.readableDatabase.rawQuery(SQL_QUERY_ID_COLOR_BY_ID, arrayOf(id.toString()))
            ?.use { it.readFirstItem(IdColorTable::idColorFromCursor) }
    }
}
