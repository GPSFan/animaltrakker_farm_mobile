package com.weyr_associates.animaltrakkerfarmmobile.repository

import com.weyr_associates.animaltrakkerfarmmobile.model.Owner

interface OwnerRepository {
    fun queryOwners(): List<Owner>
    fun queryOwner(ownerId: Int, typeId: Int): Owner?
}
