package com.weyr_associates.animaltrakkerfarmmobile.repository

import com.weyr_associates.animaltrakkerfarmmobile.model.DefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.model.ItemEntry

interface DefaultSettingsRepository {
    fun queryDefaultSettingsEntries(): List<ItemEntry>
    fun queryDefaultSettingsEntryById(id: Int): ItemEntry?
    fun queryStandardDefaultSettings(): DefaultSettings
    fun queryDefaultSettingsById(id: Int): DefaultSettings?

    fun updateNextTrichIdNumber(id: Int, nextTrichId: Int): Int
}
