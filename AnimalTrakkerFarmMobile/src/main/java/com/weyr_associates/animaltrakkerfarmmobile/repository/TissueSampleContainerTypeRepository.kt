package com.weyr_associates.animaltrakkerfarmmobile.repository

import com.weyr_associates.animaltrakkerfarmmobile.model.TissueSampleContainerType

interface TissueSampleContainerTypeRepository {
    fun queryTissueSampleContainerTypes(): List<TissueSampleContainerType>
    fun queryTissueSampleContainerTypeById(id: Int): TissueSampleContainerType?
}
