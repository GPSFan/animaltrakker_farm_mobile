package com.weyr_associates.animaltrakkerfarmmobile.repository

import com.weyr_associates.animaltrakkerfarmmobile.model.Species

interface SpeciesRepository {
    suspend fun querySpeciesById(id: Int): Species?
}
