package com.weyr_associates.animaltrakkerfarmmobile.repository.database

import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.database.ContactTable
import com.weyr_associates.animaltrakkerfarmmobile.database.readAllItems
import com.weyr_associates.animaltrakkerfarmmobile.database.readFirstItem
import com.weyr_associates.animaltrakkerfarmmobile.model.Contact
import com.weyr_associates.animaltrakkerfarmmobile.repository.ContactRepository

class ContactRepositoryImpl(private val databaseHandler: DatabaseHandler) : ContactRepository {

    companion object {
        private const val SQL_QUERY_CONTACTS =
            """SELECT * FROM ${ContactTable.NAME}
                ORDER BY ${ContactTable.Columns.LAST_NAME} ASC"""

        private const val SQL_QUERY_CONTACT_BY_ID =
            """SELECT * FROM ${ContactTable.NAME}
                WHERE ${ContactTable.Columns.ID} = ?"""
    }
    override fun queryContacts(): List<Contact> {
        return databaseHandler.readableDatabase.rawQuery(SQL_QUERY_CONTACTS, null)
            ?.use { it.readAllItems(ContactTable::contactFromCursor) } ?: emptyList()
    }

    override fun queryContact(id: Int): Contact? {
        return databaseHandler.readableDatabase.rawQuery(SQL_QUERY_CONTACT_BY_ID, arrayOf(id.toString()))
            ?.use { it.readFirstItem(ContactTable::contactFromCursor) }
    }
}
