package com.weyr_associates.animaltrakkerfarmmobile.repository.database

import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.database.SpeciesTable
import com.weyr_associates.animaltrakkerfarmmobile.database.readFirstItem
import com.weyr_associates.animaltrakkerfarmmobile.model.Species
import com.weyr_associates.animaltrakkerfarmmobile.repository.SpeciesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SpeciesRepositoryImpl(private val databaseHandler: DatabaseHandler) : SpeciesRepository {
    override suspend fun querySpeciesById(id: Int): Species? {
        return withContext(Dispatchers.IO) {
            databaseHandler.readableDatabase.rawQuery(
                SpeciesTable.Sql.SQL_QUERY_SPECIES_BY_ID,
                arrayOf(id.toString())
            )?.use { cursor ->
                cursor.readFirstItem(SpeciesTable::speciesFromCursor)
            }
        }
    }
}
