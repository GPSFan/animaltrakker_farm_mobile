package com.weyr_associates.animaltrakkerfarmmobile.repository

import com.weyr_associates.animaltrakkerfarmmobile.model.Laboratory

interface LaboratoryRepository {
    fun queryLaboratories(): List<Laboratory>
    fun queryLaboratoryById(id: Int): Laboratory?
    fun queryLaboratoryByCompanyId(companyId: Int): Laboratory?
}
