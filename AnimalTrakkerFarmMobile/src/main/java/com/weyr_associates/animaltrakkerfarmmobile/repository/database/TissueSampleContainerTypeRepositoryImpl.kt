package com.weyr_associates.animaltrakkerfarmmobile.repository.database

import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.database.TissueSampleContainerTypeTable
import com.weyr_associates.animaltrakkerfarmmobile.database.TissueSampleContainerTypeTable.Sql.SQL_QUERY_TISSUE_SAMPLE_CONTAINER_TYPES
import com.weyr_associates.animaltrakkerfarmmobile.database.TissueSampleContainerTypeTable.Sql.SQL_QUERY_TISSUE_SAMPLE_CONTAINER_TYPE_BY_ID
import com.weyr_associates.animaltrakkerfarmmobile.database.readAllItems
import com.weyr_associates.animaltrakkerfarmmobile.database.readFirstItem
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueSampleContainerType
import com.weyr_associates.animaltrakkerfarmmobile.repository.TissueSampleContainerTypeRepository

class TissueSampleContainerTypeRepositoryImpl(
    private val databaseHandler: DatabaseHandler
) : TissueSampleContainerTypeRepository {
    override fun queryTissueSampleContainerTypes(): List<TissueSampleContainerType> {
        return databaseHandler.readableDatabase.rawQuery(
            SQL_QUERY_TISSUE_SAMPLE_CONTAINER_TYPES,
            emptyArray()
        )?.use { cursor ->
            return cursor.readAllItems(TissueSampleContainerTypeTable::tissueSampleContainerTypeFrom)
        } ?: emptyList()
    }

    override fun queryTissueSampleContainerTypeById(id: Int): TissueSampleContainerType? {
        return databaseHandler.readableDatabase.rawQuery(
            SQL_QUERY_TISSUE_SAMPLE_CONTAINER_TYPE_BY_ID,
            arrayOf(id.toString())
        )?.use { cursor ->
            return cursor.readFirstItem(TissueSampleContainerTypeTable::tissueSampleContainerTypeFrom)
        }
    }
}
