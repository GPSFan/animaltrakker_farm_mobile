package com.weyr_associates.animaltrakkerfarmmobile.repository

import com.weyr_associates.animaltrakkerfarmmobile.model.SavedEvaluation

interface EvaluationRepository {
    suspend fun querySavedEvaluationById(id: Int): SavedEvaluation?
}
