package com.weyr_associates.animaltrakkerfarmmobile.repository.database

object AnimalTissueTestRequestTable {

    const val NAME = "animal_tissue_test_request_table"

    object Columns {
        const val ID = "id_animaltissuetestrequestid"
        const val SAMPLE_TAKEN_ID = "id_animaltissuesampletakenid"
        const val TEST_ID = "id_tissuetestid"
        const val LABORATORY_COMPANY_ID = "id_companylaboratoryid"
        const val LABORATORY_ACCESSION_ID = "tissue_sample_lab_accession_id"
        const val TEST_RESULTS = "tissue_test_results"
        const val TEST_RESULTS_DATE = "tissue_test_results_date"
        const val TEST_RESULTS_TIME = "tissue_test_results_time"
        const val ANIMAL_EXTERNAL_FILE_ID = "id_tissue_test_results_id_animalexternalfileid"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
