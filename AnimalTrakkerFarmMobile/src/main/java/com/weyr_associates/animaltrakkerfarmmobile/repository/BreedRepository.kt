package com.weyr_associates.animaltrakkerfarmmobile.repository

import com.weyr_associates.animaltrakkerfarmmobile.model.Breed

interface BreedRepository {
    fun queryBreedsForSpecies(speciesId: Int): List<Breed>

    fun queryBreed(id: Int): Breed?
}
