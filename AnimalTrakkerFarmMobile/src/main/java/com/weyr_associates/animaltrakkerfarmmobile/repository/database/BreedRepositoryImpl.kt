package com.weyr_associates.animaltrakkerfarmmobile.repository.database

import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.database.BreedTable
import com.weyr_associates.animaltrakkerfarmmobile.database.readAllItems
import com.weyr_associates.animaltrakkerfarmmobile.database.readFirstItem
import com.weyr_associates.animaltrakkerfarmmobile.model.Breed
import com.weyr_associates.animaltrakkerfarmmobile.repository.BreedRepository

class BreedRepositoryImpl(private val databaseHandler: DatabaseHandler) : BreedRepository {

    companion object {

        private const val SQL_QUERY_BREEDS =
            """SELECT * FROM ${BreedTable.NAME}
                WHERE ${BreedTable.Columns.SPECIES_ID} = ?
                ORDER BY ${BreedTable.Columns.ORDER}"""

        private const val SQL_QUERY_BREED_BY_ID =
            """SELECT * FROM ${BreedTable.NAME}
                WHERE ${BreedTable.Columns.ID} = ?"""
    }

    override fun queryBreedsForSpecies(speciesId: Int): List<Breed> {
        return databaseHandler.readableDatabase.rawQuery(SQL_QUERY_BREEDS, arrayOf(speciesId.toString()))
            ?.use { it.readAllItems(BreedTable::breedFromCursor) } ?: emptyList()
    }

    override fun queryBreed(id: Int): Breed? {
        return databaseHandler.readableDatabase.rawQuery(SQL_QUERY_BREED_BY_ID, arrayOf(id.toString()))
            ?.use { it.readFirstItem(BreedTable::breedFromCursor) }
    }
}
