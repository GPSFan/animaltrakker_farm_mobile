package com.weyr_associates.animaltrakkerfarmmobile.repository.database

import android.content.ContentValues
import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.database.AnimalBreedTable
import com.weyr_associates.animaltrakkerfarmmobile.database.AnimalEvaluationTable
import com.weyr_associates.animaltrakkerfarmmobile.database.AnimalFlockPrefixTable
import com.weyr_associates.animaltrakkerfarmmobile.database.AnimalIdInfoTable
import com.weyr_associates.animaltrakkerfarmmobile.database.AnimalLocationHistoryTable
import com.weyr_associates.animaltrakkerfarmmobile.database.AnimalNoteTable
import com.weyr_associates.animaltrakkerfarmmobile.database.AnimalOwnershipHistoryTable
import com.weyr_associates.animaltrakkerfarmmobile.database.AnimalRegistrationTable
import com.weyr_associates.animaltrakkerfarmmobile.database.AnimalTable
import com.weyr_associates.animaltrakkerfarmmobile.database.BreedTable
import com.weyr_associates.animaltrakkerfarmmobile.database.CompanyTable
import com.weyr_associates.animaltrakkerfarmmobile.database.ContactTable
import com.weyr_associates.animaltrakkerfarmmobile.database.FlockPrefixTable
import com.weyr_associates.animaltrakkerfarmmobile.database.IdColorTable
import com.weyr_associates.animaltrakkerfarmmobile.database.IdLocationTable
import com.weyr_associates.animaltrakkerfarmmobile.database.IdTypeTable
import com.weyr_associates.animaltrakkerfarmmobile.database.SexTable
import com.weyr_associates.animaltrakkerfarmmobile.database.SpeciesTable
import com.weyr_associates.animaltrakkerfarmmobile.database.Sql
import com.weyr_associates.animaltrakkerfarmmobile.database.getBoolean
import com.weyr_associates.animaltrakkerfarmmobile.database.getInt
import com.weyr_associates.animaltrakkerfarmmobile.database.getLocalDate
import com.weyr_associates.animaltrakkerfarmmobile.database.getLocalTime
import com.weyr_associates.animaltrakkerfarmmobile.database.getOptString
import com.weyr_associates.animaltrakkerfarmmobile.database.getString
import com.weyr_associates.animaltrakkerfarmmobile.database.isNull
import com.weyr_associates.animaltrakkerfarmmobile.database.putOrNullish
import com.weyr_associates.animaltrakkerfarmmobile.database.readAllItems
import com.weyr_associates.animaltrakkerfarmmobile.database.readFirstItem
import com.weyr_associates.animaltrakkerfarmmobile.database.readItem
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalBasicInfo
import com.weyr_associates.animaltrakkerfarmmobile.model.Breeder
import com.weyr_associates.animaltrakkerfarmmobile.model.IdBasicInfo
import com.weyr_associates.animaltrakkerfarmmobile.model.IdRemoveReason
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType
import com.weyr_associates.animaltrakkerfarmmobile.model.Owner
import com.weyr_associates.animaltrakkerfarmmobile.model.Sex
import com.weyr_associates.animaltrakkerfarmmobile.model.SexStandard
import com.weyr_associates.animaltrakkerfarmmobile.model.Species
import com.weyr_associates.animaltrakkerfarmmobile.model.TransferReason
import com.weyr_associates.animaltrakkerfarmmobile.model.UnitOfMeasure
import com.weyr_associates.animaltrakkerfarmmobile.repository.AnimalRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.time.LocalDate
import java.time.LocalDateTime

class AnimalRepositoryImpl(private val databaseHandler: DatabaseHandler) : AnimalRepository {

    override suspend fun searchAnimalsByIdType(
        partialId: String,
        idTypeId: Int,
        speciesId: Int,
        sexStandard: SexStandard?
    ): List<AnimalBasicInfo> {
        return withContext(Dispatchers.IO) {
            databaseHandler.readableDatabase.rawQuery(
                SQL_SEARCH_ANIMAL_BASIC_INFO_BY_ID_TYPE.takeIf { sexStandard == null }
                    ?: SQL_SEARCH_ANIMAL_BASIC_INFO_BY_ID_TYPE_AND_SEX_STANDARD,
                appendOptionalSexStandardParam(
                    arrayOf(
                        speciesId.toString(),
                        idTypeId.toString(),
                        Sql.escapeWildcards(partialId)
                    ),
                    sexStandard
                )
            )?.use { it.readAllItems(::animalBasicInfoFrom) } ?: emptyList()
        }
    }

    override suspend fun searchAnimalsByName(
        partialName: String,
        speciesId: Int,
        sexStandard: SexStandard?
    ): List<AnimalBasicInfo> {
        return withContext(Dispatchers.IO) {
            databaseHandler.readableDatabase.rawQuery(
                SQL_SEARCH_ANIMAL_BASIC_INFO_BY_NAME.takeIf { sexStandard == null}
                    ?: SQL_SEARCH_ANIMAL_BASIC_INFO_BY_NAME_AND_SEX_STANDARD,
                appendOptionalSexStandardParam(
                    arrayOf(
                        speciesId.toString(),
                        Sql.escapeWildcards(partialName)
                    ),
                    sexStandard
                )
            )?.use { it.readAllItems(::animalBasicInfoFrom) } ?: emptyList()
        }
    }

    override suspend fun queryEIDExistence(eidNumber: String): Boolean {
        return withContext(Dispatchers.IO) {
            databaseHandler.readableDatabase.rawQuery(
                SQL_QUERY_EXISTENCE_OF_EID,
                arrayOf(eidNumber)
            )?.use { cursor ->
                cursor.moveToFirst() && cursor.getBoolean(COLUMN_NAME_EID_EXISTS)
            } ?: false
        }
    }

    override suspend fun queryEIDExistenceForUpdate(idToUpdate: Int, eidNumber: String): Boolean {
        return withContext(Dispatchers.IO) {
            databaseHandler.readableDatabase.rawQuery(
                SQL_QUERY_EXISTENCE_OF_EID_EXCEPT_FOR_ID,
                arrayOf(eidNumber, idToUpdate.toString())
            )?.use { cursor ->
                cursor.moveToFirst() && cursor.getBoolean(COLUMN_NAME_EID_EXISTS)
            } ?: false
        }
    }

    override suspend fun queryAnimalByAnimalId(animalId: Int): AnimalBasicInfo? {
        return withContext(Dispatchers.IO) {
            databaseHandler.readableDatabase.rawQuery(
                SQL_QUERY_ANIMAL_BASIC_INFO_BY_ANIMAL_ID,
                arrayOf(animalId.toString())
            )?.use { cursor ->
                cursor.readFirstItem(::animalBasicInfoFrom)
            }
        }
    }

    override suspend fun queryAnimalByEID(eidNumber: String): AnimalBasicInfo? {
        return withContext(Dispatchers.IO) {
            databaseHandler.readableDatabase.rawQuery(
                SQL_QUERY_ANIMAL_BASIC_INFO_BY_EID,
                arrayOf(eidNumber)
            )?.use { cursor ->
                cursor.readFirstItem(::animalBasicInfoFrom)
            }
        }
    }

    override suspend fun addAnimal(
        animalName: String,
        breedId: Int,
        sexId: Int,
        birthDate: LocalDate,
        birthTypeId: Int,
        rearTypeId: Int,
        ownerId: Int,
        ownerType: Owner.Type,
        ownerPremiseId: Int,
        breederId: Int,
        breederTypeId: Int,
        timeStampOn: LocalDateTime
    ): Long {
        return databaseHandler.writableDatabase.run {
            val createdAtDateTimeString = Sql.formatDateTime(timeStampOn)
            val birthDateString = Sql.formatDate(birthDate)
            beginTransaction()
            try {
                val animalId = insertOrThrow(
                    AnimalTable.NAME,
                    null,
                    ContentValues().apply {
                        put(AnimalTable.Columns.NAME, animalName)
                        put(AnimalTable.Columns.SEX_ID, sexId)
                        put(AnimalTable.Columns.BIRTH_DATE, birthDateString)
                        put(AnimalTable.Columns.BIRTH_TIME, "08:00:00")
                        put(AnimalTable.Columns.BIRTH_TYPE_ID, birthTypeId)
                        put(AnimalTable.Columns.BIRTH_WEIGHT, Sql.NULLISH)
                        put(AnimalTable.Columns.BIRTH_WEIGH_UNITS_ID, Sql.NULLISH)
                        put(AnimalTable.Columns.BIRTH_ORDER, 0)
                        put(AnimalTable.Columns.REAR_TYPE, rearTypeId)
                        put(AnimalTable.Columns.WEANED_DATE, Sql.NULLISH)
                        put(AnimalTable.Columns.DEATH_DATE, Sql.NULLISH)
                        put(AnimalTable.Columns.DEATH_REASON_ID, 0)
                        put(
                            AnimalTable.Columns.SIRE_ID,
                            Species.unknownSireIdForSpecies(
                                Sex.speciesIdFromSexId(sexId)
                            )
                        )
                        put(
                            AnimalTable.Columns.DAM_ID,
                            Species.unknownDamIdForSpecies(
                                Sex.speciesIdFromSexId(sexId)
                            )
                        )
                        put(AnimalTable.Columns.FOSTER_DAM_ID, Sql.NULLISH)
                        put(AnimalTable.Columns.SURROGATE_DAM_ID, Sql.NULLISH)
                        put(AnimalTable.Columns.IS_HAND_REARED, Sql.NULLISH)
                        put(AnimalTable.Columns.MANAGEMENT_GROUP_ID, Sql.NULLISH)
                        put(AnimalTable.Columns.ALERT, Sql.NULLISH)
                        put(AnimalTable.Columns.CREATED, createdAtDateTimeString)
                        put(AnimalTable.Columns.MODIFIED, createdAtDateTimeString)
                    }
                )
                insertOrThrow(
                    AnimalOwnershipHistoryTable.NAME,
                    null,
                    ContentValues().apply {
                        put(AnimalOwnershipHistoryTable.Columns.ANIMAL_ID, animalId)
                        put(AnimalOwnershipHistoryTable.Columns.TRANSFER_DATE, birthDateString)
                        put(AnimalOwnershipHistoryTable.Columns.FROM_CONTACT_ID, Sql.NULLISH)
                        put(AnimalOwnershipHistoryTable.Columns.FROM_COMPANY_ID, Sql.NULLISH)
                        putOrNullish(
                            AnimalOwnershipHistoryTable.Columns.TO_CONTACT_ID,
                            ownerId,
                            shouldPutValue = ownerType.isContact
                        )
                        putOrNullish(
                            AnimalOwnershipHistoryTable.Columns.TO_COMPANY_ID,
                            ownerId,
                            shouldPutValue = ownerType.isCompany)
                        put(
                            AnimalOwnershipHistoryTable.Columns.TRANSFER_REASON_ID,
                            TransferReason.ID_NATURAL_ADDITION
                        )
                        put(AnimalOwnershipHistoryTable.Columns.SELL_PRICE, 0)
                        put(
                            AnimalOwnershipHistoryTable.Columns.SELL_PRICE_UNITS_ID,
                            UnitOfMeasure.SALE_PRICE_US_DOLLARS
                        )
                        put(
                            AnimalOwnershipHistoryTable.Columns.CREATED,
                            createdAtDateTimeString
                        )
                        put(
                            AnimalOwnershipHistoryTable.Columns.MODIFIED,
                            createdAtDateTimeString
                        )
                    }
                )
                insertOrThrow(
                    AnimalLocationHistoryTable.NAME,
                    null,
                    ContentValues().apply {
                        put(AnimalLocationHistoryTable.Columns.ANIMAL_ID, animalId)
                        put(AnimalLocationHistoryTable.Columns.FROM_PREMISE_ID, Sql.NULLISH)
                        put(AnimalLocationHistoryTable.Columns.TO_PREMISE_ID, ownerPremiseId)
                        put(AnimalLocationHistoryTable.Columns.MOVEMENT_DATE, birthDateString)
                        put(AnimalLocationHistoryTable.Columns.CREATED, createdAtDateTimeString)
                        put(AnimalLocationHistoryTable.Columns.MODIFIED, createdAtDateTimeString)
                    }
                )
                insertOrThrow(
                    AnimalBreedTable.NAME,
                    null,
                    ContentValues().apply {
                        put(AnimalBreedTable.Columns.ANIMAL_ID, animalId)
                        put(AnimalBreedTable.Columns.BREED_ID, breedId)
                        //TODO (Post-Wool-Growers) this is currently a single breed. Structure in database in place to handle breed percentages but not implemented yet
                        put(AnimalBreedTable.Columns.BREED_PERCENTAGE, 100)
                        put(AnimalBreedTable.Columns.CREATED, createdAtDateTimeString)
                        put(AnimalBreedTable.Columns.MODIFIED, createdAtDateTimeString)
                    }
                )
                insertOrThrow(
                    AnimalRegistrationTable.NAME,
                    null,
                    ContentValues().apply {
                        put(AnimalRegistrationTable.Columns.ANIMAL_ID, animalId)
                        put(AnimalRegistrationTable.Columns.ANIMAL_NAME, animalName)
                        put(AnimalRegistrationTable.Columns.REGISTRATION_NUMBER, Sql.NULLISH)
                        put(AnimalRegistrationTable.Columns.ID_REGISTRY_COMPANY_ID, 700)
                        put(AnimalRegistrationTable.Columns.ANIMAL_REGISTRATION_TYPE_ID, 0)
                        put(AnimalRegistrationTable.Columns.FLOCK_BOOK_ID, 0)
                        put(AnimalRegistrationTable.Columns.REGISTRATION_DATE, Sql.NULLISH)
                        put(AnimalRegistrationTable.Columns.REGISTRATION_DESCRIPTION, Sql.NULLISH)
                        putOrNullish(
                            AnimalRegistrationTable.Columns.BREEDER_CONTACT_ID,
                            breederId,
                            shouldPutValue = breederTypeId == Breeder.Type.CONTACT.id
                        )
                        putOrNullish(
                            AnimalRegistrationTable.Columns.BREEDER_COMPANY_ID,
                            breederId,
                            shouldPutValue = breederTypeId == Breeder.Type.COMPANY.id
                        )
                        put(AnimalRegistrationTable.Columns.CREATED, createdAtDateTimeString)
                        put(AnimalRegistrationTable.Columns.MODIFIED, createdAtDateTimeString)
                    }
                )
                setTransactionSuccessful()
                animalId
            } finally {
                endTransaction()
            }
        }
    }

    override suspend fun addNotesToAnimal(
        animalId: Int,
        customNote: String?,
        predefinedNoteIds: List<Int>,
        timeStamp: LocalDateTime
    ) {
        return databaseHandler.writableDatabase.run {
            val createdAtDateTimeString = Sql.formatDateTime(timeStamp)
            beginTransaction()
            try {
                predefinedNoteIds.forEach { predefinedNoteId ->
                    insertOrThrow(
                        AnimalNoteTable.NAME,
                        null,
                        ContentValues().apply {
                            put(AnimalNoteTable.Columns.ANIMAL_ID, animalId)
                            put(AnimalNoteTable.Columns.NOTE_TEXT, Sql.NULLISH)
                            put(AnimalNoteTable.Columns.PREDEFINED_NOTE_ID, predefinedNoteId)
                            put(AnimalNoteTable.Columns.NOTE_DATE, Sql.formatDate(timeStamp))
                            put(AnimalNoteTable.Columns.NOTE_TIME, Sql.formatTime(timeStamp))
                            put(AnimalNoteTable.Columns.CREATED, createdAtDateTimeString)
                            put(AnimalNoteTable.Columns.MODIFIED, createdAtDateTimeString)
                        }
                    )
                }
                if (!customNote.isNullOrBlank()) {
                    insertOrThrow(
                        AnimalNoteTable.NAME,
                        null,
                        ContentValues().apply {
                            put(AnimalNoteTable.Columns.ANIMAL_ID, animalId)
                            put(AnimalNoteTable.Columns.NOTE_TEXT, customNote)
                            put(AnimalNoteTable.Columns.PREDEFINED_NOTE_ID, Sql.NULLISH)
                            put(AnimalNoteTable.Columns.NOTE_DATE, Sql.formatDate(timeStamp))
                            put(AnimalNoteTable.Columns.NOTE_TIME, Sql.formatTime(timeStamp))
                            put(AnimalNoteTable.Columns.CREATED, createdAtDateTimeString)
                            put(AnimalNoteTable.Columns.MODIFIED, createdAtDateTimeString)
                        }
                    )
                }
                setTransactionSuccessful()
            } finally {
                endTransaction()
            }
        }
    }

    override suspend fun addIdToAnimal(
        animalId: Long,
        idTypeId: Int,
        idColorId: Int,
        idLocationId: Int,
        idNumber: String,
        isOfficial: Boolean,
        timeStampOn: LocalDateTime
    ): Long {
        return databaseHandler.writableDatabase.run {
            val createdAtDateTimeString = Sql.formatDateTime(timeStampOn)
            insertOrThrow(
                AnimalIdInfoTable.NAME,
                null,
                ContentValues().apply {
                    put(AnimalIdInfoTable.Columns.ANIMAL_ID, animalId)
                    put(AnimalIdInfoTable.Columns.ID_TYPE_ID, idTypeId)
                    put(AnimalIdInfoTable.Columns.MALE_ID_COLOR_ID, idColorId)
                    put(AnimalIdInfoTable.Columns.FEMALE_ID_COLOR_ID, idColorId)
                    put(AnimalIdInfoTable.Columns.ID_LOCATION_ID, idLocationId)
                    put(AnimalIdInfoTable.Columns.NUMBER, idNumber)
                    put(AnimalIdInfoTable.Columns.IS_OFFICIAL_ID, Sql.booleanValue(isOfficial))
                    put(AnimalIdInfoTable.Columns.DATE_ON, Sql.formatDate(timeStampOn))
                    put(AnimalIdInfoTable.Columns.TIME_ON, Sql.formatTime(timeStampOn))
                    put(AnimalIdInfoTable.Columns.DATE_OFF, Sql.NULLISH)
                    put(AnimalIdInfoTable.Columns.TIME_OFF, Sql.NULLISH)
                    put(AnimalIdInfoTable.Columns.REMOVE_REASON_ID, Sql.NULLISH)
                    put(AnimalIdInfoTable.Columns.SCRAPIE_FLOCK_ID, Sql.NULLISH)
                    put(AnimalIdInfoTable.Columns.CREATED, createdAtDateTimeString)
                    put(AnimalIdInfoTable.Columns.MODIFIED, createdAtDateTimeString)
                }
            )
        }
    }

    override suspend fun updateIdOnAnimal(
        id: Int,
        typeId: Int,
        colorId: Int,
        locationId: Int,
        number: String,
        timeStamp: LocalDateTime
    ): Boolean {
        return 0 < databaseHandler.writableDatabase.update(
            AnimalIdInfoTable.NAME,
            ContentValues().apply {
                put(AnimalIdInfoTable.Columns.ID_TYPE_ID, typeId)
                put(AnimalIdInfoTable.Columns.MALE_ID_COLOR_ID, colorId)
                put(AnimalIdInfoTable.Columns.FEMALE_ID_COLOR_ID, colorId)
                put(AnimalIdInfoTable.Columns.ID_LOCATION_ID, locationId)
                put(AnimalIdInfoTable.Columns.NUMBER, number)
                put(AnimalIdInfoTable.Columns.REMOVE_REASON_ID, IdRemoveReason.ID_CORRECT_TAG_DATA)
                put(AnimalIdInfoTable.Columns.MODIFIED, Sql.formatDateTime(timeStamp))
            },
            "${AnimalIdInfoTable.Columns.ID} = ?",
            arrayOf(id.toString())
        )
    }

    override suspend fun removeIdFromAnimal(
        id: Int,
        removeReasonId: Int,
        timeStamp: LocalDateTime
    ): Boolean {
        return 0 < databaseHandler.writableDatabase.update(
            AnimalIdInfoTable.NAME,
            ContentValues().apply {
                put(AnimalIdInfoTable.Columns.REMOVE_REASON_ID, removeReasonId)
                put(AnimalIdInfoTable.Columns.DATE_OFF, Sql.formatDate(timeStamp))
                put(AnimalIdInfoTable.Columns.TIME_OFF, Sql.formatTime(timeStamp))
                put(AnimalIdInfoTable.Columns.MODIFIED, Sql.formatDateTime(timeStamp))
            },
            "${AnimalIdInfoTable.Columns.ID} = ?",
            arrayOf(id.toString())
        )
    }

    override suspend fun addEvaluationForAnimal(
        animalId: Int,
        ageInDays: Long,
        timeStamp: LocalDateTime,
        trait01Id: Int,
        trait01Score: Int,
        trait02Id: Int,
        trait02Score: Int,
        trait03Id: Int,
        trait03Score: Int,
        trait04Id: Int,
        trait04Score: Int,
        trait05Id: Int,
        trait05Score: Int,
        trait06Id: Int,
        trait06Score: Int,
        trait07Id: Int,
        trait07Score: Int,
        trait08Id: Int,
        trait08Score: Int,
        trait09Id: Int,
        trait09Score: Int,
        trait10Id: Int,
        trait10Score: Int,
        trait11Id: Int,
        trait11Score: Float,
        trait11UnitsId: Int,
        trait12Id: Int,
        trait12Score: Float,
        trait12UnitsId: Int,
        trait13Id: Int,
        trait13Score: Float,
        trait13UnitsId: Int,
        trait14Id: Int,
        trait14Score: Float,
        trait14UnitsId: Int,
        trait15Id: Int,
        trait15Score: Float,
        trait15UnitsId: Int,
        trait16Id: Int,
        trait16OptionId: Int,
        trait17Id: Int,
        trait17OptionId: Int,
        trait18Id: Int,
        trait18OptionId: Int,
        trait19Id: Int,
        trait19OptionId: Int,
        trait20Id: Int,
        trait20OptionId: Int
    ): Long {
        return databaseHandler.writableDatabase.run {
            val createdAtDateTimeString = Sql.formatDateTime(timeStamp)
            insertOrThrow(
                AnimalEvaluationTable.NAME,
                null,
                ContentValues().apply {
                    put(AnimalEvaluationTable.Columns.ANIMAL_ID, animalId)
                    put(AnimalEvaluationTable.Columns.AGE_IN_DAYS, ageInDays)
                    put(AnimalEvaluationTable.Columns.ANIMAL_RANK, Sql.NULLISH)
                    put(AnimalEvaluationTable.Columns.NUMBER_RANKED, Sql.NULLISH)
                    put(AnimalEvaluationTable.Columns.EVAL_DATE, Sql.formatDate(timeStamp))
                    put(AnimalEvaluationTable.Columns.EVAL_TIME, Sql.formatTime(timeStamp))
                    put(AnimalEvaluationTable.Columns.CREATED, createdAtDateTimeString)
                    put(AnimalEvaluationTable.Columns.MODIFIED, createdAtDateTimeString)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_01, trait01Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_02, trait02Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_03, trait03Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_04, trait04Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_05, trait05Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_06, trait06Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_07, trait07Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_08, trait08Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_09, trait09Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_10, trait10Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_11, trait11Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_12, trait12Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_13, trait13Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_14, trait14Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_15, trait15Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_16, trait16Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_17, trait17Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_18, trait18Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_19, trait19Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_ID_20, trait20Id)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_01, trait01Score)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_02, trait02Score)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_03, trait03Score)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_04, trait04Score)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_05, trait05Score)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_06, trait06Score)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_07, trait07Score)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_08, trait08Score)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_09, trait09Score)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_10, trait10Score)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_11, trait11Score)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_12, trait12Score)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_13, trait13Score)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_14, trait14Score)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_15, trait15Score)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_16, trait16OptionId)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_17, trait17OptionId)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_18, trait18OptionId)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_19, trait19OptionId)
                    put(AnimalEvaluationTable.Columns.TRAIT_SCORE_20, trait20OptionId)
                    put(AnimalEvaluationTable.Columns.TRAIT_UNITS_ID_11, trait11UnitsId)
                    put(AnimalEvaluationTable.Columns.TRAIT_UNITS_ID_12, trait12UnitsId)
                    put(AnimalEvaluationTable.Columns.TRAIT_UNITS_ID_13, trait13UnitsId)
                    put(AnimalEvaluationTable.Columns.TRAIT_UNITS_ID_14, trait14UnitsId)
                    put(AnimalEvaluationTable.Columns.TRAIT_UNITS_ID_15, trait15UnitsId)
                }
            )
        }
    }

    override suspend fun addTissueTestForAnimal(
        animalId: Int,
        tissueSampleTypeId: Int,
        tissueSampleContainerTypeId: Int,
        tissueSampleContainerId: String,
        tissueSampleContainerExpDate: String,
        tissueTestId: Int,
        laboratoryCompanyId: Int,
        timeStampOn: LocalDateTime
    ): Long {
        return databaseHandler.writableDatabase.run {
            val createdAtDateTimeString = Sql.formatDateTime(timeStampOn)
            beginTransaction()
            try {
                val tissueSampleTakenId = insertOrThrow(
                    AnimalTissueSampleTakenTable.NAME,
                    null,
                    ContentValues().apply {
                        put(AnimalTissueSampleTakenTable.Columns.ANIMAL_ID, animalId)
                        put(AnimalTissueSampleTakenTable.Columns.SAMPLE_TYPE_ID, tissueSampleTypeId)
                        put(AnimalTissueSampleTakenTable.Columns.SAMPLE_DATE, Sql.formatDate(timeStampOn))
                        put(AnimalTissueSampleTakenTable.Columns.SAMPLE_TIME, Sql.formatTime(timeStampOn))
                        put(AnimalTissueSampleTakenTable.Columns.CONTAINER_TYPE_ID, tissueSampleContainerTypeId)
                        put(AnimalTissueSampleTakenTable.Columns.CONTAINER_ID, tissueSampleContainerId)
                        put(AnimalTissueSampleTakenTable.Columns.CONTAINER_EXP_DATE, tissueSampleContainerExpDate)
                        put(AnimalTissueSampleTakenTable.Columns.CREATED, createdAtDateTimeString)
                        put(AnimalTissueSampleTakenTable.Columns.MODIFIED, createdAtDateTimeString)
                    }
                )
                insertOrThrow(
                    AnimalTissueTestRequestTable.NAME,
                    null,
                    ContentValues().apply {
                        put(AnimalTissueTestRequestTable.Columns.SAMPLE_TAKEN_ID, tissueSampleTakenId)
                        put(AnimalTissueTestRequestTable.Columns.TEST_ID, tissueTestId)
                        put(AnimalTissueTestRequestTable.Columns.LABORATORY_COMPANY_ID, laboratoryCompanyId)
                        put(AnimalTissueTestRequestTable.Columns.LABORATORY_ACCESSION_ID, Sql.NULLISH)
                        put(AnimalTissueTestRequestTable.Columns.TEST_RESULTS, Sql.NULLISH)
                        put(AnimalTissueTestRequestTable.Columns.TEST_RESULTS_DATE, Sql.NULLISH)
                        put(AnimalTissueTestRequestTable.Columns.TEST_RESULTS_TIME, Sql.NULLISH)
                        put(AnimalTissueTestRequestTable.Columns.ANIMAL_EXTERNAL_FILE_ID, -1)
                        put(AnimalTissueTestRequestTable.Columns.CREATED, createdAtDateTimeString)
                        put(AnimalTissueTestRequestTable.Columns.MODIFIED, createdAtDateTimeString)
                    }
                ).also { setTransactionSuccessful() }
            } finally {
                endTransaction()
            }
        }
    }

    private fun appendOptionalSexStandardParam(arguments: Array<String>, sexStandard: SexStandard?): Array<String> {
        return sexStandard?.let {
            buildList{ addAll(arguments); add(it.code) }.toTypedArray()
        } ?: arguments
    }

    companion object {

        private const val COLUMN_NAME_EID_EXISTS = "eid_exists"

        private const val SQL_QUERY_EXISTENCE_OF_EID =
            """SELECT EXISTS (
                SELECT 1 FROM ${AnimalIdInfoTable.NAME}
                WHERE ${AnimalIdInfoTable.Columns.ID_TYPE_ID} = ${IdType.ID_TYPE_ID_EID}
                    AND ${AnimalIdInfoTable.Columns.NUMBER} = ?
                ) AS $COLUMN_NAME_EID_EXISTS"""

        private const val SQL_QUERY_EXISTENCE_OF_EID_EXCEPT_FOR_ID =
            """SELECT EXISTS (
                SELECT 1 FROM ${AnimalIdInfoTable.NAME}
                WHERE ${AnimalIdInfoTable.Columns.ID_TYPE_ID} = ${IdType.ID_TYPE_ID_EID}
                    AND ${AnimalIdInfoTable.Columns.NUMBER} = ?
                    AND ${AnimalIdInfoTable.Columns.ID} != ?
                ) AS $COLUMN_NAME_EID_EXISTS"""

        private const val SQL_QUERY_ANIMAL_BASIC_INFO =
            """SELECT ${AnimalTable.NAME}.${AnimalTable.Columns.ID},
                    ${AnimalTable.NAME}.${AnimalTable.Columns.NAME},
                    ${AnimalTable.NAME}.${AnimalTable.Columns.ALERT},
                    ${AnimalTable.NAME}.${AnimalTable.Columns.BIRTH_DATE},
				    ${AnimalIdInfoTable.NAME}.${AnimalIdInfoTable.Columns.NUMBER}, 
    				${FlockPrefixTable.NAME}.${FlockPrefixTable.Columns.PREFIX},
				    ${CompanyTable.NAME}.${CompanyTable.Columns.NAME},
				    ${ContactTable.Columns.FULL_NAME_PROJECTION} as ${ContactTable.Columns.FULL_NAME_ALIAS},
                    ${AnimalIdInfoTable.NAME}.${AnimalIdInfoTable.Columns.ID},
                    ${IdTypeTable.NAME}.${IdTypeTable.Columns.ID},
				    ${IdTypeTable.NAME}.${IdTypeTable.Columns.NAME},
                    ${IdTypeTable.NAME}.${IdTypeTable.Columns.ABBREVIATION},
                    ${IdColorTable.NAME}.${IdColorTable.Columns.ID},
                    ${IdColorTable.NAME}.${IdColorTable.Columns.NAME},
                    ${IdColorTable.NAME}.${IdColorTable.Columns.ABBREVIATION},
                    ${IdLocationTable.NAME}.${IdLocationTable.Columns.ID},
                    ${IdLocationTable.NAME}.${IdLocationTable.Columns.NAME},
                    ${IdLocationTable.NAME}.${IdLocationTable.Columns.ABBREVIATION},
                    ${AnimalIdInfoTable.NAME}.${AnimalIdInfoTable.Columns.IS_OFFICIAL_ID},
                    ${AnimalIdInfoTable.NAME}.${AnimalIdInfoTable.Columns.DATE_ON},
                    ${AnimalIdInfoTable.NAME}.${AnimalIdInfoTable.Columns.TIME_ON},
                    ${SpeciesTable.NAME}.${SpeciesTable.Columns.ID},
                    ${SpeciesTable.NAME}.${SpeciesTable.Columns.COMMON_NAME},
                    ${BreedTable.NAME}.${BreedTable.Columns.ID},
                    ${BreedTable.NAME}.${BreedTable.Columns.NAME},
                    ${BreedTable.NAME}.${BreedTable.Columns.ABBREVIATION},
                    ${SexTable.NAME}.${SexTable.Columns.ID},
                    ${SexTable.NAME}.${SexTable.Columns.NAME},
                    ${SexTable.NAME}.${SexTable.Columns.ABBREVIATION},
                    ${SexTable.NAME}.${SexTable.Columns.STANDARD},
                    ${SexTable.NAME}.${SexTable.Columns.STANDARD_ABBREVIATION}
                FROM ${AnimalTable.NAME}
                LEFT OUTER JOIN (
                    SELECT * FROM ${AnimalIdInfoTable.NAME}
                    WHERE (
                        ${AnimalIdInfoTable.NAME}.${AnimalIdInfoTable.Columns.DATE_OFF} IS NULL
                        OR ${AnimalIdInfoTable.NAME}.${AnimalIdInfoTable.Columns.DATE_OFF} = ''
                    )
                ) AS ${AnimalIdInfoTable.NAME}
                    ON ${AnimalTable.NAME}.${AnimalTable.Columns.ID} = ${AnimalIdInfoTable.NAME}.${AnimalIdInfoTable.Columns.ANIMAL_ID}
                LEFT OUTER JOIN ${IdColorTable.NAME} 
                    ON ${AnimalIdInfoTable.NAME}.${AnimalIdInfoTable.Columns.MALE_ID_COLOR_ID} = ${IdColorTable.NAME}.${IdColorTable.Columns.ID}
                LEFT OUTER JOIN ${IdLocationTable.NAME}
                    ON ${AnimalIdInfoTable.NAME}.${AnimalIdInfoTable.Columns.ID_LOCATION_ID} = ${IdLocationTable.NAME}.${IdLocationTable.Columns.ID}
                LEFT OUTER JOIN ${IdTypeTable.NAME}
                    ON ${AnimalIdInfoTable.NAME}.${AnimalIdInfoTable.Columns.ID_TYPE_ID} = ${IdTypeTable.NAME}.${IdTypeTable.Columns.ID}
                LEFT OUTER JOIN ${AnimalFlockPrefixTable.NAME}
                    ON ${AnimalTable.NAME}.${AnimalTable.Columns.ID} = ${AnimalFlockPrefixTable.NAME}.${AnimalFlockPrefixTable.Columns.ANIMAL_ID}
                LEFT OUTER JOIN ${FlockPrefixTable.NAME}
                    ON ${AnimalFlockPrefixTable.NAME}.${AnimalFlockPrefixTable.Columns.FLOCK_PREFIX_ID} = ${FlockPrefixTable.NAME}.${FlockPrefixTable.Columns.ID}
                LEFT OUTER JOIN (
                    ${AnimalOwnershipHistoryTable.Sql.SQL_CURRENT_ANIMAL_OWNERSHIP}
                ) AS animal_ownership_table
                    ON ${AnimalTable.NAME}.${AnimalTable.Columns.ID} = animal_ownership_table.${AnimalOwnershipHistoryTable.Columns.ANIMAL_ID}
                LEFT OUTER JOIN ${CompanyTable.NAME}
                    ON animal_ownership_table.${AnimalOwnershipHistoryTable.Columns.TO_COMPANY_ID} = ${CompanyTable.NAME}.${CompanyTable.Columns.ID}
                LEFT OUTER JOIN ${ContactTable.NAME}
                    ON animal_ownership_table.${AnimalOwnershipHistoryTable.Columns.TO_CONTACT_ID} = ${ContactTable.NAME}.${ContactTable.Columns.ID}
                INNER JOIN ${SexTable.NAME} 
                    ON ${AnimalTable.NAME}.${AnimalTable.Columns.SEX_ID} = ${SexTable.NAME}.${SexTable.Columns.ID}
                INNER JOIN ${SpeciesTable.NAME}
                    ON ${SexTable.NAME}.${SexTable.Columns.SPECIES_ID} = ${SpeciesTable.NAME}.${SpeciesTable.Columns.ID}
                INNER JOIN (
                    SELECT
                        ${AnimalBreedTable.Columns.ANIMAL_ID},
                        ${AnimalBreedTable.Columns.BREED_ID},
                        MAX(${AnimalBreedTable.Columns.BREED_PERCENTAGE}) AS ${AnimalBreedTable.Columns.BREED_PERCENTAGE}
                    FROM ${AnimalBreedTable.NAME}
                    GROUP BY ${AnimalBreedTable.Columns.ANIMAL_ID}
                ) AS ${AnimalBreedTable.NAME}
                    ON ${AnimalTable.NAME}.${AnimalTable.Columns.ID} = ${AnimalBreedTable.NAME}.${AnimalBreedTable.Columns.ANIMAL_ID}
                INNER JOIN ${BreedTable.NAME}
                    ON ${AnimalBreedTable.NAME}.${AnimalBreedTable.Columns.BREED_ID} = ${BreedTable.NAME}.${BreedTable.Columns.ID}"""

        private const val ORDER_BY_FOR_ANIMAL_BASIC_INFO =
            """ORDER BY
                ${AnimalTable.NAME}.${AnimalTable.Columns.NAME} ASC,
                ${AnimalTable.NAME}.${AnimalTable.Columns.ID} ASC,
                ${IdTypeTable.NAME}.${IdTypeTable.Columns.NAME} ASC"""

        private const val QUALIFIER_FOR_ANIMAL_BASIC_INFO_BY_SPECIES =
            """${SexTable.NAME}.${SexTable.Columns.SPECIES_ID} = ?1"""

        private const val QUALIFIER_FOR_ANIMAL_BASIC_INFO_BY_ID_TYPE =
            """${AnimalTable.NAME}.${AnimalTable.Columns.ID} IN (
                    SELECT ${AnimalIdInfoTable.Columns.ANIMAL_ID}
                    FROM ${AnimalIdInfoTable.NAME}
                    WHERE ${AnimalIdInfoTable.Columns.ID_TYPE_ID} = ?2
                    AND ${AnimalIdInfoTable.Columns.NUMBER}
                        LIKE '%' || ?3 || '%' ${Sql.ESCAPE_CLAUSE}
                )"""

        private const val SQL_SEARCH_ANIMAL_BASIC_INFO_BY_ID_TYPE =
            """$SQL_QUERY_ANIMAL_BASIC_INFO
                WHERE $QUALIFIER_FOR_ANIMAL_BASIC_INFO_BY_SPECIES
                AND $QUALIFIER_FOR_ANIMAL_BASIC_INFO_BY_ID_TYPE
                $ORDER_BY_FOR_ANIMAL_BASIC_INFO"""

        private const val SQL_SEARCH_ANIMAL_BASIC_INFO_BY_ID_TYPE_AND_SEX_STANDARD =
            """$SQL_QUERY_ANIMAL_BASIC_INFO
                WHERE $QUALIFIER_FOR_ANIMAL_BASIC_INFO_BY_SPECIES
                AND $QUALIFIER_FOR_ANIMAL_BASIC_INFO_BY_ID_TYPE
                AND ${SexTable.Columns.STANDARD_ABBREVIATION} = ?4
                $ORDER_BY_FOR_ANIMAL_BASIC_INFO"""

        private const val QUALIFIER_FOR_ANIMAL_BASIC_INFO_BY_NAME =
            """${AnimalTable.Columns.NAME}
                LIKE '%' || ?2 || '%' ${Sql.ESCAPE_CLAUSE}"""

        private const val SQL_SEARCH_ANIMAL_BASIC_INFO_BY_NAME =
            """$SQL_QUERY_ANIMAL_BASIC_INFO
                WHERE $QUALIFIER_FOR_ANIMAL_BASIC_INFO_BY_SPECIES
                AND $QUALIFIER_FOR_ANIMAL_BASIC_INFO_BY_NAME
                $ORDER_BY_FOR_ANIMAL_BASIC_INFO"""

        private const val SQL_SEARCH_ANIMAL_BASIC_INFO_BY_NAME_AND_SEX_STANDARD =
            """$SQL_QUERY_ANIMAL_BASIC_INFO
                WHERE $QUALIFIER_FOR_ANIMAL_BASIC_INFO_BY_SPECIES
                AND $QUALIFIER_FOR_ANIMAL_BASIC_INFO_BY_NAME
                AND ${SexTable.Columns.STANDARD_ABBREVIATION} = ?3
                $ORDER_BY_FOR_ANIMAL_BASIC_INFO"""

        private const val QUALIFIER_FOR_ANIMAL_BASIC_INFO_BY_ANIMAL_ID =
            """${AnimalTable.NAME}.${AnimalTable.Columns.ID} = ?"""

        private const val SQL_QUERY_ANIMAL_BASIC_INFO_BY_ANIMAL_ID =
            """$SQL_QUERY_ANIMAL_BASIC_INFO
                WHERE $QUALIFIER_FOR_ANIMAL_BASIC_INFO_BY_ANIMAL_ID
                $ORDER_BY_FOR_ANIMAL_BASIC_INFO"""

        private const val QUALIFIER_FOR_ANIMAL_BASIC_INFO_BY_EID =
            """${AnimalTable.NAME}.${AnimalTable.Columns.ID} IN (
                    SELECT ${AnimalIdInfoTable.Columns.ANIMAL_ID}
                    FROM ${AnimalIdInfoTable.NAME}
                    WHERE ${AnimalIdInfoTable.Columns.ID_TYPE_ID} = ${IdType.ID_TYPE_ID_EID}
                    AND ${AnimalIdInfoTable.Columns.NUMBER} = ?
                )"""

        private const val SQL_QUERY_ANIMAL_BASIC_INFO_BY_EID =
            """$SQL_QUERY_ANIMAL_BASIC_INFO
                WHERE $QUALIFIER_FOR_ANIMAL_BASIC_INFO_BY_EID
                $ORDER_BY_FOR_ANIMAL_BASIC_INFO"""

        private fun animalBasicInfoFrom(cursor: Cursor): AnimalBasicInfo {
            val animalBasicInfo = AnimalBasicInfo(
                id = cursor.getInt(AnimalTable.Columns.ID),
                name = cursor.getString(AnimalTable.Columns.NAME),
                alert = cursor.getOptString(AnimalTable.Columns.ALERT),
                flockPrefix = cursor.getOptString(FlockPrefixTable.Columns.PREFIX),
                ownerName = when {
                    !cursor.isNull(CompanyTable.Columns.NAME) -> {
                        cursor.getString(CompanyTable.Columns.NAME)
                    }
                    !cursor.isNull(ContactTable.Columns.FULL_NAME_ALIAS) -> {
                        cursor.getString(ContactTable.Columns.FULL_NAME_ALIAS)
                    }
                    else -> null
                },
                speciesId = cursor.getInt(SpeciesTable.Columns.ID),
                speciesCommonName = cursor.getString(SpeciesTable.Columns.COMMON_NAME),
                breedId = cursor.getInt(BreedTable.Columns.ID),
                breedName = cursor.getString(BreedTable.Columns.NAME),
                breedAbbreviation = cursor.getString(BreedTable.Columns.ABBREVIATION),
                sexId = cursor.getInt(SexTable.Columns.ID),
                sexName = cursor.getString(SexTable.Columns.NAME),
                sexAbbreviation = cursor.getString(SexTable.Columns.ABBREVIATION),
                sexStandardName = cursor.getString(SexTable.Columns.STANDARD),
                sexStandardAbbreviation = cursor.getString(SexTable.Columns.STANDARD_ABBREVIATION),
                birthDate = cursor.getLocalDate(AnimalTable.Columns.BIRTH_DATE)
            )
            val ids = mutableListOf<IdBasicInfo>().also {
                if (hasIdBasicInfoIn(cursor)) {
                    it.add(cursor.readItem(::idBasicInfoFrom))
                }
                while (cursor.moveToNext()) {
                    val animalId = cursor.getInt(
                        AnimalTable.Columns.ID
                    )
                    if (animalBasicInfo.id == animalId) {
                        if (hasIdBasicInfoIn(cursor)) {
                            it.add(cursor.readItem(::idBasicInfoFrom))
                        }
                    } else {
                        cursor.moveToPrevious()
                        break;
                    }
                }
            }
            return animalBasicInfo.copy(ids = ids)
        }

        private fun hasIdBasicInfoIn(cursor: Cursor): Boolean {
            return !cursor.isNull(AnimalIdInfoTable.Columns.ID) &&
                !cursor.isNull(AnimalIdInfoTable.Columns.NUMBER) &&
                !cursor.isNull(IdTypeTable.Columns.NAME) &&
                !cursor.isNull(IdColorTable.Columns.ABBREVIATION) &&
                !cursor.isNull(IdLocationTable.Columns.ABBREVIATION)
        }

        private fun idBasicInfoFrom(cursor: Cursor): IdBasicInfo {
            return IdBasicInfo(
                id = cursor.getInt(AnimalIdInfoTable.Columns.ID),
                number = cursor.getString(AnimalIdInfoTable.Columns.NUMBER),
                typeId = cursor.getInt(IdTypeTable.Columns.ID),
                typeName = cursor.getString(IdTypeTable.Columns.NAME),
                typeAbbreviation = cursor.getString(IdTypeTable.Columns.ABBREVIATION),
                colorId = cursor.getInt(IdColorTable.Columns.ID),
                colorName = cursor.getString(IdColorTable.Columns.NAME),
                colorAbbreviation = cursor.getString(IdColorTable.Columns.ABBREVIATION),
                locationId = cursor.getInt(IdLocationTable.Columns.ID),
                locationName = cursor.getString(IdLocationTable.Columns.NAME),
                locationAbbreviation = cursor.getString(IdLocationTable.Columns.ABBREVIATION),
                isOfficial = cursor.getBoolean(AnimalIdInfoTable.Columns.IS_OFFICIAL_ID),
                dateOn = cursor.getLocalDate(AnimalIdInfoTable.Columns.DATE_ON),
                timeOn = cursor.getLocalTime(AnimalIdInfoTable.Columns.TIME_ON)
            )
        }
    }
}
