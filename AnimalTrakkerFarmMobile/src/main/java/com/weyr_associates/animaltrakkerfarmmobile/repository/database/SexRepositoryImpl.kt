package com.weyr_associates.animaltrakkerfarmmobile.repository.database

import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.database.SexTable
import com.weyr_associates.animaltrakkerfarmmobile.database.readAllItems
import com.weyr_associates.animaltrakkerfarmmobile.database.readFirstItem
import com.weyr_associates.animaltrakkerfarmmobile.model.Sex
import com.weyr_associates.animaltrakkerfarmmobile.repository.SexRepository

class SexRepositoryImpl(private val databaseHandler: DatabaseHandler) : SexRepository {

    companion object {
        private const val SQL_QUERY_SEXES_BY_SPECIES_ID =
            """SELECT * FROM ${SexTable.NAME}
                WHERE ${SexTable.Columns.SPECIES_ID} = ?
                ORDER BY ${SexTable.Columns.ORDER}"""

        private const val SQL_QUERY_SEXES_BY_ID =
            """SELECT * FROM ${SexTable.NAME}
                WHERE ${SexTable.Columns.ID} = ?"""
    }

    override fun querySexes(speciesId: Int): List<Sex> {
        return databaseHandler.readableDatabase.rawQuery(SQL_QUERY_SEXES_BY_SPECIES_ID, arrayOf(speciesId.toString()))
            ?.use { it.readAllItems(SexTable::sexFromCursor) } ?: emptyList()
    }

    override fun querySex(id: Int): Sex? {
        return databaseHandler.readableDatabase.rawQuery(SQL_QUERY_SEXES_BY_ID, arrayOf(id.toString()))
            ?.use { it.readFirstItem(SexTable::sexFromCursor) }
    }
}
