package com.weyr_associates.animaltrakkerfarmmobile.repository

import com.weyr_associates.animaltrakkerfarmmobile.model.Contact

interface ContactRepository {
    fun queryContacts(): List<Contact>
    fun queryContact(id: Int): Contact?
}
