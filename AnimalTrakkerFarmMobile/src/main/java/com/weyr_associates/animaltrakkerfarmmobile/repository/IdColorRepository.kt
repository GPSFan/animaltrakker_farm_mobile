package com.weyr_associates.animaltrakkerfarmmobile.repository

import com.weyr_associates.animaltrakkerfarmmobile.model.IdColor

interface IdColorRepository {
    fun queryIdColors(): List<IdColor>

    fun queryIdColor(id: Int): IdColor?
}
