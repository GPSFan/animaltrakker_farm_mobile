package com.weyr_associates.animaltrakkerfarmmobile.repository.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.database.CompanyPremiseTable
import com.weyr_associates.animaltrakkerfarmmobile.database.ContactPremiseTable
import com.weyr_associates.animaltrakkerfarmmobile.database.PremiseTable
import com.weyr_associates.animaltrakkerfarmmobile.database.getInt
import com.weyr_associates.animaltrakkerfarmmobile.database.readFirstItem
import com.weyr_associates.animaltrakkerfarmmobile.model.Owner
import com.weyr_associates.animaltrakkerfarmmobile.model.Premise
import com.weyr_associates.animaltrakkerfarmmobile.model.PremiseType
import com.weyr_associates.animaltrakkerfarmmobile.repository.PremiseRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PremiseRepositoryImpl(private val databaseHandler: DatabaseHandler) : PremiseRepository {

    override suspend fun queryPhysicalPremiseForOwner(ownerId: Int, ownerType: Owner.Type): Premise? {
        return withContext(Dispatchers.IO) {
            databaseHandler.readableDatabase.rawQuery(
                when (ownerType) {
                    Owner.Type.CONTACT -> SQL_QUERY_CONTACT_CURRENT_PHYSICAL_PREMISE
                    Owner.Type.COMPANY -> SQL_QUERY_COMPANY_CURRENT_PHYSICAL_PREMISE
                },
                arrayOf(ownerId.toString())
            )?.use { cursor -> cursor.readFirstItem(::premiseFromCursor) }
        }
    }

    companion object {
        const val SQL_QUERY_CONTACT_CURRENT_PHYSICAL_PREMISE =
            """SELECT ${PremiseTable.NAME}.${PremiseTable.Columns.ID}
                FROM ${PremiseTable.NAME}
                INNER JOIN ${ContactPremiseTable.NAME}
                    ON ${ContactPremiseTable.NAME}.${ContactPremiseTable.Columns.PREMISE_ID} =
                        ${PremiseTable.NAME}.${PremiseTable.Columns.ID}
                WHERE ${ContactPremiseTable.NAME}.${ContactPremiseTable.Columns.CONTACT_ID} = ?
                AND (${PremiseTable.NAME}.${PremiseTable.Columns.TYPE_ID} = ${PremiseType.ID_PHYSICAL} OR
                    ${PremiseTable.NAME}.${PremiseTable.Columns.TYPE_ID} = ${PremiseType.ID_BOTH})
                AND ${ContactPremiseTable.NAME}.${ContactPremiseTable.Columns.USAGE_END} = ''
                ORDER BY ${ContactPremiseTable.NAME}.${ContactPremiseTable.Columns.USAGE_START} DESC
                LIMIT 1"""

        const val SQL_QUERY_COMPANY_CURRENT_PHYSICAL_PREMISE =
            """SELECT ${PremiseTable.NAME}.${PremiseTable.Columns.ID}
                FROM ${CompanyPremiseTable.NAME}
                INNER JOIN ${PremiseTable.NAME}
                    ON ${CompanyPremiseTable.NAME}.${CompanyPremiseTable.Columns.PREMISE_ID} =
                        ${PremiseTable.NAME}.${PremiseTable.Columns.ID}
                WHERE ${CompanyPremiseTable.NAME}.${CompanyPremiseTable.Columns.COMPANY_ID} = ?
                AND (${PremiseTable.NAME}.${PremiseTable.Columns.TYPE_ID} = ${PremiseType.ID_PHYSICAL} OR
                    ${PremiseTable.NAME}.${PremiseTable.Columns.TYPE_ID} = ${PremiseType.ID_BOTH})
                AND ${CompanyPremiseTable.NAME}.${CompanyPremiseTable.Columns.USAGE_END} = ''
                ORDER BY ${CompanyPremiseTable.NAME}.${CompanyPremiseTable.Columns.USAGE_START} DESC
                LIMIT 1"""

        private fun premiseFromCursor(cursor: Cursor): Premise {
            return Premise(id = cursor.getInt(PremiseTable.Columns.ID))
        }
    }
}
