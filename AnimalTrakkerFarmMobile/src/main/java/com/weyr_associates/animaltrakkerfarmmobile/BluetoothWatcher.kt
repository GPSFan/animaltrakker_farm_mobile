package com.weyr_associates.animaltrakkerfarmmobile

import android.bluetooth.BluetoothAdapter.ACTION_STATE_CHANGED
import android.bluetooth.BluetoothAdapter.EXTRA_PREVIOUS_STATE
import android.bluetooth.BluetoothAdapter.EXTRA_STATE
import android.bluetooth.BluetoothAdapter.STATE_OFF
import android.bluetooth.BluetoothAdapter.STATE_ON
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter

class BluetoothWatcher(private val context: Context) {

    var onActivationChanged: ((Boolean) -> Unit)? = null

    private val intentFilter = IntentFilter().apply {
        addAction(ACTION_STATE_CHANGED)
    }

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == ACTION_STATE_CHANGED) {
                val currentState = requireNotNull(intent.extras)
                    .getInt(EXTRA_STATE)
                val previousState = requireNotNull(intent.extras)
                    .getInt(EXTRA_PREVIOUS_STATE)
                if (currentState != previousState) {
                    when (currentState) {
                        STATE_ON -> onActivationChanged?.invoke(true)
                        STATE_OFF -> onActivationChanged?.invoke(false)
                    }
                }
            }
        }
    }

    fun startWatching() {
        context.registerReceiver(receiver, intentFilter)
    }

    fun stopWatching() {
        context.unregisterReceiver(receiver)
    }
}
