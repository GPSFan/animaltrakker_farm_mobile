package com.weyr_associates.animaltrakkerfarmmobile;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.TopButtonBar;
import com.weyr_associates.animaltrakkerfarmmobile.app.main.MainActivity;
import com.weyr_associates.animaltrakkerfarmmobile.database.Cursors;
import com.weyr_associates.animaltrakkerfarmmobile.database.IdTypeTable;
import com.weyr_associates.animaltrakkerfarmmobile.databinding.UpdateTagsBinding;
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderService;

import java.util.ArrayList;
import java.util.List;

import kotlin.Unit;

public class UpdateTags extends ListActivity {

    private DatabaseHandler dbh;
    public String     	cmd;
    public Button btn;
    public String alert_text;
    public Cursor 	cursor;
    public Cursor 	 tagCursor, tagLocationCursor, tagColorCursor, newtagCursor, tagRemoveReasonCursor;
    public Object 	crsr, tagcrsr, taglocationcrsr, tagcolorcrsr, tagremovereasoncrsr ;
    ArrayAdapter<String> dataAdapter;
    public int 		thisanimal_id;
    public String mytoday;
    public String mytime;
    public String created, modified;
    public int new_tag_type, new_tag_color, new_tag_location;
    public int num_tags;
    public int tag_remove_reason_id;
    public int farm_tag_color_male, farm_tag_color_female, eid_tag_color_male, eid_tag_color_female, trich_tag_color_male, trich_tag_color_female, paint_tag_color;
    public int farm_tag_on_eid_tag, farm_tag_number_digits_from_eid;
    public int last_id_entered, official_id;
    public Spinner tag_type_spinner, tag_location_spinner, tag_color_spinner, tag_remove_reason_spinner;
    public List<String> tag_types, tag_locations, tag_colors, tag_remove_reasons;
    public int trich_tag_location, eid_tag_location, farm_tag_location;
    public List<String> tag_types_display_order, tag_types_id_order;
    public List<String> tag_color_display_order, tag_color_id_order;
    public List<String> tag_location_display_order, tag_location_id_order;
    public List<String> tag_remove_reasons_display_order, tag_remove_reasons_id_order;
    public List<String> tag_list;
    public List<Integer> test_bull_tag_id_info;
    public SparseBooleanArray sparse_array;
    ListView test_tag_list;
    public String tag_type_label, tag_color_label, tag_location_label, new_tag_number, tag_country_code, tag_remove_reason ;
    public SimpleCursorAdapter myadapter;

    /////////////////////////////////////////////////////
    Messenger mService = null;
    boolean mIsBound;

    final Messenger mMessenger = new Messenger(new IncomingHandler());
    // variable to hold the string
    public String LastEID ;

    private final BluetoothWatcher bluetoothWatcher = new BluetoothWatcher(this);

    private UpdateTagsBinding binding;

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case EIDReaderService.MSG_UPDATE_STATUS:
                    Bundle b1 = msg.getData();
                    break;
                case EIDReaderService.MSG_NEW_EID_FOUND:
                    Bundle b2 = msg.getData();
                    LastEID = (b2.getString("info1"));
                    //We have a good whole EID number
                    Log.i ("in handler case" , "got eid of " + LastEID);
                    gotEID ();
                    break;
                case EIDReaderService.MSG_UPDATE_LOG_APPEND:
                    //Bundle b3 = msg.getData();
                    //Log.i("UpdateTags", "Add to Log.");

                    break;
                case EIDReaderService.MSG_UPDATE_LOG_FULL:
                    //Log.i("UpdateTags", "Log Full.");

                    break;
                case EIDReaderService.MSG_THREAD_SUICIDE:
                    Log.i("UpdateTags", "Service informed Activity of Suicide.");
                    doUnbindService();
                    stopService(new Intent(UpdateTags.this, EIDReaderService.class));

                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    public ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);
            Log.i("UpdateTags", "At Service.");
            try {
                //Register client with service
                Message msg = Message.obtain(null, EIDReaderService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);

                //Request a status update.
                //msg = Message.obtain(null, eidService.MSG_UPDATE_STATUS, 0, 0);
                //				mService.send(msg);

                //Request full log from service.
                //				msg = Message.obtain(null, eidService.MSG_UPDATE_LOG_FULL, 0, 0);
                //				mService.send(msg);

            } catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            mService = null;
        }
    };

    private void CheckIfServiceIsRunning() {
        //If the service is running when the activity starts, we want to automatically bind to it.
        Log.i("UpdateTags", "At eid isRunning?.");
        if (EIDReaderService.isRunning()) {
            doBindService();
        } else {
            Log.i("UpdateTags", "is not, start it");
            startService(new Intent(UpdateTags.this, EIDReaderService.class));
            doBindService();
        }
    }

    void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
//		Log.i("UpdateTags", "At doBind1.");
        bindService(new Intent(this, EIDReaderService.class), mConnection, Context.BIND_AUTO_CREATE);
//		Log.i("UpdateTags", "At doBind2.");
        mIsBound = true;
        if (mService != null) {
//			Log.i("UpdateTags", "At doBind3.");
            try {
                //Request status update
                Message msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_STATUS, 0, 0);
                msg.replyTo = mMessenger;
                mService.send(msg);
//				Log.i("BullTest", "At doBind4.");
                //Request full log from service.
                msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_LOG_FULL, 0, 0);
                mService.send(msg);
            } catch (RemoteException e) {
            }
        }
//		Log.i("UpdateTags", "At doBind5.");
    }
    void doUnbindService() {
        //Log.i("UpdateTags", "At DoUnbindservice");
        if (mService != null) {
            try {
                //Stop eidService from sending tags
                Message msg = Message.obtain(null, EIDReaderService.MSG_NO_TAGS_PLEASE);
                msg.replyTo = mMessenger;
                mService.send(msg);

            } catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        if (mIsBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, EIDReaderService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection);
            mIsBound = false;
        }
    }
    //  user clicked 'Scan' button
    private void scanEid( View v){
        // Here is where I need to get a tag scanned and put the data into the variable LastEID
//        tag_type_spinner.setSelection(1);
        if (mService != null) {
            try {
                //Start eidService sending tags
                Message msg = Message.obtain(null, EIDReaderService.MSG_SEND_ME_TAGS);
                msg.replyTo = mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
    }
    public void gotEID( ) {
        View v = null;

        Log.i("goteid", "before go to add new tag" );
        addTag(v );
        Log.i("goteid", "after add new tag" );
        // 	Display the EID number
        TextView TV = (TextView) binding.newTagNumber;
        TV.setText( LastEID );
        Log.i("in gotEID ", "with LastEID of " + LastEID);

        // Put the EID Tag data into the add tag section of the screen
        tag_type_spinner = binding.tagTypeSpinner;
        tag_color_spinner = binding.tagColorSpinner;
        tag_location_spinner = binding.tagLocationSpinner;
        TV = binding.newTagNumber;
        TV.setText( LastEID );
        tag_type_spinner.setSelection(1);
//	  	Log.i("updateTag", "Tag type is " + tag_type_label);
        tag_color_spinner.setSelection(1);
//	  	Log.i("updateTag", "Tag color is " + tag_color_label);
        tag_location_spinner.setSelection(0);
//	  	Log.i("updateTag", "Tag location is " + tag_location_label);
        // Put the last EID into the new tag number
        new_tag_number = LastEID;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        doUnbindService();
        dbh.close();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = UpdateTagsBinding.inflate(getLayoutInflater());
        CheckIfServiceIsRunning();
        setContentView(binding.getRoot());
        Log.i("update tags", " after set content view");
        View v = null;
        String dbfile = getString(R.string.real_database_file);
        Log.i("AddAnimal", " after get database file");
        dbh = new DatabaseHandler(this, dbfile);
        mytoday = Utilities.TodayIs();
        mytime = Utilities.TimeIs();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            thisanimal_id = extras.getInt("id_animalid");
            Log.i("update tags ", " this animal ID is  " + String.valueOf(thisanimal_id));
            LastEID = extras.getString("animal_eid");
            Log.i("update tags ", " EID tag is " + LastEID);
        }
        // Get the defaults and fill the defaults list for use later.
        // Fill the Tag Type Spinner
        // todo figure out how to attach an event so that when this is changed the color and location
        //  are changed to the defaults for that tag type
        tag_type_spinner = binding.tagTypeSpinner;
        tag_types = new ArrayList<String>();
        tag_types_id_order = new ArrayList<String>();
        tag_types_display_order = new ArrayList<String>();
        // Select All fields from id types to build the spinner
        cmd = "select * from id_type_table order by id_type_display_order";
        Log.i("fill tag spinner", "command is " + cmd);
        tagcrsr = dbh.exec(cmd);
        tagCursor = (Cursor) tagcrsr;
        Log.i("fill tag spinner", "number tag types is " + String.valueOf(tagCursor.getCount()));
        dbh.moveToFirstRecord();
        tag_types.add("Select a Type");
        tag_types_id_order.add("tag_types_id");
        tag_types_display_order.add("tag_type_display_order");
        Log.i("fill tag spinner", "added first option ");
        // looping through all rows and adding to list
        for (tagCursor.moveToFirst(); !tagCursor.isAfterLast(); tagCursor.moveToNext()) {
            String tagId = Cursors.getString(tagCursor, IdTypeTable.Columns.ID);
            tag_types_id_order.add(tagId);
            Log.i("UpdateTags", "tag_types_id_order " + tagId);
            String tagName = Cursors.getString(tagCursor, IdTypeTable.Columns.NAME);
            tag_types.add(tagName);
            Log.i("UpdateTags", "tag_type" + tagName);
            String tagDisplayOrder = Cursors.getString(tagCursor, IdTypeTable.Columns.ORDER);
            tag_types_display_order.add(tagDisplayOrder);
            Log.i("UpdateTags", "tag_types_display_order " + tagDisplayOrder);
        }
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, tag_types);
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
        // todo set an adapter here so that when a new item is selected it changes the tag location
        //  and color to match the defaults for the new tag type
        tag_type_spinner.setAdapter(dataAdapter);
        // todo go get the value for the default tag type and figure out what the spinner should be set to.
        //  The default tag type is in item 12 of the defaults array list
        // Todo figure out how to bind a change of the other spinners when the tag type is modified to get the defaults for that tag type
//        tag_type_spinner.setOnItemSelectedListener(new MyOnItemSelectedListener());
        tag_type_spinner.setSelection(0);

        // Fill the Tag Location Spinner
        tag_location_spinner = binding.tagLocationSpinner;
        tag_locations = new ArrayList<String>();
        tag_location_id_order = new ArrayList<String>();
        tag_location_display_order = new ArrayList<String>();
        // Select All fields from id types to build the spinner
        cmd = "select * from id_location_table order by id_location_display_order";
        Log.i("fill tag spinner", "command is " + cmd);
        taglocationcrsr = dbh.exec(cmd);
        tagLocationCursor = (Cursor) taglocationcrsr;
        dbh.moveToFirstRecord();
        tag_locations.add("Location");
        tag_location_id_order.add("tag_location_id");
        tag_location_display_order.add("tag_location_display_order");
        Log.i("fill tag spinner", "added first option ");
        // looping through all rows and adding to list
        for (tagLocationCursor.moveToFirst(); !tagLocationCursor.isAfterLast(); tagLocationCursor.moveToNext()) {
            tag_location_id_order.add(tagLocationCursor.getString(0));
            Log.i("UpdateTags", "tag_location_id_order " + tagLocationCursor.getString(0));
            tag_locations.add(tagLocationCursor.getString(1));
            Log.i("UpdateTags", "tag_location name" + tagLocationCursor.getString(1));
            tag_location_display_order.add(tagLocationCursor.getString(3));
            Log.i("UpdateTags", "tag_location_display_order " + tagLocationCursor.getString(3));
        }
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, tag_locations);
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
        tag_location_spinner.setAdapter(dataAdapter);
        // todo go get the value for the default tag location and figure out what the spinner should be set to.
        //  The default tag location in the defaults array list varies depending on what the tag type is.
        //  This is tied to the tag type and should change when the tag type changes
        tag_location_spinner.setSelection(0);

        // Fill the Tag Color Spinner
        tag_color_spinner = binding.tagColorSpinner;
        tag_colors = new ArrayList<String>();
        tag_color_id_order = new ArrayList<String>();
        tag_color_display_order = new ArrayList<String>();
        // Select All fields from id types to build the spinner
        cmd = "select * from id_color_table order by id_color_display_order";
        Log.i("fill tag color spinner", "command is " + cmd);
        tagcolorcrsr = dbh.exec(cmd);
        tagColorCursor = (Cursor) tagcolorcrsr;
        dbh.moveToFirstRecord();
        tag_colors.add("Color");
        tag_color_id_order.add("tag_color_id");
        tag_color_display_order.add("tag_color_display_order");
        Log.i("fill tag color spinner", "added first option ");
        // looping through all rows and adding to list
        for (tagColorCursor.moveToFirst(); !tagColorCursor.isAfterLast(); tagColorCursor.moveToNext()) {
            tag_color_id_order.add(tagColorCursor.getString(0));
            Log.i("UpdateTags", "tag_color_id_order " + tagColorCursor.getString(0));
            tag_colors.add(tagColorCursor.getString(1));
            Log.i("UpdateTags", "tag_color name" + tagColorCursor.getString(1));
            tag_color_display_order.add(tagColorCursor.getString(3));
            Log.i("UpdateTags", "tag_colorn_display_order " + tagColorCursor.getString(3));
        }
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, tag_colors);
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
        tag_color_spinner.setAdapter(dataAdapter);
        // todo go get the value for the default tag color and figure out what the spinner should be set to.
        //  The default tag color in the defaults array list varies depending on what the tag type is.
        //  This is tied to the tag type and should change when the tag type changes
        tag_color_spinner.setSelection(0);

        // Fill the Tag Remove Reason Spinner
        tag_remove_reason_spinner = binding.tagRemoveReasonSpinner;
        tag_remove_reasons = new ArrayList<String>();
        tag_remove_reasons_id_order = new ArrayList<String>();
        tag_remove_reasons_display_order = new ArrayList<String>();
        // Select All fields from id types to build the spinner
        cmd = "select * from id_remove_reason_table order by id_remove_reason_display_order";
        Log.i("fill remove ", "reason spinner command is " + cmd);
        tagremovereasoncrsr = dbh.exec(cmd);
        tagRemoveReasonCursor = (Cursor) tagremovereasoncrsr;
        dbh.moveToFirstRecord();
        tag_remove_reasons.add("Enter a Remove Reason");
        tag_remove_reasons_id_order.add("tag_remove_reasons_id");
        tag_remove_reasons_display_order.add("tag_remove_reasons_display_order");
        Log.i("fill remove ", "reason spinner added first option ");
        // looping through all rows and adding to list
        for (tagRemoveReasonCursor.moveToFirst(); !tagRemoveReasonCursor.isAfterLast(); tagRemoveReasonCursor.moveToNext()) {
            tag_remove_reasons_id_order.add(tagRemoveReasonCursor.getString(0));
            Log.i("UpdateTags", "tag_location_id_order " + tagRemoveReasonCursor.getString(0));
            tag_remove_reasons.add(tagRemoveReasonCursor.getString(1));
            Log.i("UpdateTags", "tag_location name" + tagRemoveReasonCursor.getString(1));
            tag_remove_reasons_display_order.add(tagRemoveReasonCursor.getString(3));
            Log.i("UpdateTags", "tag_location_display_order " + tagRemoveReasonCursor.getString(3));
        }
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, tag_remove_reasons);
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
        tag_remove_reason_spinner.setAdapter(dataAdapter);
        // todo go get the value for the default id remove reason and figure out what the spinner should be set to.
        //  The default tag remove reason is in item 34 of the defaults array list
        tag_remove_reason_spinner.setSelection(0);

        //	make the scan eid button red
        btn = binding.buttonPanelTop.getScanEIDButton();
        btn.setOnClickListener(this::scanEid);

        //	make the remove tags button red and disabled
        btn = binding.removeTagsBtn;
        btn.setEnabled(false);

        formatTagRecord(v);

        binding.buttonPanelTop.show(TopButtonBar.UI_SCAN_EID);

        bluetoothWatcher.setOnActivationChanged(enabled -> {
            onBluetoothActivationChanged();
            return Unit.INSTANCE;
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkRequiredPermissions();
        bluetoothWatcher.startWatching();
    }

    @Override
    protected void onPause() {
        super.onPause();
        bluetoothWatcher.stopWatching();
    }

    public void formatTagRecord (View v) {
        Log.i("formatAnimalRecord", " top of format tags");
        cmd = String.format("select animal_table.animal_name, animal_id_info_table.id_number, id_location_table.id_location_name," +
                " id_color_table.id_color_name, id_type_table.id_type_name,  " +
                "animal_id_info_table.id_animalidinfoid as _id, animal_id_info_table.id_date_off  " +
                "from animal_table inner join animal_id_info_table on animal_table.id_animalid = animal_id_info_table.id_animalid " +
                "left outer join id_color_table on animal_id_info_table.id_male_id_idcolorid = id_color_table.id_idcolorid " +
                "left outer join id_location_table on animal_id_info_table.id_idlocationid = id_location_table.id_idlocationid " +
                "inner join id_type_table on animal_id_info_table.id_idtypeid = id_type_table.id_idtypeid " +
                "where animal_id_info_table.id_animalid ='%s' and (animal_id_info_table.id_date_off is null or " +
                "animal_id_info_table.id_date_off is '')order by id_type_name asc", thisanimal_id);
        Log.i("formatAnimalRecord", " Before running get tags command");
        tagcrsr = dbh.exec(cmd);
        newtagCursor = (Cursor) tagcrsr;
        num_tags = newtagCursor.getCount();
        tag_list = new ArrayList<String>();
        test_bull_tag_id_info = new ArrayList<Integer>();
        newtagCursor.moveToFirst();
        Log.i("formatAnimalTags", " the tag cursor is of size " + String.valueOf(dbh.getSize()));
        for (newtagCursor.moveToFirst(); !newtagCursor.isAfterLast(); newtagCursor.moveToNext()) {
            tag_list.add(newtagCursor.getString(1) + "\t\t " + newtagCursor.getString(3) + "\t\t " + newtagCursor.getString(2) + "\t\t " + newtagCursor.getString(4));
            test_bull_tag_id_info.add(newtagCursor.getInt(5));
            Log.i("formatAnimalTags", " the current tag is " + newtagCursor.getString(1) + " " + newtagCursor.getString(3) + " " + newtagCursor.getString(2) + " " + newtagCursor.getString(4));
        }
        Log.i("formatAnimalRecord", " before formatting results");

        newtagCursor.moveToFirst();
        test_tag_list = binding.list;
        if (num_tags > 0) {
            ArrayAdapter<String> adapter = (new ArrayAdapter<String>(this, R.layout.update_tag_list_entry, tag_list));
            test_tag_list.setAdapter(adapter);
            test_tag_list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            test_tag_list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    View v = test_tag_list.getChildAt(position);
//                    Log.i("in click", "I am inside onItemClick and position is:" + String.valueOf(position));
                    //	make the remove tags button green and enabled
                    btn = binding.removeTagsBtn;
                    btn.setEnabled(true);
                }
            });
            getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            sparse_array = getListView().getCheckedItemPositions();
        } else {
            // No tag data - publish an empty list to clear tags
            Log.i(" Update tags", "no current tags");
        }
    }

    // user clicked the 'back' button
    public void backBtn (View v ){
        clearBtn(null);
        cursor.close();
        dbh.closeDB();
        finish();
    }

    public void showAlert (View v){
        // Display alerts here
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        Log.i("ShowAlert", "Alert Text is" + alert_text);
        builder.setMessage(alert_text)
                .setTitle(R.string.alert_warning);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int idx) {
                // User clicked OK button
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // user clicked 'clear' button
    public void clearBtn (View v) {
        tag_type_spinner = binding.tagTypeSpinner;
        tag_color_spinner = binding.tagColorSpinner;
        tag_location_spinner = binding.tagLocationSpinner;
        tag_type_spinner.setSelection(0);
        tag_color_spinner.setSelection(0);
        tag_location_spinner.setSelection(0);
        tag_remove_reason_spinner.setSelection(0);
    }

    public void doNote (View v )
        {
            Utilities.takeNote(v, thisanimal_id, this);
        }

    public void addTag (View v ){
//        todo add tag time on here
        Object crsr;
        String cmd;
        TextView TV;
        Boolean tagok;
        mytoday = Utilities.TodayIs();
        mytime = Utilities.TimeIs();
        //	Initially set tag to not ok until we verify tag entry is good
        tagok = false;
//        official_id = 0;
        // Get the data from the add tag section of the screen
        tag_type_spinner = binding.tagTypeSpinner;
        tag_color_spinner = binding.tagTypeSpinner;
        tag_location_spinner = binding.tagLocationSpinner;

        tag_type_label = tag_type_spinner.getSelectedItem().toString();
        Log.i("addTag", "Tag type is " + tag_type_label);
        tag_color_label = tag_color_spinner.getSelectedItem().toString();
        Log.i("addTag", "Tag color is " + tag_color_label);
        tag_location_label = tag_location_spinner.getSelectedItem().toString();
        Log.i("addTag", "Tag location is " + tag_location_label);
        TV = binding.newTagNumber;
        new_tag_number = TV.getText().toString();
        Log.i("before if ", "new tag number " + new_tag_number);
        created = mytoday + " " + mytime;
        modified = mytoday + " " + mytime;
        if (tag_type_label == "Select a Type" || tag_location_label == "Location" || tag_color_label == "Color"
                || TV.getText().toString().isEmpty()) {
            new_tag_type = 0;
            // Missing data so display an alert
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.convert_fill_fields)
                    .setTitle(R.string.enter_tag_data);
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int idx) {
                    // User clicked OK button
                    return;
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            Log.i("before ", "getting tag type looking for " + tag_type_label);
            cmd = String.format("select id_idtypeid from id_type_table " +
                    "where id_type_name='%s'", tag_type_label);
            crsr = dbh.exec(cmd);
            cursor = (Cursor) crsr;
            dbh.moveToFirstRecord();
            new_tag_type = dbh.getInt(IdTypeTable.Columns.ID);
            Log.i("after ", "getting tag type" + String.valueOf(new_tag_type));

            Log.i("before ", "getting tag color looking for " + tag_color_label);
            cmd = String.format("select id_color_table.id_idcolorid from id_color_table " +
                    "where id_color_name='%s'", tag_color_label);
            crsr = dbh.exec(cmd);
            cursor = (Cursor) crsr;
            dbh.moveToFirstRecord();
            new_tag_color = dbh.getInt(0);
            Log.i("after ", "getting tag color" + String.valueOf(new_tag_color));

            Log.i("before ", "getting tag location looking for " + tag_location_label);
            cmd = String.format("select id_location_table.id_idlocationid, id_location_table.id_location_name from id_location_table " +
                    "where id_location_name='%s'", tag_location_label);
            crsr = dbh.exec(cmd);
            cursor = (Cursor) crsr;
            dbh.moveToFirstRecord();
            new_tag_location = dbh.getInt(0);
            Log.i("New Location ID ", String.valueOf(new_tag_location));
            tag_location_label = dbh.getStr(1);
            Log.i("New Location ", tag_location_label);

            //	Need to test if the type is an ear tag and the location is not an ear then must request change
            switch (new_tag_type) {
                case 1:
                case 2:
                case 4:
                case 6:
                case 7:
                case 10:
                case 11:
                case 12:
                    // Tag Type is Federal Scrapie, Electronic, Farm, Split, Notch, Trich, NUES or Sale Order so require an ear location
                    switch (new_tag_location) {
                        case 1:
                        case 2:
                            // Ear locations so ok
                            tagok = true;
                            cmd = String.format("insert into animal_id_info_table (id_animalid, id_idtypeid, id_male_id_idcolorid," +
                                            " id_female_id_idcolorid, id_idlocationid, id_date_on, id_time_on, id_date_off, id_time_off, id_number, " +
                                            "id_scrapieflockid, official_id, id_idremovereasonid, created, modified) values " +
                                            " (%s, %s, %s, %s, %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') ",
                                    thisanimal_id, new_tag_type, new_tag_color, new_tag_color,
                                    new_tag_location, mytoday, mytime, "", "", new_tag_number, "",official_id, "", created, modified);
                            Log.i("add tag to ", "db cmd is " + cmd);
                            dbh.exec(cmd);
                            Log.i("add tag ", "after insert into id_info_table");
                            cmd = String.format("select last_insert_rowid()");
                            crsr = dbh.exec( cmd );
                            cursor   = ( Cursor ) crsr;
                            dbh.moveToFirstRecord();
                            last_id_entered = dbh.getInt(0);
                            break;
                        case 3:
                        case 4:
                        case 5:
                            // flank or side not allowed for tags, split or notches
                            tagok = false;
                            break;
                    }
                    break;
                case 3:
                    // paint brand only location allowed is side
                    switch (new_tag_location) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                            tagok = false;
                            break;
                        case 5:
                            // side so ok
                            tagok = true;
                            cmd = String.format("insert into animal_id_info_table (id_animalid, id_idtypeid, id_male_id_idcolorid," +
                                            " id_female_id_idcolorid, id_idlocationid, id_date_on, id_time_on, id_date_off, id_time_off, id_number, " +
                                            "id_scrapieflockid, official_id, id_idremovereasonid, created, modified) values " +
                                            " (%s, %s, %s, %s, %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') ",
                                    thisanimal_id, new_tag_type, new_tag_color, new_tag_color,
                                    new_tag_location, mytoday, mytime, "", "", new_tag_number, "", "", "", created, modified);
                            Log.i("add tag to ", "db cmd is " + cmd);
                            dbh.exec(cmd);
                            Log.i("add tag ", "after insert into id_info_table");
                            break;
                    }
                    break;
                case 5:
                    // tattoo so all except side
                    switch (new_tag_location) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            // Ear and flank locations ok
                            tagok = true;
                            cmd = String.format("insert into animal_id_info_table (id_animalid, id_idtypeid, id_male_id_idcolorid," +
                                            " id_female_id_idcolorid, id_idlocationid, id_date_on, id_time_on, id_date_off, id_time_off, id_number, " +
                                            "id_scrapieflockid, official_id, id_idremovereasonid, created, modified) values " +
                                            " (%s, %s, %s, %s, %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') ",
                                    thisanimal_id, new_tag_type, new_tag_color, new_tag_color,
                                    new_tag_location, mytoday, mytime, "", "", new_tag_number, "", "", "", created, modified);
                            Log.i("add tag to ", "db cmd is " + cmd);
                            dbh.exec(cmd);
                            Log.i("add tag ", "after insert into id_info_table");
                            break;
                        case 5:
                        case 6:
                        case 8:
                        case 9:
                            // side , unknown, and neck not allowed
                            tagok = false;
                            break;
                    }
                    break;
            }
            if (tagok) {
                switch (new_tag_type){
                    case 1: //	Tag is a federal scrapie tag or a NUES tag
                    case 11:
                        official_id = 1;
                        cmd = String.format(" update animal_id_info_table set official_id = '%s' where id_animalidinfoid = %d ", official_id, last_id_entered);
                        Log.i("update tag to ", "db cmd is " + cmd);
                        dbh.exec(cmd);
                        Log.i("update tag ", "after insert official into id_info_table");
                        break;
                    case 2:	//	Tag is an electronic tag
                        // test for official tag
                        tag_country_code = new_tag_number.substring(0, 3);
                        Log.i("tag_country_code ", "is " + tag_country_code);
                        if (tag_country_code.equals("840")) { // USA code
                            official_id = 1;
                        }
                        if (tag_country_code.equals("124")) { // Canada code
                            official_id = 1;
                        }
                        if (tag_country_code.equals("826")) { // United Kingdom code
                            official_id = 1;
                        }
                        if (tag_country_code.equals("484")) { // Mexico code
                            official_id = 1;
                        }
                        if (tag_country_code.equals("036")) { // Australia code
                            official_id = 1;
                        }
                        if (tag_country_code.equals("554")) { // New Zealand code
                            official_id = 1;
                        }
                        if (tag_country_code.equals("756")) { // Switzerland code
                            official_id = 1;
                        }
                        cmd = String.format(" update animal_id_info_table set official_id = '%s' where id_animalidinfoid = %d ", official_id, last_id_entered);
                        Log.i("update tag to ", "db cmd is " + cmd);
                        dbh.exec(cmd);
                        Log.i("update tag ", "after insert official into id_info_table");
                        break;
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        break;
                }
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.enter_tag_data)
                        .setTitle(R.string.enter_tag_data);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idx) {
                        // User clicked OK button
                        return;
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
//
        }
        //  Set the Add Tag button to green and enabled now
        btn = binding.addTagBtn;
        btn.setEnabled(true);
        TV = binding.newTagNumber;
        TV.setText("");
        // todo set these according to the defaults
        tag_type_spinner.setSelection(0);
        tag_color_spinner.setSelection(0);
        tag_location_spinner.setSelection(0);
        tag_remove_reason_spinner.setSelection(0);
        formatTagRecord(v);
    }

    public void removeTag (View v ) {
        String cmd;
        TextView TV;
        Object crsr;
        boolean temp_value;
        int this_tag_id, temp_size, temp_location;
        tag_remove_reason_spinner = binding.tagRemoveReasonSpinner;
        tag_remove_reason = tag_remove_reason_spinner.getSelectedItem().toString();
        if (tag_remove_reason == "Enter a Remove Reason") {
            //	Need to require a value for tag remove reason here
//              Missing data so display an alert
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.remove_tag_fill_fields)
                    .setTitle(R.string.remove_tag_fill_fields);
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int idx) {
                    // User clicked OK button
                    // make update database button normal and enabled so we can try again
//                    btn = (Button) findViewById(R.id.update_database_btn);
//                    btn.setEnabled(true);
//                    return;
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
            btn = binding.removeTagsBtn;
            btn.setEnabled(true);
//            //	make the look up animal button green
//            btn = (Button) findViewById( R.id.look_up_animal_btn );
            //	make the scan eid button red
//            btn = (Button) findViewById(R.id.scan_eid_btn);
//            //	Disable the Next Record and Prev. Record buttons until we have multiple records
//            btn = (Button) findViewById(R.id.next_rec_btn);
//            btn.setEnabled(false);
            return;
        } else {
            temp_size = sparse_array.size();
            Log.i("in Update ", "sparse array size is " + String.valueOf(temp_size));
            for (int i = 0; i < temp_size; i++) {
                temp_value = sparse_array.valueAt(i);
                temp_location = sparse_array.keyAt(i);
                if (temp_value) {
                    this_tag_id = test_bull_tag_id_info.get(temp_location);
                    Log.i("for loop", "the tag " + " " + tag_list.get(temp_location) + " is checked");
                    Log.i("for loop", "the tag_id is " + String.valueOf(this_tag_id));
                    cmd = String.format("update animal_id_info_table set id_date_off = '%s',id_time_off = '%s' where id_animalidinfoid = %s ", mytoday, mytime, this_tag_id);
                    Log.i("update tag remove ", "date db cmd is " + cmd);
                    dbh.exec(cmd);
                    Log.i("add tag ", "after update into animal_id_info_table");
                    //Get the remove reason id here
                    tag_remove_reason_spinner = binding.tagRemoveReasonSpinner;
                    // Get the tag remove reason here
                    tag_remove_reason_id = tag_remove_reason_spinner.getSelectedItemPosition();
                    cmd = String.format("update animal_id_info_table set id_idremovereasonid = %s where id_animalidinfoid = %s ", tag_remove_reason_id, this_tag_id);
                    dbh.exec(cmd);
                    // Set the remove tag spinner to no reason
                    // tag_remove_reason_spinner = (Spinner) findViewById(R.id.tag_remove_reason_spinner);
                    // tag_remove_reason_spinner.setSelection(0);
                }
                btn = binding.removeTagsBtn;
                btn.setEnabled(false);
            }
            formatTagRecord(v);
        }
    }

    private boolean checkRequiredPermissions() {
        if (!RequiredPermissions.areFulfilled(this)) {
            MainActivity.returnFrom(this);
            return false;
        }
        return true;
    }

    private void onBluetoothActivationChanged() {
        checkRequiredPermissions();
    }
}