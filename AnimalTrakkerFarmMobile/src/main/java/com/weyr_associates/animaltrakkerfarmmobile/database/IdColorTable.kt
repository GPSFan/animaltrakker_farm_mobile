package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.IdColor

object IdColorTable {

    const val NAME = "id_color_table"

    fun idColorFromCursor(cursor: Cursor): IdColor {
        return IdColor(
            id = cursor.getInt(Columns.ID),
            name = cursor.getString(Columns.NAME),
            abbreviation = cursor.getString(Columns.ABBREVIATION),
            order = cursor.getInt(Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_idcolorid"
        const val NAME = "id_color_name"
        const val ABBREVIATION = "id_color_abbrev"
        const val ORDER = "id_color_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
