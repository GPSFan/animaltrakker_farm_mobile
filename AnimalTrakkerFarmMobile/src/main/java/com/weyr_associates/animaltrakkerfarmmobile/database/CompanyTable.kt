package com.weyr_associates.animaltrakkerfarmmobile.database

object CompanyTable {

    const val NAME = "company_table"

    object Columns {
        const val ID = "id_companyid"
        const val NAME = "company"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
