package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.Sex

object SexTable {

    const val NAME = "sex_table"

    fun sexFromCursor(cursor: Cursor): Sex {
        return Sex(
            id = cursor.getInt(Columns.ID),
            name = cursor.getString(Columns.NAME),
            abbreviation = cursor.getString(Columns.ABBREVIATION),
            standard = cursor.getString(Columns.STANDARD),
            standardAbbreviation = cursor.getString(Columns.STANDARD_ABBREVIATION),
            order = cursor.getInt(Columns.ORDER),
            speciesId = cursor.getInt(Columns.SPECIES_ID)
        )
    }

    object Columns {
        const val ID = "id_sexid"
        const val NAME = "sex_name"
        const val ABBREVIATION = "sex_abbrev"
        const val STANDARD = "sex_standard"
        const val STANDARD_ABBREVIATION = "sex_abbrev_standard"
        const val ORDER = "sex_display_order"
        const val SPECIES_ID = "id_speciesid"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
