package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.IdLocation

object IdLocationTable {

    const val NAME = "id_location_table"

    fun idLocationFromCursor(cursor: Cursor): IdLocation {
        return IdLocation(
            id = cursor.getInt(Columns.ID),
            name = cursor.getString(Columns.NAME),
            abbreviation = cursor.getString(Columns.ABBREVIATION),
            order = cursor.getInt(Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_idlocationid"
        const val NAME = "id_location_name"
        const val ABBREVIATION = "id_location_abbrev"
        const val ORDER = "id_location_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
