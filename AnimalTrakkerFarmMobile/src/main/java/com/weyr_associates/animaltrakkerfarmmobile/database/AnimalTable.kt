package com.weyr_associates.animaltrakkerfarmmobile.database

object AnimalTable {

    const val NAME = "animal_table"

    object Columns {
        const val ID = "id_animalid"
        const val NAME = "animal_name"
        const val SEX_ID = "id_sexid"
        const val BIRTH_DATE = "birth_date"
        const val BIRTH_TIME = "birth_time"
        const val BIRTH_TYPE_ID = "id_birthtypeid"
        const val BIRTH_WEIGHT = "birth_weight"
        const val BIRTH_WEIGH_UNITS_ID = "birth_weight_id_unitsid"
        const val BIRTH_ORDER = "birth_order"
        const val REAR_TYPE = "rear_type"
        const val WEANED_DATE = "weaned_date"
        const val DEATH_DATE = "death_date"
        const val DEATH_REASON_ID = "id_deathreasonid"
        const val SIRE_ID = "sire_id"
        const val DAM_ID = "dam_id"
        const val FOSTER_DAM_ID = "foster_dam_id"
        const val SURROGATE_DAM_ID = "surrogate_dam_id"
        const val IS_HAND_REARED = "hand_reared"
        const val MANAGEMENT_GROUP_ID = "id_managementgroupid"
        const val ALERT = "alert"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {
        const val QUERY_COUNT_ALL_ANIMALS =
            """SELECT COUNT(*) FROM ${AnimalTable.NAME}"""
    }
}
