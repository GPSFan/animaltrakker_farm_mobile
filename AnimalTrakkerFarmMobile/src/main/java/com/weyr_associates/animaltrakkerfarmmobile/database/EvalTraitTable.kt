package com.weyr_associates.animaltrakkerfarmmobile.database

object EvalTraitTable {

    const val NAME = "evaluation_trait_table"

    object Columns {
        const val ID = "id_evaluationtraitid"
        const val NAME = "trait_name"
        const val TYPE_ID = "id_evaluationtraittypeid"
        const val ORDER = "evaluation_trait_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
