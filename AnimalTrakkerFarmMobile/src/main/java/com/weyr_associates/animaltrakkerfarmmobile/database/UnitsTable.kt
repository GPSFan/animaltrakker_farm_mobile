package com.weyr_associates.animaltrakkerfarmmobile.database

object UnitsTable {

    const val NAME = "units_table"

    object Columns {
        const val ID = "id_unitsid"
        const val NAME = "units_name"
        const val ABBREVIATION = "units_abbrev"
        const val TYPE_ID = "id_unitstypeid"
        const val ORDER = "units_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
