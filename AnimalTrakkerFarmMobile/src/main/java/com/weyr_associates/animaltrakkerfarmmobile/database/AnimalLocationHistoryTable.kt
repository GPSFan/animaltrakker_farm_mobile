package com.weyr_associates.animaltrakkerfarmmobile.database

object AnimalLocationHistoryTable {

    const val NAME = "animal_location_history_table"

    object Columns {
        const val ID = "id_animalhistorylocationid"
        const val ANIMAL_ID = "id_animalid"
        const val MOVEMENT_DATE = "movement_date"
        const val FROM_PREMISE_ID = "from_id_premiseid"
        const val TO_PREMISE_ID = "to_id_premiseid"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
