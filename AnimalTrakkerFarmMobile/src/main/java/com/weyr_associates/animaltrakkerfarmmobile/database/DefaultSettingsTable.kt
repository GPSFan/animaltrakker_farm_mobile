package com.weyr_associates.animaltrakkerfarmmobile.database

import android.content.Context
import android.database.Cursor
import androidx.preference.PreferenceManager
import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.ActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.model.DefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType
import com.weyr_associates.animaltrakkerfarmmobile.model.ItemEntry

object DefaultSettingsTable {

    const val NAME = "animaltrakker_default_settings_table"

    private const val ID_LOCATION_ID_DEFAULT = 1 //RIGHT EAR
    private const val ID_COLOR_ID_DEFAULT = 1 //YELLOW

    private const val DEFAULT_SETTINGS_QUERY =
        "SELECT * FROM $NAME WHERE ${Columns.ID} = ?"

    @JvmStatic
    fun readAsMap(context: Context): Map<String,Int> {
        DatabaseHandler.create(context).use {  databaseHandler ->
            return readAsMapFrom(context, databaseHandler)
        }
    }

    @JvmStatic
    fun readAsMapFrom(context: Context, databaseHandler: DatabaseHandler): Map<String,Int> {
        val activeDefaultSettings = ActiveDefaultSettings(
            PreferenceManager.getDefaultSharedPreferences(context)
        )
        val activeDefaultSettingsId = activeDefaultSettings.loadActiveDefaultSettingsId()
        databaseHandler.readableDatabase.rawQuery(
            DEFAULT_SETTINGS_QUERY,
            arrayOf(activeDefaultSettingsId.toString())
        )?.use { cursor ->
            if (cursor.moveToFirst()) {
                return readAsMapFrom(cursor)
            } else {
                throw IllegalStateException ("No default settings records found.")
            }
        } ?: throw IllegalStateException("No cursor for default settings query.")
    }

    @JvmStatic
    fun readAsMapFrom(cursor: Cursor): Map<String,Int> {
        //TODO: This is temporary to get off of indices and move to column names.
        return buildMap {
            for (index: Int in 0 until cursor.columnCount) {
                put(cursor.getColumnName(index), cursor.getInt(index))
            }
        }
    }

    @JvmStatic
    fun defaultIdLocationFromIdTypeId(defaults: Map<String,Int>, idTypeId: Int): Int = defaults.getOrDefault(
        when (idTypeId) {
            IdType.ID_TYPE_ID_FED -> Columns.FED_TAG_LOCATION
            IdType.ID_TYPE_ID_EID -> Columns.EID_TAG_LOCATION
            IdType.ID_TYPE_ID_PAINT -> Columns.PAINT_MARK_LOCATION
            IdType.ID_TYPE_ID_FARM -> Columns.FARM_TAG_LOCATION
            IdType.ID_TYPE_ID_TATTOO -> Columns.TATTOO_LOCATION
            IdType.ID_TYPE_ID_TRICH -> Columns.TRICH_TAG_LOCATION
            IdType.ID_TYPE_ID_NUES -> Columns.NUES_TAG_LOCATION
            IdType.ID_TYPE_ID_SALE_ORDER -> Columns.SALE_ORDER_TAG_LOCATION
            IdType.ID_TYPE_ID_BANGS -> Columns.BANGS_TAG_LOCATION
            else -> ID_LOCATION_ID_DEFAULT
        },
        ID_LOCATION_ID_DEFAULT
    )

    @JvmStatic
    fun defaultIdColorFromIdTypeId(defaults: Map<String,Int>, idTypeId: Int): Int = defaults.getOrDefault(
        when (idTypeId) {
            IdType.ID_TYPE_ID_FED -> Columns.FED_TAG_COLOR_MALE
            IdType.ID_TYPE_ID_EID -> Columns.EID_TAG_COLOR_MALE
            IdType.ID_TYPE_ID_PAINT -> Columns.PAINT_MARK_COLOR
            IdType.ID_TYPE_ID_FARM -> Columns.FARM_TAG_COLOR_MALE
            IdType.ID_TYPE_ID_TATTOO -> Columns.TATTOO_COLOR
            IdType.ID_TYPE_ID_TRICH -> Columns.TRICH_TAG_COLOR_MALE
            IdType.ID_TYPE_ID_NUES -> Columns.NUES_TAG_COLOR_MALE
            IdType.ID_TYPE_ID_SALE_ORDER -> Columns.SALE_ORDER_TAG_COLOR_MALE
            IdType.ID_TYPE_ID_BANGS -> Columns.BANGS_TAG_COLOR_MALE
            else -> ID_COLOR_ID_DEFAULT
        },
        ID_COLOR_ID_DEFAULT
    )

    fun defaultSettingsEntryFromCursor(cursor: Cursor) = with (cursor) {
        ItemEntry(
            id = getInt(Columns.ID),
            name = getString(Columns.NAME)
        )
    }

    fun defaultSettingsFromCursor(cursor: Cursor) = with (cursor) {
        DefaultSettings(
            id = getInt(Columns.ID),
            name = getString(Columns.NAME),
            ownerContactId = getInt(Columns.OWNER_CONTACT_ID),
            ownerCompanyId = getInt(Columns.OWNER_COMPANY_ID),
            ownerPremiseId = getInt(Columns.OWNER_PREMISE_ID),
            breederContactId = getInt(Columns.BREEDER_CONTACT_ID),
            breederCompanyId = getInt(Columns.BREEDER_COMPANY_ID),
            breederPremiseId = getInt(Columns.BREEDER_PREMISE_ID),
            vetContactId = getInt(Columns.VET_CONTACT_ID),
            vetPremiseId = getInt(Columns.VET_PREMISE_ID),
            labCompanyId = getInt(Columns.LAB_COMPANY_ID),
            labPremiseId = getInt(Columns.LAB_PREMISE_ID),
            registryCompanyId = getInt(Columns.REGISTRY_COMPANY_ID),
            registryPremiseId = getInt(Columns.REGISTRY_PREMISE_ID),
            stateId = getInt(Columns.STATE_ID),
            countyId = getInt(Columns.COUNTY_ID),
            flockPrefixId = getInt(Columns.FLOCK_PREFIX_ID),
            speciesId = getInt(Columns.SPECIES_ID),
            breedId = getInt(Columns.BREED_ID),
            sexId = getInt(Columns.SEX_ID),
            idTypeIdPrimary = getInt(Columns.ID_TYPE_ID_PRIMARY),
            idTypeIdSecondary = getInt(Columns.ID_TYPE_ID_SECONDARY),
            idTypeIdTertiary = getInt(Columns.ID_TYPE_ID_TERTIARY),
            eidKeepMaleFemaleIdColorSame = getBoolean(Columns.EID_TAG_MALE_COLOR_FEMALE_COLOR_SAME),
            eidColorMale = getInt(Columns.EID_TAG_COLOR_MALE),
            eidColorFemale = getInt(Columns.EID_TAG_COLOR_FEMALE),
            eidIdLocation = getInt(Columns.EID_TAG_LOCATION),
            farmKeepMaleFemaleIdColorSame = getBoolean(Columns.FARM_TAG_MALE_COLOR_FEMALE_COLOR_SAME),
            farmIdBasedOnEid = getBoolean(Columns.FARM_TAG_BASED_ON_EID_TAG),
            farmIdNumberDigitsFromEid = getBoolean(Columns.FARM_TAG_NUMBER_DIGITS_FROM_EID),
            farmIdColorMale = getInt(Columns.FARM_TAG_COLOR_MALE),
            farmIdColorFemale = getInt(Columns.FARM_TAG_COLOR_FEMALE),
            farmIdLocation = getInt(Columns.FARM_TAG_LOCATION),
            fedKeepMaleFemaleIdColorSame = getBoolean(Columns.FED_TAG_MALE_COLOR_FEMALE_COLOR_SAME),
            fedIdColorMale = getInt(Columns.FED_TAG_COLOR_MALE),
            fedIdColorFemale = getInt(Columns.FED_TAG_COLOR_FEMALE),
            fedIdLocation = getInt(Columns.FED_TAG_LOCATION),
            nuesMaleFemaleKeepIdColorSame = getBoolean(Columns.NUES_TAG_MALE_COLOR_FEMALE_COLOR_SAME),
            nuesIdColorMale = getInt(Columns.NUES_TAG_COLOR_MALE),
            nuesIdColorFemale = getInt(Columns.NUES_TAG_COLOR_FEMALE),
            nuesIdLocation = getInt(Columns.NUES_TAG_LOCATION),
            trichKeepMaleFemaleIdColorSame = getBoolean(Columns.TRICH_TAG_MALE_COLOR_FEMALE_COLOR_SAME),
            trichIdColorMale = getInt(Columns.TRICH_TAG_COLOR_MALE),
            trichIdColorFemale = getInt(Columns.TRICH_TAG_COLOR_FEMALE),
            trichIdLocation = getInt(Columns.TRICH_TAG_LOCATION),
            trichIdAutoIncrement = getBoolean(Columns.TRICH_TAG_AUTO_INCREMENT),
            trichNextIdNumber = getInt(Columns.TRICH_TAG_NEXT_TAG_NUMBER),
            bangsIdKeepMaleFemaleIdColorSame = getBoolean(Columns.BANGS_TAG_MALE_COLOR_FEMALE_COLOR_SAME),
            bangsIdColorMale = getInt(Columns.BANGS_TAG_COLOR_MALE),
            bangsIdColorFemale = getInt(Columns.BANGS_TAG_COLOR_FEMALE),
            bangsIdLocation = getInt(Columns.BANGS_TAG_LOCATION),
            saleOrderKeepMaleFemaleIdColorSame = getBoolean(Columns.SALE_ORDER_TAG_MALE_COLOR_FEMALE_COLOR_SAME),
            saleOrderIdColorMale = getInt(Columns.SALE_ORDER_TAG_COLOR_MALE),
            saleOrderIdColorFemale = getInt(Columns.SALE_ORDER_TAG_COLOR_FEMALE),
            saleOrderIdLocation = getInt(Columns.SALE_ORDER_TAG_LOCATION),
            usePaintMarks = getBoolean(Columns.USE_PAINT_MARKS),
            paintMarkColor = getInt(Columns.PAINT_MARK_COLOR),
            paintMarkLocation = getInt(Columns.PAINT_MARK_LOCATION),
            tattooColor = getInt(Columns.TATTOO_COLOR),
            tattooLocation = getInt(Columns.TATTOO_LOCATION),
            freezeBrandLocation = getInt(Columns.FREEZE_BRAND_LOCATION),
            removeReasonId = getInt(Columns.REMOVE_REASON_ID),
            tissueSampleTypeId = getInt(Columns.TISSUE_SAMPLE_TYPE_ID),
            tissueTestId = getInt(Columns.TISSUE_TEST_ID),
            tissueSampleContainerTypeId = getInt(Columns.TISSUE_CONTAINER_TYPE_ID),
            birthTypeId = getInt(Columns.BIRTH_TYPE),
            rearTypeId = getInt(Columns.REAR_TYPE),
            minimumBirthWeight = getFloat(Columns.MINIMUM_BIRTH_WEIGHT),
            maximumBirthWeight = getFloat(Columns.MAXIMUM_BIRTH_WEIGHT),
            birthWeightUnitsId = getInt(Columns.BIRTH_WEIGHT_UNITS_ID),
            weightUnitsId = getInt(Columns.WEIGHT_UNITS_ID),
            salePriceUnitsId = getInt(Columns.SALE_PRICE_UNITS_ID),
            evaluationUpdateAlert = getInt(Columns.EVALUATION_UPDATE_ALERT),
            deathReasonId = getInt(Columns.DEATH_REASON_ID),
            deathReasonContactId = getInt(Columns.DEATH_REASON_CONTACT_ID),
            deathReasonCompanyId = getInt(Columns.DEATH_REASON_COMPANY_ID),
            transferReasonId = getInt(Columns.TRANSFER_REASON_ID),
            transferReasonContactId = getInt(Columns.TRANSFER_REASON_CONTACT_ID),
            transferReasonCompanyId = getInt(Columns.TRANSFER_REASON_COMPANY_ID),
            userSystemSerialNumber = getInt(Columns.USER_SYSTEM_SERIAL_NUMBER)
        )
    }

    object Columns {
        const val ID = "id_animaltrakkerdefaultsettingsid"
        const val NAME = "default_settings_name"
        const val OWNER_CONTACT_ID = "owner_id_contactid"
        const val OWNER_COMPANY_ID = "owner_id_companyid"
        const val OWNER_PREMISE_ID = "owner_id_premiseid"
        const val BREEDER_CONTACT_ID = "breeder_id_contactid"
        const val BREEDER_COMPANY_ID = "breeder_id_companyid"
        const val BREEDER_PREMISE_ID = "breeder_id_premiseid"
        const val VET_CONTACT_ID = "vet_id_contactid"
        const val VET_PREMISE_ID = "vet_id_premiseid"
        const val LAB_COMPANY_ID = "lab_id_companyid"
        const val LAB_PREMISE_ID = "lab_id_premiseid"
        const val REGISTRY_COMPANY_ID = "id_registry_id_companyid"
        const val REGISTRY_PREMISE_ID = "registry_id_premiseid"
        const val STATE_ID = "id_stateid"
        const val COUNTY_ID = "id_countyid"
        const val FLOCK_PREFIX_ID = "id_flockprefixid"
        const val SPECIES_ID = "id_speciesid"
        const val BREED_ID = "id_breedid"
        const val SEX_ID = "id_sexid"
        const val ID_TYPE_ID_PRIMARY = "id_idtypeid_primary"
        const val ID_TYPE_ID_SECONDARY = "id_idtypeid_secondary"
        const val ID_TYPE_ID_TERTIARY = "id_idtypeid_tertiary"
        const val EID_TAG_MALE_COLOR_FEMALE_COLOR_SAME = "id_eid_tag_male_color_female_color_same"
        const val EID_TAG_COLOR_MALE = "eid_tag_color_male"
        const val EID_TAG_COLOR_FEMALE = "eid_tag_color_female"
        const val EID_TAG_LOCATION = "eid_tag_location"
        const val FARM_TAG_MALE_COLOR_FEMALE_COLOR_SAME = "id_farm_tag_male_color_female_color_same"
        const val FARM_TAG_BASED_ON_EID_TAG = "farm_tag_based_on_eid_tag"
        const val FARM_TAG_NUMBER_DIGITS_FROM_EID = "farm_tag_number_digits_from_eid"
        const val FARM_TAG_COLOR_MALE = "farm_tag_color_male"
        const val FARM_TAG_COLOR_FEMALE = "farm_tag_color_female"
        const val FARM_TAG_LOCATION = "farm_tag_location"
        const val FED_TAG_MALE_COLOR_FEMALE_COLOR_SAME = "id_fed_tag_male_color_female_color_same"
        const val FED_TAG_COLOR_MALE = "fed_tag_color_male"
        const val FED_TAG_COLOR_FEMALE = "fed_tag_color_female"
        const val FED_TAG_LOCATION = "fed_tag_location"
        const val NUES_TAG_MALE_COLOR_FEMALE_COLOR_SAME = "id_nues_tag_male_color_female_color_same"
        const val NUES_TAG_COLOR_MALE = "nues_tag_color_male"
        const val NUES_TAG_COLOR_FEMALE = "nues_tag_color_female"
        const val NUES_TAG_LOCATION = "nues_tag_location"
        const val TRICH_TAG_MALE_COLOR_FEMALE_COLOR_SAME =
            "id_trich_tag_male_color_female_color_same"
        const val TRICH_TAG_COLOR_MALE = "trich_tag_color_male"
        const val TRICH_TAG_COLOR_FEMALE = "trich_tag_color_female"
        const val TRICH_TAG_LOCATION = "trich_tag_location"
        const val TRICH_TAG_AUTO_INCREMENT = "trich_tag_auto_increment"
        const val TRICH_TAG_NEXT_TAG_NUMBER = "trich_tag_next_tag_number"
        const val BANGS_TAG_MALE_COLOR_FEMALE_COLOR_SAME =
            "id_bangs_tag_male_color_female_color_same"
        const val BANGS_TAG_COLOR_MALE = "bangs_tag_color_male"
        const val BANGS_TAG_COLOR_FEMALE = "bangs_tag_color_female"
        const val BANGS_TAG_LOCATION = "bangs_tag_location"
        const val SALE_ORDER_TAG_MALE_COLOR_FEMALE_COLOR_SAME =
            "id_sale_order_tag_male_color_female_color_same"
        const val SALE_ORDER_TAG_COLOR_MALE = "sale_order_tag_color_male"
        const val SALE_ORDER_TAG_COLOR_FEMALE = "sale_order_tag_color_female"
        const val SALE_ORDER_TAG_LOCATION = "sale_order_tag_location"
        const val USE_PAINT_MARKS = "use_paint_marks"
        const val PAINT_MARK_COLOR = "paint_mark_color"
        const val PAINT_MARK_LOCATION = "paint_mark_location"
        const val TATTOO_COLOR = "tattoo_color"
        const val TATTOO_LOCATION = "tattoo_location"
        const val FREEZE_BRAND_LOCATION = "freeze_brand_location"
        const val REMOVE_REASON_ID = "id_idremovereasonid"
        const val TISSUE_SAMPLE_TYPE_ID = "id_tissuesampletypeid"
        const val TISSUE_TEST_ID = "id_tissuetestid"
        const val TISSUE_CONTAINER_TYPE_ID = "id_tissuesamplecontainertypeid"
        const val BIRTH_TYPE = "birth_type"
        const val REAR_TYPE = "rear_type"
        const val MINIMUM_BIRTH_WEIGHT = "minimum_birth_weight"
        const val MAXIMUM_BIRTH_WEIGHT = "maximum_birth_weight"
        const val BIRTH_WEIGHT_UNITS_ID = "birth_weight_id_unitsid"
        const val WEIGHT_UNITS_ID = "weight_id_unitsid"
        const val SALE_PRICE_UNITS_ID = "sale_price_id_unitsid"
        const val EVALUATION_UPDATE_ALERT = "evaluation_update_alert"
        const val DEATH_REASON_CONTACT_ID = "death_reason_id_contactid"
        const val DEATH_REASON_COMPANY_ID = "death_reason_id_companyid"
        const val DEATH_REASON_ID = "id_deathreasonid"
        const val TRANSFER_REASON_CONTACT_ID = "transfer_reason_id_contactid"
        const val TRANSFER_REASON_COMPANY_ID = "transfer_reason_id_companyid"
        const val TRANSFER_REASON_ID = "id_transferreasonid"
        const val USER_SYSTEM_SERIAL_NUMBER = "user_system_serial_number"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
