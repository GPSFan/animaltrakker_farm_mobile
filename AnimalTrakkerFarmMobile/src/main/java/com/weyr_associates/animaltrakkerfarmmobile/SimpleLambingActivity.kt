package com.weyr_associates.animaltrakkerfarmmobile

import android.app.AlertDialog
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.database.Cursor
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import android.widget.RadioGroup
import android.widget.RatingBar
import android.widget.ScrollView
import android.widget.SimpleCursorAdapter
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.weyr_associates.animaltrakkerfarmmobile.RequiredPermissions.areFulfilled
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.LegacyAddAnimalActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.TopButtonBar
import com.weyr_associates.animaltrakkerfarmmobile.app.main.MainActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.select.SelectAnimal
import com.weyr_associates.animaltrakkerfarmmobile.app.select.SelectAnimalActivity.Companion.newIntent
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ActivitySimpleLambingBinding
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderService
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderStatePresenter
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType
import com.weyr_associates.animaltrakkerfarmmobile.model.SexStandard
import java.util.Calendar
import java.util.Date

class SimpleLambingActivity : AppCompatActivity() {
    var cmd: String? = null
    var tempLabel: String? = null
    var i: Int? = null
    var thisanimal_id: Int = 0
    var id_predefinednotesid: Int = 0
    var button: Button? = null
    lateinit var btn: Button
    lateinit var btn2: Button
    private lateinit var listView: ListView

    var current_tag_type_id: Int = 0
    var last_location: Int = 0
    var default_owner: Int = 0
    var trait01_data: Int = 0
    var trait02_data: Int = 0
    var trait03_data: Int = 0
    var trait04_data: Int = 0
    var trait05_data: Int = 0
    var trait06_data: Int = 0
    var trait07_data: Int = 0
    var trait08_data: Int = 0
    var trait09_data: Int = 0
    var trait10_data: Int = 0
    var trait11_data: Float? = null
    var trait12_data: Float? = null
    var trait13_data: Float? = null
    var trait14_data: Float? = null
    var trait15_data: Float? = null
    var trait16_data: Int = 0
    var trait17_data: Int = 0
    var trait18_data: Int = 0
    var trait19_data: Int = 0
    var trait20_data: Int = 0

    var trait01: Int = 0
    var trait02: Int = 0
    var trait03: Int = 0
    var trait04: Int = 0
    var trait05: Int = 0
    var trait06: Int = 0
    var trait07: Int = 0
    var trait08: Int = 0
    var trait09: Int = 0
    var trait10: Int = 0
    var trait11: Int = 0
    var trait12: Int = 0
    var trait13: Int = 0
    var trait14: Int = 0
    var trait15: Int = 0
    var trait16: Int = 0
    var trait17: Int = 0
    var trait18: Int = 0
    var trait19: Int = 0
    var trait20: Int = 0
    var trait11_unitid: Int = 0
    var trait12_unitid: Int = 0
    var trait13_unitid: Int = 0
    var trait14_unitid: Int = 0
    var trait15_unitid: Int = 0
    lateinit var ratingBar: RatingBar
    val tag_types_display_order: MutableList<String> = mutableListOf()
    val tag_types_id_order: MutableList<String> = mutableListOf()
    lateinit var alert_text: String
    var current_note: String? = null
    var current_tag_number: String? = null
    var this_animal_alert: String? = null

    var radioGroup: RadioGroup? = null
    private var recNo = 0
    private var nRecs = 0
    var numbertagrecords: Int = 0
    var numberrealdataitems: Int = 0
    var numberuserdataitems: Int = 0
    var numbercustomdataitems: Int = 0
    var numberanimals: Int = 0
    var currentanimalrecord: Int = 0

    val user_scores: MutableList<Int> = mutableListOf()
    val real_scores: MutableList<Float> = mutableListOf()
    val scored_evaluation_traits: MutableList<String> = mutableListOf()
    val data_evaluation_traits: List<String> = listOf()
    val trait_units: List<String> = listOf()
    val user_evaluation_traits: List<String> = listOf()
    val scored_trait_numbers: MutableList<Int> = mutableListOf()
    val data_trait_numbers: List<Int> = listOf()
    val user_trait_numbers: List<Int> = listOf()
    val user_trait_number_items: List<Int> = listOf()

    private lateinit var dataAdapter: ArrayAdapter<String>

    private var LabelText: String? = ""
    private val EID = ""
    private var TrichTag = "99999"
    private var TrichTagColor = "Blue"
    var ThirdTag: String = "99999"
    var ThirdTagColor: String = "Unknown"
    var ThirdTagType: String = "Farm"
    private val BAA = ""
    private var AnimalName = ""
    private var AutoPrint = false
    private var BirthSummary = false
    private val ReadyToPrint = false

    lateinit var myadapter: SimpleCursorAdapter
    lateinit var noteadapter: SimpleCursorAdapter
    private lateinit var dbh: DatabaseHandler

    private lateinit var cursor: Cursor
    private lateinit var cursor2: Cursor
    private lateinit var cursor3: Cursor
    private lateinit var cursor4: Cursor
    private lateinit var tagCursor: Cursor
    private lateinit var animalcursor: Cursor
    private lateinit var breedCursor: Cursor
    private lateinit var ownerCursor: Cursor
    private lateinit var labCursor: Cursor
    private lateinit var locationCursor: Cursor
    private lateinit var noteCursor: Cursor
    private lateinit var crsr: Any
    private lateinit var crsr2: Any
    private lateinit var crsr3: Any
    private lateinit var crsr4: Any
    private lateinit var tagcrsr: Any
    private lateinit var animalcrsr: Any
    private lateinit var breedcrsr: Any
    private lateinit var ownercrsr: Any
    private lateinit var labcrsr: Any
    private lateinit var locationcrsr: Any
    private lateinit var notecrsr: Any
    private var numnotes: Int = 0
    private var note_text: String? = null
    private var today: Date? = null

    private var mService: Messenger? = null
    private var mScaleService: Messenger? = null
    private var mBaaCodeService: Messenger? = null
    private var mIsBound: Boolean = false
    private var mIsScaleBound: Boolean = false
    private var mIsBAABound: Boolean = false

    private val mMessenger: Messenger = Messenger(IncomingHandler())

    // variable to hold the string
    private var LastEID: String? = null
    private var LastBAA: String? = null
    private val Weight: String? = null
    private var mytoday: String? = null
    private var mytime: String? = null

    private val bluetoothWatcher = BluetoothWatcher(this)

    private lateinit var binding: ActivitySimpleLambingBinding

    private lateinit var eidReaderStatePresenter: EIDReaderStatePresenter

    internal inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                EIDReaderService.MSG_UPDATE_STATUS -> {
                    val b1 = msg.data
                    val State = b1.getString("stat")
                    if (State != null) {
                        eidReaderStatePresenter.updateReaderState(State)
                    }
                }

                EIDReaderService.MSG_NEW_EID_FOUND -> {
                    val b2 = msg.data
                    LastEID = (b2.getString("info1"))
                    //				    We have a good whole EID number
                    onEIDScanned(null)
                }

                baacodeService.MSG_NEW_Baacode_FOUND -> {
                    val b4 = msg.data
                    LastBAA = (b4.getString("info1"))
                    Log.i("SimpleBirth", "Got BaaCode.")
                    //				    We have a good baacode
                    gotBAA(null)
                }

                EIDReaderService.MSG_UPDATE_LOG_APPEND -> {}
                baacodeService.MSG_UPDATE_LOG_APPEND -> {
                    Log.i("SimpleBirth", "Baacode data received")
                    val b5 = msg.data
                    Log.i("SimpleBirth", "BaaCode logappend.")
                }

                EIDReaderService.MSG_UPDATE_LOG_FULL -> {}
                EIDReaderService.MSG_THREAD_SUICIDE -> {
                    //				    Log.i("SimpleBirth", "Service informed Activity of Suicide.");
                    doUnbindService()
                    stopService(Intent(this@SimpleLambingActivity, EIDReaderService::class.java))
                }

                baacodeService.MSG_THREAD_SUICIDE -> {
                    //				    Log.i("SimpleBirth", "Service informed Activity of Suicide.");
                    doUnbindbaacodeService()
                    stopService(Intent(this@SimpleLambingActivity, baacodeService::class.java))
                }

                scaleService.MSG_THREAD_SUICIDE -> {
                    //				Log.i("SimpleBirth", "Service informed Activity of Suicide.");
                    doUnbindScaleService()
                    stopService(Intent(this@SimpleLambingActivity, scaleService::class.java))
                }

                else -> super.handleMessage(msg)
            }
        }
    }

    private fun LoadPreferences(NotifyOfChanges: Boolean) {
        //todo decide whether the deal with this deprecated structure
        val preferences = PreferenceManager.getDefaultSharedPreferences(
            baseContext
        )
        try {
            val TextForLabel = preferences.getString("label", "text")
            LabelText = TextForLabel
            AutoPrint = preferences.getBoolean("autop", false)
            BirthSummary = preferences.getBoolean("birth_summary", false)
        } catch (nfe: NumberFormatException) {
            //todo figure what to do if there is an exception throw an error to a log file?
        }
    }

    //    ***    Service connections & disconnections one for each eidService baacodeService and scaleService
    var mConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, binder: IBinder) {
            val service = Messenger(binder).also { mService = it }
            //			Log.i("SimpleBirth", "At Service.");
            try {
                //Register client with service
                val msg = Message.obtain(null, EIDReaderService.MSG_REGISTER_CLIENT)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }

        //  *** This is the eid service disconnection
        override fun onServiceDisconnected(className: ComponentName) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            Log.i("SimpleBirth", "At eid Service Disconnected.")
            mService = null
        }
    }

    //    ***    Now the scale service connection
    var mScaleConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, binder: IBinder) {
            val service = Messenger(binder).also { mScaleService = it }
            try {
                //Register client with service
                val msg = Message.obtain(null, scaleService.MSG_REGISTER_CLIENT)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }

        //    ***    Then the scale service disconnection
        override fun onServiceDisconnected(className: ComponentName) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            Log.i("SimpleBirth", "At scaleService Disconnected.")
            mScaleService = null
        }
    }

    //  baacode service connection & disconnection
    var mbaacodeConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, binder: IBinder) {
            val service = Messenger(binder).also { mBaaCodeService = it }
            Log.i("SimpleBirth", "At baacodeService.")
            try {
                //Register client with service
                val msg = Message.obtain(null, baacodeService.MSG_REGISTER_CLIENT)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                Log.i("SimpleBirth", "At baacodeService exception.")
                // In this case the service has crashed before we could even do anything with it
            }
            //Update status
            try {
                val msg = Message.obtain(null, baacodeService.MSG_UPDATE_STATUS, 0, 0)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                Log.i("SimpleBirth", "At baacodeService update status crash.")
                // In this case the service has crashed before we could even do anything with it
            }
            //Request full log from service.
            try {
                //Update status
                val msg = Message.obtain(null, baacodeService.MSG_UPDATE_LOG_FULL, 0, 0)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                Log.i("SimpleBirth", "At baacodeService update log full crash.")
                // In this case the service has crashed before we could even do anything with it
            }
        }

        //    ***    And last the baacode service disconnection
        override fun onServiceDisconnected(className: ComponentName) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            mBaaCodeService = null
        }
    }

    //  Really should be check if services are running...
    //  Start the service if it is not already running and bind to it
    //  Should be one each for eid, scale and baacode
    private fun CheckIfServiceIsRunning() {
        //If the service is running when the activity starts, we want to automatically bind to it.
        Log.i("SimpleBirth", "At eid isRunning?.")
        if (EIDReaderService.isRunning()) {
//			Log.i("SimpleBirth", " eidService is running");
            doBindService()
        } else {
//			Log.i("SimpleBirth", " eidService is not running, start it");
            startService(Intent(this@SimpleLambingActivity, EIDReaderService::class.java))
            doBindService()
        }
        if (scaleService.isScaleRunning()) {
//			Log.i("SimpleBirth", " check if scaleService is running");
            doBindScaleService()
        } else {
//			Log.i("SimpleBirth", " scaleService is not running, start it");
            startService(Intent(this@SimpleLambingActivity, scaleService::class.java))
            doBindScaleService()
        }
        if (baacodeService.isBaaRunning()) {
            //  Log.i("SimpleBirth", " check if baacodeService service is running");
            doBindbaacodeService()
        } else {
            Log.i("SimpleBirth", " baacodeService is not running, start it")
            startService(Intent(this@SimpleLambingActivity, baacodeService::class.java))
            Log.i("SimpleBirth", "At baacodeService service started")
            doBindbaacodeService()
            Log.i("SimpleBirth", "At baacodeService service bound")
        }
    }

    //  Bind services, should be one set of bind & unbind methods for eid, scale and baacode
    //  First is the eid service bind
    fun doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
//		Log.i("SimpleBirth", "At doBind1.");
        bindService(Intent(this, EIDReaderService::class.java), mConnection, BIND_AUTO_CREATE)
        //		Log.i("SimpleBirth", "At doBind2.");
        mIsBound = true
        mService?.let { service ->
//			Log.i("SimpleBirth", "At doBind3.");
            try {
                //Request status update
                var msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_STATUS, 0, 0)
                msg.replyTo = mMessenger
                service.send(msg)
                //				Log.i("SimpleBirth", "At doBind4.");
                //Request full log from service.
                msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_LOG_FULL, 0, 0)
                service.send(msg)
            } catch (e: RemoteException) {
                //NO-OP
            }
        }
        //		Log.i("SimpleBirth", "At doBind5.");
    }

    //     Next the unbind eid servides
    fun doUnbindService() {
//		Log.i("SimpleBirth", "At DoUnbindservice");
        mService?.let { service ->
            try {
                //Stop eidService from sending tags
                val msg = Message.obtain(null, EIDReaderService.MSG_NO_TAGS_PLEASE)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        if (mIsBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            mService?.let { service ->
                try {
                    val msg = Message.obtain(null, EIDReaderService.MSG_UNREGISTER_CLIENT)
                    msg.replyTo = mMessenger
                    service.send(msg)
                } catch (e: RemoteException) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection)
            mIsBound = false
        }
    }

    //      Now for the scale service bind
    fun doBindScaleService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
//		Log.i("SimpleBirth", "At doBind1.");
        bindService(Intent(this, scaleService::class.java), mScaleConnection, BIND_AUTO_CREATE)
        //		Log.i("SimpleBirth", "At doBind2.");
        mIsScaleBound = true
    }

    //      And unbind
    fun doUnbindScaleService() {
//		Log.i("SimpleBirth", "At DoUnbindscaleservice");
        mScaleService?.let { service ->
            try {
                //Stop eidService from sending tags
                val msg = Message.obtain(null, scaleService.MSG_NO_TAGS_PLEASE)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        if (mIsScaleBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            mScaleService?.let { service ->
                try {
                    val msg = Message.obtain(null, scaleService.MSG_UNREGISTER_CLIENT)
                    msg.replyTo = mMessenger
                    service.send(msg)
                } catch (e: RemoteException) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mScaleConnection)
            mIsScaleBound = false
        }
    }

    //  Finally the baacode bind
    fun doBindbaacodeService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        Log.i("SimpleBirth", "At baacode bind1.")
        bindService(Intent(this, baacodeService::class.java), mbaacodeConnection, BIND_AUTO_CREATE)
        mIsBAABound = true
        mBaaCodeService?.let { service ->
            try {
                //Request status update
                var msg = Message.obtain(null, baacodeService.MSG_UPDATE_STATUS, 0, 0)
                msg.replyTo = mMessenger
                service.send(msg)
                //			Log.i("SimpleBirth", "At baacode bind2.");
                //Request full log from service.
                msg = Message.obtain(null, baacodeService.MSG_UPDATE_LOG_FULL, 0, 0)
                service.send(msg)
            } catch (e: RemoteException) {
//			Log.i("SimpleBirth", "message exception bind");
            }
        }
    }

    /// baacode unbind
    fun doUnbindbaacodeService() {
        Log.i("SimpleBirth", "At baacodeUnbindservice")
        mBaaCodeService?.let { service ->
            try {
                //Stop baacodeService from sending tags
                val msg = Message.obtain(null, baacodeService.MSG_NO_TAGS_PLEASE)
                msg.replyTo = mMessenger
                Log.i("SimpleBirth", "At baacode unbind.")
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        if (mIsBAABound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            mBaaCodeService?.let { service ->
                try {
                    val msg = Message.obtain(null, baacodeService.MSG_UNREGISTER_CLIENT)
                    msg.replyTo = mMessenger
                    service.send(msg)
                } catch (e: RemoteException) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mbaacodeConnection)
            mIsBAABound = false
        }
    }

    // use EID reader to look up an animal
    fun onEIDScanned(v: View?) {
        Log.i("in gotEID ", "with LastEID of $LastEID")
        lookForAnimal(null)
    }

    // use BaaCode reader
    fun gotBAA(v: View?) {
        Log.i("in gotBAA ", "with LastBAA of $LastBAA")
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySimpleLambingBinding.inflate(
            layoutInflater
        )
        setContentView(binding.root)
        val dbname = getString(R.string.real_database_file)
        Log.i("SimpleBirth", "dbname is $dbname")
        var TV: TextView
        thisanimal_id = 0
        dbh = DatabaseHandler(this, dbname)
        //		CheckIfServiceIsRunning();
        LoadPreferences(true)
        Log.i("SimpleBirth", "back from isRunning")

        // Fill the trait name variables from the saved Generic Simple Lambing evaluation
        var cmd =
            "Select * from saved_evaluations_table where evaluation_name LIKE 'Simple Lambing%' AND " +
                    " saved_evaluation_id_companyid = 700 "
        //        cmd = "select * from last_evaluation_table";
        crsr = dbh.exec(cmd)
        cursor = crsr as Cursor
        nRecs = cursor.count
        dbh.moveToFirstRecord()
        cursor.moveToFirst()
        //        evaluation_name = dbh.getString(cursor.getColumnIndex("evaluation_name"));
        // We don't care about whose evaluation this is so we ignore the contact and company
//        id_contactid = dbh.getInt(cursor.getColumnIndex("saved_evaluation_id_contactid"));
//        id_companyid = dbh.getInt(cursor.getColumnIndex("saved_evaluation_id_companyid"));
        //trait01 = dbh.getInt(cursor.getColumnIndex("trait_name01"));
        trait01 = dbh.getInt(4)
        scored_trait_numbers.add(trait01)
        trait02 = dbh.getInt(cursor.getColumnIndex("trait_name02"))
        scored_trait_numbers.add(trait02)
        trait03 = dbh.getInt(cursor.getColumnIndex("trait_name03"))
        scored_trait_numbers.add(trait03)
        trait04 = dbh.getInt(cursor.getColumnIndex("trait_name04"))
        scored_trait_numbers.add(trait04)
        trait05 = dbh.getInt(cursor.getColumnIndex("trait_name05"))
        scored_trait_numbers.add(trait05)
        trait06 = dbh.getInt(cursor.getColumnIndex("trait_name06"))
        scored_trait_numbers.add(trait06)
        trait07 = dbh.getInt(cursor.getColumnIndex("trait_name07"))
        scored_trait_numbers.add(trait07)
        trait08 = dbh.getInt(cursor.getColumnIndex("trait_name08"))
        scored_trait_numbers.add(trait08)
        trait09 = dbh.getInt(cursor.getColumnIndex("trait_name09"))
        scored_trait_numbers.add(trait09)
        trait10 = dbh.getInt(cursor.getColumnIndex("trait_name10"))
        scored_trait_numbers.add(trait10)
        trait11 = dbh.getInt(cursor.getColumnIndex("trait_name11"))
        trait11_unitid = dbh.getInt(cursor.getColumnIndex("trait_units11"))
        trait12 = dbh.getInt(cursor.getColumnIndex("trait_name12"))
        trait12_unitid = dbh.getInt(cursor.getColumnIndex("trait_units12"))
        trait13 = dbh.getInt(cursor.getColumnIndex("trait_name13"))
        trait13_unitid = dbh.getInt(cursor.getColumnIndex("trait_units13"))
        trait14 = dbh.getInt(cursor.getColumnIndex("trait_name14"))
        trait14_unitid = dbh.getInt(cursor.getColumnIndex("trait_units14"))
        trait15 = dbh.getInt(cursor.getColumnIndex("trait_name15"))
        trait15_unitid = dbh.getInt(cursor.getColumnIndex("trait_units15"))
        trait16 = dbh.getInt(cursor.getColumnIndex("trait_name16"))
        trait17 = dbh.getInt(cursor.getColumnIndex("trait_name17"))
        trait18 = dbh.getInt(cursor.getColumnIndex("trait_name18"))
        trait19 = dbh.getInt(cursor.getColumnIndex("trait_name19"))
        trait20 = dbh.getInt(cursor.getColumnIndex("trait_name20"))
        
        //	Fill the trait name variables from the last evaluation
//        trait01 = dbh.getInt(1);
//        cursor.moveToNext();
//        trait02 = dbh.getInt(1);
//        cursor.moveToNext();
//        trait03 = dbh.getInt(1);
//        cursor.moveToNext();
//        trait04 = dbh.getInt(1);
//        cursor.moveToNext();
//        trait05 = dbh.getInt(1);
//        cursor.moveToNext();
//        trait06 = dbh.getInt(1);
//        cursor.moveToNext();
//        trait07 = dbh.getInt(1);
//        cursor.moveToNext();
//        trait08 = dbh.getInt(1);
//        cursor.moveToNext();
//        trait09 = dbh.getInt(1);
//        cursor.moveToNext();
//        trait10 = dbh.getInt(1);
//        cursor.moveToNext();
//        trait11 = dbh.getInt(1);
//        trait11_unitid = dbh.getInt(2);
//        cursor.moveToNext();
//        trait12 = dbh.getInt(1);
//        trait12_unitid = dbh.getInt(2);
//        cursor.moveToNext();
//        trait13 = dbh.getInt(1);
//        trait13_unitid = dbh.getInt(2);
//        cursor.moveToNext();
//        trait14 = dbh.getInt(1);
//        trait14_unitid = dbh.getInt(2);
//        cursor.moveToNext();
//        trait15 = dbh.getInt(1);
//        trait15_unitid = dbh.getInt(2);
//        cursor.moveToNext();
//        trait16 = dbh.getInt(1);
//        cursor.moveToNext();
//        trait17 = dbh.getInt(1);
//        cursor.moveToNext();
//        trait18 = dbh.getInt(1);
//        cursor.moveToNext();
//        trait19 = dbh.getInt(1);
//        cursor.moveToNext();
//        trait20 = dbh.getInt(1);


//todo Problem with this code is that it requires that you know exactly how many traits in the
// selected evaluation have data in them. It would probably be better to add logic to test if a
// trait is empty or 0 and if so skip it and move on.
        cmd = String.format(
            "SELECT trait_name FROM evaluation_trait_table WHERE id_evaluationtraitid = '%s' ",
            trait01
        )
        crsr = dbh.exec(cmd)
        cursor = crsr as Cursor
        dbh.moveToFirstRecord()
        cursor.moveToFirst()
        scored_evaluation_traits.add(cursor.getString(0))
        Log.i("evaluate2", " trait name is " + cursor.getString(0))

        cmd = String.format(
            "SELECT trait_name FROM evaluation_trait_table WHERE id_evaluationtraitid = '%s' ",
            trait02
        )
        crsr = dbh.exec(cmd)
        cursor = crsr as Cursor
        dbh.moveToFirstRecord()
        cursor.moveToFirst()
        scored_evaluation_traits.add(cursor.getString(0))
        Log.i("evaluate2", " trait name is " + cursor.getString(0))

        cmd = String.format(
            "SELECT trait_name FROM evaluation_trait_table WHERE id_evaluationtraitid = '%s' ",
            trait03
        )
        crsr = dbh.exec(cmd)
        cursor = crsr as Cursor
        dbh.moveToFirstRecord()
        cursor.moveToFirst()
        scored_evaluation_traits.add(cursor.getString(0))
        Log.i("evaluate2", " trait name is " + cursor.getString(0))

        cmd = String.format(
            "SELECT trait_name FROM evaluation_trait_table WHERE id_evaluationtraitid = '%s' ",
            trait04
        )
        crsr = dbh.exec(cmd)
        cursor = crsr as Cursor
        dbh.moveToFirstRecord()
        cursor.moveToFirst()
        scored_evaluation_traits.add(cursor.getString(0))
        Log.i("evaluate2", " trait name is " + cursor.getString(0))

        cmd = String.format(
            "SELECT trait_name FROM evaluation_trait_table WHERE id_evaluationtraitid = '%s' ",
            trait05
        )
        crsr = dbh.exec(cmd)
        cursor = crsr as Cursor
        dbh.moveToFirstRecord()
        cursor.moveToFirst()
        scored_evaluation_traits.add(cursor.getString(0))
        Log.i("evaluate2", " trait name is " + cursor.getString(0))

        cmd = String.format(
            "SELECT trait_name FROM evaluation_trait_table WHERE id_evaluationtraitid = '%s' ",
            trait06
        )
        crsr = dbh.exec(cmd)
        cursor = crsr as Cursor
        dbh.moveToFirstRecord()
        cursor.moveToFirst()
        scored_evaluation_traits.add(cursor.getString(0))
        Log.i("evaluate2", " trait name is " + cursor.getString(0))

        cmd = String.format(
            "SELECT trait_name FROM evaluation_trait_table WHERE id_evaluationtraitid = '%s' ",
            trait07
        )
        crsr = dbh.exec(cmd)
        cursor = crsr as Cursor
        dbh.moveToFirstRecord()
        cursor.moveToFirst()
        scored_evaluation_traits.add(cursor.getString(0))
        Log.i("evaluate2", " trait name is " + cursor.getString(0))

        //        cmd = String.format("SELECT trait_name FROM evaluation_trait_table WHERE id_evaluationtraitid = '%s' ", trait08);
//        crsr = dbh.exec(cmd);
//        cursor = (Cursor) crsr;
//        dbh.moveToFirstRecord();
//        cursor.moveToFirst();
//        scored_evaluation_traits.add(cursor.getString(0));
//        Log.i("evaluate2", " trait name is " + cursor.getString(0));
//
//        cmd = String.format("SELECT trait_name FROM evaluation_trait_table WHERE id_evaluationtraitid = '%s' ", trait09);
//        crsr = dbh.exec(cmd);
//        cursor = (Cursor) crsr;
//        dbh.moveToFirstRecord();
//        cursor.moveToFirst();
//        scored_evaluation_traits.add(cursor.getString(0));
//        Log.i("evaluate2", " trait name is " + cursor.getString(0));
//
//        cmd = String.format("SELECT trait_name FROM evaluation_trait_table WHERE id_evaluationtraitid = '%s' ", trait10);
//        crsr = dbh.exec(cmd);
//        cursor = (Cursor) crsr;
//        dbh.moveToFirstRecord();
//        cursor.moveToFirst();
//        scored_evaluation_traits.add(cursor.getString(0));
//        Log.i("evaluate2", " trait name is " + cursor.getString(0));
        //	Set up the scored traits and inflate the layout
        //        cmd = String.format("select evaluation_trait_table.trait_name, evaluation_trait_table.id_evaluationtraitid " +
//                "from evaluation_trait_table inner join last_evaluation_table where " +
//                " evaluation_trait_table.id_evaluationtraitid = last_evaluation_table.id_evaluationtraitid and evaluation_trait_table.id_evaluationtraittypeid = 1 ");
////    	Log.i("evaluate2", " cmd is " + cmd);
//        crsr = dbh.exec(cmd);
//        cursor = (Cursor) crsr;
//        nRecs = cursor.getCount();
//        Log.i("SimpleBirths", " nRecs is " + String.valueOf(nRecs));
//        dbh.moveToFirstRecord();
//        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
//            scored_trait_numbers.add(cursor.getInt(1));
//       	Log.i("evaluate2", " trait number is " + String.valueOf(cursor.getInt(1)));
//            scored_evaluation_traits.add(cursor.getString(0));
//    	Log.i("evaluate2", " trait name is " + cursor.getString(0));
//        }

//        Log.i("SimpleBirths", "number of records in scored traits cursor is " + String.valueOf(nRecs));
        val inflater = layoutInflater
        //    	Log.i ("evaluate2", scored_evaluation_traits.get(0));
        //todo hard coded for 7 evals for simple lambing needs to be done properly
        for (ii in 0..6) {
            Log.i("in for loop", " trait name is " + scored_evaluation_traits.get(ii))
            val table = binding.TableLayout01
            Log.i("in for loop", " after TableLayout")
            val row = inflater.inflate(R.layout.eval_item_entry, table, false) as TableRow
            tempLabel = scored_evaluation_traits.get(ii)
            Log.i("in for loop", " tempLabel is $tempLabel")
            (row.findViewById<View>(R.id.rb1_lbl) as TextView).text = tempLabel
            Log.i("in for loop", " after set text view")
            table.addView(row)
        }

        mytoday = Utilities.TodayIs()

        // make the alert button normal and disabled
        var btn = binding.buttonPanelTop.showAlertButton
        btn.setOnClickListener { v: View? -> this.showAlert(v) }
        btn.isEnabled = false

        btn = binding.buttonPanelTop.lookupAnimalButton
        btn.setOnClickListener { v: View -> this.onLookupAnimalClicked(v) }

        btn = binding.buttonPanelTop.mainActionButton
        btn.setOnClickListener { view: View? -> updateDatabase() }
        btn.isEnabled = false
        btn = binding.buttonPanelTop.takeNoteButton
        btn.setOnClickListener { v: View -> this.doNote(v) }
        btn.isEnabled = false
        btn = binding.buttonPanelTop.clearDataButton
        btn.setOnClickListener { v: View? -> this.clearBtn(v) }
        btn.isEnabled = true

        //	make the scan eid button red
        btn = binding.buttonPanelTop.scanEIDButton
        btn.setOnClickListener { v: View -> this.scanEid(v) }

        binding.buttonPanelTop.show(
            TopButtonBar.UI_ALL or
                    TopButtonBar.UI_ACTION_UPDATE_DATABASE
        )

        listView = binding.list

        eidReaderStatePresenter = EIDReaderStatePresenter(
            this,
            binding.buttonPanelTop.imageScannerStatus
        )

        bluetoothWatcher.onActivationChanged = { enabled: Boolean? ->
            onBluetoothActivationChanged()
            Unit
        }
    }

    //  user clicked 'Scan' button
    private fun scanEid(v: View) {
        // Here is where I need to get a tag scanned and put the data into the variable LastEID
        clearBtn(v)
        mService?.let { service ->
            try {
                //Start eidService sending tags
                val msg = Message.obtain(null, EIDReaderService.MSG_SEND_ME_TAGS)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE_SELECT_ANIMAL) {
            if (resultCode == RESULT_OK && data != null && data.hasExtra(SelectAnimal.EXTRA_SELECTED_ANIMAL_ID)) {
                val selectedAnimalId = data.getIntExtra(SelectAnimal.EXTRA_SELECTED_ANIMAL_ID, -1)
                if (selectedAnimalId != -1) {
                    thisanimal_id = selectedAnimalId
                    formatAnimalRecord(null)
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun onLookupAnimalClicked(v: View) {
        startActivityForResult(
            newIntent(this, SexStandard.FEMALE),
            REQUEST_CODE_SELECT_ANIMAL
        )
    }

    fun lookForAnimal(v: View?) {
        var tag_num = LastEID
        current_tag_number = tag_num
        Log.i("LookForAnimal", " got to LookForAnimal with Tag Number of $tag_num")
        current_tag_type_id = IdType.ID_TYPE_ID_EID
        cmd = String.format(
            "select id_animalid from animal_id_info_table where id_number='%s' " +
                    "and animal_id_info_table.id_idtypeid='%s' ",
            tag_num, IdType.ID_TYPE_ID_EID
        )
        Log.i("searchByNumber", "command is $cmd")
        animalcrsr = dbh.exec(cmd)
        animalcursor = animalcrsr as Cursor
        recNo = 1
        numberanimals = animalcursor.count
        Log.i("searchByNumber", " nRecs = $numberanimals")
        dbh.moveToFirstRecord()
        Log.i("searchByNumber", " the cursor is of size " + dbh.size.toString())
        if (dbh.size == 0) {
            val builder = AlertDialog.Builder(this)
            builder.setMessage(R.string.add_bull)
            builder.setTitle(R.string.add_bull)
            builder.setPositiveButton(R.string.yes_label) { dialog, idx -> // User clicked yes button
                addAnimal(v)
            }
            builder.setNegativeButton(R.string.no_label) { dialog, idx -> // User clicked no button
                dialog.dismiss()
                clearBtn(v)
            }
            val dialog = builder.create()
            dialog.show()
            return
        }
        thisanimal_id = dbh.getInt(0)
        Log.i("searchByNumber", "This animal is record $thisanimal_id")
        Log.i("searchByNumber", " Before formatting the record")
        //	We need to call the format the record method
        formatAnimalRecord(v)

        //  Set update database button green
        btn = binding.buttonPanelTop.mainActionButton
        btn.isEnabled = true
        btn = binding.buttonPanelTop.takeNoteButton
        btn.isEnabled = true
    }

    fun formatAnimalRecord(v: View?) {
        var table: TableLayout
        var TV: TextView
        val notelist = binding.notelist
        Log.i("formatAnimalRecord", "This animal is record $thisanimal_id")
        Log.i("formatAnimalRecord", " the animal cursor is of size $numberanimals")
        Log.i("formatAnimalRecord", " record number is $recNo")
        //  Set the Update Tags button Green
        cmd = String.format(
            "select animal_table.animal_name, animal_id_info_table.id_number, id_location_table.id_location_abbrev," +
                    " id_color_table.id_color_name, id_type_table.id_type_name,  " +
                    "animal_id_info_table.id_animalidinfoid as _id, animal_id_info_table.id_date_off  " +
                    "from animal_table inner join animal_id_info_table on animal_table.id_animalid = animal_id_info_table.id_animalid " +
                    "left outer join id_color_table on animal_id_info_table.id_male_id_idcolorid = id_color_table.id_idcolorid " +
                    "left outer join id_location_table on animal_id_info_table.id_idlocationid = id_location_table.id_idlocationid " +
                    "inner join id_type_table on animal_id_info_table.id_idtypeid = id_type_table.id_idtypeid " +
                    "where animal_id_info_table.id_animalid ='%s' and (animal_id_info_table.id_date_off is null or " +
                    "animal_id_info_table.id_date_off is '')order by id_type_name asc",
            thisanimal_id
        )
        Log.i("formatAnimalRecord", " Before running get ids command")
        tagcrsr = dbh.exec(cmd)
        tagCursor = tagcrsr as Cursor
        tagCursor.moveToFirst()
        Log.i("formatAnimalRecord", " the id cursor is of size " + dbh.size.toString())
        Log.i("formatAnimalRecord", " before formatting results")
        //        TV = (TextView) findViewById(R.id.inputText);
//        TV.setText(dbh.getStr(0));
        AnimalName = dbh.getStr(0)
        //	Get set up to use the CursorAdapter to display all the tag data
        //	Select only the columns I need for the tag display section
        val fromColumns =
            arrayOf("id_number", "id_color_name", "id_location_abbrev", "id_type_name")
        Log.i("FormatRecord", "after setting string array fromColumns")
        //	Set the views for each column for each line. A tag takes up 1 line on the screen
        val toViews = intArrayOf(
            R.id.tag_number,
            R.id.tag_color_name,
            R.id.id_location_abbrev,
            R.id.idtype_name
        )
        //        Log.i("FormatRecord", "after setting string array toViews");
        myadapter = SimpleCursorAdapter(this, R.layout.tag_list, tagCursor, fromColumns, toViews, 0)
        //        Log.i("FormatRecord", "after setting myadapter");
        listView.adapter = myadapter
        //        Log.i("FormatRecord", "after setting list adapter");
        try {
//            Log.i("try block", " Before finding an electronic tag if it exists");
            cmd = String.format(
                "select animal_table.id_animalid, id_type_table.id_type_name, " +
                        "animal_id_info_table.id_number " +
                        "from animal_table inner join animal_id_info_table on animal_table.id_animalid = animal_id_info_table.id_animalid " +
                        "inner join id_type_table on animal_id_info_table.id_idtypeid = id_type_table.id_idtypeid " +
                        "where animal_id_info_table.id_animalid ='%s' and (animal_id_info_table.id_date_off is null or " +
                        " animal_id_info_table.id_date_off is '') and " +
                        "animal_id_info_table.id_idtypeid = 2 " +
                        "order by id_type_name asc", thisanimal_id
            )
            crsr4 = dbh.exec(cmd)
            cursor4 = crsr4 as Cursor
            cursor4.moveToFirst()
            LastEID = dbh.getStr(2)
            Log.i("LastEID is ", dbh.getStr(2))
        } catch (r: Exception) {
            LastEID = "000_000000000000"
            Log.v("fill LAST EID ", " in  SimpleBirth test RunTimeException with all zeros$r")
        }

        try {
            // todo This needs to be fixed to be a second tag type not always a trich tag for species independence
            Log.i("FormatRecord", "try block Before finding a trich tag if it exists")
            cmd = String.format(
                "select animal_table.id_animalid, id_type_table.id_type_name " +
                        " , animal_id_info_table.id_number , id_color_table.id_color_name " +
                        "from animal_table " +
                        "inner join animal_id_info_table on animal_table.id_animalid = animal_id_info_table.id_animalid " +
                        "inner join id_type_table on animal_id_info_table.id_idtypeid = id_type_table.id_idtypeid " +
                        "inner join id_color_table on animal_id_info_table.id_male_id_idcolorid = id_color_table.id_idcolorid " +
                        "where animal_id_info_table.id_animalid ='%s' and (animal_id_info_table.id_date_off is null or " +
                        " animal_id_info_table.id_date_off is '') and " +
                        "animal_id_info_table.id_idtypeid = 10 " +
                        "order by id_type_name asc", thisanimal_id
            )
            crsr4 = dbh.exec(cmd)
            cursor4 = crsr4 as Cursor
            cursor4.moveToFirst()
            TrichTag = dbh.getStr(2)
            TrichTagColor = dbh.getStr(3)
            Log.i("TrichTag is ", dbh.getStr(2))
        } catch (r: Exception) {
            TrichTag = "99999"
            Log.v("fill trich ", " in  SimpleBirth test RunTimeException with nines$r")
        }
        try {
            //todo again needs some thought for how to handle not trich testing for multi species
            // Fill third number with a farm tag for printing if no trich tag available
            Log.i("FormatRecord", "try block Before finding a farm tag if it exists")
            cmd = String.format(
                "select animal_table.id_animalid, id_type_table.id_type_name " +
                        " , animal_id_info_table.id_number , id_color_table.id_color_name " +
                        "from animal_table " +
                        "inner join animal_id_info_table on animal_table.id_animalid = animal_id_info_table.id_animalid " +
                        "inner join id_type_table on animal_id_info_table.id_idtypeid = id_type_table.id_idtypeid " +
                        "inner join id_color_table on animal_id_info_table.id_male_id_idcolorid = id_color_table.id_idcolorid " +
                        "where animal_id_info_table.id_animalid ='%s' and (animal_id_info_table.id_date_off is null or " +
                        " animal_id_info_table.id_date_off is '') and " +
                        "animal_id_info_table.id_idtypeid = 4 " +
                        "order by id_type_name asc", thisanimal_id
            )
            crsr4 = dbh.exec(cmd)
            cursor4 = crsr4 as Cursor
            cursor4.moveToFirst()
            ThirdTag = dbh.getStr(2)
            ThirdTagColor = dbh.getStr(3)
            ThirdTagType = "Farm"
            Log.i("FarmTag is ", dbh.getStr(2))
        } catch (r: Exception) {
            ThirdTag = "99999"
            Log.v("fill third tag ", " in simplebirth test RunTimeException with nines$r")
        }


        // todo Trying to figure out how to set the display to show the lambs born from here if the ewe has been scanned but that not working.
// Find an evaluation if it exists
        //todo hard coded for first of April but need to figure out how to get the data and find the most recent eval for the ewe
//        scored_evaluation_traits = new ArrayList<String>();
//        cmd = String.format("SELECT * from animal_evaluation_table where id_animalid = %s AND eval_date > '%s'",thisanimal_id, "2023-04-01" );
//        crsr2 = dbh.exec( cmd );
//        cursor2   = ( Cursor ) crsr2;
//        dbh.moveToFirstRecord();

        // Now I want to get the data for the rating bars from the eval and set the display of those bars to that value

//        List<Integer> rating_scores;
//        rating_scores(0) = dbh.getInt(3);
//        trait02_data = dbh.getInt(5);
//        trait03_data = dbh.getInt(7);
//        trait04_data = dbh.getInt(9);
//        trait05_data = dbh.getInt(11);
//        trait06_data = dbh.getInt(12);
//        trait07_data = dbh.getInt(13);
//        trait08_data = dbh.getInt(7);
//        trait09_data = dbh.getInt(8);
//        trait10_data = dbh.getInt(9);

//        table = (TableLayout) findViewById(R.id.TableLayout01);
//        Log.i("in format record", " number rating bars is " + String.valueOf(nRecs));
//        if (nRecs != 0) {
//            for (int ii = 0; ii < nRecs; ii++) {
//                TableRow row1 = (TableRow) table.getChildAt(ii);
//                ratingBar = (RatingBar) row1.getChildAt(1);
//                ratingBar.setRating();
//            }

        //	Now go get all the notes for this animal and format them
        cmd = String.format(
            "select animal_note_table.id_animalnoteid as _id, animal_note_table.note_date, " +
                    "animal_note_table.note_text, predefined_notes_table.predefined_note_text " +
                    " from animal_note_table left join predefined_notes_table " +
                    "on predefined_notes_table.id_predefinednotesid = animal_note_table.id_predefinednotesid" +
                    " where id_animalid='%s' " +
                    "order by note_date desc ", thisanimal_id
        )
        Log.i("format record", " command is  $cmd")
        notecrsr = dbh.exec(cmd)
        noteCursor = notecrsr as Cursor
        numnotes = noteCursor.count
        Log.i("formatrecord", " nRecs4 is $numnotes")
        noteCursor.moveToFirst()
        if (numnotes > 0) {
            // format the note records
            val fromColumns3 = arrayOf("note_date", "note_text", "predefined_note_text")
            //	Set the views for each column for each line. A note takes up 1 line on the screen
            val toViews2 = intArrayOf(R.id.note_date, R.id.note_text, R.id.predefined_note_text)
            Log.i("simpleBirth", "after setting string array toViews for notes")
            noteadapter = SimpleCursorAdapter(
                this,
                R.layout.note_entry,
                noteCursor,
                fromColumns3,
                toViews2,
                0
            )
            Log.i("simpleBirth", "after setting myadapter to show notes")
            notelist.adapter = noteadapter
        } else {
            // No note data - publish an empty list to clear notes
            noteadapter = SimpleCursorAdapter(this, R.layout.note_entry, null, null, null, 0)
            notelist.adapter = noteadapter
        }

        Log.i("FormatRecord ", " this animal is id number $thisanimal_id")
        cmd = String.format(
            """
    SELECT animal_table.id_animalid, birth_date, animal_breed_table.id_breedid, alert FROM animal_table 
    LEFT JOIN animal_breed_table ON animal_breed_table.id_animalid = animal_table.id_animalid WHERE animal_table.id_animalid ='%s' 
    """.trimIndent(), thisanimal_id
        )
        Log.i("formatrecord", "command is $cmd")
        crsr2 = dbh.exec(cmd)
        cursor2 = crsr2 as Cursor
        cursor2.moveToFirst()
        alert_text = dbh.getStr(3)
        this_animal_alert = alert_text
        Log.i("formatrecord", "alert text  is $alert_text")

        //        The current breed gets set to zero somehow and is not keeping its status with the 63 from when we added the animal.

//        Log.i("in formatAnimalRecord ", "Alert Text is " + alert_text);
//    	Now to test of the animal has an alert and if so then display the alert & set the alerts button to red
        if (alert_text != null && !alert_text.isEmpty() && !alert_text.trim { it <= ' ' }
                .isEmpty()) {
            // make the alert button red
            val btn = binding.buttonPanelTop.showAlertButton
            btn.isEnabled = true
            showAlert(v)
        }
    }

    private fun updateDatabase() {
        var TV: TextView
        var tempData: Float
        var tempRadioBtn: Int
        var tempTrait: Int
        val rating_scores: MutableList<Int> = ArrayList()
        //   rating_scores = new ArrayList<Float>();
        val blank_space = ""

        mytoday = Utilities.TodayIs()
        mytime = Utilities.TimeIs()
        val calendar = Calendar.getInstance()
        // Disable Update Database button and make it red to prevent getting 2 records at one time
        val btn = binding.buttonPanelTop.mainActionButton
        btn.isEnabled = false


        Log.i("in updateDatabase", " this animal id is $thisanimal_id")

        // 	get the rating bar scores and fill the rating_scores array
        val table = binding.TableLayout01
        Log.i("in save scores", " number rating bars is $nRecs")
        if (nRecs != 0) {
            for (ii in 0 until nRecs) {
                Log.i("in save scores", " in 1st for loop ii is$ii")
                val row1 = table.getChildAt(ii) as TableRow
                ratingBar = row1.getChildAt(1) as RatingBar
                rating_scores.add(ratingBar.rating.toInt())
                //  rating_scores.add(ratingBar.getRating());
                Log.i("RatingBar01 ", ratingBar.rating.toString())
            }
            //	Fill the rest of the array with zeros
            for (ii in nRecs..9) {
                // rating_scores.add((float) 0.0f);
                rating_scores.add(0)
                Log.i("in save scores ", "Filling remainder of rating bar array with zeros")
            }
        } else {
            //	Nothing to add so fill the entire thing with zeros
            for (ii in 0..9) {
                //               rating_scores.add((float) 0.0f);
                rating_scores.add(0)
                Log.i("in save scores ", "Filling entire rating bar array with zeros")
            }
        }

        // Fill the rating bar score variables from the rating_scores array

        // todo Will need mods for multiple species that use other breeding soundness parameters
        trait01_data = rating_scores[0]
        //    		Log.i("trait01_ratingbar ", String.valueOf(trait01_data));
        trait02_data = rating_scores[1]
        //    		Log.i("trait02_ratingbar ", String.valueOf(trait02_data));
        trait03_data = rating_scores[2]
        //    		Log.i("trait03_ratingbar ", String.valueOf(trait03_data));
        trait04_data = rating_scores[3]
        //    		Log.i("trait04_ratingbar ", String.valueOf(trait04_data));
        trait05_data = rating_scores[4]
        //    		Log.i("trait05_ratingbar ", String.valueOf(trait05_data));
        trait06_data = rating_scores[5]
        //    		Log.i("trait06_ratingbar ", String.valueOf(trait06_data));
        trait07_data = rating_scores[6]
        //    		Log.i("trait07_ratingbar ", String.valueOf(trait07_data));
        trait08_data = rating_scores[7]
        //    		Log.i("trait08_ratingbar ", String.valueOf(trait08_data));
        trait09_data = rating_scores[8]
        //    		Log.i("trait09_ratingbar ", String.valueOf(trait09_data));
        trait10_data = rating_scores[9]

        //    		Log.i("trait10_ratingbar ", String.valueOf(trait10_data));

// no real scores so fill with zeros
        for (ii in 0..4) {
            Log.i("in save scores ", "Filling entire real data array with zeros")
            real_scores.add(0.0f)
        }
        //	Fill the real score variables from the real_scores array
        trait11_data = real_scores.get(0)
        //        Log.i("trait11_data ", String.valueOf(trait11_data));
        trait12_data = real_scores.get(1)
        //    		Log.i("trait12_data ", String.valueOf(trait12_data));
        trait13_data = real_scores.get(2)
        //       	Log.i("trait13_data ", String.valueOf(trait13_data));
        trait14_data = real_scores.get(3)
        //       	Log.i("trait14_data ", String.valueOf(trait14_data));
        trait15_data = real_scores.get(4)

        //    		Log.i("trait15_data ", String.valueOf(trait15_data));

        // no user scores so fill with zeros
        for (ii in 0..4) {
//    				Log.i("in save scores ", "no user scores so make all 0");
            user_scores.add(0)
        }

        //Fill the user score variables from the user_scores array
        trait16_data = user_scores.get(0)
        Log.i("trait16_data ", trait16_data.toString())
        trait17_data = user_scores.get(1)
        Log.i("trait17_data ", trait17_data.toString())
        trait18_data = user_scores.get(2)
        Log.i("trait18_data ", trait18_data.toString())
        trait19_data = user_scores.get(3)
        Log.i("trait19_data ", trait19_data.toString())
        trait20_data = user_scores.get(4)
        Log.i("trait20_data ", trait20_data.toString())

        // Calculate the age in days for this animal for this evaluation to fill the age_in_days field
        var cmd = String.format(
            "SELECT julianday(birth_date) FROM animal_table WHERE id_animalid = '%s'",
            thisanimal_id
        )
        Log.i("get birthdate eval ", cmd)
        dbh.exec(cmd)
        crsr3 = dbh.exec(cmd)
        cursor3 = crsr3 as Cursor
        dbh.moveToFirstRecord()
        val temp_integer = Utilities.GetJulianDate().toInt() - (dbh.getInt(0))
        Log.i("get age in days ", temp_integer.toString())
        val create_modified = "$mytoday $mytime"
        cmd = String.format(
            "INSERT INTO animal_evaluation_table (id_animalid, " +
                    "trait_name01, trait_score01, trait_name02, trait_score02, trait_name03, trait_score03, " +
                    "trait_name04, trait_score04, trait_name05, trait_score05, trait_name06, trait_score06," +
                    "trait_name07, trait_score07, trait_name08, trait_score08, trait_name09, trait_score09, " +
                    "trait_name10, trait_score10, trait_name11, trait_score11, trait_name12, trait_score12, " +
                    "trait_name13, trait_score13, trait_name14, trait_score14, trait_name15, trait_score15, " +
                    "trait_name16, trait_score16, trait_name17, trait_score17, trait_name18, trait_score18, " +
                    "trait_name19, trait_score19, trait_name20, trait_score20, animal_rank, number_animals_ranked, " +
                    "trait_units11, trait_units12, trait_units13, trait_units14, trait_units15, eval_date, eval_time, age_in_days, created, modified) " +
                    "values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s," +
                    "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,'%s','%s', %s,%s,%s,%s,%s,'%s','%s',%s,'%s', '%s') ",
            thisanimal_id,
            trait01,
            trait01_data,
            trait02,
            trait02_data,
            trait03,
            trait03_data,
            trait04,
            trait04_data,
            trait05,
            trait05_data,
            trait06,
            trait06_data,
            trait07,
            trait07_data,
            trait08,
            trait08_data,
            trait09,
            trait09_data,
            trait10,
            trait10_data,
            trait11,
            trait11_data,
            trait12,
            trait12_data,
            trait13,
            trait13_data,
            trait14,
            trait14_data,
            trait15,
            trait15_data,
            trait16,
            trait16_data,
            trait17,
            trait17_data,
            trait18,
            trait18_data,
            trait19,
            trait19_data,
            trait20,
            trait20_data,
            blank_space,
            blank_space,
            trait11_unitid,
            trait12_unitid,
            trait13_unitid,
            trait14_unitid,
            trait15_unitid,
            mytoday,
            mytime,
            temp_integer,
            create_modified,
            create_modified
        )
        Log.i("save eval ", cmd)
        dbh.exec(cmd)
        //todo fix this to read the trait data more accurately from the actual trait evaluation table
        alert_text = """$mytoday$mytime
Lambed Already
$trait01_data Wether Lambs 
$trait02_data Ewe Lambs 
$trait03_data Ram Lambs 
$trait04_data Unknown Sex Lambs 
$trait05_data Stillborn Lambs 
$trait06_data Aborted Lambs 
$trait07_data Adopted Lambs 
$alert_text"""
        cmd = String.format(
            "update animal_table set alert='%s' where id_animalid=%d",
            alert_text,
            thisanimal_id
        )
        dbh.exec(cmd)
        cmd = String.format(
            "update animal_table set modified ='%s' where id_animalid=%d",
            create_modified,
            thisanimal_id
        )
        dbh.exec(cmd)
        note_text = """Lambed Already
$trait01_data Wether Lambs 
$trait02_data Ewe Lambs 
$trait03_data Ram Lambs 
$trait04_data Unknown Sex Lambs 
$trait05_data Stillborn Lambs 
$trait06_data Aborted Lambs 
$trait07_data Adopted Lambs """
        cmd = String.format(
            "INSERT INTO animal_note_table (id_animalid, note_text, note_date, note_time, id_predefinednotesid, created, modified) " +
                    "values ( %s, '%s', '%s', '%s', '%s', '%s', '%s') ",
            thisanimal_id,
            note_text,
            mytoday,
            mytime,
            blank_space,
            create_modified,
            create_modified
        )
        dbh.exec(cmd)
        clearBtn(null)
    }

    public override fun onResume() {
        super.onResume()
        Log.i("SimpleBirth", " OnResume")
        if (checkRequiredPermissions()) {
            CheckIfServiceIsRunning()
            Log.i("SimpleBirth", " OnResume after Check if service is running")
            // Re-draw the screen when we come back from the update tags activity
            //TODO: look up animal by EID here if required, removing lookup animal click dispatch
        }
        bluetoothWatcher.startWatching()
    }

    public override fun onPause() {
        super.onPause()
        Log.i("SimpleBirth", " OnPause")
        doUnbindService()
        doUnbindbaacodeService()
        doUnbindScaleService()
        bluetoothWatcher.stopWatching()
    }

    private fun doNote(v: View) {
        Utilities.takeNote(v, thisanimal_id, this)
    }

    fun addAnimal(v: View?) {
        Log.i("addAnimal", " at the beginning")
        var i: Intent? = null
        i = Intent(this@SimpleLambingActivity, LegacyAddAnimalActivity::class.java)
        i.putExtra("tagtype", current_tag_type_id)
        i.putExtra("tagnumber", current_tag_number)
        this@SimpleLambingActivity.startActivity(i)
    }


    private fun clearBtn(v: View?) {
        var sv: ScrollView
        var ratingBar: RatingBar
        //  Set update database button green
        val table: TableLayout
        // clear out the display of everything
        try {
            table = binding.TableLayout01
            //			Log.i("in clear button", " number rating bars is " + String.valueOf(nRecs));
            if (nRecs != 0) {
                //todo Problem with clearing was that there is only 1 record when we use the
                // saved_evaluation_table So the quick workaround for the Simple Lambing version is
                // to hard code the number of rating bars to clear out. Must be fixed for versions
                // of simple births for other species.
//                for( int ii = 0; ii < nRecs; ii++ ){
                for (ii in 0..6) {
//					Log.i("in clear button", " in 1st for loop ii is" + String.valueOf(ii));
                    val row1 = table.getChildAt(ii) as TableRow
                    ratingBar = row1.getChildAt(1) as RatingBar
                    ratingBar.rating = 0.0f
                    //					Log.i("RatingBar01 ", String.valueOf(ratingBar.getRating()));
                }
            }
        } catch (e: Exception) {
            //	something failed so log it
            Log.i("in clear button", " in catch of try clearing rating, real and radio groups ")
        }
        //  Clear out the tag list
        Log.i("clear btn", "before changing myadapter")
        try {
            myadapter.changeCursor(null)
        } catch (e: Exception) {
            // In this case there is no adapter so do nothing
        }

        // make the alert button normal and disabled
        btn = binding.buttonPanelTop.showAlertButton
        btn.isEnabled = false
        btn = binding.buttonPanelTop.scanEIDButton
        btn.isEnabled = true
    }

    fun showAlert(v: View?) {
        // Display alerts here
        val builder = AlertDialog.Builder(this)
        builder.setMessage(alert_text)
            .setTitle(R.string.alert_warning)
        builder.setPositiveButton(R.string.ok) { dialog, idx ->
            // User clicked OK button
        }
        val dialog = builder.create()
        dialog.show()
    }

    public override fun onDestroy() {
        super.onDestroy()
        dbh.close()
    }

    private fun checkRequiredPermissions(): Boolean {
        if (!areFulfilled(this)) {
            MainActivity.returnFrom(this)
            return false
        }
        return true
    }

    private fun onBluetoothActivationChanged() {
        checkRequiredPermissions()
    }

    companion object {
        private const val REQUEST_CODE_SELECT_ANIMAL = 500
    }
}
