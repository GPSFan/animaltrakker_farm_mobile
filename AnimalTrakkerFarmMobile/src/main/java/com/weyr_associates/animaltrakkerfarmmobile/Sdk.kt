package com.weyr_associates.animaltrakkerfarmmobile

import android.os.Build

object Sdk {
    @JvmStatic
    fun requiresRuntimeBluetoothPermissions(): Boolean {
        return Build.VERSION_CODES.S <= Build.VERSION.SDK_INT
    }

    @JvmStatic
    fun allowsProgrammaticActivationOfBluetooth(): Boolean {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU
    }
}
