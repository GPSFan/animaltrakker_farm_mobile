package com.weyr_associates.animaltrakkerfarmmobile

import android.app.AlertDialog
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.database.Cursor
import android.database.sqlite.SQLiteException
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.ScrollView
import android.widget.SimpleCursorAdapter
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.weyr_associates.animaltrakkerfarmmobile.RequiredPermissions.areFulfilled
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.LegacyAddAnimalActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.ItemSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.TopButtonBar
import com.weyr_associates.animaltrakkerfarmmobile.app.main.MainActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.select.SelectAnimal
import com.weyr_associates.animaltrakkerfarmmobile.app.select.SelectAnimalActivity.Companion.newIntent
import com.weyr_associates.animaltrakkerfarmmobile.app.select.ageMonthsSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.ageYearsSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.breedSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.ownerSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.database.BreedTable
import com.weyr_associates.animaltrakkerfarmmobile.database.DefaultSettingsTable
import com.weyr_associates.animaltrakkerfarmmobile.database.DefaultSettingsTable.readAsMapFrom
import com.weyr_associates.animaltrakkerfarmmobile.database.getString
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ActivityMaleBreedingSoundnessBinding
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderService
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderStatePresenter
import com.weyr_associates.animaltrakkerfarmmobile.model.Breed
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType
import com.weyr_associates.animaltrakkerfarmmobile.model.Owner
import com.weyr_associates.animaltrakkerfarmmobile.model.SexStandard
import java.time.LocalDate
import java.time.Period
import java.util.Calendar

class MaleBreedingSoundnessActivity : AppCompatActivity() {
    private lateinit var cmd: String
    private var tempText: String? = null
    private var tempLabel: String? = null
    private var radioBtnText: Array<String> = emptyArray()

    private var trait01_data: Float = 0.0f
    private var trait02_data: Float = 0.0f
    private var trait03_data: Float = 0.0f
    private var trait04_data: Float = 0.0f
    private var trait05_data: Float = 0.0f
    private var trait06_data: Float = 0.0f
    private var trait07_data: Float = 0.0f
    private var trait08_data: Float = 0.0f
    private var trait09_data: Float = 0.0f
    private var trait10_data: Float = 0.0f
    private var trait11_data: Float = 0.0f
    private var trait12_data: Float = 0.0f
    private var trait13_data: Float = 0.0f
    private var trait14_data: Float = 0.0f
    private var trait15_data: Float = 0.0f

    private var trait16_data: Int = 0
    private var trait17_data: Int = 0
    private var trait18_data: Int = 0
    private var trait19_data: Int = 0
    private var trait20_data: Int = 0

    private var trait01: Int = 0
    private var trait02: Int = 0
    private var trait03: Int = 0
    private var trait04: Int = 0
    private var trait05: Int = 0
    private var trait06: Int = 0
    private var trait07: Int = 0
    private var trait08: Int = 0
    private var trait09: Int = 0
    private var trait10: Int = 0
    private var trait11: Int = 0
    private var trait12: Int = 0
    private var trait13: Int = 0
    private var trait14: Int = 0
    private var trait15: Int = 0
    private var trait16: Int = 0
    private var trait17: Int = 0
    private var trait18: Int = 0
    private var trait19: Int = 0
    private var trait20: Int = 0
    private var trait11_unitid: Int = 0
    private var trait12_unitid: Int = 0
    private var trait13_unitid: Int = 0
    private var trait14_unitid: Int = 0
    private var trait15_unitid: Int = 0

    private lateinit var alert_text: String
    private lateinit var summary_data: String
    private var breed_code: String? = null
    private var ageinyears: String? = null
    private var thisanimal_id: Int = 0
    private lateinit var radioGroup: RadioGroup
    private var recNo = 0
    private var numbertagrecords: Int = 0
    private var numberrealdataitems: Int = 0
    private var numberuserdataitems: Int = 0
    private var numbercustomdataitems: Int = 0
    private var numberanimals: Int = 0
    private var currentanimalrecord: Int = 0

    private lateinit var breedSpinnerPresenter: ItemSelectionPresenter<Breed>
    private var selectedBreed: Breed? = null

    private lateinit var user_scores: MutableList<Int>
    private lateinit var real_scores: MutableList<Float>

    private lateinit var data_evaluation_traits: MutableList<String>
    private lateinit var user_evaluation_traits: MutableList<String>
    private lateinit var data_trait_numbers: MutableList<Int>
    private lateinit var user_trait_numbers: MutableList<Int>
    private lateinit var user_trait_number_items: MutableList<Int>

    private lateinit var ageYearSpinnerPresenter: ItemSelectionPresenter<Int>
    private var selectedAgeYears: Int = 0

    private lateinit var ageMonthSpinnerPresenter: ItemSelectionPresenter<Int>
    private var selectedAgeMonths: Int = 0

    private var last_owner: Int = 0
    private var current_breed_id: Int = 0
    private var last_location: Int = 0
    private var default_owner: Int = 0
    private var to_id_premiseid: Int = 0
    private var default_species: Int = -1
    private var default_breed: Int = -1
    private var default_sample_type: Int = -1
    private var default_container_type: Int = -1
    private var default_lab: Int = -1
    private lateinit var current_animal_birth_date: String
    private lateinit var this_animal_alert: String
    private var created: String? = null
    private var modified: String? = null

    private lateinit var ownerNameSpinnerPresenter: ItemSelectionPresenter<Owner>
    private var selectedOwner: Owner? = null

    private var current_note: String? = null

    private var owner_id_contactsid: Int = 0

    private lateinit var dbh: DatabaseHandler
    
    private lateinit var cursor: Cursor
    private lateinit var cursor2: Cursor
    private lateinit var cursor3: Cursor
    private lateinit var cursor4: Cursor
    private lateinit var tagCursor: Cursor
    private lateinit var animalcursor: Cursor
    private lateinit var breedCursor: Cursor
    private lateinit var ownerCursor: Cursor
    private lateinit var locationCursor: Cursor
    private lateinit var crsr: Any
    private lateinit var crsr2: Any
    private lateinit var crsr3: Any
    private lateinit var crsr4: Any
    private lateinit var tagcrsr: Any
    private lateinit var animalcrsr: Any
    private lateinit var breedcrsr: Any
    private lateinit var ownercrsr: Any
    private lateinit var locationcrsr: Any

    private lateinit var animaltrakker_defaults: Map<String, Int>

    private var AnimalName = ""

    private lateinit var myadapter: SimpleCursorAdapter

    private var mService: Messenger? = null
    private var mIsBound: Boolean = false

    private val mMessenger: Messenger = Messenger(IncomingHandler())

    // variable to hold the string
    private lateinit var LastEID: String
    private var mytoday: String? = null
    private var mytime: String? = null

    private val bluetoothWatcher = BluetoothWatcher(this)

    private lateinit var binding: ActivityMaleBreedingSoundnessBinding
    private lateinit var eidReaderStatePresenter: EIDReaderStatePresenter

    internal inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                EIDReaderService.MSG_UPDATE_STATUS -> {
                    val b1 = msg.data
                    val State = b1.getString("stat")
                    if (State != null) {
                        eidReaderStatePresenter.updateReaderState(State)
                    }
                }

                EIDReaderService.MSG_NEW_EID_FOUND -> {
                    val bundle = msg.data
                    val eid = bundle.getString("info1")
                    // We have a good whole EID number
                    //TODO: Add format validation here.
                    if (eid != null) {
                        onEIDScanned(eid)
                    }
                }

                EIDReaderService.MSG_UPDATE_LOG_APPEND -> {}
                baacodeService.MSG_UPDATE_LOG_APPEND -> {
                    Log.i("MaleBreeding", "Baacode data received")
                    val b5 = msg.data
                    Log.i("MaleBreeding", "BaaCode logappend.")
                }

                EIDReaderService.MSG_UPDATE_LOG_FULL -> {}
                EIDReaderService.MSG_THREAD_SUICIDE -> {
                    //				    Log.i("MaleBreeding", "Service informed Activity of Suicide.");
                    doUnbindService()
                    stopService(Intent(this@MaleBreedingSoundnessActivity, EIDReaderService::class.java))
                }

                else -> super.handleMessage(msg)
            }
        }
    }


    //    ***    Service connections & disconnections one for each eidService baacodeService and scaleService
    var mConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, binder: IBinder) {
            val service = Messenger(binder).also { mService = it }
            //			Log.i("MaleBreeding", "At Service.");
            try {
                //Register client with service
                val msg = Message.obtain(null, EIDReaderService.MSG_REGISTER_CLIENT)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }

        //  *** This is the eid service disconnection
        override fun onServiceDisconnected(className: ComponentName) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            Log.i("Breeding", "At eid Service Disconnected.")
            mService = null
        }
    }

    //  Really should be check if services are running...
    //  Start the service if it is not already running and bind to it
    //  Should be one each for eid, scale and baacode
    private fun CheckIfServiceIsRunning() {
        //If the service is running when the activity starts, we want to automatically bind to it.
        Log.i("Breeding", "At eid isRunning?.")
        if (EIDReaderService.isRunning()) {
//			Log.i("Breeding", " eidService is running");
            doBindService()
        } else {
//			Log.i("Breeding", " eidService is not running, start it");
            startService(Intent(this@MaleBreedingSoundnessActivity, EIDReaderService::class.java))
            doBindService()
        }
    }

    //  Bind services, should be one set of bind & unbind methods for eid, scale and baacode
    //  First is the eid service bind
    fun doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
//		Log.i("Breeding", "At doBind1.");
        bindService(Intent(this, EIDReaderService::class.java), mConnection, BIND_AUTO_CREATE)
        //		Log.i("Breeding", "At doBind2.");
        mIsBound = true
        val service = mService
        if (service != null) {
//			Log.i("Breeding", "At doBind3.");
            try {
                //Request status update
                var msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_STATUS, 0, 0)
                msg.replyTo = mMessenger
                service.send(msg)
                //				Log.i("Breeding", "At doBind4.");
                //Request full log from service.
                msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_LOG_FULL, 0, 0)
                service.send(msg)
            } catch (e: RemoteException) {
                //NO-OP
            }
        }
        //		Log.i("Breeding", "At doBind5.");
    }

    //     Next the unbind eid servides
    fun doUnbindService() {
//		Log.i("Breeding", "At DoUnbindservice");
        val service = mService
        if (service != null) {
            try {
                //Stop eidService from sending tags
                val msg = Message.obtain(null, EIDReaderService.MSG_NO_TAGS_PLEASE)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        if (mIsBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (service != null) {
                try {
                    val msg = Message.obtain(null, EIDReaderService.MSG_UNREGISTER_CLIENT)
                    msg.replyTo = mMessenger
                    service.send(msg)
                } catch (e: RemoteException) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection)
            mIsBound = false
        }
    }

    // use EID reader to look up an animal
    fun onEIDScanned(eid: String) {
        Log.i("in onEIDScanned ", "with eid of $eid")
        LastEID = eid
        lookForAnimalByEID(eid)
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMaleBreedingSoundnessBinding.inflate(
            layoutInflater
        )
        setContentView(binding.root)

        var cmd: String
        thisanimal_id = 0
        dbh = DatabaseHandler.create(this)
        // Get the defaults and fill the defaults list for use later.
        animaltrakker_defaults = readAsMapFrom(this, dbh)
        //		CheckIfServiceIsRunning();
        Log.i("Breeding", "back from isRunning")

        data_evaluation_traits = ArrayList()
        user_evaluation_traits = ArrayList()

        data_trait_numbers = ArrayList()
        user_trait_numbers = ArrayList()
        user_trait_number_items = ArrayList()

        numbertagrecords = 0
        numberrealdataitems = 0
        numberuserdataitems = 0
        numbercustomdataitems = 0 // Used to fill the radio buttons
        numberanimals = 0
        currentanimalrecord = 0
        //todo Get a saved evaluation instead.
        //	Fill the trait name variables from the last evaluation
        cmd = "select * from last_evaluation_table"
        crsr = dbh.exec(cmd)
        cursor = crsr as Cursor
        dbh.moveToFirstRecord()

        trait01 = dbh.getInt(1)
        cursor.moveToNext()
        trait02 = dbh.getInt(1)
        cursor.moveToNext()
        trait03 = dbh.getInt(1)
        cursor.moveToNext()
        trait04 = dbh.getInt(1)
        cursor.moveToNext()
        trait05 = dbh.getInt(1)
        cursor.moveToNext()
        trait06 = dbh.getInt(1)
        cursor.moveToNext()
        trait07 = dbh.getInt(1)
        cursor.moveToNext()
        trait08 = dbh.getInt(1)
        cursor.moveToNext()
        trait09 = dbh.getInt(1)
        cursor.moveToNext()
        trait10 = dbh.getInt(1)
        cursor.moveToNext()
        trait11 = dbh.getInt(1)
        trait11_unitid = dbh.getInt(2)
        cursor.moveToNext()
        trait12 = dbh.getInt(1)
        trait12_unitid = dbh.getInt(2)
        cursor.moveToNext()
        trait13 = dbh.getInt(1)
        trait13_unitid = dbh.getInt(2)
        cursor.moveToNext()
        trait14 = dbh.getInt(1)
        trait14_unitid = dbh.getInt(2)
        cursor.moveToNext()
        trait15 = dbh.getInt(1)
        trait15_unitid = dbh.getInt(2)
        cursor.moveToNext()
        trait16 = dbh.getInt(1)
        cursor.moveToNext()
        trait17 = dbh.getInt(1)
        cursor.moveToNext()
        trait18 = dbh.getInt(1)
        cursor.moveToNext()
        trait19 = dbh.getInt(1)
        cursor.moveToNext()
        trait20 = dbh.getInt(1)

        // Capture default species to support looking up breed.
        default_species = animaltrakker_defaults[DefaultSettingsTable.Columns.SPECIES_ID]!!
        default_breed = animaltrakker_defaults[DefaultSettingsTable.Columns.BREED_ID]!!
        default_sample_type = animaltrakker_defaults[DefaultSettingsTable.Columns.TISSUE_SAMPLE_TYPE_ID]!!
        //TODO: SpinnerUpdates - Handle this for owner/contact
        default_owner = animaltrakker_defaults[DefaultSettingsTable.Columns.OWNER_CONTACT_ID]!!
        default_lab = animaltrakker_defaults[DefaultSettingsTable.Columns.LAB_COMPANY_ID]!!
        default_container_type = animaltrakker_defaults[DefaultSettingsTable.Columns.TISSUE_CONTAINER_TYPE_ID]!!

        breedSpinnerPresenter = breedSelectionPresenter(
            speciesId = default_species,
            binding.animalBreedSpinner
        ) {
            selectedBreed = it
            breedSpinnerPresenter.displaySelectedItem(it)
        }

        ageYearSpinnerPresenter = ageYearsSelectionPresenter(binding.ageYearSpinner) {
            selectedAgeYears = it
            ageYearSpinnerPresenter.displaySelectedItem(it)
        }

        ageMonthSpinnerPresenter = ageMonthsSelectionPresenter(binding.ageMonthSpinner) {
            selectedAgeMonths = it
            ageMonthSpinnerPresenter.displaySelectedItem(it)
        }

        //  Fill the data traits
//        FROM last_evaluation_table  LEFT JOIN evaluation_trait_table on
//        evaluation_trait_table.id_evaluationtraitid = last_evaluation_table.id_traitid
//        where  evaluation_trait_table.id_evaluationtraittypeid = 2 AND last_evaluation_table. id_traitid <> 0
//todo Need to set this to pull one of the saved male breeding soundness evaluation sets. 
        // Consider whether this is best added as part of setup to decide what male breeding soundness will get used?
        //If so then have to figure out how to switch on the fly. 
        //Use case is a ranch with both cattle and sheep and doing breeding soundness on both species 
        // need 2 sets of evals and an easy way to switch between them 
        cmd = String.format(
            "SELECT evaluation_trait_table.trait_name, evaluation_trait_table.id_evaluationtraitid " +
                    " FROM last_evaluation_table INNER JOIN evaluation_trait_table " +
                    "ON evaluation_trait_table.id_evaluationtraitid = last_evaluation_table.id_evaluationtraitid " +
                    "WHERE " +  //                " evaluation_trait_table.id_evaluationtraitid = last_evaluation_table.id_lastevaluationid " +
                    " evaluation_trait_table.id_evaluationtraittypeid = 2 AND last_evaluation_table.id_evaluationtraitid<> 0"
        )
        crsr = dbh.exec(cmd)
        cursor = crsr as Cursor
        numberrealdataitems = cursor.count
        dbh.moveToFirstRecord()
        cursor.moveToFirst()
        while (!cursor.isAfterLast) {
            data_trait_numbers.add(cursor.getInt(1))
            //        	Log.i("MaleBreeding2", " trait number is " + String.valueOf(cursor.getInt(1)));
            data_evaluation_traits.add(cursor.getString(0))
            Log.i("MaleBreeding2", " trait name is " + cursor.getString(0))
            cursor.moveToNext()
        }
        Log.i("MaleBreeding", "number of records in data traits cursor is $numberrealdataitems")
        var inflater = layoutInflater
        for (ii in 0 until numberrealdataitems) {
//    		Log.i("MaleBreeding" , " ii is " + String.valueOf(ii));
//    		Log.i ("MaleBreeding", " trait name is " + data_evaluation_traits.get(ii));
            val table = binding.TableLayout02
            //			Log.i("MaleBreeding", " after TableLayout");
            val row = inflater.inflate(R.layout.eval_data_item_entry, table, false) as TableRow
            tempLabel = data_evaluation_traits.get(ii)
            //	    	Log.i("MaleBreeding", " tempLabel is " + tempLabel);
            (row.findViewById<View>(R.id.data_lbl) as TextView).text = tempLabel
            //	    	Log.i("MaleBreeding", " after set text view");
            table.addView(row)
        }

        // Set up the user traits
        // todo hard coded for Breeding Soundness as the only trait for now. Also hard coded for the location of the traits in the database.
        //  Might need to fix once I fill the table with other defaults
        //todo consider how to implement when I combine database from LambTracker with several traits.
        // Should I implement some form of standard evaluation types that work for multiple species?
        cmd = String.format(
            "SELECT evaluation_trait_table.trait_name, evaluation_trait_table.id_evaluationtraitid, " +
                    "custom_evaluation_number_options_table.custom_eval_number " +
                    "FROM evaluation_trait_table INNER JOIN last_evaluation_table on " +
                    " evaluation_trait_table.id_evaluationtraitid = last_evaluation_table.id_evaluationtraitid" +
                    " INNER JOIN custom_evaluation_number_options_table ON evaluation_trait_table.id_evaluationtraitid = " +
                    " custom_evaluation_number_options_table.id_evaluationtraitid WHERE evaluation_trait_table.id_evaluationtraittypeid = 3 "
        )
        //    	Log.i("MaleBreeding", " cmd is " + cmd);
        crsr = dbh.exec(cmd)
        cursor = crsr as Cursor
        numberuserdataitems = cursor.count // number of user defined traits to use
        dbh.moveToFirstRecord()
        cursor.moveToFirst()
        while (!cursor.isAfterLast) {
            user_evaluation_traits.add(cursor.getString(0))
            Log.i("MaleBreeding", " trait name is " + cursor.getString(0))
            user_trait_numbers.add(cursor.getInt(1))
            Log.i("MaleBreeding", " trait id number is " + cursor.getInt(1).toString())
            user_trait_number_items.add(cursor.getInt(2))
            Log.i(
                "MaleBreeding",
                " number of items for this trait is " + cursor.getInt(2).toString()
            )
            cursor.moveToNext()
        }
        Log.i("MaleBreeding", "number of records in user traits cursor is $numberuserdataitems")
        inflater = layoutInflater
        val table = binding.TableLayout03
        for (ii in 0 until numberuserdataitems) {
            Log.i("in for loop", " ii is $ii")
            Log.i("in first loop", " user trait number is " + user_trait_numbers.get(ii).toString())
            Log.i("in for loop", " trait name is " + user_evaluation_traits.get(ii))
            Log.i(
                "in first loop",
                " number of trait entries is " + user_trait_number_items.get(ii).toString()
            )
            val row = inflater.inflate(R.layout.eval_custom_item, table, false) as TableRow
            tempLabel = user_evaluation_traits.get(ii)
            Log.i("in first loop", " tempLabel is $tempLabel")
            //	Set the text for the radiogroup label
            (row.findViewById<View>(R.id.radioGroup1_lbl) as TextView).text = tempLabel
            //	Get the text for the buttons
            tempText = user_trait_numbers.get(ii).toString()
            Log.i("in first loop", "trait numbers is $tempText")
            cmd = String.format(
                "select custom_evaluation_traits_table.custom_evaluation_item " +
                        " from custom_evaluation_traits_table " +
                        " where custom_evaluation_traits_table.id_evaluationtraitid = '%s' " +
                        " order by custom_evaluation_traits_table.custom_evaluation_order ASC ",
                tempText
            )
            Log.i("MaleBreeding", " ready to get button text cmd is $cmd")
            crsr = dbh.exec(cmd)
            cursor = crsr as Cursor
            numbercustomdataitems = cursor.count
            dbh.moveToFirstRecord()
            val buttons: ArrayList<Any?> = ArrayList<Any?>()
            cursor.moveToFirst()
            while (!cursor.isAfterLast) {
                buttons.add(cursor.getString(0))
                cursor.moveToNext()
            }
            radioBtnText = buttons.toTypedArray() as Array<String>
            // Build the radio buttons here
            radioGroup = (row.findViewById<View>(R.id.radioGroup1) as RadioGroup)
            addRadioButtons(user_trait_number_items.get(ii), radioBtnText)
            table.addView(row)
            Log.i("MaleBreeding ", " after inflate the row")
        }
        Log.i("TissueSample", " start of fill out the tissue sampling stuff ")


        ownerNameSpinnerPresenter = ownerSelectionPresenter(
            binding.ownerNameSpinner
        ) {
            selectedOwner = it
            ownerNameSpinnerPresenter.displaySelectedItem(it)
        }

        // make the alert button normal and disabled
        var btn = binding.buttonPanelTop.showAlertButton
        btn.setOnClickListener { showAlert() }
        btn.isEnabled = false

        //	make the look up animal button green
        btn = binding.buttonPanelTop.lookupAnimalButton
        btn.setOnClickListener { v: View -> this.onLookupAnimalClicked(v) }

        btn = binding.buttonPanelTop.mainActionButton
        btn.setOnClickListener { view: View? -> updateDatabase() }
        btn.isEnabled = false

        //	make the scan eid button red
        btn = binding.buttonPanelTop.scanEIDButton
        btn.setOnClickListener { v: View -> this.scanEid(v) }

        binding.buttonPanelTop.show(
            (TopButtonBar.UI_ALL and TopButtonBar.UI_CLEAR_DATA.inv()) or
                    TopButtonBar.UI_ACTION_UPDATE_DATABASE
        )

        eidReaderStatePresenter = EIDReaderStatePresenter(
            this,
            binding.buttonPanelTop.imageScannerStatus
        )

        bluetoothWatcher.onActivationChanged = { enabled: Boolean? ->
            onBluetoothActivationChanged()
            Unit
        }
    }

    private fun addRadioButtons(numButtons: Int, radioBtnText: Array<String>) {
        var i = 0
        while (i < numButtons) {
            //instantiate...
            val radioBtn = RadioButton(this)
            //set the values that you would otherwise hardcode in the xml...
            radioBtn.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            //label the button...
            radioBtn.text = radioBtnText[i]
            Log.i("addradiobuttons", radioBtnText[i])
            radioBtn.id = i
            //todo deal with deprecated styles
            radioBtn.setTextAppearance(this, android.R.style.TextAppearance_Large)
            //add it to the group.
            radioGroup.addView(radioBtn, i)
            Log.i("Breeding ", " after add radio Button ")
            i++
        }
    }

    //  user clicked 'Scan' button
    private fun scanEid(v: View) {
        // Here is where I need to get a tag scanned and put the data into the variable LastEID
        clearBtn()
        mService?.let { service ->
            try {
                //Start eidService sending tags
                val msg = Message.obtain(null, EIDReaderService.MSG_SEND_ME_TAGS)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
    }

    private fun onLookupAnimalClicked(v: View) {
        startActivityForResult(
            newIntent(this, SexStandard.MALE),
            REQUEST_CODE_SELECT_ANIMAL
        )
    }

    private fun lookForAnimalByEID(eid: String) {

        binding.buttonPanelTop.mainActionButton.isEnabled = true

        Log.i("LookForAnimal", " got to LookForAnimal with EID of $eid")
        if (eid.isNotEmpty()) {
            // Get the animal id from the id table for this tag number and selected tag type
            cmd = String.format(
                "select id_animalid from animal_id_info_table where id_number='%s' " +
                        "and animal_id_info_table.id_idtypeid='%s' ",
                eid, IdType.ID_TYPE_ID_EID
            )
            Log.i("searchByEID", "command is $cmd")
            animalcrsr = dbh.exec(cmd)
            animalcursor = animalcrsr as Cursor
            recNo = 1
            numberanimals = animalcursor.count
            Log.i("searchByNumber", " nRecs = $numberanimals")
            dbh.moveToFirstRecord()
            Log.i("searchByNumber", " the cursor is of size " + dbh.size.toString())
            if (dbh.size == 0) {
                addAnimalWithEID(eid)
                return
            }
            thisanimal_id = dbh.getInt(0)
            Log.i("searchByNumber", "This animal is record $thisanimal_id")
            Log.i("searchByNumber", " Before formatting the record")
            //	We need to call the format the record method
            formatAnimalRecord()
        }

        //  Set update database button green
        binding.buttonPanelTop.mainActionButton.isEnabled = true
    }

    private fun formatAnimalRecord() {
        Log.i("formatAnimalRecord", "This animal is record $thisanimal_id")
        Log.i("formatAnimalRecord", " the animal cursor is of size $numberanimals")
        Log.i("formatAnimalRecord", " record number is $recNo")

        // todo hid this to move it into the update database button section
        cmd = String.format(
            "select animal_table.animal_name, animal_id_info_table.id_number, id_location_table.id_location_abbrev," +
                    " id_color_table.id_color_name, id_type_table.id_type_name,  " +
                    "animal_id_info_table.id_animalidinfoid as _id, animal_id_info_table.id_date_off  " +
                    "from animal_table inner join animal_id_info_table on animal_table.id_animalid = animal_id_info_table.id_animalid " +
                    "left outer join id_color_table on animal_id_info_table.id_male_id_idcolorid = id_color_table.id_idcolorid " +
                    "left outer join id_location_table on animal_id_info_table.id_idlocationid = id_location_table.id_idlocationid " +
                    "inner join id_type_table on animal_id_info_table.id_idtypeid = id_type_table.id_idtypeid " +
                    "where animal_id_info_table.id_animalid ='%s' and (animal_id_info_table.id_date_off is null or " +
                    "animal_id_info_table.id_date_off is '')order by id_type_name asc",
            thisanimal_id
        )
        Log.i("formatAnimalRecord", " Before running get ids command")
        tagcrsr = dbh.exec(cmd)
        tagCursor = tagcrsr as Cursor
        tagCursor.moveToFirst()
        Log.i("formatAnimalRecord", " the id cursor is of size " + dbh.size.toString())
        Log.i("formatAnimalRecord", " before formatting results")
        //        TV = (TextView) findViewById(R.id.inputText);
//        TV.setText(dbh.getStr(0));
        AnimalName = dbh.getStr(0)
        //	Get set up to use the CursorAdapter to display all the tag data
        //	Select only the columns I need for the tag display section
        val fromColumns =
            arrayOf("id_number", "id_color_name", "id_location_abbrev", "id_type_name")
        Log.i("FormatRecord", "after setting string array fromColumns")
        //	Set the views for each column for each line. A tag takes up 1 line on the screen
        val toViews = intArrayOf(
            R.id.tag_number,
            R.id.tag_color_name,
            R.id.id_location_abbrev,
            R.id.idtype_name
        )
        //        Log.i("FormatRecord", "after setting string array toViews");
        myadapter = SimpleCursorAdapter(this, R.layout.tag_list, tagCursor, fromColumns, toViews, 0)
        //        Log.i("FormatRecord", "after setting myadapter");
        binding.list.adapter = myadapter
        //        Log.i("FormatRecord", "after setting list adapter");
        try {
//            Log.i("try block", " Before finding an electronic tag if it exists");
            cmd = String.format(
                "select animal_table.id_animalid, id_type_table.id_type_name, " +
                        "animal_id_info_table.id_number " +
                        "from animal_table inner join animal_id_info_table on animal_table.id_animalid = animal_id_info_table.id_animalid " +
                        "inner join id_type_table on animal_id_info_table.id_idtypeid = id_type_table.id_idtypeid " +
                        "where animal_id_info_table.id_animalid ='%s' and (animal_id_info_table.id_date_off is null or " +
                        " animal_id_info_table.id_date_off is '') and " +
                        "animal_id_info_table.id_idtypeid = 2 " +
                        "order by id_type_name asc", thisanimal_id
            )
            Log.i("CMD", cmd)
            crsr4 = dbh.exec(cmd)
            cursor4 = crsr4 as Cursor
            cursor4.moveToFirst()
            Log.i("xxxx", "with LastEID of $LastEID")
            LastEID = dbh.getStr(2)
            Log.i("xxxxx", "with LastEID of $LastEID")
            Log.i("LastEID is ", dbh.getStr(2))
        } catch (r: Exception) {
            LastEID = "000_000000000000"
            Log.v("fill LAST EID ", " in  MaleBreeding test RunTimeException with all zeros$r")
        }

        // TODO This only works for animals owned by a person not a company
        try { //  Get the current owner for this animal and put the spinner to that
            Log.i("before get owner", " getting ready to get owner ID ")
            cmd = String.format(
                "select to_id_contactid from animal_ownership_history_table " +
                        "where id_animalid ='%s'  order by transfer_date", thisanimal_id
            )
            Log.i("formatrecord", "command is $cmd")
            ownercrsr = dbh.exec(cmd)
            ownerCursor = ownercrsr as Cursor
            ownerCursor.moveToLast()
            owner_id_contactsid = dbh.getInt(0)
            // now I have the id of the current owner
            Log.i("get owner", " owner_id is $owner_id_contactsid")
            //            cmd = String.format("SELECT ");
            //TODO: SpinnerUpdates - should set selected owner to something
            //selectedOwner = <something>
//            owner_name_spinner = binding.ownerNameSpinner
//            owner_name_spinner.setSelection(possible_owners_id.indexOf(owner_id_contactsid))
        } catch (r: Exception) {
            //TODO: SpinnerUpdates - should set to default here? Old implementation was unselecting spinner.
            selectedOwner = null
            ownerNameSpinnerPresenter.displaySelectedItem(null)
        }
        Log.i("FormatRecord ", " this animal is id number $thisanimal_id")
        cmd = String.format(
            """
    SELECT animal_table.id_animalid, birth_date, animal_breed_table.id_breedid, alert FROM animal_table 
    LEFT JOIN animal_breed_table ON animal_breed_table.id_animalid = animal_table.id_animalid WHERE animal_table.id_animalid ='%s' 
    """.trimIndent(), thisanimal_id
        )
        Log.i("formatrecord", "command is $cmd")
        crsr2 = dbh.exec(cmd)
        cursor2 = crsr2 as Cursor
        cursor2.moveToFirst()
        alert_text = dbh.getStr(3)
        this_animal_alert = alert_text
        Log.i("formatrecord", "alert text  is $alert_text")
        current_animal_birth_date = dbh.getStr(1)
        if (TextUtils.isEmpty(current_animal_birth_date)) {
            Log.i("formatrecord", "animal birth is empty ")
            //  Set the age spinners to the zero record
            selectedAgeYears = 0
            selectedAgeMonths = 0
        } else {
            val today = LocalDate.now()
            val animalyear = current_animal_birth_date.substring(0, 4).toInt()
            val animalmonth = current_animal_birth_date.substring(5, 7).toInt()
            val animalday = current_animal_birth_date.substring(8, 10).toInt()
            val birthday = LocalDate.of(animalyear, animalmonth, animalday)
            val p = Period.between(birthday, today)
            selectedAgeYears = p.years + 1
            selectedAgeMonths = p.months + 1
        }

        ageYearSpinnerPresenter.displaySelectedItem(selectedAgeYears)
        ageMonthSpinnerPresenter.displaySelectedItem(selectedAgeMonths)

        //        The current breed gets set to zero somehow and is not keeping its status with the 63 from when we added the animal.
        current_breed_id = dbh.getInt(2)
        Log.i("formatrecord", "animal breed from database $current_breed_id")
        if (current_breed_id == 0) {
            Log.i("formatrecord", "animalbreed is empty ")
            //  Set the breed spinner to the zero record
            // TODO: SpinnerUpdates -  set this to the default breed?
            breedSpinnerPresenter.displaySelectedItem(null)
            selectedBreed = null
        } else {
            Log.i("formatrecord", "animal breed $current_breed_id")
            cmd = String.format(
                "select breed_display_order, breed_name from breed_table where id_breedid = '%s' ",
                current_breed_id
            )
            breedcrsr = dbh.exec(cmd)
            breedCursor = breedcrsr as Cursor
            breedCursor.moveToFirst()
            val breedDisplayOrder = dbh.getInt(BreedTable.Columns.ORDER)
            val breedName = dbh.getStr(BreedTable.Columns.NAME)
            Log.i("formatrecord", "breed display order $breedDisplayOrder")
            Log.i("formatrecord", "breed name $breedName")
            Log.i("formatrecord", "before set the breed spinner ")

            // TODO: SpinnerUpdates -  fix this should be getting the position not the breed display order
            //selectedBreed = <something>
            breedSpinnerPresenter.displaySelectedItem(selectedBreed)
//            animal_breed_spinner = binding.animalBreedSpinner
//            animal_breed_spinner.setSelection(breedDisplayOrder)

            val test = breedDisplayOrder + 1
            Log.i("formatrecord", "animal breed spinner selection $test")
            Log.i("formatrecord", "animal breed spinner selection " + (breedDisplayOrder + 1))
            Log.i("formatrecord", "animal breed  after set the breed spinner")
            Log.i("formatrecord", "with LastEID of $LastEID")
        }
        //        Log.i("in formatAnimalRecord ", "Alert Text is " + alert_text);
//    	Now to test of the animal has an alert and if so then display the alert & set the alerts button to
        if (alert_text != null && !alert_text.isEmpty() && !alert_text.trim { it <= ' ' }
                .isEmpty()) {
            // make the alert button red
            val btn = binding.buttonPanelTop.showAlertButton
            btn.isEnabled = true
            showAlert()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i("BreedingSound", "requestCode ")
        if (requestCode == REQUEST_CODE_SELECT_ANIMAL) {
            if (resultCode == RESULT_OK && data != null && data.hasExtra(SelectAnimal.EXTRA_SELECTED_ANIMAL_ID)) {
                val selectedAnimalId = data.getIntExtra(SelectAnimal.EXTRA_SELECTED_ANIMAL_ID, -1)
                if (selectedAnimalId != -1) {
                    thisanimal_id = selectedAnimalId
                    formatAnimalRecord()
                }
            }
        } else if (resultCode == RESULT_OK) {
            Log.i("MaleBreeding4", "zzz ")
        }
    }

    private fun updateDatabase() {
        var cmd: String
        var TV: TextView
        var tempData: Float
        var tempRadioBtn: Int
        var tempTrait: Int
        val temp_integer: Int
        val rating_scores: MutableList<Float> = ArrayList()
        real_scores = ArrayList()
        user_scores = ArrayList()
        var btn: Button
        val blank_space = ""


        mytoday = Utilities.TodayIs()
        mytime = Utilities.TimeIs()
        created = "$mytoday $mytime"
        modified = "$mytoday $mytime"
        val calendar = Calendar.getInstance()
        //        int month = calendar.get(Calendar.MONTH);
//        int year = calendar.get(Calendar.YEAR);
        // Disable Update Database button and make it red to prevent getting 2 records at one time
        btn = binding.buttonPanelTop.mainActionButton
        btn.isEnabled = false

        Log.i("in updateDatabase", " this animal id is $thisanimal_id")
        // 	Fill the rating bar scores array with zeros as no rating bars in this evaluation
        for (ii in 0..9) {
            rating_scores.add(0.0f)
            Log.i("in save scores ", "Filling rating bar array with zeros")
        }
        // Fill the rating bar score variables from the rating_scores array
        // They are all zeros for the Breeding Soundness Test per user requirements
        // todo Will need mods for multiple species that use other breeding soundness parameters
        trait01_data = rating_scores[0]
        //    		Log.i("trait01_ratingbar ", String.valueOf(trait01_data));
        trait02_data = rating_scores[1]
        //    		Log.i("trait02_ratingbar ", String.valueOf(trait02_data));
        trait03_data = rating_scores[2]
        //    		Log.i("trait03_ratingbar ", String.valueOf(trait03_data));
        trait04_data = rating_scores[3]
        //    		Log.i("trait04_ratingbar ", String.valueOf(trait04_data));
        trait05_data = rating_scores[4]
        //    		Log.i("trait05_ratingbar ", String.valueOf(trait05_data));
        trait06_data = rating_scores[5]
        //    		Log.i("trait06_ratingbar ", String.valueOf(trait06_data));
        trait07_data = rating_scores[6]
        //    		Log.i("trait07_ratingbar ", String.valueOf(trait07_data));
        trait08_data = rating_scores[7]
        //    		Log.i("trait08_ratingbar ", String.valueOf(trait08_data));
        trait09_data = rating_scores[8]
        //    		Log.i("trait09_ratingbar ", String.valueOf(trait09_data));
        trait10_data = rating_scores[9]

        //    		Log.i("trait10_ratingbar ", String.valueOf(trait10_data));

        //	get the real data values
        Log.i("in updateDatabase", " number real data points is $numberrealdataitems")
        var table = binding.TableLayout02
        //    		Log.i("in save scores", " after find tablelayout02 ");
        if (numberrealdataitems != 0) {
            for (ii in 0 until numberrealdataitems) {
                val row1 = table.getChildAt(ii) as TableRow
                TV = row1.getChildAt(1) as EditText
                tempData = try {
                    TV.text.toString().toFloat()
                } catch (ex: Exception) {
                    0.0f
                }
                real_scores.add(ii, tempData)
                Log.i("index ii ", ii.toString())
                Log.i("real_score ", real_scores.get(ii).toString())
                Log.i("realscore ", tempData.toString())
            }
            for (ii in numberrealdataitems..4) {
                Log.i("in save scores ", "Filling remainder of real data array with zeros")
                real_scores.add(0.0f)
            }
        } else {
            for (ii in 0..4) {
                Log.i("in save scores ", "Filling entire real data array with zeros")
                real_scores.add(0.0f)
            }
        }
        //	Fill the real score variables from the real_scores array
        trait11_data = real_scores.get(0)
        trait12_data = real_scores.get(1)
        trait13_data = real_scores.get(2)
        trait14_data = real_scores.get(3)
        trait15_data = real_scores.get(4)

        //    	get the User data values
//    		Log.i("in save scores", " number user data points is " + String.valueOf(nRecs3));
        table = binding.TableLayout03
        //    		Log.i("in save scores", " after find tablelayout03 ");
        if (numberuserdataitems != 0) {
            for (ii in 0 until numberuserdataitems) {
//    				Log.i("in save scores", " before get child row ");
                val row1 = table.getChildAt(ii) as TableRow
                //    				Log.i("in save scores", " after get child row ");
                tempTrait = user_trait_numbers[ii]
                Log.i("in save scores", " trait number is $tempTrait")
                val rg = (row1.findViewById<View>(R.id.radioGroup1) as RadioGroup)
                Log.i("in save scores", " after get radiogroup view ")
                try {
//	    				Log.i("in save scores", " in try block ");
                    tempRadioBtn = rg.checkedRadioButtonId
                    Log.i("try radioBtn ", tempRadioBtn.toString())
                    cmd = String.format(
                        "SELECT custom_evaluation_traits_table.id_custom_evaluationtraitsid " +
                                " FROM custom_evaluation_traits_table " +
                                " WHERE custom_evaluation_traits_table.id_evaluationtraitid = %s " +
                                " AND custom_evaluation_traits_table.custom_evaluation_order =  %s ",
                        tempTrait,
                        tempRadioBtn + 1
                    )
                    crsr2 = dbh.exec(cmd)
                    cursor2 = crsr2 as Cursor
                    dbh.moveToFirstRecord()
                    tempRadioBtn = cursor2.getInt(0)
                    Log.i("try ", tempRadioBtn.toString())
                } catch (ex: Exception) {
                    tempRadioBtn = 0
                    Log.i("catch ", tempRadioBtn.toString())
                }
                Log.i("out of try ", "before adding score to user.scores")
                Log.i("index ii ", ii.toString())
                user_scores.add(tempRadioBtn)
                Log.i("user_score ", user_scores.get(ii).toString())
                Log.i("user_score ", tempRadioBtn.toString())
            }
            for (ii in numberuserdataitems..4) {
//    	    		Log.i("in save scores ", "Filling remainder of user data array with zeros");
                user_scores.add(0)
            }
        } else {
            for (ii in 0..4) {
//    				Log.i("in save scores ", "no user scores so make all 0");
                user_scores.add(0)
            }
        }
        //    		Fill the user score variables from the user_scores array
        // todo this is hard coded for the testing we currently have but it should be pulled from database instead I think
        trait16_data = user_scores.get(0)
        Log.i("trait16_data ", trait16_data.toString())
        //        trait16 contains the id_evaluationtraitid for the custom trait we are using
//        trait16_data contains the id_customevaluationtraitid primary key from the custom_evaluation_traits_table
//         It should be more generic to handle more variations or different traits.
        //  This covers the standard Trich testing breeding soundness exam
        if (trait16 == 46) {
            if (trait16_data == 31) {
                this_animal_alert = "Recheck $mytoday\n$this_animal_alert"
                cmd = String.format(
                    "UPDATE animal_table set alert = '%s' WHERE id_animalid = %s ",
                    this_animal_alert,
                    thisanimal_id
                )
                dbh.exec(cmd)
            }
            if (trait16_data == 30) {
                this_animal_alert = "Unsatisfactory $mytoday\n$this_animal_alert"
                cmd = String.format(
                    "UPDATE animal_table set alert = '%s' WHERE id_animalid = %s ",
                    this_animal_alert,
                    thisanimal_id
                )
                dbh.exec(cmd)
            }
            if (trait16_data == 29) {
                this_animal_alert = "Satisfactory $mytoday\n$this_animal_alert"

                cmd = String.format(
                    "UPDATE animal_table set alert = '%s' WHERE id_animalid = %s ",
                    this_animal_alert,
                    thisanimal_id
                )
                dbh.exec(cmd)
            }
        }
        //  This covers the Optimal Ag's Custom Version
        if (trait16 == 53) {
            if (trait16_data == 39) {
                this_animal_alert = "Unsatisfactory $mytoday\n$this_animal_alert"
                cmd = String.format(
                    "UPDATE animal_table set alert = '%s' WHERE id_animalid = %s ",
                    this_animal_alert,
                    thisanimal_id
                )
                dbh.exec(cmd)
            }
            if (trait16_data == 38) {
                this_animal_alert = "Questionable $mytoday\n$this_animal_alert"
                cmd = String.format(
                    "UPDATE animal_table set alert = '%s' WHERE id_animalid = %s ",
                    this_animal_alert,
                    thisanimal_id
                )
                dbh.exec(cmd)
            }
            if (trait16_data == 37) {
                this_animal_alert = "Satisfactory $mytoday\n$this_animal_alert"
                cmd = String.format(
                    "UPDATE animal_table set alert = '%s' WHERE id_animalid = %s ",
                    this_animal_alert,
                    thisanimal_id
                )
                dbh.exec(cmd)
            }
            if (trait16_data == 36) {
                this_animal_alert = "Excellent $mytoday\n$this_animal_alert"

                cmd = String.format(
                    "UPDATE animal_table set alert = '%s' WHERE id_animalid = %s ",
                    this_animal_alert,
                    thisanimal_id
                )
                dbh.exec(cmd)
            }
        }
        //  This covers the Simple Sort option
        if (trait16 == 54) {
            if (trait16_data == 40) {
                this_animal_alert = "Keep $mytoday\n$this_animal_alert"
                cmd = String.format(
                    "UPDATE animal_table set alert = '%s' WHERE id_animalid = %s ",
                    this_animal_alert,
                    thisanimal_id
                )
                dbh.exec(cmd)
            }
            if (trait16_data == 41) {
                this_animal_alert = "Ship $mytoday\n$this_animal_alert"
                cmd = String.format(
                    "UPDATE animal_table set alert = '%s' WHERE id_animalid = %s ",
                    this_animal_alert,
                    thisanimal_id
                )
                dbh.exec(cmd)
            }
            if (trait16_data == 42) {
                this_animal_alert = "Cull $mytoday\n$this_animal_alert"
                cmd = String.format(
                    "UPDATE animal_table set alert = '%s' WHERE id_animalid = %s ",
                    this_animal_alert,
                    thisanimal_id
                )
                dbh.exec(cmd)
            }
            if (trait16_data == 43) {
                this_animal_alert = "Other $mytoday\n$this_animal_alert"
                cmd = String.format(
                    "UPDATE animal_table set alert = '%s' WHERE id_animalid = %s ",
                    this_animal_alert,
                    thisanimal_id
                )
                dbh.exec(cmd)
            }
        }
        trait17_data = user_scores.get(1)
        Log.i("trait17_data ", trait17_data.toString())
        trait18_data = user_scores.get(2)
        Log.i("trait18_data ", trait18_data.toString())
        trait19_data = user_scores.get(3)
        Log.i("trait19_data ", trait19_data.toString())
        trait20_data = user_scores.get(4)
        Log.i("trait20_data ", trait20_data.toString())

        // todo verify this works if the display order of owners is changed
        //TODO: SpinnerUpdates - update to handle company/contact
        owner_id_contactsid = selectedOwner?.id!!

        //        if (current_tag_type_id == 0 || owner_id_contactsid == 0 || current_breed_id == 0 ||
//                current_lab_name_id == 0) {
        if (owner_id_contactsid == 0 || current_breed_id == 0) {
            // Missing data so  display an alert
            val builder = AlertDialog.Builder(this)
            builder.setMessage(R.string.fill_fields)
                .setTitle(R.string.fill_fields)
            builder.setPositiveButton(R.string.ok) { dialog, idx ->
                // User clicked OK button
                //  Set update database button green
                val btn1 = binding.buttonPanelTop.mainActionButton
                btn1.isEnabled = true
            }
            val dialog = builder.create()
            dialog.show()
        } else {
            Log.i("breed ", current_breed_id.toString())
            cmd = String.format(
                "UPDATE animal_breed_table SET id_breedid = %s WHERE id_animalid = %s ",
                current_breed_id,
                thisanimal_id
            )
            dbh.exec(cmd)
            Log.i("breed ", current_breed_id.toString())
            // Calculate the age in days for this animal for this evaluation to fill the age_in_days field
            cmd = String.format(
                "SELECT julianday(birth_date) FROM animal_table WHERE id_animalid = '%s'",
                thisanimal_id
            )
            Log.i("get birthdate eval ", cmd)
            dbh.exec(cmd)
            crsr3 = dbh.exec(cmd)
            cursor3 = crsr3 as Cursor
            dbh.moveToFirstRecord()
            temp_integer = Utilities.GetJulianDate().toInt() - (dbh.getInt(0))
            Log.i("get age in days ", temp_integer.toString())
            cmd = String.format(
                "INSERT INTO animal_evaluation_table (id_animalid, " +
                        "trait_name01, trait_score01, trait_name02, trait_score02, trait_name03, trait_score03, " +
                        "trait_name04, trait_score04, trait_name05, trait_score05, trait_name06, trait_score06," +
                        "trait_name07, trait_score07, trait_name08, trait_score08, trait_name09, trait_score09, " +
                        "trait_name10, trait_score10, trait_name11, trait_score11, trait_name12, trait_score12, " +
                        "trait_name13, trait_score13, trait_name14, trait_score14, trait_name15, trait_score15, " +
                        "trait_name16, trait_score16, trait_name17, trait_score17, trait_name18, trait_score18, " +
                        "trait_name19, trait_score19, trait_name20, trait_score20, animal_rank, number_animals_ranked, " +
                        "trait_units11, trait_units12, trait_units13, trait_units14, trait_units15, eval_date, eval_time, age_in_days, created, modified) " +
                        "values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s," +
                        "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,'%s','%s', %s,%s,%s,%s,%s,'%s','%s',%s,'%s','%s') ",
                thisanimal_id,
                trait01,
                trait01_data,
                trait02,
                trait02_data,
                trait03,
                trait03_data,
                trait04,
                trait04_data,
                trait05,
                trait05_data,
                trait06,
                trait06_data,
                trait07,
                trait07_data,
                trait08,
                trait08_data,
                trait09,
                trait09_data,
                trait10,
                trait10_data,
                trait11,
                trait11_data,
                trait12,
                trait12_data,
                trait13,
                trait13_data,
                trait14,
                trait14_data,
                trait15,
                trait15_data,
                trait16,
                trait16_data,
                trait17,
                trait17_data,
                trait18,
                trait18_data,
                trait19,
                trait19_data,
                trait20,
                trait20_data,
                blank_space,
                blank_space,
                trait11_unitid,
                trait12_unitid,
                trait13_unitid,
                trait14_unitid,
                trait15_unitid,
                mytoday,
                mytime,
                temp_integer,
                created,
                modified
            )
            Log.i("save eval ", cmd)
            dbh.exec(cmd)

            //  Go get the note if there is one
            TV = binding.noteText
            current_note = TV.text.toString()
            Log.i("add note ", "add to animal_note_table this note $current_note")
            //  Update the note if there is one
            if (TextUtils.isEmpty(current_note)) {
                // doing nothing
                Log.i("add note ", "empty note ")
            } else {
                // todo add a section for some pre-defined notes for standard problems or issues
                // Look in LambTracker for how that handles predefined notes.

                cmd = String.format(
                    "insert into animal_note_table (id_animalid, note_text, note_date, note_time, id_predefinednotesid, created, modified) " +
                            "Values (%s, '%s', '%s', '%s', %s, '%s', '%s')",
                    thisanimal_id, current_note, mytoday, mytime, '0', created, modified
                )
                //                    , thisanimal_id, StringEscapeUtils.current_note.replace("'","`"), mytoday, mytime);
                Log.i("add note ", "add to animal_note_table db cmd is $cmd")
                dbh.exec(cmd)
                Log.i("add note ", "after add to animal_note_table db cmd is $cmd")
            }
            // todo Need to see if the ownership history and location history tables are correct and if not then update them both
            // look for all the ownership records for this animal and then find the latest one
            cmd = String.format(
                "SELECT id_animalid, to_id_contactid FROM  (SELECT id_animalid, MAX(transfer_date) " +
                        " , to_id_contactid, id_animalownershiphistoryid FROM animal_ownership_history_table GROUP BY id_animalid) " +
                        " AS last_owner WHERE id_animalid = %s ", thisanimal_id
            )
            Log.i("ownership history ", " db cmd is $cmd")
            ownercrsr = dbh.exec(cmd)
            ownerCursor = ownercrsr as Cursor
            dbh.moveToFirstRecord()
            last_owner = (ownerCursor.getInt(1))
            if (last_owner == owner_id_contactsid) {
                // same last owner no need to change the info
                Log.i("update ownership", " same last owner ")
            } else {
                // need to update owner and location records
                // todo hard coded for sold for breeding might need to change
                cmd = String.format(
                    "INSERT into animal_ownership_history_table (id_animalid, transfer_date, from_id_contactid, " +
                            "to_id_contactid, id_transferreasonid, sell_price, sell_price_id_unitsid, created, modified) " +
                            "Values (%s, '%s', %s, %s, %s, %s, %s, '%s', '%s' )",
                    thisanimal_id,
                    mytoday,
                    last_owner,
                    owner_id_contactsid,
                    1,
                    0.0,
                    1,
                    created,
                    modified
                )
                Log.i("update ownership", " history db cmd is $cmd")
                dbh.exec(cmd)
                // now update the location history table
                // first get the last location for this animal
                cmd = String.format(
                    "SELECT id_animalid, to_id_premiseid FROM  (SELECT id_animalid, MAX(movement_date) " +
                            " , to_id_premiseid, id_animallocationhistoryid FROM animal_location_history_table GROUP BY id_animalid) " +
                            " AS last_location WHERE id_animalid = %s ", thisanimal_id
                )
                Log.i("update location", " history db cmd is $cmd")
                locationcrsr = dbh.exec(cmd)
                locationCursor =locationcrsr as Cursor
                dbh.moveToFirstRecord()
                last_location = (locationCursor.getInt(1))

                // Next get the new premise where the animal is going

                // todo currently putting in the old owner's premise id for both from and to need a query here the cross references the new owner to the correct premise
                to_id_premiseid = last_location

                cmd = String.format(
                    "INSERT into animal_location_history_table (id_animalid, movement_date, from_id_premiseid, " +
                            "to_id_premiseid, created, modified ) Values (%s, '%s', %s, %s, '%s', '%s')",
                    thisanimal_id,
                    mytoday,
                    last_location,
                    to_id_premiseid,
                    created,
                    modified
                )
                dbh.exec(cmd)
            }

            // todo this fails if you look up mixed species animals due to the breed spinner being set to the wrong species' list
            // TODO: SpinnerUpdates - make sure this becomes a local extracted from selectedBreed
            current_breed_id = selectedBreed?.id!!

            //        Get the breed abbreviation for the selected breed and add to the summary info.
            cmd = String.format(
                "SELECT breed_abbrev from breed_table where id_breedid = '%s'",
                current_breed_id
            )
            breedcrsr = dbh.exec(cmd)
            breedCursor = breedcrsr as Cursor
            dbh.moveToFirstRecord()
            //        Add the breed code to the summary data here
            breed_code = breedCursor.getString(BreedTable.Columns.ABBREVIATION) // Breed code

            //TODO: SpinnerUpdates - make this an Int ID.
            ageinyears = selectedAgeYears.toString()
            //        Trait11 is scrotal circumgerence, trait 14 is body condition score
            summary_data = LastEID.substring(
                13,
                16
            ) + " - " + ageinyears + " - " + breed_code + " - " + trait11_data.toString() + " - " + trait14_data.toString()
            Log.i("summary", summary_data)

            //  Set update database button green
            btn = binding.buttonPanelTop.mainActionButton
            btn.isEnabled = true
        }
    }

    public override fun onResume() {
        super.onResume()
        if (checkRequiredPermissions()) {
            Log.i("Breeding", " OnResume")
            CheckIfServiceIsRunning()

            //		scanEid( null );
            Log.i("Breeding", " OnResume after Check if service is running")
            // Re-draw the screen when we come back from the update tags activity
            //TODO: Check on the necessity of this, but if it is required make it reload selected animal, not search for animal.
            //lookForAnimal()
        }
        bluetoothWatcher.startWatching()
    }

    public override fun onPause() {
        super.onPause()
        Log.i("Breeding", " OnPause")
        doUnbindService()
        bluetoothWatcher.stopWatching()
    }

    fun doNote(v: View?) {
        Utilities.takeNote(v, thisanimal_id, this)
    }

    private fun addAnimalWithEID(eid: String) {
        Log.i("addAnimal", " at the beginning")
        AlertDialog.Builder(this)
            .setMessage(R.string.add_bull)
            .setTitle(R.string.add_bull)
            .setPositiveButton(R.string.yes_label) { _, _ -> // User clicked yes button
                val intent = Intent(this@MaleBreedingSoundnessActivity, LegacyAddAnimalActivity::class.java)
                    .putExtra("tagtype", IdType.ID_TYPE_ID_EID)
                    .putExtra("tagnumber", eid)
                this@MaleBreedingSoundnessActivity.startActivity(intent)
            }
            .setNegativeButton(R.string.no_label) { dialog, _ -> // User clicked no button
                dialog.dismiss()
                clearBtn()
            }
            .create()
            .show()
    }

    private fun clearBtn() {
        // clear out the display of everything
        var TV: TextView
        var sv: ScrollView
        //  Set update database button green
        binding.buttonPanelTop.mainActionButton.isEnabled = true
        var table: TableLayout
        binding.noteText.setText("")
        //  Clear out the tag list
        Log.i("clear btn", "before changing myadapter")
        try {
            myadapter.changeCursor(null)
        } catch (e: Exception) {
            // In this case there is no adapter so do nothing
        }
        try {
            //	Clear the real scored traits
            table = binding.TableLayout02
            if (numberrealdataitems != 0) {
                for (ii in 0 until numberrealdataitems) {
                    val row1 = table.getChildAt(ii) as TableRow
                    TV = row1.getChildAt(1) as EditText
                    TV.setText("")
                }
                //  move the scroll view up to the top of the real traits
                sv = binding.scroll02
                sv.scrollTo(0, 0)
            }
            //	Clear the radio group checks
//			Log.i("in clear button", " number radio group traits is " + String.valueOf(nRecs3));
            table = binding.TableLayout03
            if (numberuserdataitems != 0) {
                for (ii in 0 until numberuserdataitems) {
                    val row1 = table.getChildAt(ii) as TableRow
                    val rg = (row1.findViewById<View>(R.id.radioGroup1) as RadioGroup)
                    rg.clearCheck()
                }
                // move the scroll view up to the top of the radio traits
                sv = binding.scroll03
                sv.scrollTo(0, 0)
            }
        } catch (e: Exception) {
            //	something failed so log it
            Log.i("in clear button", " in catch of try clearing rating, real and radio groups ")
        }
        // make the alert button normal and disabled
        binding.buttonPanelTop.showAlertButton.isEnabled = false
        //	make the scan eid button red
        binding.buttonPanelTop.scanEIDButton.isEnabled = true
    }

    fun tableExists(table: String): Boolean {
        try {
            dbh.exec("select * from $table")
            return true
        } catch (e: SQLiteException) {
            return false
        }
    }

    private fun showAlert() {
        // Display alerts here
        val builder = AlertDialog.Builder(this)
        builder.setMessage(alert_text)
            .setTitle(R.string.alert_warning)
        builder.setPositiveButton(R.string.ok) { dialog, idx ->
            // User clicked OK button
        }
        val dialog = builder.create()
        dialog.show()
    }

    private fun checkRequiredPermissions(): Boolean {
        if (!areFulfilled(this)) {
            MainActivity.returnFrom(this)
            return false
        }
        return true
    }

    private fun onBluetoothActivationChanged() {
        checkRequiredPermissions()
    }

    public override fun onDestroy() {
        super.onDestroy()
        dbh.close()
    }

    companion object {
        private const val REQUEST_CODE_SELECT_ANIMAL = 200
    }
}