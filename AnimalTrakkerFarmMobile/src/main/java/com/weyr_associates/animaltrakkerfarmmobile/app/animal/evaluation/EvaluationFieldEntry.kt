package com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation

enum class EvaluationFieldEntry {
    REQUIRED,
    OPTIONAL,
    UNCOLLECTED;

    val isNotRequired
        get() = this != REQUIRED

    val isUncollected
        get() = this == UNCOLLECTED
}
