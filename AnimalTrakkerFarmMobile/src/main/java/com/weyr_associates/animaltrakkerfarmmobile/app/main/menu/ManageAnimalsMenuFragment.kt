package com.weyr_associates.animaltrakkerfarmmobile.app.main.menu

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.weyr_associates.animaltrakkerfarmmobile.Counter
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.SimpleAddAnimalActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.ManageAnimalIdsActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.core.checkDatabaseIsPresentThen
import com.weyr_associates.animaltrakkerfarmmobile.databinding.FragmentMenuManageAnimalsBinding

class ManageAnimalsMenuFragment : Fragment(R.layout.fragment_menu_manage_animals) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(FragmentMenuManageAnimalsBinding.bind(view)) {
            btnScanCountLog.setOnClickListener {
                checkDatabaseIsPresentThen {
                    startActivity(Intent(requireActivity(), Counter::class.java))
                }
            }
            btnSimpleAddAnimal.setOnClickListener {
                checkDatabaseIsPresentThen {
                    startActivity(Intent(requireActivity(), SimpleAddAnimalActivity::class.java))
                }
            }
            btnUpdateAnimalId.setOnClickListener {
                checkDatabaseIsPresentThen {
                    startActivity(Intent(requireActivity(), ManageAnimalIdsActivity::class.java))
                }
            }
            listOf(
                btnAnimalDeaths,
                btnUpdateAnimalDetails
            ).forEach { it.deactivate() }
        }
    }
}
