package com.weyr_associates.animaltrakkerfarmmobile.app.animal.add

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.viewmodel.CreationExtras
import androidx.preference.PreferenceManager
import com.weyr_associates.animaltrakkerfarmmobile.BluetoothWatcher
import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.RequiredPermissions.areFulfilled
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.SimpleAddAnimalViewModel.Event
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.SimpleAddAnimalViewModel.InputEvent
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.SimpleAddAnimalViewModel.UpdateDatabaseEvent
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.SimpleAddAnimalViewModel.ValidationError
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.AutoIncrementNextTrichIdFeature
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.DuplicationOfEIDs
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.IdEntry
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.IdValidationErrorDialog
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.IdValidations
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.IdsValidationError
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.IdsValidationError.ExceededEIDOfficialIdLimits
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.IdsValidationError.ExceededIdLimitForIdType
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.IdsValidationError.RequiredOfficialIdsNotMet
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.coroutines.flow.observeOneTimeEvents
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.ItemSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.TopButtonBar
import com.weyr_associates.animaltrakkerfarmmobile.app.main.MainActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.select.AbbreviationDisplayTextProvider
import com.weyr_associates.animaltrakkerfarmmobile.app.select.ageMonthsSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.ageYearsSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.breedSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.idColorSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.idLocationSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.idTypeSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.optionalIdColorSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.optionalIdLocationSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.optionalIdTypeSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.ownerSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.sexSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.ActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.LoadActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ActivitySimpleAddAnimalBinding
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderService
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderStatePresenter
import com.weyr_associates.animaltrakkerfarmmobile.model.Breed
import com.weyr_associates.animaltrakkerfarmmobile.model.IdColor
import com.weyr_associates.animaltrakkerfarmmobile.model.IdLocation
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType
import com.weyr_associates.animaltrakkerfarmmobile.model.Owner
import com.weyr_associates.animaltrakkerfarmmobile.model.Sex
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.AnimalRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.BreedRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.DefaultSettingsRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.IdColorRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.IdLocationRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.IdTypeRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.OwnerRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.PremiseRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.SexRepositoryImpl
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class SimpleAddAnimalActivity : AppCompatActivity() {

    companion object {
        fun newIntent(context: Context) = Intent(context, SimpleAddAnimalActivity::class.java)

        fun startToAddAndSelect(activity: Activity, primaryIdType: Int, primaryIdNumber: String, requestCode: Int) {
            activity.startActivityForResult(
                Intent(activity, SimpleAddAnimalActivity::class.java).apply {
                    action = AddAnimal.ACTION_ADD_AND_SELECT
                    putExtra(AddAnimal.EXTRA_PRIMARY_ID_TYPE_ID, primaryIdType)
                    putExtra(AddAnimal.EXTRA_PRIMARY_ID_NUMBER, primaryIdNumber)
                },
                requestCode
            )
        }

        private const val REQUEST_SELECT_ID_TYPE_1 = "REQUEST_SELECT_TAG_TYPE_1"
        private const val REQUEST_SELECT_ID_TYPE_2 = "REQUEST_SELECT_TAG_TYPE_2"
        private const val REQUEST_SELECT_ID_TYPE_3 = "REQUEST_SELECT_TAG_TYPE_3"
        private const val REQUEST_SELECT_ID_LOCATION_1 = "REQUEST_SELECT_ID_LOCATION_1"
        private const val REQUEST_SELECT_ID_LOCATION_2 = "REQUEST_SELECT_ID_LOCATION_2"
        private const val REQUEST_SELECT_ID_LOCATION_3 = "REQUEST_SELECT_ID_LOCATION_3"
        private const val REQUEST_SELECT_ID_COLOR_1 = "REQUEST_SELECT_ID_COLOR_1"
        private const val REQUEST_SELECT_ID_COLOR_2 = "REQUEST_SELECT_ID_COLOR_2"
        private const val REQUEST_SELECT_ID_COLOR_3 = "REQUEST_SELECT_ID_COLOR_3"
    }

    private val viewModel: SimpleAddAnimalViewModel
        by viewModels<SimpleAddAnimalViewModel> {
            ViewModelFactory(this)
        }

    private lateinit var binding: ActivitySimpleAddAnimalBinding

    private lateinit var breedPresenter: ItemSelectionPresenter<Breed>
    private lateinit var ageYearsPresenter: ItemSelectionPresenter<Int>
    private lateinit var ageMonthsPresenter: ItemSelectionPresenter<Int>
    private lateinit var sexPresenter: ItemSelectionPresenter<Sex>
    private lateinit var ownerPresenter: ItemSelectionPresenter<Owner>

    private lateinit var idType1Presenter: ItemSelectionPresenter<IdType>
    private lateinit var idColor1Presenter: ItemSelectionPresenter<IdColor>
    private lateinit var idLocation1Presenter: ItemSelectionPresenter<IdLocation>

    private lateinit var idType2Presenter: ItemSelectionPresenter<IdType>
    private lateinit var idColor2Presenter: ItemSelectionPresenter<IdColor>
    private lateinit var idLocation2Presenter: ItemSelectionPresenter<IdLocation>

    private lateinit var idType3Presenter: ItemSelectionPresenter<IdType>
    private lateinit var idColor3Presenter: ItemSelectionPresenter<IdColor>
    private lateinit var idLocation3Presenter: ItemSelectionPresenter<IdLocation>

    private lateinit var eidReaderStatePresenter: EIDReaderStatePresenter

    private var mService: Messenger? = null
    private var mIsBound = false
    private val mMessenger: Messenger = Messenger(IncomingHandler(Looper.getMainLooper()))
    private val bluetoothWatcher = BluetoothWatcher(this)
    private val mConnection: ServiceConnection = EidServiceConnection()

    private var breedCollectionJob: Job? = null
    private var sexCollectionJob: Job? = null

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySimpleAddAnimalBinding.inflate(layoutInflater)
        setContentView(binding.getRoot())
        checkIfServiceIsRunning()
        setupTopButtonBar()
        setupInputFields()
        setupBreedAndSexSpinner()
        setupAgeSpinners()
        setupOwnerSpinner()
        setupIdTypeSpinners()
        setupTagLocationSpinners()
        setupTagColorSpinners()
        bindToEvents()
        eidReaderStatePresenter = EIDReaderStatePresenter(this, binding.buttonPanelTop.imageScannerStatus)
        bluetoothWatcher.onActivationChanged = { onBluetoothActivationChanged() }
    }

    public override fun onResume() {
        super.onResume()
        Log.i("SimpleAdd", " OnResume")
        if (checkRequiredPermissions()) {
            checkIfServiceIsRunning()
            Log.i("SimpleAdd", " OnResume after Check ")
        }
        bluetoothWatcher.startWatching()
    }

    override fun onPause() {
        super.onPause()
        bluetoothWatcher.stopWatching()
    }

    private fun setupTopButtonBar() {
        binding.buttonPanelTop.show(
            TopButtonBar.UI_SCANNER_STATUS or
                    TopButtonBar.UI_SCAN_EID or
                    TopButtonBar.UI_CLEAR_DATA or
                    TopButtonBar.UI_ACTION_UPDATE_DATABASE
        )
        binding.buttonPanelTop.scanEIDButton.setOnClickListener { scanEid() }
        binding.buttonPanelTop.clearDataButton.setOnClickListener {
            viewModel.clearData()
        }
        binding.buttonPanelTop.mainActionButton.setOnClickListener {
            viewModel.saveToDatabase()
        }
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.canClearData.collectLatest { canClear ->
                    binding.buttonPanelTop.clearDataButton.isEnabled = canClear
                }
            }
        }
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.canSaveToDatabase.collectLatest { canSave ->
                    binding.buttonPanelTop.mainActionButton.isEnabled = canSave
                }
            }
        }
    }

    private fun setupInputFields() {
        with(binding.inputAnimalName) {
            setText(viewModel.animalName)
            addTextChangedListener {
                viewModel.animalName = it.toString()
            }
        }

        with(binding.inputAnimalId1.inputIdNumber) {
            setText(viewModel.idNumber1)
            addTextChangedListener {
                viewModel.idNumber1 = it.toString()
            }
        }

        with(binding.inputAnimalId2.inputIdNumber) {
            setText(viewModel.idNumber2)
            addTextChangedListener {
                viewModel.idNumber2 = it.toString()
            }
        }

        with(binding.inputAnimalId3.inputIdNumber) {
            setText(viewModel.idNumber3)
            addTextChangedListener {
                viewModel.idNumber3 = it.toString()
            }
        }
    }

    private fun setupBreedAndSexSpinner() {
        //Reassign breed and sex presenters
        //whenever the species ID changes
        //so proper breed and sex options are available.
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.speciesId.collect { speciesId ->
                    breedCollectionJob?.cancel()
                    breedPresenter = breedSelectionPresenter(
                        speciesId = speciesId,
                        button = binding.animalBreedSpinner,
                        hintText = getString(R.string.hint_select_breed)
                    ) { breed -> viewModel.selectBreed(breed) }.also {
                        breedCollectionJob = it.bindToFlow(
                            this@SimpleAddAnimalActivity,
                            lifecycleScope,
                            viewModel.selectedBreed
                        )
                    }
                    sexCollectionJob?.cancel()
                    sexPresenter = sexSelectionPresenter(
                        speciesId = speciesId,
                        button = binding.animalSexSpinner,
                        hintText = getString(R.string.hint_select_sex)
                    ) { sex -> viewModel.selectSex(sex) }.also {
                        sexCollectionJob = it.bindToFlow(
                            this@SimpleAddAnimalActivity,
                            lifecycleScope,
                            viewModel.selectedSex
                        )
                    }
                }
            }
        }
    }

    private fun setupAgeSpinners() {
        ageYearsPresenter = ageYearsSelectionPresenter(
            button = binding.ageYearSpinner,
            hintText = getString(R.string.hint_select_age_years)
        ) { ageYears -> viewModel.selectAgeYears(ageYears) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedAgeYears)
        }

        ageMonthsPresenter = ageMonthsSelectionPresenter(
            button = binding.ageMonthSpinner,
            hintText = getString(R.string.hint_select_age_months)
        ) { ageMonths -> viewModel.selectAgeMonths(ageMonths) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedAgeMonths)
        }
    }

    private fun setupOwnerSpinner() {
        ownerPresenter = ownerSelectionPresenter(
            button = binding.ownerNameSpinner,
            hintText = getString(R.string.hint_select_owner)
        ) { owner -> viewModel.selectOwner(owner) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedOwner)
        }
    }

    private fun setupIdTypeSpinners() {
        idType1Presenter = idTypeSelectionPresenter(
            button = binding.inputAnimalId1.spinnerIdType,
            requestKey = REQUEST_SELECT_ID_TYPE_1
        ) { idType -> viewModel.selectIdType1(idType) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdType1)
        }
        idType2Presenter = optionalIdTypeSelectionPresenter(
            button = binding.inputAnimalId2.spinnerIdType,
            requestKey = REQUEST_SELECT_ID_TYPE_2
        ) { idType -> viewModel.selectIdType2(idType) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdType2)
        }
        idType3Presenter = optionalIdTypeSelectionPresenter(
            button = binding.inputAnimalId3.spinnerIdType,
            requestKey = REQUEST_SELECT_ID_TYPE_3
        ) { idType -> viewModel.selectIdType3(idType) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdType3)
        }
    }

    private fun setupTagLocationSpinners() {
        idLocation1Presenter = idLocationSelectionPresenter(
            button = binding.inputAnimalId1.spinnerIdLocation,
            requestKey = REQUEST_SELECT_ID_LOCATION_1,
            itemDisplayTextProvider = AbbreviationDisplayTextProvider
        ) { idLocation -> viewModel.selectIdLocation1(idLocation) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdLocation1)
        }
        idLocation2Presenter = optionalIdLocationSelectionPresenter(
            button = binding.inputAnimalId2.spinnerIdLocation,
            requestKey = REQUEST_SELECT_ID_LOCATION_2,
            itemDisplayTextProvider = AbbreviationDisplayTextProvider
        ) { idLocation -> viewModel.selectIdLocation2(idLocation) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdLocation2)
        }
        idLocation3Presenter = optionalIdLocationSelectionPresenter(
            button = binding.inputAnimalId3.spinnerIdLocation,
            requestKey = REQUEST_SELECT_ID_LOCATION_3,
            itemDisplayTextProvider = AbbreviationDisplayTextProvider
        ) { idLocation -> viewModel.selectIdLocation3(idLocation) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdLocation3)
        }
    }

    private fun setupTagColorSpinners() {
        idColor1Presenter = idColorSelectionPresenter(
            button = binding.inputAnimalId1.spinnerIdColor,
            requestKey = REQUEST_SELECT_ID_COLOR_1,
            itemDisplayTextProvider = AbbreviationDisplayTextProvider
        ) { idColor -> viewModel.selectIdColor1(idColor) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdColor1)
        }
        idColor2Presenter = optionalIdColorSelectionPresenter(
            button = binding.inputAnimalId2.spinnerIdColor,
            requestKey = REQUEST_SELECT_ID_COLOR_2,
            itemDisplayTextProvider = AbbreviationDisplayTextProvider
        ) { idColor -> viewModel.selectIdColor2(idColor) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdColor2)
        }
        idColor3Presenter = optionalIdColorSelectionPresenter(
            button = binding.inputAnimalId3.spinnerIdColor,
            requestKey = REQUEST_SELECT_ID_COLOR_3,
            itemDisplayTextProvider = AbbreviationDisplayTextProvider
        ) { idColor -> viewModel.selectIdColor3(idColor) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdColor3)
        }
    }

    private fun bindToEvents() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.events.observeOneTimeEvents {
                    handleEvent(it)
                }
            }
        }
    }

    // use EID reader to look up an animal
    private fun onEIDScanned(scannedEID: String) {
        viewModel.onEIDScanned(scannedEID)
        Log.i("in onEIDScanned ", "with EID of $scannedEID")
    }

    private fun handleEvent(event: Event) {
        when (event) {
            is InputEvent -> handleInputEvent(event)
            is ValidationError -> handleValidationError(event)
            is UpdateDatabaseEvent -> handleUpdateDatabaseEvent(event)
        }
    }

    private fun handleInputEvent(inputEvent: InputEvent) {
        when (inputEvent) {
            InputEvent.AnimalNameChanged -> {
                binding.inputAnimalName.setText(viewModel.animalName)
            }
            InputEvent.IdNumber1Changed -> {
                binding.inputAnimalId1.inputIdNumber.setText(viewModel.idNumber1)
            }
            InputEvent.IdNumber2Changed -> {
                binding.inputAnimalId2.inputIdNumber.setText(viewModel.idNumber2)
            }
            InputEvent.IdNumber3Changed -> {
                binding.inputAnimalId3.inputIdNumber.setText(viewModel.idNumber3)
            }
        }
    }

    private fun handleValidationError(validationError: ValidationError) {
        when (validationError) {
            ValidationError.IncompleteAnimalEntry -> {
                showIncompleteAnimalEntryError()
            }
            ValidationError.IdEntryRequired -> {
                IdValidationErrorDialog.showIdEntryIsRequiredError(this)
            }
            ValidationError.PartialIdEntry -> {
                IdValidationErrorDialog.showPartialIdEntryError(this)
            }
            is ValidationError.InvalidIdNumberFormat -> {
                IdValidationErrorDialog.showIdNumberFormatError(this, validationError.idEntry)
            }
            is ValidationError.InvalidIdCombination -> {
                IdValidationErrorDialog.showIdCombinationError(this, validationError.error)
            }
        }
    }

    private fun handleUpdateDatabaseEvent(event: UpdateDatabaseEvent) {
        when (event) {
            is UpdateDatabaseEvent.Success -> {
                if (intent?.action == AddAnimal.ACTION_ADD_AND_SELECT) {
                    //TODO: Pass this animal id as a long and not an int once IDs are sorted out as longs.
                    setResult(RESULT_OK, Intent().apply {
                        putExtra(AddAnimal.EXTRA_RESULTING_ANIMAL_ID, event.animalId.toInt())
                        putExtra(AddAnimal.EXTRA_RESULTING_ANIMAL_NAME, event.animalName)
                    })
                    finish()
                } else {
                    //TODO: Discuss what, if anything else, to show here.
                    Toast.makeText(
                        this,
                        getString(R.string.toast_add_animal_success, event.animalName),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            is UpdateDatabaseEvent.Error -> {
                //TODO: Discuss what, if anything else, to show here.
                Toast.makeText(
                    this,
                    getString(R.string.toast_add_animal_failure, event.animalName),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun showIncompleteAnimalEntryError() {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title_incomplete_animal_entry)
            .setMessage(R.string.dialog_message_incomplete_animal_entry)
            .setPositiveButton(R.string.ok) { _, _ -> /*NO-OP*/ }
            .create()
            .show()
    }

    //  user clicked 'Scan' button
    private fun scanEid() {
        if (mService != null) {
            try {
                //Start eidService sending tags
                val msg = Message.obtain(null, EIDReaderService.MSG_SEND_ME_TAGS)
                msg.replyTo = mMessenger
                mService!!.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
    }

    private fun checkRequiredPermissions(): Boolean {
        if (!areFulfilled(this)) {
            MainActivity.returnFrom(this)
            return false
        }
        return true
    }

    private fun onBluetoothActivationChanged() {
        checkRequiredPermissions()
    }

    private fun updateEidServiceConnectionStatus(eidReaderState: String) {
        eidReaderStatePresenter.updateReaderState(eidReaderState)
    }

    private fun checkIfServiceIsRunning() {
        //If the service is running when the activity starts, we want to automatically bind to it.
        Log.i("SimpleAdd", "At eid isRunning?.")
        if (EIDReaderService.isRunning()) {
//			Log.i("SimpleAdd", " eidService is running");
            doBindEidService()
        } else {
//			Log.i("SimpleAdd", " eidService is not running, start it");
            startService(Intent(this@SimpleAddAnimalActivity, EIDReaderService::class.java))
            doBindEidService()
        }
    }

    //  Bind services, should be one set of bind & unbind methods for eid, scale and baacode
    //  First is the eid service bind
    private fun doBindEidService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
//		Log.i("SimpleAdd", "At doBind1.");
        bindService(Intent(this, EIDReaderService::class.java), mConnection, BIND_AUTO_CREATE)
        //		Log.i("SimpleAdd", "At doBind2.");
        mIsBound = true
        mService?.let { service ->
//			Log.i("SimpleAdd", "At doBind3.");
            try {
                //Request status update
                var msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_STATUS, 0, 0)
                msg.replyTo = mMessenger
                service.send(msg)
                //				Log.i("SimpleAdd", "At doBind4.");
                //Request full log from service.
                msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_LOG_FULL, 0, 0)
                service.send(msg)
            } catch (e: RemoteException) {
                //NO-OP
            }
        }
        //		Log.i("Breeding", "At doBind5.");
    }

    //     Next the unbind eid servides
    private fun doUnbindEidService() {
//		Log.i("SimpleAdd", "At DoUnbindservice");
        mService?.let { service ->
            try {
                //Stop eidService from sending tags
                val msg = Message.obtain(null, EIDReaderService.MSG_NO_TAGS_PLEASE)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        if (mIsBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            mService?.let { service ->
                try {
                    val msg = Message.obtain(null, EIDReaderService.MSG_UNREGISTER_CLIENT)
                    msg.replyTo = mMessenger
                    service.send(msg)
                } catch (e: RemoteException) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection)
            mIsBound = false
        }
    }

    private inner class IncomingHandler(looper: Looper) : Handler(looper) {
        override fun handleMessage(msg: Message) {
            val bundle = msg.data
            when (msg.what) {
                EIDReaderService.MSG_UPDATE_STATUS -> {
                    val state: String = bundle.getString("stat", EIDReaderService.STATE_STRING_NONE)
                    updateEidServiceConnectionStatus(state)
                }

                EIDReaderService.MSG_NEW_EID_FOUND -> {
                    val scannedEID = bundle.getString("info1")
                    if (scannedEID != null) {
                        onEIDScanned(scannedEID)
                    }
                    //We have a good whole EID number
                }

                EIDReaderService.MSG_UPDATE_LOG_APPEND -> {}
                EIDReaderService.MSG_UPDATE_LOG_FULL -> {}
                EIDReaderService.MSG_THREAD_SUICIDE -> {
                    //				    Log.i("BullTest", "Service informed Activity of Suicide.");
                    doUnbindEidService()
                    stopService(Intent(this@SimpleAddAnimalActivity, EIDReaderService::class.java))
                }

                else -> super.handleMessage(msg)
            }
        }
    }

    private inner class EidServiceConnection : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            mService = Messenger(service)
            try {
                //Register client with service
                val msg = Message.obtain(null, EIDReaderService.MSG_REGISTER_CLIENT)
                msg.replyTo = mMessenger
                mService?.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }

        //  *** This is the eid service disconnection
        override fun onServiceDisconnected(className: ComponentName) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            Log.i("SimpleAdd", "At eid Service Disconnected.")
            mService = null
        }
    }
}

private class ViewModelFactory(context: Context) : ViewModelProvider.Factory {

    private val appContext = context.applicationContext

    override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
        when(modelClass) {
            SimpleAddAnimalViewModel::class.java -> {
                val dbh = DatabaseHandler.create(appContext)
                val activeDefSettings = ActiveDefaultSettings(
                    PreferenceManager.getDefaultSharedPreferences(appContext)
                )
                val defSettingsRepo = DefaultSettingsRepositoryImpl(dbh)
                val loadActiveDefaultSettings = LoadActiveDefaultSettings(
                    activeDefaultSettings = activeDefSettings,
                    defSettingsRepo
                )
                val animalRepo = AnimalRepositoryImpl(dbh)
                @Suppress("UNCHECKED_CAST")
                return SimpleAddAnimalViewModel(
                    savedStateHandle = extras.createSavedStateHandle(),
                    databaseHandler = dbh,
                    loadActiveDefaultSettings = loadActiveDefaultSettings,
                    animalRepo = animalRepo,
                    breedRepo = BreedRepositoryImpl(dbh),
                    sexRepo = SexRepositoryImpl(dbh),
                    ownerRepo = OwnerRepositoryImpl(dbh),
                    premiseRepo = PremiseRepositoryImpl(dbh),
                    idTypeRepo = IdTypeRepositoryImpl(dbh),
                    idColorRepo = IdColorRepositoryImpl(dbh),
                    idLocationRepo = IdLocationRepositoryImpl(dbh),
                    idValidations = IdValidations(animalRepo),
                    autoUpdateTrichId = AutoIncrementNextTrichIdFeature(
                        loadActiveDefaultSettings = loadActiveDefaultSettings,
                        defaultSettingsRepository = defSettingsRepo
                    )
                ) as T
            }
            else -> {
                throw IllegalStateException("${modelClass.simpleName} is not supported.")
            }
        }
    }
}
