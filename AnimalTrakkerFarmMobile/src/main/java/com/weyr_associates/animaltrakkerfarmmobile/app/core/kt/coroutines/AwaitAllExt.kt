package com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.coroutines

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.awaitAll

suspend fun <A, B, R> awaitAll(
    a: Deferred<A>,
    b: Deferred<B>,
    transform: suspend (A, B) -> R): R {
    awaitAll(a, b)
    return transform(
        a.await(),
        b.await()
    )
}

suspend fun <A, B, C, R> awaitAll(
    a: Deferred<A>,
    b: Deferred<B>,
    c: Deferred<C>,
    transform: suspend (A, B, C) -> R): R {
    awaitAll(a, b, c)
    return transform(
        a.await(),
        b.await(),
        c.await()
    )
}