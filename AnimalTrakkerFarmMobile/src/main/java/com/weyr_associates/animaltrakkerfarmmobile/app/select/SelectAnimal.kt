package com.weyr_associates.animaltrakkerfarmmobile.app.select

object SelectAnimal {
    const val EXTRA_SEX_STANDARD = "EXTRA_SEX_STANDARD"
    const val EXTRA_SELECTED_ANIMAL_ID = "EXTRA_SELECTED_ANIMAL_ID"
}
