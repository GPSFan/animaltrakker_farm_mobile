package com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue

import android.app.AlertDialog
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import android.preference.PreferenceManager
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.viewmodel.CreationExtras
import com.weyr_associates.animaltrakkerfarmmobile.BluetoothWatcher
import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.RequiredPermissions
import com.weyr_associates.animaltrakkerfarmmobile.StartMenu
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.AnimalBasicInfoPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.AnimalDialogs
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.AddAnimal
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue.TissueSampleViewModel.AnimalSpeciesMismatch
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue.TissueSampleViewModel.IncompleteDataEntry
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue.TissueSampleViewModel.InputEvent
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue.TissueSampleViewModel.PrintLabelRequestedError
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue.TissueSampleViewModel.PrintLabelRequestedEvent
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue.TissueSampleViewModel.UpdateDatabaseEvent
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.ManageAnimalIdsViewModel
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.note.TakeNotesActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.setupForAnimalNotFound
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.setupForAnimalWithEIDNotFound
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.setupForInitialLookup
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.coroutines.flow.observeOneTimeEvents
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.ItemSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.TopButtonBar
import com.weyr_associates.animaltrakkerfarmmobile.app.label.ExtractPrintLabelData
import com.weyr_associates.animaltrakkerfarmmobile.app.label.PrintLabel
import com.weyr_associates.animaltrakkerfarmmobile.app.label.PrintLabelData
import com.weyr_associates.animaltrakkerfarmmobile.app.main.MainActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.select.SelectAnimal
import com.weyr_associates.animaltrakkerfarmmobile.app.select.SelectAnimalActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.select.laboratorySelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.tissueSampleContainerTypeSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.tissueSampleTypeSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.tissueTestSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.ActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.LoadActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.LoadDefaultIdTypeIds
import com.weyr_associates.animaltrakkerfarmmobile.baacodeService
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ActivityTissueSampleBinding
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderService
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderStatePresenter
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalBasicInfo
import com.weyr_associates.animaltrakkerfarmmobile.model.Laboratory
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueSampleContainerType
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueSampleType
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueTest
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.AnimalRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.DefaultSettingsRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.LaboratoryRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.SpeciesRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.TissueSampleContainerTypeRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.TissueSampleTypeRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.TissueTestRepositoryImpl
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class TissueSampleActivity : AppCompatActivity() {

    companion object {

        fun newIntent(
            context: Context,
            animalBasicInfo: AnimalBasicInfo,
            titleAddition: String? = null,
            defaultLabCompanyId: Int? = null,
            printLabelData: PrintLabelData? = null
        ) = Intent(context, TissueSampleActivity::class.java).apply {
                action = TissueSample.ACTION_TAKE_TISSUE_SAMPLE_FOR_ANIMAL
                putExtra(TissueSample.EXTRA_ANIMAL_BASIC_INFO, animalBasicInfo)
                titleAddition?.let { putExtra(TissueSample.EXTRA_TITLE_ADDITION, it) }
                defaultLabCompanyId?.let { putExtra(TissueSample.EXTRA_DEFAULT_LAB_COMPANY_ID, it) }
                printLabelData?.let { putExtra(TissueSample.EXTRA_PRINT_LABEL_DATA, it) }
            }

        private const val REQUEST_CODE_LOOKUP_ANIMAL = 200
        private const val REQUEST_CODE_ADD_AND_SELECT_ANIMAL = 201
        private const val REQUEST_CODE_PRINT_LABEL = 999
    }

    private var autoPrint = false
    private var readyToPrint = false

    private var mService: Messenger? = null
    private var mIsBound: Boolean = false
    private var mIsBAABound: Boolean = false
    private var mBaaCodeService: Messenger? = null

    private val mMessenger: Messenger = Messenger(IncomingHandler())

    private val viewModel by viewModels<TissueSampleViewModel> { ViewModelFactory(this) }

    private val binding: ActivityTissueSampleBinding by lazy {
        ActivityTissueSampleBinding.inflate(layoutInflater)
    }

    private var animalBasicInfoPresenter = AnimalBasicInfoPresenter()

    private lateinit var labNameSpinnerPresenter: ItemSelectionPresenter<Laboratory>

    private lateinit var sampleContainerTypeSpinnerPresenter: ItemSelectionPresenter<TissueSampleContainerType>

    private lateinit var sampleTypeSpinnerPresenter: ItemSelectionPresenter<TissueSampleType>

    private lateinit var sampleTestSpinnerPresenter: ItemSelectionPresenter<TissueTest>

    private val bluetoothWatcher = BluetoothWatcher(this)
    private lateinit var eidReaderStatePresenter: EIDReaderStatePresenter
    private lateinit var baaCodeReaderStatePresenter: EIDReaderStatePresenter

    //    ***    Service connections & disconnections one for each eidService baacodeService and scaleService
    private val mConnection: ServiceConnection = EidReaderServiceConnection()

    //  baacode service connection & disconnection
    private val mbaacodeConnection: ServiceConnection = BaacodeServiceConnection()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (intent.hasExtra(TissueSample.EXTRA_TITLE_ADDITION)) {
            val titleString = getString(R.string.title_activity_tissue_sample)
            val titleAddition = intent.getStringExtra(TissueSample.EXTRA_TITLE_ADDITION)
            title = "$titleString - $titleAddition"
        }
        setContentView(binding.root)
        loadPreferences()

        with(binding.buttonPanelTop) {

            if (intent.action == TissueSample.ACTION_TAKE_TISSUE_SAMPLE_FOR_ANIMAL) {
                show(
                    TopButtonBar.UI_SHOW_ALERT or
                            TopButtonBar.UI_TAKE_NOTE or
                            TopButtonBar.UI_CLEAR_DATA or
                            TopButtonBar.UI_ACTION_UPDATE_DATABASE
                )
            } else {
                show(TopButtonBar.UI_ALL or TopButtonBar.UI_ACTION_UPDATE_DATABASE)
            }

            eidReaderStatePresenter = EIDReaderStatePresenter(
                this@TissueSampleActivity, imageScannerStatus
            )
            scanEIDButton.setOnClickListener { onScanEID() }
            lookupAnimalButton.setOnClickListener { onLookupAnimal() }
            clearDataButton.setOnClickListener { viewModel.clearData() }
            mainActionButton.setOnClickListener { viewModel.saveToDatabase() }

            binding.buttonPanelTop.takeNoteButton.isEnabled = false
            binding.buttonPanelTop.showAlertButton.isEnabled = false
        }

        baaCodeReaderStatePresenter = EIDReaderStatePresenter(
            this@TissueSampleActivity, binding.imageBaacodeReaderStatus
        )

        animalBasicInfoPresenter.binding = binding.animalBasicInfo

        labNameSpinnerPresenter = laboratorySelectionPresenter(
            binding.labNameSpinner
        ) {
            viewModel.selectLaboratory(it)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedLaboratory)
        }

        sampleTypeSpinnerPresenter = tissueSampleTypeSelectionPresenter(
            binding.sampleTypeSpinner
        ) {
            viewModel.selectTissueSampleType(it)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedTissueSampleType)
        }

        sampleTestSpinnerPresenter = tissueTestSelectionPresenter(
            binding.sampleTestSpinner
        ) {
            viewModel.selectTissueTestType(it)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedTissueTestType)
        }

        sampleContainerTypeSpinnerPresenter = tissueSampleContainerTypeSelectionPresenter(
            binding.sampleContainerTypeSpinner
        ) {
            viewModel.selectTissueSampleContainerType(it)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectTissueContainerType)
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.canClearData.collectLatest { canClear ->
                    binding.buttonPanelTop.clearDataButton.isEnabled = canClear
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.canSaveToDatabase.collectLatest { canSave ->
                    binding.buttonPanelTop.mainActionButton.isEnabled = canSave
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.canScanTSU.collectLatest { canScan ->
                    binding.btnScanSampleContainer.isEnabled = canScan
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.canPrintLabel.collectLatest { canPrint ->
                    binding.printLabelBtn.isEnabled = canPrint
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.events.observeOneTimeEvents {
                    handleEvent(it)
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.animalInfoState.collectLatest { animalInfoState ->
                    updateTopBarAnimalActions(animalInfoState)
                    updateAnimalInfoDisplay(animalInfoState)
                }
            }
        }

        binding.sampleContainerId.addTextChangedListener { viewModel.containerId = it.toString() }
        binding.sampleContainerId.setText(viewModel.containerId)
        binding.sampleExpDate.addTextChangedListener { viewModel.containerExpDate = it.toString() }
        binding.sampleExpDate.setText(viewModel.containerExpDate)

        binding.btnScanSampleContainer.setOnClickListener { scanTSU() }
        binding.printLabelBtn.setOnClickListener { viewModel.printLabel() }

        bluetoothWatcher.onActivationChanged = {
            onBluetoothActivationChanged()
        }
    }

    override fun onResume() {
        super.onResume()
        if (checkRequiredPermissions()) {
            checkIfServiceIsRunning()
            checkForPrint()
        }
        bluetoothWatcher.startWatching()
    }

    public override fun onPause() {
        super.onPause()
        Log.i("Breeding", " OnPause")
        doUnbindService()
        doUnbindbaacodeService()
        bluetoothWatcher.stopWatching()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE_LOOKUP_ANIMAL) {
            if (resultCode == RESULT_OK && data != null && data.hasExtra(SelectAnimal.EXTRA_SELECTED_ANIMAL_ID)) {
                val selectedAnimalId = data.getIntExtra(SelectAnimal.EXTRA_SELECTED_ANIMAL_ID, -1)
                if (selectedAnimalId != -1) {
                    viewModel.onAnimalSelected(selectedAnimalId)
                }
            }
        }
        else if (requestCode == REQUEST_CODE_ADD_AND_SELECT_ANIMAL) {
            if (resultCode == RESULT_OK && data != null && data.hasExtra(AddAnimal.EXTRA_RESULTING_ANIMAL_ID)) {
                val resultingAnimalId = data.getIntExtra(AddAnimal.EXTRA_RESULTING_ANIMAL_ID, -1)
                if (resultingAnimalId != -1) {
                    viewModel.onAnimalSelected(resultingAnimalId)
                }
            }
        }
        else if (requestCode == 999) {
            if (resultCode == 444) {
                readyToPrint = true
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun onScanEID() {
        mService?.let { service ->
            try {
                //Start eidService sending tags
                val msg = Message.obtain(null, EIDReaderService.MSG_SEND_ME_TAGS)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
    }

    private fun onLookupAnimal() {
        startActivityForResult(
            Intent(this, SelectAnimalActivity::class.java),
            REQUEST_CODE_LOOKUP_ANIMAL
        )
    }

    private fun checkRequiredPermissions(): Boolean {
        if (!RequiredPermissions.areFulfilled(this)) {
            MainActivity.returnFrom(this)
            return false
        }
        return true
    }

    private fun onBluetoothActivationChanged() {
        checkRequiredPermissions()
    }

    //  Really should be check if services are running...
    //  Start the service if it is not already running and bind to it
    //  Should be one each for eid, scale and baacode
    private fun checkIfServiceIsRunning() {
        //If the service is running when the activity starts, we want to automatically bind to it.
        Log.i("Breeding", "At eid isRunning?.")
        if (EIDReaderService.isRunning()) {
//			Log.i("Breeding", " eidService is running");
            doBindService()
        } else {
//			Log.i("Breeding", " eidService is not running, start it");
            startService(Intent(this, EIDReaderService::class.java))
            doBindService()
        }
        if (baacodeService.isBaaRunning()) {
            doBindbaacodeService()
        } else {
            startService(Intent(this, baacodeService::class.java))
            doBindbaacodeService()
        }
    }

    private fun updateTopBarAnimalActions(animalInfoState: TissueSampleViewModel.AnimalInfoState) {
        when (animalInfoState) {
            is TissueSampleViewModel.AnimalInfoState.AnimalInfoLoaded -> {
                with(binding.buttonPanelTop) {
                    with(takeNoteButton) {
                        isEnabled = true
                        setOnClickListener {
                            startActivity(
                                TakeNotesActivity.newIntent(
                                    this@TissueSampleActivity,
                                    animalInfoState.animalBasicInfo.id
                                )
                            )
                        }
                    }
                    with(showAlertButton) {
                        val alertText = animalInfoState.animalBasicInfo.alert
                        when {
                            alertText.isNullOrBlank() -> {
                                isEnabled = false
                                setOnClickListener(null)
                            }
                            else -> {
                                isEnabled = true
                                setOnClickListener {
                                    AnimalDialogs.showAnimalAlert(this@TissueSampleActivity, alertText)
                                }
                            }
                        }
                    }
                }
            }
            else -> {
                with(binding.buttonPanelTop) {
                    with(takeNoteButton) {
                        isEnabled = false
                        setOnClickListener(null)
                    }
                    with(showAlertButton) {
                        isEnabled = false
                        setOnClickListener(null)
                    }
                }
            }
        }
    }

    private fun updateAnimalInfoDisplay(animalInfoState: TissueSampleViewModel.AnimalInfoState) {
        when (animalInfoState) {
            TissueSampleViewModel.AnimalInfoState.Initial -> {
                animalBasicInfoPresenter.animalBasicInfo = null
                binding.animalBasicInfo.root.isInvisible = true
                binding.containerNoAnimal.root.isVisible = true
                binding.containerNoAnimal.setupForInitialLookup()
            }
            is TissueSampleViewModel.AnimalInfoState.AnimalInfoLoaded -> {
                binding.animalBasicInfo.root.isVisible = true
                binding.containerNoAnimal.root.isGone = true
                animalBasicInfoPresenter.animalBasicInfo = animalInfoState.animalBasicInfo
            }
            is TissueSampleViewModel.AnimalInfoState.AnimalInfoNotFound -> {
                animalBasicInfoPresenter.animalBasicInfo = null
                binding.animalBasicInfo.root.isInvisible = true
                binding.containerNoAnimal.root.isVisible = true
                when (val lookup = animalInfoState.lookup) {
                    is TissueSampleViewModel.Lookup.ByAnimalId -> {
                        binding.containerNoAnimal.setupForAnimalNotFound()
                    }
                    is TissueSampleViewModel.Lookup.ByScannedEID -> {
                        binding.containerNoAnimal.setupForAnimalWithEIDNotFound(
                            this@TissueSampleActivity,
                            lookup.scannedEID,
                            REQUEST_CODE_ADD_AND_SELECT_ANIMAL
                        )
                    }
                }
            }
        }
    }

    private fun checkForPrint() {
        if (readyToPrint) {
            val print = Intent(this@TissueSampleActivity, StartMenu::class.java)
            print.putExtra(Intent.EXTRA_STREAM, StartMenu.tempFileForBarcodePrinting(this))
            print.setType("image/png")
            if (autoPrint) {
                print.addFlags(Intent.FLAG_ACTIVITY_NO_USER_ACTION)
            }
            startActivity(print)
            readyToPrint = false
            val btn = binding.printLabelBtn
            btn.isEnabled = false
            return
        }
    }

    private fun handlePrintLabelRequestedEvent(event: PrintLabelRequestedEvent) {
        val encodeIntent = PrintLabel.newIntentToEncode(event.printLabelData, autoPrint)
        startActivityForResult(encodeIntent, REQUEST_CODE_PRINT_LABEL)
    }

    private fun loadPreferences() {
        val preferences = PreferenceManager.getDefaultSharedPreferences(baseContext)
        autoPrint = preferences.getBoolean("autop", false)
    }

    private fun handleEvent(event: TissueSampleViewModel.Event) {
        when (event) {
            InputEvent.ContainerIdChanged -> {
                binding.sampleContainerId.setText(viewModel.containerId)
            }
            InputEvent.ContainerExpDateChanged -> {
                binding.sampleExpDate.setText(viewModel.containerExpDate)
            }
            IncompleteDataEntry -> {
                showIncompleteDataEntryError()
            }
            is AnimalSpeciesMismatch -> {
                showAnimalSpeciesMismatchPrompt(
                    event.defaultSpeciesName,
                    event.animalSpeciesName
                )
            }
            is PrintLabelRequestedEvent -> {
                handlePrintLabelRequestedEvent(event)
            }
            is PrintLabelRequestedError -> {
                showEIDRequiredToPrintError()
            }
            UpdateDatabaseEvent.Success -> {
                Toast.makeText(
                    this,
                    R.string.toast_add_tissue_test_success,
                    Toast.LENGTH_SHORT
                ).show()
            }
            UpdateDatabaseEvent.Error -> {
                Toast.makeText(
                    this,
                    R.string.toast_add_tissue_test_error,
                    Toast.LENGTH_SHORT
                ).show()
            }
            is TissueSampleViewModel.AnimalAlert -> {
                AnimalDialogs.showAnimalAlert(this, event.alertText)
            }
        }
    }

    private fun showIncompleteDataEntryError() {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title_incomplete_tissue_sample_entry)
            .setMessage(R.string.dialog_message_incomplete_tissue_sample_entry)
            .setPositiveButton(R.string.ok) { _, _ -> /*NO-OP*/ }
            .create()
            .show()
    }

    private fun showAnimalSpeciesMismatchPrompt(defaultSpecies: String, currentSpecies: String) {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title_animal_species_mismatch_prompt)
            .setMessage(
                getString(
                    R.string.dialog_message_animal_species_mismatch_prompt,
                    defaultSpecies,
                    currentSpecies
                )
            )
            .setPositiveButton(R.string.yes_label) { _, _ -> /*NO-OP*/ }
            .setNegativeButton(R.string.no_label) { _, _ -> viewModel.resetAnimalInfo() }
            .create()
            .show()
    }

    private fun showEIDRequiredToPrintError() {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title_eid_required_to_print_label)
            .setMessage(R.string.dialog_message_eid_required_to_print_label)
            .setPositiveButton(R.string.ok) { _, _ -> /*NO-OP*/ }
            .create()
            .show()
    }

    private fun scanTSU() {
        mBaaCodeService?.let { service ->
            try {
                //Start baaCodeService sending tags
                val msg = Message.obtain(null, baacodeService.MSG_SEND_ME_TAGS)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
    }

    private fun onEIDScanned(eidNumber: String) {
        viewModel.onEIDScanned(eidNumber)
    }

    // use BaaCode reader
    private fun onBAAScanned(baaCode: String) {
        viewModel.onBaaCodeScanned(baaCode)
    }

    //  Bind services, should be one set of bind & unbind methods for eid, scale and baacode
    //  First is the eid service bind
    private fun doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
//		Log.i("Breeding", "At doBind1.");
        bindService(Intent(this, EIDReaderService::class.java), mConnection, BIND_AUTO_CREATE)
        //		Log.i("Breeding", "At doBind2.");
        mIsBound = true
        val service = mService
        if (service != null) {
//			Log.i("Breeding", "At doBind3.");
            try {
                //Request status update
                var msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_STATUS, 0, 0)
                msg.replyTo = mMessenger
                service.send(msg)
                //				Log.i("Breeding", "At doBind4.");
                //Request full log from service.
                msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_LOG_FULL, 0, 0)
                service.send(msg)
            } catch (e: RemoteException) {
                //NO-OP
            }
        }
        //		Log.i("Breeding", "At doBind5.");
    }

    //     Next the unbind eid servides
    private fun doUnbindService() {
//		Log.i("Breeding", "At DoUnbindservice");
        val service = mService
        if (service != null) {
            try {
                //Stop eidService from sending tags
                val msg = Message.obtain(null, EIDReaderService.MSG_NO_TAGS_PLEASE)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        if (mIsBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (service != null) {
                try {
                    val msg = Message.obtain(null, EIDReaderService.MSG_UNREGISTER_CLIENT)
                    msg.replyTo = mMessenger
                    service.send(msg)
                } catch (e: RemoteException) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection)
            mIsBound = false
        }
    }

    //  Finally the baacode bind
    private fun doBindbaacodeService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        Log.i("Breeding", "At baacode bind1.")
        bindService(Intent(this, baacodeService::class.java), mbaacodeConnection, BIND_AUTO_CREATE)
        mIsBAABound = true
        val service = mBaaCodeService
        if (service != null) {
            try {
                //Request status update
                var msg = Message.obtain(null, baacodeService.MSG_UPDATE_STATUS, 0, 0)
                msg.replyTo = mMessenger
                service.send(msg)
                //			Log.i("Breeding", "At baacode bind2.");
                //Request full log from service.
                msg = Message.obtain(null, baacodeService.MSG_UPDATE_LOG_FULL, 0, 0)
                service.send(msg)
            } catch (e: RemoteException) {
//			Log.i("Breeding", "message exception bind");
            }
        }
    }

    /// baacode unbind
    fun doUnbindbaacodeService() {
        Log.i("Breeding", "At baacodeUnbindservice")
        val service = mBaaCodeService
        if (service != null) {
            try {
                //Stop baacodeService from sending tags
                val msg = Message.obtain(null, baacodeService.MSG_NO_TAGS_PLEASE)
                msg.replyTo = mMessenger
                Log.i("Breeding", "At baacode unbind.")
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        if (mIsBAABound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (service != null) {
                try {
                    val msg = Message.obtain(null, baacodeService.MSG_UNREGISTER_CLIENT)
                    msg.replyTo = mMessenger
                    service.send(msg)
                } catch (e: RemoteException) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mbaacodeConnection)
            mIsBAABound = false
        }
    }

    inner class EidReaderServiceConnection : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, binder: IBinder) {
            val service = Messenger(binder).also { mService = it }
            try {
                //Register client with service
                val msg = Message.obtain(null, EIDReaderService.MSG_REGISTER_CLIENT)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }

        //  *** This is the eid service disconnection
        override fun onServiceDisconnected(className: ComponentName) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            Log.i("Breeding", "At eid Service Disconnected.")
            mService = null
        }
    }

    private inner class BaacodeServiceConnection : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, binder: IBinder) {
            val service = Messenger(binder).also { mBaaCodeService = it }
            Log.i("Breeding", "At baacodeService.")
            try {
                //Register client with service
                val msg = Message.obtain(null, baacodeService.MSG_REGISTER_CLIENT)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                Log.i("Breeding", "At baacodeService exception.")
                // In this case the service has crashed before we could even do anything with it
            }
            //Update status
            try {
                val msg = Message.obtain(null, baacodeService.MSG_UPDATE_STATUS, 0, 0)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                Log.i("Breeding", "At baacodeService update status crash.")
                // In this case the service has crashed before we could even do anything with it
            }
            //Request full log from service.
            try {
                //Update status
                val msg = Message.obtain(null, baacodeService.MSG_UPDATE_LOG_FULL, 0, 0)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                Log.i("Breeding", "At baacodeService update log full crash.")
                // In this case the service has crashed before we could even do anything with it
            }
        }

        //    ***    And last the baacode service disconnection
        override fun onServiceDisconnected(className: ComponentName) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            mBaaCodeService = null
        }
    }

    internal inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                EIDReaderService.MSG_UPDATE_STATUS -> {
                    val b1 = msg.data
                    val state = b1.getString("stat")
                    if (state != null) {
                        eidReaderStatePresenter.updateReaderState(state)
                    }
                }

                EIDReaderService.MSG_NEW_EID_FOUND -> {
                    val bundle = msg.data
                    val eid = bundle.getString("info1")
                    // We have a good whole EID number
                    //TODO: Add format validation here.
                    if (eid != null) {
                        onEIDScanned(eid)
                    }
                }

                baacodeService.MSG_UPDATE_STATUS -> {
                    val data = msg.data
                    val state = data.getString("stat1")
                    if (state != null) {
                        baaCodeReaderStatePresenter.updateReaderState(state)
                    }
                }

                baacodeService.MSG_NEW_Baacode_FOUND -> {
                    val b4 = msg.data
                    val baaCode = b4.getString("info1")
                    if (baaCode != null) {
                        onBAAScanned(baaCode)
                    }
                }

                EIDReaderService.MSG_UPDATE_LOG_APPEND -> {}
                baacodeService.MSG_UPDATE_LOG_APPEND -> {
                    val b5 = msg.data
                }

                EIDReaderService.MSG_UPDATE_LOG_FULL -> {}
                EIDReaderService.MSG_THREAD_SUICIDE -> {
                    doUnbindService()
                    stopService(Intent(this@TissueSampleActivity, EIDReaderService::class.java))
                }

                baacodeService.MSG_THREAD_SUICIDE -> {
                    doUnbindbaacodeService()
                    stopService(Intent(this@TissueSampleActivity, baacodeService::class.java))
                }

                else -> super.handleMessage(msg)
            }
        }
    }
}

private class ViewModelFactory(context: Context) : ViewModelProvider.Factory {

    private val appContext = context.applicationContext

    override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
        return when (modelClass) {
            TissueSampleViewModel::class.java -> {
                val databaseHandler = DatabaseHandler.create(appContext)
                val sharedPreferences = androidx.preference.PreferenceManager.getDefaultSharedPreferences(appContext)
                val activeDefSettings = ActiveDefaultSettings(sharedPreferences)
                val defSettingsRepo = DefaultSettingsRepositoryImpl(databaseHandler)
                val loadActiveDefaults = LoadActiveDefaultSettings(
                    activeDefaultSettings = activeDefSettings,
                    defSettingsRepo
                )
                val loadDefaultIdTypeIds = LoadDefaultIdTypeIds(loadActiveDefaults)
                val animalRepo = AnimalRepositoryImpl(databaseHandler)
                val speciesRepo = SpeciesRepositoryImpl(databaseHandler)
                @Suppress("UNCHECKED_CAST")
                TissueSampleViewModel(
                    extras.createSavedStateHandle(),
                    databaseHandler,
                    loadActiveDefaultSettings = loadActiveDefaults,
                    animalRepo = animalRepo,
                    speciesRepo = speciesRepo,
                    laboratoryRepository = LaboratoryRepositoryImpl(databaseHandler),
                    tissueSampleTypeRepository = TissueSampleTypeRepositoryImpl(databaseHandler),
                    tissueTestRepository = TissueTestRepositoryImpl(databaseHandler),
                    tissueSampleContainerTypeRepository = TissueSampleContainerTypeRepositoryImpl(databaseHandler),
                    extractPrintLabelData = ExtractPrintLabelData(sharedPreferences, loadDefaultIdTypeIds)
                ) as T
            }
            else -> throw IllegalStateException("${modelClass.simpleName} is not supported.")
        }

    }
}
