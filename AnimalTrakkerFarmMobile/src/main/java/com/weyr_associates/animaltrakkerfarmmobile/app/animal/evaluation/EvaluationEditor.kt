package com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation

import com.weyr_associates.animaltrakkerfarmmobile.model.EvalTraitOption
import com.weyr_associates.animaltrakkerfarmmobile.model.UnitOfMeasure
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

interface EvaluationEditor {

    sealed interface Event

    data object EvaluationLoaded : Event

    data object FieldValuesCleared : Event

    data class FieldValueChanged(val field: EvaluationField) : Event

    val events: Flow<Event>

    // Entry Requirements

    val trait01Entry: EvaluationFieldEntry
    val trait02Entry: EvaluationFieldEntry
    val trait03Entry: EvaluationFieldEntry
    val trait04Entry: EvaluationFieldEntry
    val trait05Entry: EvaluationFieldEntry
    val trait06Entry: EvaluationFieldEntry
    val trait07Entry: EvaluationFieldEntry
    val trait08Entry: EvaluationFieldEntry
    val trait09Entry: EvaluationFieldEntry
    val trait10Entry: EvaluationFieldEntry
    val trait11Entry: EvaluationFieldEntry
    val trait12Entry: EvaluationFieldEntry
    val trait13Entry: EvaluationFieldEntry
    val trait14Entry: EvaluationFieldEntry
    val trait15Entry: EvaluationFieldEntry
    val trait16Entry: EvaluationFieldEntry
    val trait17Entry: EvaluationFieldEntry
    val trait18Entry: EvaluationFieldEntry
    val trait19Entry: EvaluationFieldEntry
    val trait20Entry: EvaluationFieldEntry

    val trait01Name: String
    val trait02Name: String
    val trait03Name: String
    val trait04Name: String
    val trait05Name: String
    val trait06Name: String
    val trait07Name: String
    val trait08Name: String
    val trait09Name: String
    val trait10Name: String
    val trait11Name: String
    val trait12Name: String
    val trait13Name: String
    val trait14Name: String
    val trait15Name: String
    val trait16Name: String
    val trait17Name: String
    val trait18Name: String
    val trait19Name: String
    val trait20Name: String

    val trait11Units: UnitOfMeasure
    val trait12Units: UnitOfMeasure
    val trait13Units: UnitOfMeasure
    val trait14Units: UnitOfMeasure
    val trait15Units: UnitOfMeasure

    val trait16Options: List<EvalTraitOption>
    val trait17Options: List<EvalTraitOption>
    val trait18Options: List<EvalTraitOption>
    val trait19Options: List<EvalTraitOption>
    val trait20Options: List<EvalTraitOption>

    // Scored Traits

    val trait01: StateFlow<Int>
    val trait02: StateFlow<Int>
    val trait03: StateFlow<Int>
    val trait04: StateFlow<Int>
    val trait05: StateFlow<Int>
    val trait06: StateFlow<Int>
    val trait07: StateFlow<Int>
    val trait08: StateFlow<Int>
    val trait09: StateFlow<Int>
    val trait10: StateFlow<Int>

    fun setTrait01(value: Int)
    fun setTrait02(value: Int)
    fun setTrait03(value: Int)
    fun setTrait04(value: Int)
    fun setTrait05(value: Int)
    fun setTrait06(value: Int)
    fun setTrait07(value: Int)
    fun setTrait08(value: Int)
    fun setTrait09(value: Int)
    fun setTrait10(value: Int)

    // Units Traits

    val trait11: StateFlow<Float?>
    val trait12: StateFlow<Float?>
    val trait13: StateFlow<Float?>
    val trait14: StateFlow<Float?>
    val trait15: StateFlow<Float?>

    fun setTrait11(value: Float?)
    fun setTrait12(value: Float?)
    fun setTrait13(value: Float?)
    fun setTrait14(value: Float?)
    fun setTrait15(value: Float?)

    // Options Traits

    val trait16: StateFlow<EvalTraitOption?>
    val trait17: StateFlow<EvalTraitOption?>
    val trait18: StateFlow<EvalTraitOption?>
    val trait19: StateFlow<EvalTraitOption?>
    val trait20: StateFlow<EvalTraitOption?>

    fun setTrait16(option: EvalTraitOption?)
    fun setTrait17(option: EvalTraitOption?)
    fun setTrait18(option: EvalTraitOption?)
    fun setTrait19(option: EvalTraitOption?)
    fun setTrait20(option: EvalTraitOption?)
}
