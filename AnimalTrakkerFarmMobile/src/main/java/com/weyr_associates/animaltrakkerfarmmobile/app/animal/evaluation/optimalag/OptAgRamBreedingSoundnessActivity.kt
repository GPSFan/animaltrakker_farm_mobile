package com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.optimalag

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.preference.PreferenceManager
import com.weyr_associates.animaltrakkerfarmmobile.BluetoothWatcher
import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.RequiredPermissions
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.AnimalBasicInfoPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.AnimalDialogs
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.AddAnimal
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.EvaluationEditorPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue.TissueSampleActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.note.TakeNotesActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.setupForAnimalNotFound
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.setupForAnimalWithEIDNotFound
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.setupForInitialLookup
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.coroutines.flow.observeOneTimeEvents
import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.TopButtonBar
import com.weyr_associates.animaltrakkerfarmmobile.app.label.ExtractPrintLabelData
import com.weyr_associates.animaltrakkerfarmmobile.app.main.MainActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.select.SelectAnimal
import com.weyr_associates.animaltrakkerfarmmobile.app.select.SelectAnimalActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.ActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.LoadActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.LoadDefaultIdTypeIds
import com.weyr_associates.animaltrakkerfarmmobile.baacodeService
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ActivityOptAgRamBreedingSoundnessBinding
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderService
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderStatePresenter
import com.weyr_associates.animaltrakkerfarmmobile.model.Laboratory
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.AnimalRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.DefaultSettingsRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.EvaluationRepositoryImpl
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class OptAgRamBreedingSoundnessActivity : AppCompatActivity() {

    companion object {
        private const val REQUEST_CODE_LOOKUP_ANIMAL = 200
        private const val REQUEST_CODE_ADD_AND_SELECT_ANIMAL = 201
    }

    private val viewModel: OptAgRamBreedingSoundnessViewModel by viewModels {
        ViewModelFactory(this)
    }

    private val binding by lazy {
        ActivityOptAgRamBreedingSoundnessBinding.inflate(layoutInflater)
    }

    private var animalBasicInfoPresenter = AnimalBasicInfoPresenter()
    private val bluetoothWatcher = BluetoothWatcher(this)
    private lateinit var eidReaderStatePresenter: EIDReaderStatePresenter

    //    ***    Service connections & disconnections one for each eidService baacodeService and scaleService
    private val mConnection: ServiceConnection = EidReaderServiceConnection()
    private val mMessenger: Messenger = Messenger(IncomingHandler())
    private var mService: Messenger? = null
    private var mIsBound: Boolean = false

    private val evaluationEditorPresenter by lazy {
        EvaluationEditorPresenter(binding.evaluationEditor)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        with(binding.buttonPanelTop) {
            show(TopButtonBar.UI_ALL or TopButtonBar.UI_ACTION_UPDATE_DATABASE)

            eidReaderStatePresenter = EIDReaderStatePresenter(
                this@OptAgRamBreedingSoundnessActivity, imageScannerStatus
            )
            scanEIDButton.setOnClickListener { onScanEID() }
            lookupAnimalButton.setOnClickListener { onLookupAnimal() }
            clearDataButton.setOnClickListener { viewModel.clearData() }
            mainActionButton.setOnClickListener { viewModel.saveToDatabase() }

            binding.buttonPanelTop.takeNoteButton.isEnabled = false
            binding.buttonPanelTop.showAlertButton.isEnabled = false
        }

        animalBasicInfoPresenter.binding = binding.animalBasicInfo

        binding.buttonTakeTissueSamples.setOnClickListener {
            val takeTissueSamplesInfo = viewModel.tissueSamplesInfo.value ?: return@setOnClickListener
            startActivity(
                TissueSampleActivity.newIntent(
                    this@OptAgRamBreedingSoundnessActivity,
                    takeTissueSamplesInfo.animalBasicInfo,
                    titleAddition = getString(R.string.title_activity_opt_ag_ram_bse),
                    defaultLabCompanyId = Laboratory.LAB_COMPANY_ID_OPTIMAL_LIVESTOCK,
                    printLabelData = takeTissueSamplesInfo.printLabelData
                )
            )
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.canClearData.collectLatest { canClear ->
                    binding.buttonPanelTop.clearDataButton.isEnabled = canClear
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.canSaveToDatabase.collectLatest { canSave ->
                    binding.buttonPanelTop.mainActionButton.isEnabled = canSave
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.events.observeOneTimeEvents {
                    handleEvent(it)
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.animalInfoState.collectLatest { animalInfoState ->
                    updateTopBarAnimalActions(animalInfoState)
                    updateAnimalInfoDisplay(animalInfoState)
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.tissueSamplesInfo.collectLatest { tissueSamplesInfo ->
                    binding.textTissueSamplePrintLabel.text = tissueSamplesInfo?.printLabelData?.labelText
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.canTakeTissueSamples.collectLatest { canTakeSamples ->
                    binding.buttonTakeTissueSamples.isEnabled = canTakeSamples
                }
            }
        }

        evaluationEditorPresenter.bindToEditorIn(this, viewModel.evaluationEditor)

        bluetoothWatcher.onActivationChanged = {
            onBluetoothActivationChanged()
        }
    }

    override fun onResume() {
        super.onResume()
        if (checkRequiredPermissions()) {
            checkIfServiceIsRunning()
        }
        bluetoothWatcher.startWatching()
    }

    public override fun onPause() {
        super.onPause()
        doUnbindService()
        bluetoothWatcher.stopWatching()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE_LOOKUP_ANIMAL) {
            if (resultCode == RESULT_OK && data != null && data.hasExtra(SelectAnimal.EXTRA_SELECTED_ANIMAL_ID)) {
                val selectedAnimalId = data.getIntExtra(SelectAnimal.EXTRA_SELECTED_ANIMAL_ID, -1)
                if (selectedAnimalId != -1) {
                    viewModel.onAnimalSelected(selectedAnimalId)
                }
            }
        }
        else if (requestCode == REQUEST_CODE_ADD_AND_SELECT_ANIMAL) {
            if (resultCode == RESULT_OK && data != null && data.hasExtra(AddAnimal.EXTRA_RESULTING_ANIMAL_ID)) {
                val resultingAnimalId = data.getIntExtra(AddAnimal.EXTRA_RESULTING_ANIMAL_ID, -1)
                if (resultingAnimalId != -1) {
                    viewModel.onAnimalSelected(resultingAnimalId)
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun onScanEID() {
        mService?.let { service ->
            try {
                //Start eidService sending tags
                val msg = Message.obtain(null, EIDReaderService.MSG_SEND_ME_TAGS)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
    }

    private fun onLookupAnimal() {
        startActivityForResult(
            Intent(this, SelectAnimalActivity::class.java),
            REQUEST_CODE_LOOKUP_ANIMAL
        )
    }

    private fun checkRequiredPermissions(): Boolean {
        if (!RequiredPermissions.areFulfilled(this)) {
            MainActivity.returnFrom(this)
            return false
        }
        return true
    }

    private fun onBluetoothActivationChanged() {
        checkRequiredPermissions()
    }

    //  Really should be check if services are running...
    //  Start the service if it is not already running and bind to it
    //  Should be one each for eid, scale and baacode
    private fun checkIfServiceIsRunning() {
        //If the service is running when the activity starts, we want to automatically bind to it.
        Log.i("Breeding", "At eid isRunning?.")
        if (EIDReaderService.isRunning()) {
//			Log.i("Breeding", " eidService is running");
            doBindService()
        } else {
//			Log.i("Breeding", " eidService is not running, start it");
            startService(Intent(this, EIDReaderService::class.java))
            doBindService()
        }
    }

    private fun updateTopBarAnimalActions(animalInfoState: OptAgRamBreedingSoundnessViewModel.AnimalInfoState) {
        when (animalInfoState) {
            is OptAgRamBreedingSoundnessViewModel.AnimalInfoState.AnimalInfoLoaded -> {
                with(binding.buttonPanelTop) {
                    with(takeNoteButton) {
                        isEnabled = true
                        setOnClickListener {
                            startActivity(
                                TakeNotesActivity.newIntent(
                                    this@OptAgRamBreedingSoundnessActivity,
                                    animalInfoState.animalBasicInfo.id
                                )
                            )
                        }
                    }
                    with(showAlertButton) {
                        val alertText = animalInfoState.animalBasicInfo.alert
                        when {
                            alertText.isNullOrBlank() -> {
                                isEnabled = false
                                setOnClickListener(null)
                            }
                            else -> {
                                isEnabled = true
                                setOnClickListener {
                                    AnimalDialogs.showAnimalAlert(this@OptAgRamBreedingSoundnessActivity, alertText)
                                }
                            }
                        }
                    }
                }
            }
            else -> {
                with(binding.buttonPanelTop) {
                    with(takeNoteButton) {
                        isEnabled = false
                        setOnClickListener(null)
                    }
                    with(showAlertButton) {
                        isEnabled = false
                        setOnClickListener(null)
                    }
                }
            }
        }
    }

    private fun updateAnimalInfoDisplay(animalInfoState: OptAgRamBreedingSoundnessViewModel.AnimalInfoState) {
        when (animalInfoState) {
            OptAgRamBreedingSoundnessViewModel.AnimalInfoState.Initial -> {
                animalBasicInfoPresenter.animalBasicInfo = null
                binding.animalBasicInfo.root.isInvisible = true
                binding.evaluationEditor.root.isGone = true
                binding.containerNoAnimal.root.isVisible = true
                binding.containerNoAnimal.setupForInitialLookup()
            }
            is OptAgRamBreedingSoundnessViewModel.AnimalInfoState.AnimalInfoLoaded -> {
                binding.animalBasicInfo.root.isVisible = true
                binding.containerNoAnimal.root.isGone = true
                binding.evaluationEditor.root.isVisible = true
                animalBasicInfoPresenter.animalBasicInfo = animalInfoState.animalBasicInfo
            }
            is OptAgRamBreedingSoundnessViewModel.AnimalInfoState.AnimalInfoNotFound -> {
                animalBasicInfoPresenter.animalBasicInfo = null
                binding.animalBasicInfo.root.isInvisible = true
                binding.evaluationEditor.root.isGone = true
                binding.containerNoAnimal.root.isVisible = true
                when (val lookup = animalInfoState.lookup) {
                    is OptAgRamBreedingSoundnessViewModel.Lookup.ByAnimalId -> {
                        binding.containerNoAnimal.setupForAnimalNotFound()
                    }
                    is OptAgRamBreedingSoundnessViewModel.Lookup.ByScannedEID -> {
                        binding.containerNoAnimal.setupForAnimalWithEIDNotFound(
                            this@OptAgRamBreedingSoundnessActivity,
                            lookup.scannedEID,
                            REQUEST_CODE_ADD_AND_SELECT_ANIMAL
                        )
                    }
                }
            }
        }
    }

    private fun handleEvent(event: OptAgRamBreedingSoundnessViewModel.Event) {
        when (event) {
            is OptAgRamBreedingSoundnessViewModel.AnimalSpeciesOrSexMismatch -> {
                AlertDialog.Builder(this)
                    .setTitle(R.string.dialog_title_opt_ag_ram_bse_species_sex_mismatch)
                    .setMessage(
                        getString(
                            R.string.dialog_message_opt_ag_ram_bse_species_sex_mismatch,
                            event.speciesName,
                            event.sexName
                        )
                    )
                    .setPositiveButton(R.string.ok) { _, _ ->
                        viewModel.resetAnimal()
                    }
                    .create()
                    .show()
            }
            OptAgRamBreedingSoundnessViewModel.UpdateDatabaseSuccess -> {
                Toast.makeText(
                    this,
                    R.string.toast_evaluation_saved,
                    Toast.LENGTH_LONG
                ).show()
            }
            OptAgRamBreedingSoundnessViewModel.UpdateDatabaseError -> {
                Toast.makeText(
                    this,
                    R.string.toast_evaluation_save_error,
                    Toast.LENGTH_LONG
                ).show()
            }
            is OptAgRamBreedingSoundnessViewModel.AnimalAlert -> {
                AnimalDialogs.showAnimalAlert(this, event.alertText)
            }
        }
    }

    private fun onEIDScanned(eidNumber: String) {
        viewModel.onEIDScanned(eidNumber)
    }

    //  Bind services, should be one set of bind & unbind methods for eid, scale and baacode
    //  First is the eid service bind
    private fun doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
//		Log.i("Breeding", "At doBind1.");
        bindService(Intent(this, EIDReaderService::class.java), mConnection, BIND_AUTO_CREATE)
        //		Log.i("Breeding", "At doBind2.");
        mIsBound = true
        val service = mService
        if (service != null) {
//			Log.i("Breeding", "At doBind3.");
            try {
                //Request status update
                var msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_STATUS, 0, 0)
                msg.replyTo = mMessenger
                service.send(msg)
                //				Log.i("Breeding", "At doBind4.");
                //Request full log from service.
                msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_LOG_FULL, 0, 0)
                service.send(msg)
            } catch (e: RemoteException) {
                //NO-OP
            }
        }
        //		Log.i("Breeding", "At doBind5.");
    }

    //     Next the unbind eid servides
    private fun doUnbindService() {
//		Log.i("Breeding", "At DoUnbindservice");
        val service = mService
        if (service != null) {
            try {
                //Stop eidService from sending tags
                val msg = Message.obtain(null, EIDReaderService.MSG_NO_TAGS_PLEASE)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        if (mIsBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (service != null) {
                try {
                    val msg = Message.obtain(null, EIDReaderService.MSG_UNREGISTER_CLIENT)
                    msg.replyTo = mMessenger
                    service.send(msg)
                } catch (e: RemoteException) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection)
            mIsBound = false
        }
    }

    inner class EidReaderServiceConnection : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, binder: IBinder) {
            val service = Messenger(binder).also { mService = it }
            try {
                //Register client with service
                val msg = Message.obtain(null, EIDReaderService.MSG_REGISTER_CLIENT)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }

        //  *** This is the eid service disconnection
        override fun onServiceDisconnected(className: ComponentName) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            Log.i("Breeding", "At eid Service Disconnected.")
            mService = null
        }
    }

    internal inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                EIDReaderService.MSG_UPDATE_STATUS -> {
                    val b1 = msg.data
                    val state = b1.getString("stat")
                    if (state != null) {
                        eidReaderStatePresenter.updateReaderState(state)
                    }
                }

                EIDReaderService.MSG_NEW_EID_FOUND -> {
                    val bundle = msg.data
                    val eid = bundle.getString("info1")
                    // We have a good whole EID number
                    //TODO: Add format validation here.
                    if (eid != null) {
                        onEIDScanned(eid)
                    }
                }

                EIDReaderService.MSG_UPDATE_LOG_APPEND -> {}
                baacodeService.MSG_UPDATE_LOG_APPEND -> {
                    val b5 = msg.data
                }

                EIDReaderService.MSG_UPDATE_LOG_FULL -> {}
                EIDReaderService.MSG_THREAD_SUICIDE -> {
                    doUnbindService()
                    stopService(Intent(this@OptAgRamBreedingSoundnessActivity, EIDReaderService::class.java))
                }

                else -> super.handleMessage(msg)
            }
        }
    }
}

private class ViewModelFactory(context: Context) : ViewModelProvider.Factory {

    private val appContext = context.applicationContext

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when (modelClass) {
            OptAgRamBreedingSoundnessViewModel::class.java -> {
                val databaseHandler = DatabaseHandler.create(appContext)
                val animalRepo = AnimalRepositoryImpl(databaseHandler)
                val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(appContext)
                val activeDefaultSettings = ActiveDefaultSettings(sharedPreferences)
                val defaultSettingsRepo = DefaultSettingsRepositoryImpl(databaseHandler)
                val loadActiveDefaultSettings = LoadActiveDefaultSettings(
                    activeDefaultSettings,
                    defaultSettingsRepo
                )
                val loadDefaultIdTypeIds = LoadDefaultIdTypeIds(loadActiveDefaultSettings)
                @Suppress("UNCHECKED_CAST")
                OptAgRamBreedingSoundnessViewModel(
                    databaseHandler = databaseHandler,
                    animalRepo = animalRepo,
                    evaluationRepo = EvaluationRepositoryImpl(databaseHandler),
                    extractPrintLabelData = ExtractPrintLabelData(
                        sharedPreferences,
                        loadDefaultIdTypeIds
                    )
                ) as T
            }
            else -> throw IllegalStateException("${modelClass.simpleName} is not supported.")
        }
    }
}
