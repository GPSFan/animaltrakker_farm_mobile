package com.weyr_associates.animaltrakkerfarmmobile.app.model

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Period

fun LocalDate.extractAnimalAge(): Pair<Int, Int> {
    return Period.between(this, LocalDate.now()).let { periodBetween ->
        Pair(periodBetween.years, periodBetween.months)
    }
}

fun LocalDateTime.extractAnimalAge(): Pair<Int,Int> {
    return this.toLocalDate().extractAnimalAge()
}

fun LocalDate.animalBirthDateFrom(ageYears: Int, ageMonths: Int): LocalDate {
    return minusYears(ageYears.toLong())
        .minusMonths(ageMonths.toLong())
        .withDayOfMonth(1)
}

fun LocalDateTime.animalBirthDateFrom(ageYears: Int, ageMonths: Int): LocalDate {
    return this.toLocalDate().animalBirthDateFrom(ageYears, ageMonths)
}
