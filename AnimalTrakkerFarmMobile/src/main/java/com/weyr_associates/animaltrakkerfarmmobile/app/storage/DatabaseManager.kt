package com.weyr_associates.animaltrakkerfarmmobile.app.storage

import android.content.Context
import android.net.Uri
import com.weyr_associates.animaltrakkerfarmmobile.AppDirectories
import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.app.core.Result
import com.weyr_associates.animaltrakkerfarmmobile.app.core.versionName
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.ActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.DatabaseBackupError.SourceDatabaseFileNotFound
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.DatabaseBackupError.UnableToWriteBackupDatabaseFile
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.DatabaseLoadError.LoadedDatabaseFailedQueryCheck
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.DatabaseLoadError.UnableToWriteLoadedDatabaseFile
import com.weyr_associates.animaltrakkerfarmmobile.database.AnimalTable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

sealed interface DatabaseLoadError {
    data object UnableToWriteLoadedDatabaseFile : DatabaseLoadError
    data object LoadedDatabaseFailedQueryCheck : DatabaseLoadError
}

sealed interface DatabaseBackupError {
    data object SourceDatabaseFileNotFound : DatabaseBackupError
    data object UnableToWriteBackupDatabaseFile : DatabaseBackupError
}

class DatabaseManager(context: Context) {

    companion object {
        @JvmStatic
        fun getInstance(context: Context): DatabaseManager {
            if (INSTANCE == null) {
                synchronized(INSTANCE_LOCK) {
                    if (INSTANCE == null) {
                        INSTANCE = DatabaseManager(context)
                    }
                }
            }
            return requireNotNull(INSTANCE)
        }

        private var INSTANCE: DatabaseManager? = null
        private val INSTANCE_LOCK = Any()

        //TODO: This path needs to change as technically we are not supposed to write here anymore.
        //Should move to /data/data/com.weyr_associates.animaltrakkerfarmmobile/files using Context.filesDir.
        private const val DIR_PATH_ROOT = "/data/data/com.weyr_associates.animaltrakkerfarmmobile"

        private const val DIR_NAME_DATABASES = "databases"
        private const val FILE_NAME_ATRKKR_DATABASE = "animaltrakker_db.sqlite"
        private val BACKUP_TIMESTAMP_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss")
    }

    private val appContext = context.applicationContext

    private val databasesFolder: File get() = File(DIR_PATH_ROOT).childAt(DIR_NAME_DATABASES)

    private val databaseFile: File get() = databasesFolder.childAt(FILE_NAME_ATRKKR_DATABASE)

    suspend fun loadDatabaseFromUri(uri: Uri): Result<Long, DatabaseLoadError> {
        return withContext(Dispatchers.IO) {
            try {
                appContext.contentResolver.openInputStream(uri)?.use { inputStream ->
                    databasesFolder.mkdirs()
                    databaseFile.outputStream().use { outputStream ->
                        inputStream.copyTo(outputStream)
                    }
                }
            } catch (ex: Exception) {
                return@withContext Result.Failure(UnableToWriteLoadedDatabaseFile)
            }
            try {
                DatabaseHandler.create(appContext).use { databaseHandler ->
                    databaseHandler.readableDatabase.rawQuery(
                        AnimalTable.Sql.QUERY_COUNT_ALL_ANIMALS,
                        emptyArray()
                    ).use { cursor ->
                        if (!cursor.moveToFirst()) {
                            return@withContext Result.Failure(LoadedDatabaseFailedQueryCheck)
                        }
                        return@withContext Result.Success(cursor.getLong(0))
                    }
                }
            } catch (ex: Exception) {
                return@withContext Result.Failure(LoadedDatabaseFailedQueryCheck)
            }
        }
    }

    suspend fun backupDatabaseToDocuments(): Result<File, DatabaseBackupError> {
        return withContext(Dispatchers.IO) {
            if (!databaseFile.exists()) {
                return@withContext Result.Failure(SourceDatabaseFileNotFound)
            }
            val dstFile = AppDirectories.databaseBackupsDirectory(appContext)
                .childAt(getDatabaseBackupFileName())
            try {
                databaseFile.copyTo(dstFile, overwrite = true)
                return@withContext Result.Success(dstFile)
            } catch(ex: Exception) {
                return@withContext Result.Failure(
                    when (ex) {
                        is NoSuchFileException -> SourceDatabaseFileNotFound
                        else -> UnableToWriteBackupDatabaseFile
                    }
                )
            }
        }
    }

    private fun getDatabaseBackupFileName(): String {
        val activeDefaultSettingsId = ActiveDefaultSettings.from(appContext)
            .loadActiveDefaultSettingsId()
        val appVersion = appContext.versionName
        val timeStamp = LocalDateTime.now().format(BACKUP_TIMESTAMP_FORMAT)
        val (dbVersion, serialNumber) = DatabaseHandler.create(appContext).use {
            Pair(it.queryUserVersion(), it.querySerialNumber(activeDefaultSettingsId.toLong()))
        }
        return buildString {
            append(timeStamp)
            if (serialNumber != 0) {
                append("_AT_SERIAL_${serialNumber}")
            }
            if (dbVersion != -1L) {
                append("_DB${dbVersion}")
            }
            append("_VER_${appVersion}.sqlite")
        }
    }

    private fun File.childAt(path: String): File {
        return File(this, path)
    }
}
