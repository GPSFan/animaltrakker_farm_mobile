package com.weyr_associates.animaltrakkerfarmmobile.app.animal.id

import android.view.LayoutInflater
import androidx.core.view.isGone
import androidx.core.view.isVisible
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ItemAnimalBasicInfoIdBinding
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ViewAnimalIdsBinding
import com.weyr_associates.animaltrakkerfarmmobile.model.IdBasicInfo

class AnimalIdsPresenter(binding: ViewAnimalIdsBinding? = null) {

    private val idInfoViewCache = mutableListOf<ItemAnimalBasicInfoIdBinding>()

    var binding: ViewAnimalIdsBinding? = binding
        set(value) {
            if (value != field) {
                field?.let { removeIdViewsFrom(it) }
                field = value?.also { addIdViewsTo(it) }
                bindAnimalIdViews()
            }
        }

    var idBasicInfoItems: List<IdBasicInfo>? = null
        set(value) {
            field = value
            bindAnimalIdViews()
        }

    private fun bindAnimalIdViews() {
        val binding = binding ?: return
        binding.textNoIdsFound.isVisible = idBasicInfoItems.isNullOrEmpty()
        while (idInfoViewCache.size < (idBasicInfoItems?.size ?: 0)) {
            idInfoViewCache.add(
                ItemAnimalBasicInfoIdBinding.inflate(
                    LayoutInflater.from(binding.root.context),
                    binding.root,
                    true
                )
            )
        }
        idInfoViewCache.forEachIndexed { index, idViewBinding ->
            val idInfo = index.takeIf { it < (idBasicInfoItems?.size ?: 0) }?.let { idBasicInfoItems?.get(index) }
            with(idViewBinding) {
                textIdNumber.text = idInfo?.number ?: ""
                textIdTypeName.text = idInfo?.typeName ?: ""
                textIdColorName.text = idInfo?.colorAbbreviation ?: ""
                textIdLocationName.text = idInfo?.locationAbbreviation ?: ""
                root.isGone = idInfo == null
            }
        }
    }

    private fun addIdViewsTo(binding: ViewAnimalIdsBinding) {
        idInfoViewCache.forEach { idViewBinding ->
            binding.root.addView(idViewBinding.root)
        }
    }

    private fun removeIdViewsFrom(binding: ViewAnimalIdsBinding) {
        idInfoViewCache.forEach { idViewBinding ->
            binding.root.removeView(idViewBinding.root)
        }
    }
}
