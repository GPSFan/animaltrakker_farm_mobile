package com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue.TissueSampleViewModel.AnimalInfoState.AnimalInfoLoaded
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.ManageAnimalIdsViewModel
import com.weyr_associates.animaltrakkerfarmmobile.app.core.Result
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.coroutines.flow.combine6
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.requireAs
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.takeAs
import com.weyr_associates.animaltrakkerfarmmobile.app.label.ExtractPrintLabelData
import com.weyr_associates.animaltrakkerfarmmobile.app.label.ExtractPrintLabelDataError
import com.weyr_associates.animaltrakkerfarmmobile.app.label.PrintLabelData
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.LoadActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalBasicInfo
import com.weyr_associates.animaltrakkerfarmmobile.model.Laboratory
import com.weyr_associates.animaltrakkerfarmmobile.model.Species
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueSampleContainerType
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueSampleType
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueTest
import com.weyr_associates.animaltrakkerfarmmobile.repository.AnimalRepository
import com.weyr_associates.animaltrakkerfarmmobile.repository.LaboratoryRepository
import com.weyr_associates.animaltrakkerfarmmobile.repository.SpeciesRepository
import com.weyr_associates.animaltrakkerfarmmobile.repository.TissueSampleContainerTypeRepository
import com.weyr_associates.animaltrakkerfarmmobile.repository.TissueSampleTypeRepository
import com.weyr_associates.animaltrakkerfarmmobile.repository.TissueTestRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.LocalDateTime

class TissueSampleViewModel(
    private val savedStateHandle: SavedStateHandle,
    private val databaseHandler: DatabaseHandler,
    private val loadActiveDefaultSettings: LoadActiveDefaultSettings,
    private val speciesRepo: SpeciesRepository,
    private val animalRepo: AnimalRepository,
    private val laboratoryRepository: LaboratoryRepository,
    private val tissueSampleTypeRepository: TissueSampleTypeRepository,
    private val tissueTestRepository: TissueTestRepository,
    private val tissueSampleContainerTypeRepository: TissueSampleContainerTypeRepository,
    private val extractPrintLabelData: ExtractPrintLabelData
) : ViewModel() {

    sealed interface Event

    sealed interface InputEvent : Event {
        data object ContainerIdChanged : InputEvent
        data object ContainerExpDateChanged : InputEvent
    }

    data object IncompleteDataEntry : Event

    data class AnimalAlert(
        val alertText: String
    ) : Event

    data class AnimalSpeciesMismatch(
        val defaultSpeciesName: String,
        val animalSpeciesName: String
    ) : Event

    data class PrintLabelRequestedError(
        val error: ExtractPrintLabelDataError
    ) : Event

    data class PrintLabelRequestedEvent(
        val printLabelData: PrintLabelData
    ) : Event

    sealed interface UpdateDatabaseEvent : Event {
        data object Success : UpdateDatabaseEvent
        data object Error : UpdateDatabaseEvent
    }

    sealed interface Lookup {
        data class ByAnimalId(val animalId: Int) : Lookup
        data class ByScannedEID(val scannedEID: String) : Lookup
    }

    data object ResetAnimalInfo

    sealed interface AnimalInfoState {
        data object Initial : AnimalInfoState
        data class AnimalInfoLoaded(
            val animalBasicInfo: AnimalBasicInfo,
            val scannedEID: String?) : AnimalInfoState
        data class AnimalInfoNotFound(val lookup: Lookup) : AnimalInfoState
    }

    private lateinit var defaultSpecies: Species
    private var secondaryIdTypeId: Int = 0

    private val _containerId = MutableStateFlow("")
    var containerId: String
        get() = _containerId.value
        set(value) { _containerId.update { value } }

    private val _containerExpDate = MutableStateFlow("")
    var containerExpDate: String
        get() = _containerExpDate.value
        set(value) { _containerExpDate.update { value } }

    private val onLookupAnimalChannel = Channel<Lookup>()
    private val onResetAnimalChannel = Channel<ResetAnimalInfo>()

    val animalInfoState: StateFlow<AnimalInfoState> = merge(
        onLookupAnimalChannel.receiveAsFlow(),
        onResetAnimalChannel.receiveAsFlow()
    ).mapLatest { lookup ->
        when (lookup) {
            is Lookup -> {
                val (animalInfo, scannedEID) = when (lookup) {
                    is Lookup.ByAnimalId -> {
                        Pair(animalRepo.queryAnimalByAnimalId(lookup.animalId), null)
                    }
                    is Lookup.ByScannedEID -> {
                        Pair(animalRepo.queryAnimalByEID(lookup.scannedEID), lookup.scannedEID)
                    }
                }
                animalInfo?.let { AnimalInfoLoaded(it, scannedEID) }
                    ?: AnimalInfoState.AnimalInfoNotFound(lookup)
            }
            else -> AnimalInfoState.Initial
        }
    }.onEach {
        if (!savedStateHandle.contains(TissueSample.EXTRA_ANIMAL_BASIC_INFO)) {
            it.takeAs<AnimalInfoLoaded>()?.animalBasicInfo?.alert?.let { alertText ->
                if (alertText.isNotBlank()) {
                    _eventChannel.send(AnimalAlert(alertText))
                }
            }
        }
    }.stateIn(
        viewModelScope,
        SharingStarted.Lazily,
        savedStateHandle.get<AnimalBasicInfo>(TissueSample.EXTRA_ANIMAL_BASIC_INFO)?.let {
            AnimalInfoLoaded(it, null)
        } ?: AnimalInfoState.Initial
    )

    private val _selectedLaboratory = MutableStateFlow<Laboratory?>(null)
    val selectedLaboratory = _selectedLaboratory.asStateFlow()

    private val _selectedTissueSampleType = MutableStateFlow<TissueSampleType?>(null)
    val selectedTissueSampleType = _selectedTissueSampleType.asStateFlow()

    private val _selectedTissueTestType = MutableStateFlow<TissueTest?>(null)
    val selectedTissueTestType = _selectedTissueTestType.asStateFlow()

    private val _selectedSampleContainerType = MutableStateFlow<TissueSampleContainerType?>(null)
    val selectTissueContainerType = _selectedSampleContainerType.asStateFlow()

    val canClearData = combine(_containerId, _containerExpDate) { containerId, containerExpDate ->
        containerId.isNotEmpty() || containerExpDate.isNotEmpty()
    }.stateIn(viewModelScope, SharingStarted.Lazily, false)

    private val _isUpdatingDatabase = MutableStateFlow(false)

    val canSaveToDatabase = combine6(
        animalInfoState,
        _isUpdatingDatabase,
        _selectedLaboratory,
        _selectedTissueSampleType,
        _selectedTissueTestType,
        _selectedSampleContainerType
    ) { animalInfoState, isUpdatingDatabase, laboratory, sampleType, testType, containerType ->
        animalInfoState is AnimalInfoLoaded && !isUpdatingDatabase &&
                laboratory != null && sampleType != null &&
                testType != null && containerType != null
    }.stateIn(viewModelScope, SharingStarted.Lazily, false)

    //These are the same for now, but this might change.
    val canPrintLabel = canSaveToDatabase

    private val _canScanTSU = MutableStateFlow(true)
    val canScanTSU = _canScanTSU.asStateFlow()

    private val _eventChannel = Channel<Event>()
    val events = _eventChannel.receiveAsFlow()

    init {
        viewModelScope.launch {
            loadDefaults()
        }
        viewModelScope.launch {
            animalInfoState.filter { it is AnimalInfoLoaded }
                .map { it.requireAs<AnimalInfoLoaded>().animalBasicInfo }
                .collectLatest {
                    //TODO: Find a better way to gate this check.
                    if (!savedStateHandle.contains(TissueSample.EXTRA_ANIMAL_BASIC_INFO)) {
                        if (it.speciesId != defaultSpecies.id) {
                            postEvent(
                                AnimalSpeciesMismatch(
                                    defaultSpeciesName = defaultSpecies.commonName,
                                    it.speciesCommonName
                                )
                            )
                        }
                    }
                }
        }
    }

    fun selectLaboratory(laboratory: Laboratory) {
        _selectedLaboratory.update { laboratory }
    }

    fun selectTissueSampleType(tissueSampleType: TissueSampleType) {
        _selectedTissueSampleType.update { tissueSampleType }
    }

    fun selectTissueTestType(tissueTestType: TissueTest) {
        _selectedTissueTestType.update { tissueTestType }
    }

    fun selectTissueSampleContainerType(tissueSampleContainerType: TissueSampleContainerType) {
        _selectedSampleContainerType.update { tissueSampleContainerType }
    }

    fun clearData() {
        if (canClearData.value) {
            executeClearData()
        }
    }

    fun saveToDatabase() {
        if (canSaveToDatabase.value) {
            viewModelScope.launch {
                updateDatabase()
            }
        }
    }

    fun printLabel() {
        if (canPrintLabel.value) {
            executePrintLabel()
        }
    }

    fun onAnimalSelected(animalId: Int) {
        viewModelScope.launch {
            onLookupAnimalChannel.send(Lookup.ByAnimalId(animalId))
        }
    }

    fun onEIDScanned(eidNumber: String) {
        viewModelScope.launch {
            onLookupAnimalChannel.send(Lookup.ByScannedEID(eidNumber))
        }
    }

    fun onBaaCodeScanned(baaCode: String) {
        containerId = baaCode
        postEvent(InputEvent.ContainerIdChanged)
    }

    fun resetAnimalInfo() {
        viewModelScope.launch {
            onResetAnimalChannel.send(ResetAnimalInfo)
        }
    }

    override fun onCleared() {
        databaseHandler.close()
        super.onCleared()
    }

    private suspend fun loadDefaults() {
        val defaults = loadActiveDefaultSettings()
        secondaryIdTypeId = defaults.idTypeIdSecondary
        val defaultSpeciesDeferred = viewModelScope.async {
            speciesRepo.querySpeciesById(defaults.speciesId)
        }
        val laboratoryDeferred = viewModelScope.async {
            laboratoryRepository.queryLaboratoryByCompanyId(
                savedStateHandle[TissueSample.EXTRA_DEFAULT_LAB_COMPANY_ID] ?: defaults.labCompanyId
            )
        }
        val tissueSampleTypeDeferred = viewModelScope.async {
            tissueSampleTypeRepository.queryTissueSampleTypeById(defaults.tissueSampleTypeId)
        }
        val tissueTestTypeDeferred = viewModelScope.async {
            tissueTestRepository.queryTissueTestById(defaults.tissueTestId)
        }
        val tissueSampleContainerTypeDeferred = viewModelScope.async {
            tissueSampleContainerTypeRepository.queryTissueSampleContainerTypeById(defaults.tissueSampleContainerTypeId)
        }
        awaitAll(
            defaultSpeciesDeferred,
            laboratoryDeferred,
            tissueSampleTypeDeferred,
            tissueTestTypeDeferred,
            tissueSampleContainerTypeDeferred
        )
        defaultSpecies = requireNotNull(defaultSpeciesDeferred.await())
        _selectedLaboratory.update { laboratoryDeferred.await() }
        _selectedTissueSampleType.update { tissueSampleTypeDeferred.await() }
        _selectedTissueTestType.update { tissueTestTypeDeferred.await() }
        _selectedSampleContainerType.update { tissueSampleContainerTypeDeferred.await() }
    }

    private fun executeClearData() {
        containerId = ""
        containerExpDate = ""
        postEvents(
            InputEvent.ContainerIdChanged,
            InputEvent.ContainerExpDateChanged
        )
    }

    private suspend fun updateDatabase() {

        try {

            _isUpdatingDatabase.update { true }

            val selectedLaboratoryCompanyId = _selectedLaboratory.value?.companyId
            val selectedSampleTypeId = _selectedTissueSampleType.value?.id
            val selectedTestTypeId = _selectedTissueTestType.value?.id
            val selectedContainerTypeId = _selectedSampleContainerType.value?.id

            if (selectedLaboratoryCompanyId == null ||
                selectedSampleTypeId == null ||
                selectedTestTypeId == null ||
                selectedContainerTypeId == null
            ) {
                postEvent(IncompleteDataEntry)
                return
            }

            val animalBasicInfo = animalInfoState.value
                .takeAs<AnimalInfoLoaded>()?.animalBasicInfo ?: return

            val containerId = containerId.trim()
            val containerExpDate = containerExpDate.trim()

            withContext(Dispatchers.IO) {
                animalRepo.addTissueTestForAnimal(
                    animalBasicInfo.id,
                    selectedSampleTypeId,
                    selectedContainerTypeId,
                    containerId,
                    containerExpDate,
                    selectedTestTypeId,
                    selectedLaboratoryCompanyId,
                    LocalDateTime.now()
                )
            }
            postEvent(UpdateDatabaseEvent.Success)
        } catch(ex: Exception) {
            postEvent(UpdateDatabaseEvent.Error)
        } finally {
            _isUpdatingDatabase.update { false }
        }
    }

    private fun executePrintLabel() {

        val extraPrintLabelData = savedStateHandle.get<PrintLabelData>(
            TissueSample.EXTRA_PRINT_LABEL_DATA
        )

        if (extraPrintLabelData != null) {
            postEvent(PrintLabelRequestedEvent(extraPrintLabelData))
            return
        }

        val animalLoadedState = animalInfoState.value
            .takeAs<AnimalInfoLoaded>() ?: return

        val scannedEid = animalLoadedState.scannedEID
        val animalBasicInfo = animalLoadedState.animalBasicInfo

        val result = scannedEid?.let {
            extractPrintLabelData.forStandardLabel(it, secondaryIdTypeId, animalBasicInfo.ids)
        } ?: extractPrintLabelData.forStandardLabel(secondaryIdTypeId, animalBasicInfo.ids)

        postEvent(
            when (result) {
                is Result.Failure -> PrintLabelRequestedError(result.error)
                is Result.Success -> PrintLabelRequestedEvent(result.data)
            }
        )
    }

    private fun postEvent(event: Event) {
        viewModelScope.launch {
            _eventChannel.send(event)
        }
    }

    private fun postEvents(vararg events: Event) {
        viewModelScope.launch {
            events.forEach { _eventChannel.send(it) }
        }
    }
}
