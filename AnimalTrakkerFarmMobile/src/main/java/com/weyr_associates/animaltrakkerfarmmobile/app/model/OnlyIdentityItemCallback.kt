package com.weyr_associates.animaltrakkerfarmmobile.app.model

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil
import com.weyr_associates.animaltrakkerfarmmobile.model.HasIdentity

fun <T, ID> itemCallbackUsingOnlyIdentity(): DiffUtil.ItemCallback<T> where T : HasIdentity<ID>, ID : Comparable<ID> {
    return OnlyIdentityItemCallback()
}

class OnlyIdentityItemCallback<T, ID> : DiffUtil.ItemCallback<T>() where T : HasIdentity<ID>, ID : Comparable<ID> {
    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
        return oldItem.id == newItem.id
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
        return oldItem.id == newItem.id
    }
}
