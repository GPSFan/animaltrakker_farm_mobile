package com.weyr_associates.animaltrakkerfarmmobile.app.main;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.weyr_associates.animaltrakkerfarmmobile.AboutActivity;
import com.weyr_associates.animaltrakkerfarmmobile.BluetoothWatcher;
import com.weyr_associates.animaltrakkerfarmmobile.R;
import com.weyr_associates.animaltrakkerfarmmobile.RequiredPermissions;
import com.weyr_associates.animaltrakkerfarmmobile.baacodeService;
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderService;
import com.weyr_associates.animaltrakkerfarmmobile.preferences.EditPreferencesActivity;
import com.weyr_associates.animaltrakkerfarmmobile.scaleService;

import kotlin.Unit;

public class MainActivity extends AppCompatActivity {

    public static void returnFrom(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    private Boolean KeepScreenOn = false;
    private Boolean UseSerialEID = false;
    private Boolean ResetSettings = false;
    private Messenger mService = null;
    private Messenger mScaleService = null;
    private Messenger mBaaCodeService = null;
    private boolean mIsBound;
    private boolean mIsScaleBound;
    private boolean mIsBAABound;

    private final Messenger mMessenger = new Messenger(new IncomingHandler());

    private boolean hasExecutedForegroundChecks = false;

    private final BluetoothWatcher bluetoothWatcher = new BluetoothWatcher(this);

    private final ServiceConnection mConnection = new LocalServiceConnection();

    private final OnBackPressedCallback backPressedHandler = new OnBackPressedCallback(true) {
        @Override
        public void handleOnBackPressed() {
            NavController navController = Navigation.findNavController(
                    MainActivity.this, R.id.container_content);
            navController.popBackStack();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTitle(R.string.app_name_long);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.container_content);
        NavController navController = navHostFragment.getNavController();
        navController.addOnDestinationChangedListener((navController1, navDestination, bundle) -> {
                backPressedHandler.setEnabled(navDestination.getId() != R.id.nav_dst_menu_root &&
                        navDestination.getId() != R.id.nav_dst_required_permissions);
        });
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_dst_menu_root, R.id.nav_dst_required_permissions).build();
        NavigationUI.setupActionBarWithNavController(
                this, navController, appBarConfiguration);
        getOnBackPressedDispatcher().addCallback(this, backPressedHandler);
        bluetoothWatcher.setOnActivationChanged(enabled -> {
            onBluetoothActivationChanged();
            return Unit.INSTANCE;
        });
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        UseSerialEID = preferences.getBoolean("useserialeid", false);
        CheckIfServiceIsRunning();
    }

    @Override
    public void onResume() {
        super.onResume();
        checkRequiredPermissions();
        bluetoothWatcher.startWatching();
    }

    @Override
    public void onPause(){
        super.onPause();
        Log.i("Activity", "Paused" );
        hasExecutedForegroundChecks = false;
        bluetoothWatcher.stopWatching();
        if (mService != null) {
            try {
                //Stop tags eidService from sending tags
                Message msg = Message.obtain(null, EIDReaderService.MSG_NO_TAGS_PLEASE);
                msg.replyTo = mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (KeepScreenOn) {
            getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
        try {
//			Log.i("Activity", "onDestroy");
            doUnbindService();
            doUnbindScaleService();
            doUnbindBaaService();
            stopService(new Intent(MainActivity.this, EIDReaderService.class));
            stopService(new Intent(MainActivity.this, scaleService.class));
            stopService(new Intent(MainActivity.this, baacodeService.class));
        } catch (Throwable t) {
            Log.e("MainActivity", "Failed to unbind from the service(s)", t);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.container_content);
        NavDestination currentDestination = navController.getCurrentDestination();
        boolean isShowingRequiredPermissions = currentDestination != null &&
                currentDestination.getId() == R.id.nav_dst_required_permissions;
        return (!isShowingRequiredPermissions && navController.navigateUp()) ||
                super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //startActivity(new Intent(this, EditPreferences.class));
        super.onCreateOptionsMenu(menu);
        menu.removeItem(1);
        return true;
    }

    // process three dot options menu items
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {

            case R.id.menu_settings: //Settings
                startActivity(new Intent(this, EditPreferencesActivity.class));
                return true;

            case R.id.menu_about: //About
                startActivity(new Intent(this, AboutActivity.class));
                return true;
        }
        //       default:
        return super.onOptionsItemSelected(item);
    }

    private void useBluetoothReader () {
        // Display alerts here
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage( R.string.please_use_BT_reader )
                .setTitle( R.string.please_use_BT_reader1 );
        builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int idx) {
                dialog.cancel();
            }
        }).create() .show();
    }

    private void executeForegroundChecks() {

        if (hasExecutedForegroundChecks) return;

        hasExecutedForegroundChecks = true;

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        KeepScreenOn = preferences.getBoolean("keepscreenon", false);
        ResetSettings = preferences.getBoolean("reset", false);
        UseSerialEID = preferences.getBoolean("useserialeid", false);

        if (UseSerialEID) {
            if (!"NAUTIZ_X6P".equals(Build.MODEL)) {
                useBluetoothReader ();
                Log.i("SerialEID", "Got pref2.");
            }
        }
        if (ResetSettings) {
//            Log.i("Main Activity", "ResetSettings");
            preferences.edit().putBoolean("reset",false).apply();
            preferences.edit().putString("bluetooth_mac","00:00:00:00:00:00").apply();
            preferences.edit().putString("bluetooth1_mac","00:00:00:00:00:00").apply();
            preferences.edit().putString("bluetooth2_mac","00:00:00:00:00:00").apply();
            preferences.edit().putString("bluetooth_printer","00:00:00:00:00:00").apply();
            preferences.edit().putString("filenamemod","0").apply();

        }
        if (mIsBound) { // Request a status update.
            if (mService != null) {
                Log.i("Activity", "Resume Bound" );
                try {
                    //Request service reload preferences, in case those changed
                    Message msg = Message.obtain(null, EIDReaderService.MSG_RELOAD_PREFERENCES, 0, 0);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {}
            }
        }
        if (KeepScreenOn) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
        if (mService != null) {
            try {
                //Start eidService sending tags
                Message msg = Message.obtain(null, EIDReaderService.MSG_SEND_ME_TAGS);
                msg.replyTo = mMessenger;
                mService.send(msg);

            } catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
    }

    // runs once on app startup
    private void CheckIfServiceIsRunning() {
        //If the service is running when the activity starts, we want to automatically bind to it.
        // It usually isn't.
        if (EIDReaderService.isRunning()) {
            doBindService();
        }
        if (scaleService.isScaleRunning()) {
            doBindScaleService();
        }
        if (baacodeService.isBaaRunning()) {
            doBindBaaService();
        }
    }

    // Service Binding and unbinding code below

    private void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        bindService(new Intent(this, EIDReaderService.class), mConnection, Context.BIND_AUTO_CREATE);

        mIsBound = true;
        if (mService != null) {
            try {
                //Request status update
                Message msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_STATUS, 0, 0);
                msg.replyTo = mMessenger;
                mService.send(msg);

                //Request full log from service.
                msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_LOG_FULL, 0, 0);
                mService.send(msg);
            } catch (RemoteException e) {}
        }
    }

    private void doBindScaleService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        bindService(new Intent(this, scaleService.class), mConnection, Context.BIND_AUTO_CREATE);

        mIsScaleBound = true;
        Log.i("Activity","Binding Scale Service");
        if (mScaleService != null) {
            try {
                //Request status update
                Message msg = Message.obtain(null, scaleService.MSG_UPDATE_STATUS, 0, 0);
                msg.replyTo = mMessenger;
                mScaleService.send(msg);

                //Request full log from service.
                msg = Message.obtain(null, scaleService.MSG_UPDATE_LOG_FULL, 0, 0);
                mScaleService.send(msg);
            } catch (RemoteException e) {}
        }

    }
    private void doBindBaaService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        bindService(new Intent(this, baacodeService.class), mConnection, Context.BIND_AUTO_CREATE);

        mIsBAABound = true;
        if (mBaaCodeService != null) {
            try {
                //Request status update
                Message msg = Message.obtain(null, baacodeService.MSG_UPDATE_STATUS, 0, 0);
                msg.replyTo = mMessenger;
                mBaaCodeService.send(msg);

                //Request full log from service.
                msg = Message.obtain(null, baacodeService.MSG_UPDATE_LOG_FULL, 0, 0);
                mBaaCodeService.send(msg);
            } catch (RemoteException e) {}
        }
    }

    private void doUnbindService() {
//		Log.i("Activity", "At DoUnbindservice");
        if (mService != null) {
            try {
                //Stop eidService from sending tags
                Message msg = Message.obtain(null, EIDReaderService.MSG_NO_TAGS_PLEASE);
                msg.replyTo = mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        if (mIsBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, EIDReaderService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection);
            mIsBound = false;
        }

    }

    private void doUnbindScaleService() {
//		Log.i("Activity", "At DoUnbindservice");
        if (mScaleService != null) {
            try {
                //Stop scale Service from sending tags
                Message msg = Message.obtain(null,  scaleService.MSG_NO_TAGS_PLEASE);
                msg.replyTo = mMessenger;
                mScaleService.send(msg);
            } catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        if (mIsScaleBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, scaleService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mScaleService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection);
            mIsScaleBound = false;
        }
    }

    private void doUnbindBaaService() {
//		Log.i("Activity", "At DoUnbindservice");
        if (mBaaCodeService != null) {
            try {
                //Stop baa Service from sending tags
                Message msg = Message.obtain(null, baacodeService.MSG_NO_TAGS_PLEASE);
                msg.replyTo = mMessenger;
                mBaaCodeService.send(msg);
            } catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        if (mIsBAABound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, baacodeService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection);
            mIsBAABound = false;
        }
    }

    private void checkRequiredPermissions() {
        boolean requiredPermissionsFulfilled = RequiredPermissions.areFulfilled(this);
        showRequiredPermissionsCurtain(!requiredPermissionsFulfilled);
        if (requiredPermissionsFulfilled) {
            executeForegroundChecks();
        }
    }

    private void showRequiredPermissionsCurtain(boolean show) {
        NavController navController = Navigation.findNavController(this, R.id.container_content);
        if (show) {
            navController.navigate(R.id.nav_dst_required_permissions, null,
                    new NavOptions.Builder().setLaunchSingleTop(true).build());
        } else {
            NavDestination currentDestination = navController.getCurrentDestination();
            if (currentDestination != null && currentDestination.getId() == R.id.nav_dst_required_permissions) {
                navController.popBackStack(R.id.nav_dst_required_permissions, true);
            }
        }
    }

    private void onBluetoothActivationChanged() {
        checkRequiredPermissions();
    }

    private class LocalServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);
            try {
                //Register client with service
                Message msg = Message.obtain(null, EIDReaderService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);

                //Request a status update.
                msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_STATUS, 0, 0);
                mService.send(msg);

                //Request full log from service.
                msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_LOG_FULL, 0, 0);
                mService.send(msg);

                //Register client with service
                msg = Message.obtain(null, baacodeService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);

                //Request a status update.
                msg = Message.obtain(null, baacodeService.MSG_UPDATE_STATUS, 0, 0);
                mService.send(msg);

                //Request full log from service.
                msg = Message.obtain(null, baacodeService.MSG_UPDATE_LOG_FULL, 0, 0);
                mService.send(msg);

                //Register client with service
                msg = Message.obtain(null, scaleService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);

            } catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
            }
        }

        // this may never run
        @Override
        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            mService = null;
        }
    }

    private class IncomingHandler extends Handler {

        public IncomingHandler() {
            super(Looper.getMainLooper());
        }

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == EIDReaderService.MSG_THREAD_SUICIDE) {
                Log.i("Activity", "Service informed Activity of Suicide.");
                doUnbindService();
                stopService(new Intent(MainActivity.this, EIDReaderService.class));
            } else {
                super.handleMessage(msg);
            }
        }
    }
}
