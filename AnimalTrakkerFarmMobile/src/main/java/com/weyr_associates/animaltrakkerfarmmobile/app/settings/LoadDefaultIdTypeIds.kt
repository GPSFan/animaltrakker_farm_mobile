package com.weyr_associates.animaltrakkerfarmmobile.app.settings

data class DefaultIdTypeIds(
    val primaryIdTypeId: Int,
    val secondaryIdTypeId: Int,
    val tertiaryIdTypeId: Int
)

class LoadDefaultIdTypeIds(
    private val loadActiveDefaults: LoadActiveDefaultSettings
) {
    suspend operator fun invoke(): DefaultIdTypeIds {
        return loadActiveDefaults.invoke().let {
            DefaultIdTypeIds(
                it.idTypeIdPrimary,
                it.idTypeIdSecondary,
                it.idTypeIdTertiary
            )
        }
    }
}
