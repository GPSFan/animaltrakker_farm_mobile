package com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue

object TissueSample {
    const val ACTION_TAKE_TISSUE_SAMPLE_FOR_ANIMAL = "ACTION_TAKE_TISSUE_SAMPLE_FOR_ANIMAL"
    const val EXTRA_ANIMAL_BASIC_INFO = "EXTRA_ANIMAL_BASIC_INFO"
    const val EXTRA_TITLE_ADDITION = "EXTRA_TITLE_ADDITION"
    const val EXTRA_DEFAULT_LAB_COMPANY_ID = "EXTRA_DEFAULT_LAB_COMPANY_ID"
    const val EXTRA_PRINT_LABEL_DATA = "EXTRA_PRINT_LABEL_DATA"
}
