package com.weyr_associates.animaltrakkerfarmmobile.app.animal.id

object EditAnimalId {
    val EXTRA_ANIMAL_ID_TO_EDIT = "EXTRA_ANIMAL_ID_TO_EDIT"
    val EXTRA_ANIMAL_INFO = "EXTRA_ANIMAL_INFO"
}
