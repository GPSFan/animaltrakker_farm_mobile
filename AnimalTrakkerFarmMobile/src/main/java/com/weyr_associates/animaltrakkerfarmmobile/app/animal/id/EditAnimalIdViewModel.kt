package com.weyr_associates.animaltrakkerfarmmobile.app.animal.id

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.weyr_associates.animaltrakkerfarmmobile.app.core.Result
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalBasicInfo
import com.weyr_associates.animaltrakkerfarmmobile.model.IdBasicInfo
import com.weyr_associates.animaltrakkerfarmmobile.model.IdColor
import com.weyr_associates.animaltrakkerfarmmobile.model.IdLocation
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType
import com.weyr_associates.animaltrakkerfarmmobile.repository.AnimalRepository
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.time.LocalDateTime

class EditAnimalIdViewModel(
    private val savedStateHandle: SavedStateHandle,
    private val animalRepository: AnimalRepository,
    private val idValidations: IdValidations
) : ViewModel() {

    sealed interface Event

    data object IdNumberChanged : Event
    data object IdUpdateSucceeded : Event
    data object IdUpdateFailed : Event

    sealed interface ValidationError : Event {
        data object IdEntryRequired : ValidationError
        data object PartialIdEntry : ValidationError

        data class InvalidIdNumberFormat(
            val idEntry: IdEntry
        ) : ValidationError

        data class InvalidIdCombination(
            val error: IdsValidationError
        ) : ValidationError
    }

    val originalId: IdBasicInfo by lazy {
        requireNotNull(savedStateHandle.get<IdBasicInfo>(EditAnimalId.EXTRA_ANIMAL_ID_TO_EDIT))
    }

    val animalInfo: AnimalBasicInfo by lazy {
        requireNotNull(savedStateHandle.get<AnimalBasicInfo>(EditAnimalId.EXTRA_ANIMAL_INFO))
    }

    private val _idNumber = MutableStateFlow("")
    var idNumber: String
        get() = _idNumber.value
        set(value) {
            _idNumber.update { value }
        }

    private val _selectedIdType = MutableStateFlow<IdType?>(null)
    val selectedIdType = _selectedIdType.asStateFlow()

    private val _selectedIdColor = MutableStateFlow<IdColor?>(null)
    val selectedIdColor = _selectedIdColor.asStateFlow()

    private val _selectedIdLocation = MutableStateFlow<IdLocation?>(null)
    val selectedIdLocation = _selectedIdLocation.asStateFlow()

    val canUpdateId = combine(
        _idNumber,
        selectedIdType,
        selectedIdColor,
        selectedIdLocation
    ) { idNumber, idType, idColor, idLocation ->
        (idNumber.isNotBlank() && idNumber != originalId.number) ||
                (idType != null && idType.id != originalId.typeId) ||
                (idColor != null && idColor.id != originalId.colorId) ||
                (idLocation != null && idLocation.id != originalId.locationId)
    }.stateIn(viewModelScope, SharingStarted.Lazily, false)

    val canClearData = canUpdateId

    private val _eventsChannel = Channel<Event>()
    val events = _eventsChannel.receiveAsFlow()

    init {
        resetToOriginalIdInfo()
    }

    fun selectIdType(idType: IdType) {
        _selectedIdType.update { idType }
    }

    fun selectIdColor(idColor: IdColor) {
        _selectedIdColor.update { idColor }
    }

    fun selectIdLocation(idLocation: IdLocation) {
        _selectedIdLocation.update { idLocation }
    }

    fun updateId() {
        if (canUpdateId.value) {
            viewModelScope.launch {
                executeUpdateId()
            }
        }
    }

    fun clearData() {
        resetToOriginalIdInfo()
    }

    fun onEIDScanned(eidNumber: String) {
        idNumber = eidNumber
        viewModelScope.launch {
            _eventsChannel.send(IdNumberChanged)
        }
    }

    private fun resetToOriginalIdInfo() {
        idNumber = originalId.number
        selectIdType(IdType(originalId.typeId, originalId.typeName, originalId.typeAbbreviation, 0))
        selectIdColor(IdColor(originalId.colorId, originalId.colorName, originalId.colorAbbreviation, 0))
        selectIdLocation(IdLocation(originalId.locationId, originalId.locationName, originalId.locationAbbreviation, 0))
        viewModelScope.launch { _eventsChannel.send(IdNumberChanged) }
    }

    private suspend fun executeUpdateId() {
        val idInput = IdInput(
            number = idNumber,
            type = selectedIdType.value,
            color = selectedIdColor.value,
            location = selectedIdLocation.value
        )

        var idEntry = idInput.toEntry()

        val formatCheck = idValidations.checkIdNumberFormat(idEntry)

        if (formatCheck is Result.Failure) {
            _eventsChannel.send(
                ValidationError.InvalidIdNumberFormat(
                    formatCheck.error.idEntry
                )
            )
            return
        }

        idEntry = idEntry.copy(isOfficial = idValidations.checkIdEntryIsOfficial(idEntry))

        val idEntryDuplicateEIDCheck = idValidations.checkEIDsNotDuplicated
            .withAdditionOf(idEntry)

        if (idEntryDuplicateEIDCheck is Result.Failure) {
            _eventsChannel.send(
                ValidationError.InvalidIdCombination(
                    idEntryDuplicateEIDCheck.error
                )
            )
            return
        }

        val idEntryComboCheck = idValidations.checkIdCombinationValidity
            .whenUpdatingIdOnAnimal(idEntry, animalInfo.ids.map { it.asIdEntry() })

        if (idEntryComboCheck is Result.Failure) {
            _eventsChannel.send(
                ValidationError.InvalidIdCombination(
                    idEntryComboCheck.error
                )
            )
            return
        }

        val success = animalRepository.updateIdOnAnimal(
            id = originalId.id,
            typeId = idEntry.type.id,
            colorId = idEntry.color.id,
            locationId = idEntry.location.id,
            number = idEntry.number,
            LocalDateTime.now()
        )
        _eventsChannel.send(
            if (success) IdUpdateSucceeded
            else IdUpdateFailed
        )
    }
}
