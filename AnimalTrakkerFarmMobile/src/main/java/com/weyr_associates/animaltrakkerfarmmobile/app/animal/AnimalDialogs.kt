package com.weyr_associates.animaltrakkerfarmmobile.app.animal

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.weyr_associates.animaltrakkerfarmmobile.R

object AnimalDialogs {
    fun showAnimalAlert(context: Context, alertText: String) {
        AlertDialog.Builder(context)
            .setTitle(R.string.alert_warning)
            .setMessage(alertText)
            .setPositiveButton(R.string.ok) { _, _ -> /*NO-OP*/ }
            .create()
            .show()
    }
}
