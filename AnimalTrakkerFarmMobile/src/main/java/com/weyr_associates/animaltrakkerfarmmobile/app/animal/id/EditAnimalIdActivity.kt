package com.weyr_associates.animaltrakkerfarmmobile.app.animal.id

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.viewmodel.CreationExtras
import com.weyr_associates.animaltrakkerfarmmobile.BluetoothWatcher
import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.RequiredPermissions
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.coroutines.flow.observeOneTimeEvents
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.ItemSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.TopButtonBar
import com.weyr_associates.animaltrakkerfarmmobile.app.main.MainActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.select.idColorSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.idLocationSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.idTypeSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.baacodeService
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ActivityEditAnimalIdBinding
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderService
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderStatePresenter
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalBasicInfo
import com.weyr_associates.animaltrakkerfarmmobile.model.IdBasicInfo
import com.weyr_associates.animaltrakkerfarmmobile.model.IdColor
import com.weyr_associates.animaltrakkerfarmmobile.model.IdLocation
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.AnimalRepositoryImpl
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class EditAnimalIdActivity : AppCompatActivity() {

    companion object {
        @JvmStatic
        fun newIntent(context: Context, animalIdToEdit: IdBasicInfo, animalInfo: AnimalBasicInfo) =
            Intent(context, EditAnimalIdActivity::class.java).apply {
                putExtra(EditAnimalId.EXTRA_ANIMAL_ID_TO_EDIT, animalIdToEdit)
                putExtra(EditAnimalId.EXTRA_ANIMAL_INFO, animalInfo)
            }
    }

    private val binding: ActivityEditAnimalIdBinding by lazy {
        ActivityEditAnimalIdBinding.inflate(layoutInflater)
    }

    private val viewModel: EditAnimalIdViewModel by viewModels {
        EditAnimalIdViewModelFactory(this@EditAnimalIdActivity)
    }

    private lateinit var idTypePresenter: ItemSelectionPresenter<IdType>
    private lateinit var idColorPresenter: ItemSelectionPresenter<IdColor>
    private lateinit var idLocationPresenter: ItemSelectionPresenter<IdLocation>

    private lateinit var eidReaderStatePresenter: EIDReaderStatePresenter

    private var mService: Messenger? = null
    private var mIsBound: Boolean = false
    private val mMessenger: Messenger = Messenger(IncomingHandler())

    private val mConnection: ServiceConnection = EidReaderServiceConnection()
    private val bluetoothWatcher = BluetoothWatcher(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        with(binding.buttonPanelTop) {
            show(
                TopButtonBar.UI_SCANNER_STATUS or
                        TopButtonBar.UI_SCAN_EID or
                        TopButtonBar.UI_CLEAR_DATA
            )
            scanEIDButton.setOnClickListener {
                scanEID()
            }
            clearDataButton.setOnClickListener {
                viewModel.clearData()
            }
            eidReaderStatePresenter = EIDReaderStatePresenter(
                this@EditAnimalIdActivity,
                imageScannerStatus
            )
        }

        with(binding.animalIdInfo) {
            textIdNumber.text = viewModel.originalId.number
            textIdTypeName.text = viewModel.originalId.typeName
            textIdColorName.text = viewModel.originalId.colorAbbreviation
            textIdLocationName.text = viewModel.originalId.locationAbbreviation
        }

        with(binding.inputAnimalId) {
            inputIdNumber.setText(viewModel.idNumber)
            inputIdNumber.addTextChangedListener {
                viewModel.idNumber = it.toString()
            }
            buttonAddId.setText(R.string.text_update_id)
            buttonAddId.setOnClickListener {
                viewModel.updateId()
            }
        }

        idTypePresenter = idTypeSelectionPresenter(
            button = binding.inputAnimalId.spinnerIdType
        ) { idType ->
            viewModel.selectIdType(idType)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdType)
        }

        idColorPresenter = idColorSelectionPresenter(
            button = binding.inputAnimalId.spinnerIdColor
        ) { idColor ->
            viewModel.selectIdColor(idColor)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdColor)
        }

        idLocationPresenter = idLocationSelectionPresenter(
            button = binding.inputAnimalId.spinnerIdLocation
        ) { idLocation ->
            viewModel.selectIdLocation(idLocation)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdLocation)
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.canClearData.collectLatest { canClearData ->
                    binding.buttonPanelTop.clearDataButton.isEnabled = canClearData
                }
            }
        }
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.canUpdateId.collectLatest { canUpdateId ->
                    binding.inputAnimalId.buttonAddId.isEnabled = canUpdateId
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.events.observeOneTimeEvents {
                    handleEvent(it)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (checkRequiredPermissions()) {
            checkIfServiceIsRunning()
        }
        bluetoothWatcher.startWatching()
    }

    public override fun onPause() {
        super.onPause()
        Log.i("Breeding", " OnPause")
        doUnbindService()
        bluetoothWatcher.stopWatching()
    }

    private fun scanEID() {
        mService?.let { service ->
            try {
                //Start eidService sending tags
                val msg = Message.obtain(null, EIDReaderService.MSG_SEND_ME_TAGS)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
    }

    private fun onEIDScanned(eidNumber: String) {
        viewModel.onEIDScanned(eidNumber)
    }

    private fun handleEvent(event: EditAnimalIdViewModel.Event) {
        when (event) {
            EditAnimalIdViewModel.IdNumberChanged -> {
                binding.inputAnimalId.inputIdNumber.setText(viewModel.idNumber)
            }
            EditAnimalIdViewModel.IdUpdateSucceeded -> {
                setResult(RESULT_OK)
                finish()
            }
            EditAnimalIdViewModel.IdUpdateFailed -> {
                AlertDialog.Builder(this)
                    .setTitle(R.string.dialog_title_animal_id_update_failed)
                    .setMessage(R.string.dialog_message_animal_id_update_failed)
                    .setPositiveButton(R.string.ok) { _, _ -> /*NO-OP*/ }
                    .create()
                    .show()
            }
            EditAnimalIdViewModel.ValidationError.IdEntryRequired -> {
                IdValidationErrorDialog.showIdEntryIsRequiredError(this)
            }
            EditAnimalIdViewModel.ValidationError.PartialIdEntry -> {
                IdValidationErrorDialog.showPartialIdEntryError(this)
            }
            is EditAnimalIdViewModel.ValidationError.InvalidIdNumberFormat -> {
                IdValidationErrorDialog.showIdNumberFormatError(this, event.idEntry)
            }
            is EditAnimalIdViewModel.ValidationError.InvalidIdCombination -> {
                IdValidationErrorDialog.showIdCombinationError(this, event.error)
            }
        }
    }

    private fun checkRequiredPermissions(): Boolean {
        if (!RequiredPermissions.areFulfilled(this)) {
            MainActivity.returnFrom(this)
            return false
        }
        return true
    }

    //  Really should be check if services are running...
    //  Start the service if it is not already running and bind to it
    //  Should be one each for eid, scale and baacode
    private fun checkIfServiceIsRunning() {
        //If the service is running when the activity starts, we want to automatically bind to it.
        Log.i("Breeding", "At eid isRunning?.")
        if (EIDReaderService.isRunning()) {
//			Log.i("Breeding", " eidService is running");
            doBindService()
        } else {
//			Log.i("Breeding", " eidService is not running, start it");
            startService(Intent(this, EIDReaderService::class.java))
            doBindService()
        }
    }

    //  Bind services, should be one set of bind & unbind methods for eid, scale and baacode
    //  First is the eid service bind
    private fun doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
//		Log.i("Breeding", "At doBind1.");
        bindService(Intent(this, EIDReaderService::class.java), mConnection, BIND_AUTO_CREATE)
        //		Log.i("Breeding", "At doBind2.");
        mIsBound = true
        val service = mService
        if (service != null) {
//			Log.i("Breeding", "At doBind3.");
            try {
                //Request status update
                var msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_STATUS, 0, 0)
                msg.replyTo = mMessenger
                service.send(msg)
                //				Log.i("Breeding", "At doBind4.");
                //Request full log from service.
                msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_LOG_FULL, 0, 0)
                service.send(msg)
            } catch (e: RemoteException) {
                //NO-OP
            }
        }
        //		Log.i("Breeding", "At doBind5.");
    }

    //     Next the unbind eid servides
    private fun doUnbindService() {
//		Log.i("Breeding", "At DoUnbindservice");
        val service = mService
        if (service != null) {
            try {
                //Stop eidService from sending tags
                val msg = Message.obtain(null, EIDReaderService.MSG_NO_TAGS_PLEASE)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        if (mIsBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (service != null) {
                try {
                    val msg = Message.obtain(null, EIDReaderService.MSG_UNREGISTER_CLIENT)
                    msg.replyTo = mMessenger
                    service.send(msg)
                } catch (e: RemoteException) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection)
            mIsBound = false
        }
    }

    inner class EidReaderServiceConnection : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, binder: IBinder) {
            val service = Messenger(binder).also { mService = it }
            try {
                //Register client with service
                val msg = Message.obtain(null, EIDReaderService.MSG_REGISTER_CLIENT)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }

        //  *** This is the eid service disconnection
        override fun onServiceDisconnected(className: ComponentName) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            Log.i("Breeding", "At eid Service Disconnected.")
            mService = null
        }
    }

    internal inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                EIDReaderService.MSG_UPDATE_STATUS -> {
                    val b1 = msg.data
                    val state = b1.getString("stat")
                    if (state != null) {
                        eidReaderStatePresenter.updateReaderState(state)
                    }
                }

                EIDReaderService.MSG_NEW_EID_FOUND -> {
                    val bundle = msg.data
                    val eid = bundle.getString("info1")
                    // We have a good whole EID number
                    //TODO: Add format validation here.
                    if (eid != null) {
                        onEIDScanned(eid)
                    }
                }

                EIDReaderService.MSG_UPDATE_LOG_APPEND -> {}
                baacodeService.MSG_UPDATE_LOG_APPEND -> {
                    val b5 = msg.data
                }

                EIDReaderService.MSG_UPDATE_LOG_FULL -> {}
                EIDReaderService.MSG_THREAD_SUICIDE -> {
                    doUnbindService()
                    stopService(Intent(this@EditAnimalIdActivity, EIDReaderService::class.java))
                }

                else -> super.handleMessage(msg)
            }
        }
    }
}

private class EditAnimalIdViewModelFactory(context: Context) : ViewModelProvider.Factory {

    private val appContext = context.applicationContext

    override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
        return when (modelClass) {
            EditAnimalIdViewModel::class.java -> {
                val databaseHandler = DatabaseHandler.create(appContext)
                val animalRepository = AnimalRepositoryImpl(databaseHandler)
                @Suppress("UNCHECKED_CAST")
                EditAnimalIdViewModel(
                    extras.createSavedStateHandle(),
                    animalRepository,
                    IdValidations(animalRepository)
                ) as T
            }
            else -> {
                throw IllegalStateException("${modelClass.simpleName} is not supported.")
            }
        }
    }
}
