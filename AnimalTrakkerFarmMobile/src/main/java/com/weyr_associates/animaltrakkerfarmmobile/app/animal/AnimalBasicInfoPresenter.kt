package com.weyr_associates.animaltrakkerfarmmobile.app.animal

import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.AnimalIdsPresenter
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ViewAnimalBasicInfoBinding
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalBasicInfo

class AnimalBasicInfoPresenter(binding: ViewAnimalBasicInfoBinding? = null) {

    private val animalHeaderPresenter = AnimalBasicInfoHeaderPresenter(binding?.header)
    private val animalIdsPresenter = AnimalIdsPresenter(binding?.animalIds)

    var binding: ViewAnimalBasicInfoBinding? = binding
        set(value) {
            field = value
            animalHeaderPresenter.binding = value?.header
            animalIdsPresenter.binding = value?.animalIds
            bindViews()
        }

    var animalBasicInfo: AnimalBasicInfo? = null
        set(value) {
            field = value
            bindViews()
        }

    private fun bindViews() {
        animalHeaderPresenter.animalBasicInfo = animalBasicInfo
        animalIdsPresenter.idBasicInfoItems = animalBasicInfo?.ids
    }
}
