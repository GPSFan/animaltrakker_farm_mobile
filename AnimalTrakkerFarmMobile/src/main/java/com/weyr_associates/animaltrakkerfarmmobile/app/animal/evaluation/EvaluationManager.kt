package com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation

import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.EvaluationEditor.Event
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.EvaluationEditor.FieldValueChanged
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.coroutines.flow.combine21
import com.weyr_associates.animaltrakkerfarmmobile.model.EvalTrait
import com.weyr_associates.animaltrakkerfarmmobile.model.EvalTraitOption
import com.weyr_associates.animaltrakkerfarmmobile.model.SavedEvaluation
import com.weyr_associates.animaltrakkerfarmmobile.model.UnitOfMeasure
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class EvaluationManager(private val coroutineScope: CoroutineScope) : EvaluationEditor {

    private var currentEvaluation: SavedEvaluation? = null
        set(value) {
            field = value
            _isEvaluationLoaded.update { field != null }
        }

    private val _isEvaluationLoaded = MutableStateFlow(false)
    val isEvaluationLoaded = _isEvaluationLoaded.asStateFlow()

    fun loadEvaluation(evaluation: SavedEvaluation) {
        currentEvaluation = evaluation
        sendEvent(EvaluationEditor.EvaluationLoaded)
    }

    private val eventsChannel = Channel<Event>()

    override val events: Flow<Event>
        get() = eventsChannel.receiveAsFlow()

    //region Field Entry Requirements

    override val trait01Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait01.extractFieldEntry()
    override val trait02Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait02.extractFieldEntry()
    override val trait03Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait03.extractFieldEntry()
    override val trait04Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait04.extractFieldEntry()
    override val trait05Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait05.extractFieldEntry()
    override val trait06Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait06.extractFieldEntry()
    override val trait07Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait07.extractFieldEntry()
    override val trait08Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait08.extractFieldEntry()
    override val trait09Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait09.extractFieldEntry()
    override val trait10Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait10.extractFieldEntry()
    override val trait11Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait11.extractFieldEntry()
    override val trait12Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait12.extractFieldEntry()
    override val trait13Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait13.extractFieldEntry()
    override val trait14Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait14.extractFieldEntry()
    override val trait15Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait15.extractFieldEntry()
    override val trait16Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait16.extractFieldEntry()
    override val trait17Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait17.extractFieldEntry()
    override val trait18Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait18.extractFieldEntry()
    override val trait19Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait19.extractFieldEntry()
    override val trait20Entry: EvaluationFieldEntry
        get() = currentEvaluation?.trait20.extractFieldEntry()

    //endregion

    //region Field Names

    override val trait01Name: String
        get() = currentEvaluation?.trait01.extractFieldName()
    override val trait02Name: String
        get() = currentEvaluation?.trait02.extractFieldName()
    override val trait03Name: String
        get() = currentEvaluation?.trait03.extractFieldName()
    override val trait04Name: String
        get() = currentEvaluation?.trait04.extractFieldName()
    override val trait05Name: String
        get() = currentEvaluation?.trait05.extractFieldName()
    override val trait06Name: String
        get() = currentEvaluation?.trait06.extractFieldName()
    override val trait07Name: String
        get() = currentEvaluation?.trait07.extractFieldName()
    override val trait08Name: String
        get() = currentEvaluation?.trait08.extractFieldName()
    override val trait09Name: String
        get() = currentEvaluation?.trait09.extractFieldName()
    override val trait10Name: String
        get() = currentEvaluation?.trait10.extractFieldName()
    override val trait11Name: String
        get() = currentEvaluation?.trait11.extractFieldName()
    override val trait12Name: String
        get() = currentEvaluation?.trait12.extractFieldName()
    override val trait13Name: String
        get() = currentEvaluation?.trait13.extractFieldName()
    override val trait14Name: String
        get() = currentEvaluation?.trait14.extractFieldName()
    override val trait15Name: String
        get() = currentEvaluation?.trait15.extractFieldName()
    override val trait16Name: String
        get() = currentEvaluation?.trait16.extractFieldName()
    override val trait17Name: String
        get() = currentEvaluation?.trait17.extractFieldName()
    override val trait18Name: String
        get() = currentEvaluation?.trait18.extractFieldName()
    override val trait19Name: String
        get() = currentEvaluation?.trait19.extractFieldName()
    override val trait20Name: String
        get() = currentEvaluation?.trait20.extractFieldName()

    //endregion

    //region Field Units

    override val trait11Units: UnitOfMeasure
        get() = currentEvaluation?.trait11?.units ?: UnitOfMeasure.NONE
    override val trait12Units: UnitOfMeasure
        get() = currentEvaluation?.trait12?.units ?: UnitOfMeasure.NONE
    override val trait13Units: UnitOfMeasure
        get() = currentEvaluation?.trait13?.units ?: UnitOfMeasure.NONE
    override val trait14Units: UnitOfMeasure
        get() = currentEvaluation?.trait14?.units ?: UnitOfMeasure.NONE
    override val trait15Units: UnitOfMeasure
        get() = currentEvaluation?.trait15?.units ?: UnitOfMeasure.NONE

    //endregion

    //region Field Options

    override val trait16Options: List<EvalTraitOption>
        get() = currentEvaluation?.trait16?.options ?: emptyList()
    override val trait17Options: List<EvalTraitOption>
        get() = currentEvaluation?.trait17?.options ?: emptyList()
    override val trait18Options: List<EvalTraitOption>
        get() = currentEvaluation?.trait18?.options ?: emptyList()
    override val trait19Options: List<EvalTraitOption>
        get() = currentEvaluation?.trait19?.options ?: emptyList()
    override val trait20Options: List<EvalTraitOption>
        get() = currentEvaluation?.trait20?.options ?: emptyList()

    //endregion

    //region Field Values

    private val _trait01 = MutableStateFlow(0)
    override val trait01: StateFlow<Int>
        get() = _trait01.asStateFlow()

    private val _trait02 = MutableStateFlow(0)
    override val trait02: StateFlow<Int>
        get() = _trait02.asStateFlow()

    private val _trait03 = MutableStateFlow(0)
    override val trait03: StateFlow<Int>
        get() = _trait03.asStateFlow()

    private val _trait04 = MutableStateFlow(0)
    override val trait04: StateFlow<Int>
        get() = _trait04.asStateFlow()

    private val _trait05 = MutableStateFlow(0)
    override val trait05: StateFlow<Int>
        get() = _trait05.asStateFlow()

    private val _trait06 = MutableStateFlow(0)
    override val trait06: StateFlow<Int>
        get() = _trait06.asStateFlow()

    private val _trait07 = MutableStateFlow(0)
    override val trait07: StateFlow<Int>
        get() = _trait07.asStateFlow()

    private val _trait08 = MutableStateFlow(0)
    override val trait08: StateFlow<Int>
        get() = _trait08.asStateFlow()

    private val _trait09 = MutableStateFlow(0)
    override val trait09: StateFlow<Int>
        get() = _trait09.asStateFlow()

    private val _trait10 = MutableStateFlow(0)
    override val trait10: StateFlow<Int>
        get() = _trait10.asStateFlow()

    private val _trait11 = MutableStateFlow<Float?>(null)
    override val trait11: StateFlow<Float?>
        get() = _trait11.asStateFlow()

    private val _trait12 = MutableStateFlow<Float?>(null)
    override val trait12: StateFlow<Float?>
        get() = _trait12.asStateFlow()

    private val _trait13 = MutableStateFlow<Float?>(null)
    override val trait13: StateFlow<Float?>
        get() = _trait13.asStateFlow()

    private val _trait14 = MutableStateFlow<Float?>(null)
    override val trait14: StateFlow<Float?>
        get() = _trait14.asStateFlow()

    private val _trait15 = MutableStateFlow<Float?>(null)
    override val trait15: StateFlow<Float?>
        get() = _trait15.asStateFlow()

    private val _trait16 = MutableStateFlow<EvalTraitOption?>(null)
    override val trait16: StateFlow<EvalTraitOption?>
        get() = _trait16.asStateFlow()

    private val _trait17 = MutableStateFlow<EvalTraitOption?>(null)
    override val trait17: StateFlow<EvalTraitOption?>
        get() = _trait17.asStateFlow()

    private val _trait18 = MutableStateFlow<EvalTraitOption?>(null)
    override val trait18: StateFlow<EvalTraitOption?>
        get() = _trait18.asStateFlow()

    private val _trait19 = MutableStateFlow<EvalTraitOption?>(null)
    override val trait19: StateFlow<EvalTraitOption?>
        get() = _trait19.asStateFlow()

    private val _trait20 = MutableStateFlow<EvalTraitOption?>(null)
    override val trait20: StateFlow<EvalTraitOption?>
        get() = _trait20.asStateFlow()

    //endregion

    //region Evaluation States

    val canClearData: StateFlow<Boolean> = combine21(
        isEvaluationLoaded,
        trait01, trait02, trait03, trait04, trait05,
        trait06, trait07, trait08, trait09, trait10,
        trait11, trait12, trait13, trait14, trait15,
        trait16, trait17, trait18, trait19, trait20,
    ) { isEvalLoaded, t01, t02, t03, t04, t05, t06, t07, t08, t09, t10,
        t11, t12, t13, t14, t15, t16, t17, t18, t19, t20 ->
        isEvalLoaded && (t01 != 0 || t02 != 0 || t03 != 0 || t04 != 0 ||
                t05 != 0 || t06 != 0 || t07 != 0 || t08 != 0 ||
                t09 != 0 || t10 != 0 || t11 != null || t12 != null ||
                t13 != null || t14 != null || t15 != null || t16 != null ||
                t17 != null || t18 != null || t19 != null || t20 != null)
    }.stateIn(coroutineScope, SharingStarted.Lazily, false)

    val isEvaluationComplete: Flow<Boolean> = combine21(
        isEvaluationLoaded,
        trait01, trait02, trait03, trait04, trait05,
        trait06, trait07, trait08, trait09, trait10,
        trait11, trait12, trait13, trait14, trait15,
        trait16, trait17, trait18, trait19, trait20,
    ) { isEvalLoaded, t01, t02, t03, t04, t05, t06, t07, t08, t09, t10,
        t11, t12, t13, t14, t15, t16, t17, t18, t19, t20 ->
        isEvalLoaded &&
                (t01 != 0 || trait01Entry.isNotRequired) &&
                (t02 != 0 || trait02Entry.isNotRequired) &&
                (t03 != 0 || trait03Entry.isNotRequired) &&
                (t04 != 0 || trait04Entry.isNotRequired) &&
                (t05 != 0 || trait05Entry.isNotRequired) &&
                (t06 != 0 || trait06Entry.isNotRequired) &&
                (t07 != 0 || trait07Entry.isNotRequired) &&
                (t08 != 0 || trait08Entry.isNotRequired) &&
                (t09 != 0 || trait09Entry.isNotRequired) &&
                (t10 != 0 || trait10Entry.isNotRequired) &&
                (t11 != null || trait11Entry.isNotRequired) &&
                (t12 != null || trait12Entry.isNotRequired) &&
                (t13 != null || trait13Entry.isNotRequired) &&
                (t14 != null || trait14Entry.isNotRequired) &&
                (t15 != null || trait15Entry.isNotRequired) &&
                (t16 != null || trait16Entry.isNotRequired) &&
                (t17 != null || trait17Entry.isNotRequired) &&
                (t18 != null || trait18Entry.isNotRequired) &&
                (t19 != null || trait19Entry.isNotRequired) &&
                (t20 != null || trait20Entry.isNotRequired)
    }.stateIn(coroutineScope, SharingStarted.Lazily, false)

    //endregion

    //region Field Manipulators

    override fun setTrait01(value: Int) {
        _trait01.update { value }
    }

    override fun setTrait02(value: Int) {
        _trait02.update { value }
    }

    override fun setTrait03(value: Int) {
        _trait03.update { value }
    }

    override fun setTrait04(value: Int) {
        _trait04.update { value }
    }

    override fun setTrait05(value: Int) {
        _trait05.update { value }
    }

    override fun setTrait06(value: Int) {
        _trait06.update { value }
    }

    override fun setTrait07(value: Int) {
        _trait07.update { value }
    }

    override fun setTrait08(value: Int) {
        _trait08.update { value }
    }

    override fun setTrait09(value: Int) {
        _trait09.update { value }
    }

    override fun setTrait10(value: Int) {
        _trait10.update { value }
    }

    override fun setTrait11(value: Float?) {
        _trait11.update { value }
    }

    override fun setTrait12(value: Float?) {
        _trait12.update { value }
    }

    override fun setTrait13(value: Float?) {
        _trait13.update { value }
    }

    override fun setTrait14(value: Float?) {
        _trait14.update { value }
    }

    override fun setTrait15(value: Float?) {
        _trait15.update { value }
    }

    override fun setTrait16(option: EvalTraitOption?) {
        _trait16.update { option }
    }

    override fun setTrait17(option: EvalTraitOption?) {
        _trait17.update { option }
    }

    override fun setTrait18(option: EvalTraitOption?) {
        _trait18.update { option }
    }

    override fun setTrait19(option: EvalTraitOption?) {
        _trait19.update { option }
    }

    override fun setTrait20(option: EvalTraitOption?) {
        _trait20.update { option }
    }

    fun clearEvaluation() {
        setTrait01(0)
        setTrait02(0)
        setTrait03(0)
        setTrait04(0)
        setTrait05(0)
        setTrait06(0)
        setTrait07(0)
        setTrait08(0)
        setTrait09(0)
        setTrait10(0)
        setTrait11(null)
        setTrait12(null)
        setTrait13(null)
        setTrait14(null)
        setTrait15(null)
        setTrait16(null)
        setTrait17(null)
        setTrait18(null)
        setTrait19(null)
        setTrait20(null)
        sendEvent(EvaluationEditor.FieldValuesCleared)
    }

    fun extractEvaluationEntry(): EvaluationEntry? {
        val evaluation = currentEvaluation ?: return null
        return EvaluationEntry(
            trait01Id = evaluation.trait01.id,
            trait02Id = evaluation.trait02.id,
            trait03Id = evaluation.trait03.id,
            trait04Id = evaluation.trait04.id,
            trait05Id = evaluation.trait05.id,
            trait06Id = evaluation.trait06.id,
            trait07Id = evaluation.trait07.id,
            trait08Id = evaluation.trait08.id,
            trait09Id = evaluation.trait09.id,
            trait10Id = evaluation.trait10.id,
            trait11Id = evaluation.trait11.id,
            trait12Id = evaluation.trait12.id,
            trait13Id = evaluation.trait13.id,
            trait14Id = evaluation.trait14.id,
            trait15Id = evaluation.trait15.id,
            trait16Id = evaluation.trait16.id,
            trait17Id = evaluation.trait17.id,
            trait18Id = evaluation.trait18.id,
            trait19Id = evaluation.trait19.id,
            trait20Id = evaluation.trait20.id,
            trait11UnitsId = evaluation.trait11.units.id,
            trait12UnitsId = evaluation.trait12.units.id,
            trait13UnitsId = evaluation.trait13.units.id,
            trait14UnitsId = evaluation.trait14.units.id,
            trait15UnitsId = evaluation.trait15.units.id,
            trait01Score = trait01.value,
            trait02Score = trait02.value,
            trait03Score = trait03.value,
            trait04Score = trait04.value,
            trait05Score = trait05.value,
            trait06Score = trait06.value,
            trait07Score = trait07.value,
            trait08Score = trait08.value,
            trait09Score = trait09.value,
            trait10Score = trait10.value,
            trait11Score = trait11.value ?: 0f,
            trait12Score = trait12.value ?: 0f,
            trait13Score = trait13.value ?: 0f,
            trait14Score = trait14.value ?: 0f,
            trait15Score = trait15.value ?: 0f,
            trait16OptionId = trait16.value?.id ?: 0,
            trait17OptionId = trait17.value?.id ?: 0,
            trait18OptionId = trait18.value?.id ?: 0,
            trait19OptionId = trait19.value?.id ?: 0,
            trait20OptionId = trait20.value?.id ?: 0,
        )
    }
    
    //endregion

    private fun sendFieldValueChanged(field: EvaluationField) {
        sendEvent(FieldValueChanged(field))
    }

    private fun sendEvent(event: Event) {
        coroutineScope.launch {
            eventsChannel.send(event)
        }
    }
}

private fun EvalTrait?.extractFieldEntry(): EvaluationFieldEntry = when {
    this == null -> EvaluationFieldEntry.UNCOLLECTED
    isEmpty || isDeferred -> EvaluationFieldEntry.UNCOLLECTED
    isOptional -> EvaluationFieldEntry.OPTIONAL
    else -> EvaluationFieldEntry.REQUIRED
}

private fun EvalTrait?.extractFieldName(): String {
    return this?.name ?: ""
}
