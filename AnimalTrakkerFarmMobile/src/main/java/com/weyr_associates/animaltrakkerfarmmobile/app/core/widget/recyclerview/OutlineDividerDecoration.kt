package com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.recyclerview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.weyr_associates.animaltrakkerfarmmobile.R

class OutlineDividerDecoration(context: Context) : RecyclerView.ItemDecoration() {
    private val inset = context.resources.getDimensionPixelOffset(R.dimen.interior_spacing_margin)
    private val insetInterior = inset / 2
    private val insetPaint = Paint().apply {
        color = ContextCompat.getColor(context, R.color.background_widget)
    }
    private val childBounds = Rect()
    private val decorationRect = RectF()

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val itemPosition = parent.getChildAdapterPosition(view)
        when (itemPosition) {
            0 -> {
                outRect.set(inset, inset, inset, insetInterior)
            }
            state.itemCount - 1 -> {
                outRect.set(inset, insetInterior, inset, inset)
            }
            else -> {
                outRect.set(inset, insetInterior, inset, insetInterior)
            }
        }
    }

    override fun onDraw(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        canvas.save()

        var left = 0
        var right = parent.width
        var top = 0
        var bottom = parent.height

        if (parent.clipToPadding) {
            left = parent.paddingLeft
            right = parent.width - parent.paddingRight
            top = parent.paddingTop
            bottom = parent.height - parent.paddingBottom
            canvas.clipRect(left, top, right, bottom)
        }

        val childCount = parent.childCount
        for (index: Int in 0 until childCount) {
            val child = parent.getChildAt(index)
            val itemPosition = parent.getChildAdapterPosition(child)
            parent.getDecoratedBoundsWithMargins(child, childBounds)
            val translationX = child.translationX
            val translationY = child.translationY

            //Draw left decoration
            decorationRect.set(
                left.toFloat(),
                childBounds.top + translationY,
                childBounds.left + translationX + inset,
                childBounds.bottom + translationY
            )
            drawCurrentDecoration(canvas)

            //Draw right decoration
            decorationRect.set(
                childBounds.right + translationX - inset,
                childBounds.top + translationY,
                right.toFloat(),
                childBounds.bottom + translationY
            )
            drawCurrentDecoration(canvas)

            //Draw top decoration
            when (itemPosition) {
                0 -> decorationRect.set(
                    left.toFloat(),
                    top.toFloat(),
                    right.toFloat(),
                    childBounds.top + translationY + inset
                )
                else -> decorationRect.set(
                    left.toFloat(),
                    childBounds.top + translationY,
                    right.toFloat(),
                    childBounds.top + translationY + insetInterior
                )
            }
            drawCurrentDecoration(canvas)

            //Draw bottom decoration
            when (itemPosition) {
                state.itemCount - 1 -> decorationRect.set(
                    left.toFloat(),
                    childBounds.bottom + translationY - inset,
                    right.toFloat(),
                    bottom.toFloat()
                )
                else -> decorationRect.set(
                    left.toFloat(),
                    childBounds.bottom + translationY - insetInterior,
                    right.toFloat(),
                    childBounds.bottom + translationY
                )
            }
            drawCurrentDecoration(canvas)
        }
        canvas.restore()
    }

    private fun drawCurrentDecoration(canvas: Canvas) {
        canvas.drawRect(
            decorationRect.left,
            decorationRect.top,
            decorationRect.right,
            decorationRect.bottom,
            insetPaint
        )
    }
}
