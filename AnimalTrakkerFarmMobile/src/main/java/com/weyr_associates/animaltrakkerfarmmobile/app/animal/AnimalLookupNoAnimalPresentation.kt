package com.weyr_associates.animaltrakkerfarmmobile.app.animal

import android.app.Activity
import android.app.AlertDialog
import androidx.core.view.isGone
import androidx.core.view.isVisible
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.SimpleAddAnimalActivity
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ViewLookupAnimalNoneBinding
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType

fun ViewLookupAnimalNoneBinding.setupForInitialLookup() {
    btnAddAnimal.isGone = true
    textNoAnimal.setText(R.string.text_no_animal_loaded_scan_or_lookup)
}

fun ViewLookupAnimalNoneBinding.setupForAnimalNotFound() {
    textNoAnimal.setText(R.string.text_no_animal_found_for_animal_id)
    btnAddAnimal.isGone = true
    btnAddAnimal.setOnClickListener(null)
}

fun ViewLookupAnimalNoneBinding.setupForAnimalWithEIDNotFound(activity: Activity, eidNumber: String, requestCode: Int) {
    textNoAnimal.text = activity.getString(R.string.text_no_animal_found_for_scanned_eid, eidNumber)
    btnAddAnimal.isVisible = true
    btnAddAnimal.setOnClickListener {
        promptToAddAnimalWithEID(activity, eidNumber, requestCode)
    }
}

private fun promptToAddAnimalWithEID(activity: Activity, eidNumber: String, requestCode: Int) {
    AlertDialog.Builder(activity)
        .setTitle(R.string.dialog_title_add_animal_from_unknown_eid)
        .setMessage(activity.getString(R.string.dialog_message_add_animal_from_unknown_eid, eidNumber))
        .setPositiveButton(R.string.yes_label) { _, _ ->
            SimpleAddAnimalActivity.startToAddAndSelect(
                activity,
                IdType.ID_TYPE_ID_EID,
                eidNumber,
                requestCode
            )
        }
        .setNegativeButton(R.string.no_label) { _, _ -> /*NO-OP*/ }
        .create()
        .show()
}
