package com.weyr_associates.animaltrakkerfarmmobile.app.main.menu

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.SimpleLambingActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.core.checkDatabaseIsPresentThen
import com.weyr_associates.animaltrakkerfarmmobile.databinding.FragmentMenuBirthingBinding

class BirthingMenuFragment : Fragment(R.layout.fragment_menu_birthing) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(FragmentMenuBirthingBinding.bind(view)) {
            btnSimpleLambing.setOnClickListener {
                checkDatabaseIsPresentThen {
                    startActivity(Intent(requireActivity(), SimpleLambingActivity::class.java))
                }
            }
            listOf(
                btnSimpleLambing,
                btnDetailedLambing,
                btnSimpleBirths,
                btnAddFemaleBirthingHistory
            ).forEach { it.deactivate() }
        }
    }
}