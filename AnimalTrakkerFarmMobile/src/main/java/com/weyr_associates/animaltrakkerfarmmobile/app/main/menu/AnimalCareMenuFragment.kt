package com.weyr_associates.animaltrakkerfarmmobile.app.main.menu

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.databinding.FragmentMenuAnimalCareBinding

class AnimalCareMenuFragment : Fragment(R.layout.fragment_menu_animal_care) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(FragmentMenuAnimalCareBinding.bind(view)) {
            listOf(
                btnSickAnimals,
                btnVaccinateDeworm,
                btnGeneralAnimalCare,
                btnGroupVaccinateDeworm,
                btnGroupGeneralAnimalCare
            ).forEach { it.deactivate() }
        }
    }
}