package com.weyr_associates.animaltrakkerfarmmobile.app.main.menu

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.weyr_associates.animaltrakkerfarmmobile.MaleBreedingSoundnessActivity
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.optimalag.OptAgRamBreedingSoundnessActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue.TissueSampleActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.core.checkDatabaseIsPresentThen
import com.weyr_associates.animaltrakkerfarmmobile.databinding.FragmentMenuAnimalEvaluationBinding

class AnimalEvaluationMenuFragment : Fragment(R.layout.fragment_menu_animal_evaluation) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(FragmentMenuAnimalEvaluationBinding.bind(view)) {
            btnMaleBreedingSoundness.setOnClickListener {
                checkDatabaseIsPresentThen {
                    startActivity(Intent(requireActivity(), MaleBreedingSoundnessActivity::class.java))
                }
            }
            btnTakeTissueSample.setOnClickListener {
                checkDatabaseIsPresentThen {
                    startActivity(Intent(requireActivity(), TissueSampleActivity::class.java))
                }
            }
            btnOptimalAgRamBse.setOnClickListener {
                checkDatabaseIsPresentThen {
                    startActivity(Intent(requireActivity(), OptAgRamBreedingSoundnessActivity::class.java))
                }
            }
            listOf(
                btnMaleBreedingSoundness,
                btnOptimalAgEweUltrasound,
                btnEvaluateAnimal,
                btnSelectEvaluation,
                btnCreateEvaluation
            ).forEach { it.deactivate() }
        }
    }
}
