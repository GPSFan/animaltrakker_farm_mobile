package com.weyr_associates.animaltrakkerfarmmobile.app.animal

import android.content.Context
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ViewAnimalBasicInfoHeaderBinding
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalBasicInfo

class AnimalBasicInfoHeaderPresenter(binding: ViewAnimalBasicInfoHeaderBinding? = null) {

    var binding: ViewAnimalBasicInfoHeaderBinding? = binding
        set(value) {
            field = value
            bindViews()
        }

    var animalBasicInfo: AnimalBasicInfo? = null
        set(value) {
            field = value
            bindViews()
        }

    private fun bindViews() {
        val binding = binding ?: return
        val context = binding.root.context
        binding.textAnimalName.text = animalBasicInfo?.name ?: ""
        binding.textOwnerName.text = animalBasicInfo?.let {
            it.ownerName ?: getNotApplicableString(context)
        } ?: ""
        binding.textFlockName.text = animalBasicInfo?.let {
            it.flockPrefix ?: getNotApplicableString(context)
        } ?: ""
    }

    private fun getNotApplicableString(context: Context): String {
        return context.getString(R.string.text_not_applicable)
    }
}
