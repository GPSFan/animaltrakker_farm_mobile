package com.weyr_associates.animaltrakkerfarmmobile.app.animal.note

import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.FragmentResultListener
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.viewmodel.CreationExtras
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.coroutines.flow.observeOneTimeEvents
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.selectedItem
import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.TopButtonBar.Companion.UI_ACTION_UPDATE_DATABASE
import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.TopButtonBar.Companion.UI_CLEAR_DATA
import com.weyr_associates.animaltrakkerfarmmobile.app.model.itemCallbackUsingOnlyIdentity
import com.weyr_associates.animaltrakkerfarmmobile.app.select.SelectPredefinedNoteDialogFragment
import com.weyr_associates.animaltrakkerfarmmobile.app.select.SelectPredefinedNoteDialogFragment.Companion.REQUEST_KEY_SELECT_PREDEFINED_NOTE
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ActivityTakeNotesBinding
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ItemPredefinedNoteBinding
import com.weyr_associates.animaltrakkerfarmmobile.model.PredefinedNote
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.AnimalRepositoryImpl
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class TakeNotesActivity : AppCompatActivity() {

    companion object {
        @JvmStatic
        fun newIntent(context: Context, animalId: Int): Intent {
            return Intent(context, TakeNotesActivity::class.java).apply {
                putExtra(TakeNotes.EXTRA_ANIMAL_ID, animalId)
            }
        }

        private const val TAG_FRAGMENT_SELECT_NOTE = "TAG_FRAGMENT_SELECT_NOTE"
    }

    private val binding by lazy {
        ActivityTakeNotesBinding.inflate(layoutInflater)
    }

    private val notesAdapter = PredefinedNoteAdapter(
        onUpdateNote = { id -> selectNoteToReplace(id) },
        onClearNote = { id -> clearNote(id) }
    )

    private val viewModel by viewModels<TakeNotesViewModel>() {
        ViewModelFactory(this)
    }

    private val onNoteSelected = FragmentResultListener { requestKey, result ->
        handleNoteSelected(requestKey, result.selectedItem())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.buttonPanelTop.show(UI_CLEAR_DATA or UI_ACTION_UPDATE_DATABASE)
        binding.buttonPanelTop.clearDataButton.setOnClickListener {
            viewModel.clearData()
        }
        binding.buttonPanelTop.mainActionButton.setOnClickListener {
            viewModel.saveData()
        }
        binding.buttonAddNote.setOnClickListener {
            addNewNote()
        }
        binding.inputCustomNoteText.addTextChangedListener {
            viewModel.customNoteText = it.toString()
        }
        with(binding.recyclerNotes) {
            adapter = notesAdapter
            layoutManager = LinearLayoutManager(
                this@TakeNotesActivity,
                RecyclerView.VERTICAL,
                false
            )
            addItemDecoration(NoteSpacingDecoration(this@TakeNotesActivity))
        }
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.notes.collectLatest {
                    notesAdapter.submitList(it.items)
                }
            }
        }
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.canClearData.collectLatest { canClear ->
                    binding.buttonPanelTop.clearDataButton.isEnabled = canClear
                }
            }
        }
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.canSaveData.collectLatest { canSave ->
                    binding.buttonPanelTop.mainActionButton.isEnabled = canSave
                }
            }
        }
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.events.observeOneTimeEvents {
                    handleEvent(it)
                }
            }
        }
    }

    private fun addNewNote() {
        selectNote(REQUEST_KEY_SELECT_PREDEFINED_NOTE)
    }

    private fun selectNoteToReplace(id: Int) {
        selectNote(requestKeyForSelectionToReplace(id))
    }

    private fun clearNote(id: Int) {
        viewModel.clearNote(id)
    }

    private fun selectNote(requestKey: String) {
        supportFragmentManager.setFragmentResultListener(
            requestKey,
            this,
            onNoteSelected
        )
        SelectPredefinedNoteDialogFragment.newInstance(
            requestKey = requestKey,
            excludedNoteIds = viewModel.notes.value.ids
        ).show(supportFragmentManager, TAG_FRAGMENT_SELECT_NOTE)
    }

    private fun requestKeyForSelectionToReplace(id: Int): String {
        return "${REQUEST_KEY_SELECT_PREDEFINED_NOTE}|$id"
    }

    private fun handleNoteSelected(requestKey: String, note: PredefinedNote) {
        when (requestKey) {
            REQUEST_KEY_SELECT_PREDEFINED_NOTE -> {
                viewModel.addNote(note)
            }
            else -> {
                val splits = requestKey.split("|")
                if (splits.size != 2 || splits[0] != REQUEST_KEY_SELECT_PREDEFINED_NOTE) {
                    //If we are here we have something very
                    //different from our request key scheme allows.
                    return
                }
                //We expect, based on our scheme, for this to be an integer.
                val id = splits[1].toIntOrNull() ?: return
                viewModel.replaceNote(id, note)
            }
        }
    }

    private fun handleEvent(event: TakeNotesViewModel.Event) {
        when (event) {
            TakeNotesViewModel.CustomNoteTextChanged -> {
                binding.inputCustomNoteText.setText(viewModel.customNoteText)
            }
            TakeNotesViewModel.DatabaseUpdateSucceeded -> {
                showDatabaseUpdateSucceeded()
            }
            TakeNotesViewModel.DatabaseUpdateFailed -> {
                showDatabaseUpdateFailed()
            }
        }
    }

    private fun showDatabaseUpdateSucceeded() {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title_notes_added)
            .setMessage(R.string.dialog_message_notes_added)
            .setPositiveButton(R.string.ok) { _, _ -> /*NO-OP*/ }
            .setOnDismissListener { finish() }
            .create()
            .show()
    }

    private fun showDatabaseUpdateFailed() {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title_add_notes_failed)
            .setMessage(R.string.dialog_message_add_notes_failed)
            .setPositiveButton(R.string.ok) { _, _ -> /*NO-OP*/ }
            .create()
            .show()
    }
}

private class PredefinedNoteAdapter(
    private val onUpdateNote: (Int) -> Unit,
    private val onClearNote: (Int) -> Unit
) : ListAdapter<PredefinedNote, PredefinedNoteViewHolder>(
    itemCallbackUsingOnlyIdentity()
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PredefinedNoteViewHolder {
        return PredefinedNoteViewHolder(
            ItemPredefinedNoteBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ),
            onUpdateNote,
            onClearNote
        )
    }

    override fun onBindViewHolder(holder: PredefinedNoteViewHolder, position: Int) {
        holder.bind(currentList[position])
    }
}

private class PredefinedNoteViewHolder(
    private val binding: ItemPredefinedNoteBinding,
    private val onUpdateNote: (Int) -> Unit,
    private val onClearNote: (Int) -> Unit
) : ViewHolder(binding.root) {

    fun bind(note: PredefinedNote) {
        binding.buttonSpinnerNote.text = note.text
        binding.buttonSpinnerNote.setOnClickListener {
            onUpdateNote(note.id)
        }
        binding.buttonClearNote.setOnClickListener {
            onClearNote(note.id)
        }
    }
}

private class NoteSpacingDecoration(context: Context) : RecyclerView.ItemDecoration() {
    private val inset = context.resources.getDimensionPixelOffset(R.dimen.activity_margin)
    private val insetInterior = inset / 4

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val itemPosition = parent.getChildAdapterPosition(view)
        when (itemPosition) {
            0 -> {
                outRect.set(inset, inset, inset, insetInterior)
            }
            state.itemCount - 1 -> {
                outRect.set(inset, insetInterior, inset, inset)
            }
            else -> {
                outRect.set(inset, insetInterior, inset, insetInterior)
            }
        }
    }
}

private class ViewModelFactory(context: Context) : ViewModelProvider.Factory {

    private val appContext = context.applicationContext

    override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
        return when (modelClass) {
            TakeNotesViewModel::class.java -> {
                @Suppress("UNCHECKED_CAST")
                TakeNotesViewModel(
                    extras.createSavedStateHandle(),
                    AnimalRepositoryImpl(DatabaseHandler.create(appContext))
                ) as T
            }
            else -> throw IllegalStateException("ViewModel of type ${modelClass.simpleName} not supported.")
        }
    }
}
