package com.weyr_associates.animaltrakkerfarmmobile.app.animal.add

object AddAnimal {

    const val ACTION_ADD_AND_SELECT = "ACTION_ADD_AND_SELECT"

    const val EXTRA_PRIMARY_ID_TYPE_ID = "EXTRA_PRIMARY_ID_TYPE_ID"
    const val EXTRA_PRIMARY_ID_NUMBER = "EXTRA_PRIMARY_ID_NUMBER"

    const val EXTRA_RESULTING_ANIMAL_NAME = "EXTRA_RESULTING_ANIMAL_NAME"
    const val EXTRA_RESULTING_ANIMAL_ID = "EXTRA_RESULTING_ANIMAL_ID"
}
