package com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.optimalag

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.EvaluationEditor
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.EvaluationManager
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue.TissueSampleViewModel
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.ManageAnimalIdsViewModel
import com.weyr_associates.animaltrakkerfarmmobile.app.core.Result
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.requireAs
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.takeAs
import com.weyr_associates.animaltrakkerfarmmobile.app.label.ExtractPrintLabelData
import com.weyr_associates.animaltrakkerfarmmobile.app.label.PrintLabelData
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalBasicInfo
import com.weyr_associates.animaltrakkerfarmmobile.model.SavedEvaluation
import com.weyr_associates.animaltrakkerfarmmobile.model.Sex
import com.weyr_associates.animaltrakkerfarmmobile.model.Species
import com.weyr_associates.animaltrakkerfarmmobile.repository.AnimalRepository
import com.weyr_associates.animaltrakkerfarmmobile.repository.EvaluationRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.LocalDateTime

class OptAgRamBreedingSoundnessViewModel(
    private val databaseHandler: DatabaseHandler,
    private val animalRepo: AnimalRepository,
    private val evaluationRepo: EvaluationRepository,
    private val extractPrintLabelData: ExtractPrintLabelData
) : ViewModel() {

    sealed interface Event

    data class AnimalAlert(
        val alertText: String
    ) : Event

    data object UpdateDatabaseSuccess : Event
    data object UpdateDatabaseError : Event

    data class AnimalSpeciesOrSexMismatch(
        val speciesName: String,
        val sexName: String
    ) : Event

    sealed interface Lookup {
        data class ByAnimalId(val animalId: Int) : Lookup
        data class ByScannedEID(val scannedEID: String) : Lookup
    }

    data object ResetAnimalInfo

    sealed interface AnimalInfoState {
        data object Initial : AnimalInfoState
        data class AnimalInfoLoaded(
            val animalBasicInfo: AnimalBasicInfo,
            val scannedEID: String?) : AnimalInfoState
        data class AnimalInfoNotFound(val lookup: Lookup) : AnimalInfoState
    }

    data class TakeTissueSamplesInfo(
        val animalBasicInfo: AnimalBasicInfo,
        val printLabelData: PrintLabelData
    )

    private val onLookupAnimalChannel = Channel<Lookup>()
    private val onResetAnimalChannel = Channel<ResetAnimalInfo>()

    val animalInfoState: StateFlow<AnimalInfoState> = merge(
        onLookupAnimalChannel.receiveAsFlow(),
        onResetAnimalChannel.receiveAsFlow()
    ).mapLatest { lookup ->
        when (lookup) {
            is Lookup -> {
                val (animalInfo, scannedEID) = when (lookup) {
                    is Lookup.ByAnimalId -> {
                        Pair(animalRepo.queryAnimalByAnimalId(lookup.animalId), null)
                    }
                    is Lookup.ByScannedEID -> {
                        Pair(animalRepo.queryAnimalByEID(lookup.scannedEID), lookup.scannedEID)
                    }
                }
                animalInfo?.let {
                    AnimalInfoState.AnimalInfoLoaded(
                        it,
                        scannedEID
                    )
                } ?: AnimalInfoState.AnimalInfoNotFound(lookup)
            }
            else -> AnimalInfoState.Initial
        }
    }.onEach {
        it.takeAs<AnimalInfoState.AnimalInfoLoaded>()?.animalBasicInfo?.alert?.let { alertText ->
            if (alertText.isNotBlank()) {
                _eventChannel.send(AnimalAlert(alertText))
            }
        }
        evaluationManager.clearEvaluation()
        _tissueSamplesInfo.update { null }
    }.stateIn(viewModelScope, SharingStarted.Lazily, AnimalInfoState.Initial)

    private val evaluationManager = EvaluationManager(viewModelScope)
    val evaluationEditor = evaluationManager as EvaluationEditor

    private val _tissueSamplesInfo = MutableStateFlow<TakeTissueSamplesInfo?>(null)
    val tissueSamplesInfo = _tissueSamplesInfo.asStateFlow()

    val canClearData = evaluationManager.canClearData

    private val _isUpdatingDatabase = MutableStateFlow(false)

    val canSaveToDatabase = combine(
        animalInfoState,
        evaluationManager.isEvaluationComplete,
        _isUpdatingDatabase
    ) { animalInfoState, isEvaluationComplete, isUpdatingDatabase ->
        animalInfoState is AnimalInfoState.AnimalInfoLoaded && isEvaluationComplete && !isUpdatingDatabase
    }.stateIn(viewModelScope, SharingStarted.Lazily, false)

    val canTakeTissueSamples = tissueSamplesInfo.mapLatest { it != null }
        .stateIn(viewModelScope, SharingStarted.Lazily, false)

    private val _eventChannel = Channel<Event>()
    val events = _eventChannel.receiveAsFlow()

    init {
        viewModelScope.launch {
            evaluationManager.loadEvaluation(
                requireNotNull(
                    evaluationRepo.querySavedEvaluationById(SavedEvaluation.ID_OPTIMAL_AG_RAM_TEST)
                )
            )
        }
        viewModelScope.launch {
            animalInfoState.filter { it is AnimalInfoState.AnimalInfoLoaded }
                .map { it.requireAs<AnimalInfoState.AnimalInfoLoaded>().animalBasicInfo }
                .collectLatest {
                    if (it.sexId != Sex.ID_SHEEP_RAM) {
                        _eventChannel.send(
                            AnimalSpeciesOrSexMismatch(
                                speciesName = it.speciesCommonName,
                                sexName = it.sexName
                            )
                        )
                    }
                }
        }
    }

    fun onAnimalSelected(animalId: Int) {
        viewModelScope.launch {
            onLookupAnimalChannel.send(Lookup.ByAnimalId(animalId))
        }
    }

    fun onEIDScanned(eidNumber: String) {
        viewModelScope.launch {
            onLookupAnimalChannel.send(Lookup.ByScannedEID(eidNumber))
        }
    }

    fun clearData() {
        if (canClearData.value) {
            evaluationManager.clearEvaluation()
        }
    }

    fun resetAnimal() {
        viewModelScope.launch {
            onResetAnimalChannel.send(ResetAnimalInfo)
        }
    }

    fun saveToDatabase() {
        if (canSaveToDatabase.value) {
            viewModelScope.launch {
                executeUpdateDatabase()
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        databaseHandler.close()
    }

    private suspend fun executeUpdateDatabase() {
        val animalInfoLoaded = animalInfoState.value.takeAs<AnimalInfoState.AnimalInfoLoaded>() ?: return
        val animalInfo = animalInfoLoaded.animalBasicInfo
        val evaluationEntry = evaluationManager.extractEvaluationEntry() ?: return
        val ageInDays = animalInfo.ageInDays()

        try {

            _isUpdatingDatabase.update { true }

            withContext(Dispatchers.IO) {
                animalRepo.addEvaluationForAnimal(
                    animalId = animalInfo.id,
                    ageInDays = ageInDays,
                    timeStamp = LocalDateTime.now(),
                    trait01Id = evaluationEntry.trait01Id,
                    trait02Id = evaluationEntry.trait02Id,
                    trait03Id = evaluationEntry.trait03Id,
                    trait04Id = evaluationEntry.trait04Id,
                    trait05Id = evaluationEntry.trait05Id,
                    trait06Id = evaluationEntry.trait06Id,
                    trait07Id = evaluationEntry.trait07Id,
                    trait08Id = evaluationEntry.trait08Id,
                    trait09Id = evaluationEntry.trait09Id,
                    trait10Id = evaluationEntry.trait10Id,
                    trait11Id = evaluationEntry.trait11Id,
                    trait12Id = evaluationEntry.trait12Id,
                    trait13Id = evaluationEntry.trait13Id,
                    trait14Id = evaluationEntry.trait14Id,
                    trait15Id = evaluationEntry.trait15Id,
                    trait16Id = evaluationEntry.trait16Id,
                    trait17Id = evaluationEntry.trait17Id,
                    trait18Id = evaluationEntry.trait18Id,
                    trait19Id = evaluationEntry.trait19Id,
                    trait20Id = evaluationEntry.trait20Id,
                    trait01Score = evaluationEntry.trait01Score,
                    trait02Score = evaluationEntry.trait02Score,
                    trait03Score = evaluationEntry.trait03Score,
                    trait04Score = evaluationEntry.trait04Score,
                    trait05Score = evaluationEntry.trait05Score,
                    trait06Score = evaluationEntry.trait06Score,
                    trait07Score = evaluationEntry.trait07Score,
                    trait08Score = evaluationEntry.trait08Score,
                    trait09Score = evaluationEntry.trait09Score,
                    trait10Score = evaluationEntry.trait10Score,
                    trait11Score = evaluationEntry.trait11Score,
                    trait12Score = evaluationEntry.trait12Score,
                    trait13Score = evaluationEntry.trait13Score,
                    trait14Score = evaluationEntry.trait14Score,
                    trait15Score = evaluationEntry.trait15Score,
                    trait16OptionId = evaluationEntry.trait16OptionId,
                    trait17OptionId = evaluationEntry.trait17OptionId,
                    trait18OptionId = evaluationEntry.trait18OptionId,
                    trait19OptionId = evaluationEntry.trait19OptionId,
                    trait20OptionId = evaluationEntry.trait20OptionId,
                    trait11UnitsId = evaluationEntry.trait11UnitsId,
                    trait12UnitsId = evaluationEntry.trait12UnitsId,
                    trait13UnitsId = evaluationEntry.trait13UnitsId,
                    trait14UnitsId = evaluationEntry.trait14UnitsId,
                    trait15UnitsId = evaluationEntry.trait15UnitsId
                )
            }
            val printLabelDataResult = extractPrintLabelData.forOptimalAgRamBSETissueSamples(
                animalInfoLoaded.scannedEID,
                animalInfo,
                evaluationEntry
            )
            if (printLabelDataResult is Result.Success) {
                _tissueSamplesInfo.update {
                    TakeTissueSamplesInfo(
                        animalInfo,
                        printLabelDataResult.data
                    )
                }
            }
            _eventChannel.send(UpdateDatabaseSuccess)
        } catch(exception: Exception) {
            _eventChannel.send(UpdateDatabaseError)
        } finally {
            _isUpdatingDatabase.update { false }
        }
    }
}
