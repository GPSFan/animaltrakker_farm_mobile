package com.weyr_associates.animaltrakkerfarmmobile.app.animal.add;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler;
import com.weyr_associates.animaltrakkerfarmmobile.R;
import com.weyr_associates.animaltrakkerfarmmobile.Utilities;
import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.TopButtonBar;
import com.weyr_associates.animaltrakkerfarmmobile.database.BreedTable;
import com.weyr_associates.animaltrakkerfarmmobile.database.Cursors;
import com.weyr_associates.animaltrakkerfarmmobile.database.DefaultSettingsTable;
import com.weyr_associates.animaltrakkerfarmmobile.database.IdTypeTable;
import com.weyr_associates.animaltrakkerfarmmobile.databinding.AddAnimalBinding;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class LegacyAddAnimalActivity extends AppCompatActivity {
    private DatabaseHandler dbh;
    String     	cmd;
    public Button btn;
    public Spinner first_tag_type_spinner,  first_tag_location_spinner, first_tag_color_spinner;
    public Spinner second_tag_type_spinner,  second_tag_location_spinner, second_tag_color_spinner;
    public Spinner third_tag_type_spinner,  third_tag_location_spinner, third_tag_color_spinner;
    public int first_tag_type_id, first_tag_location;
    public int second_tag_type_id, second_tag_location;
    public int third_tag_type_id, third_tag_location;
    public int first_tag_color, second_tag_color, third_tag_color;
    public String this_tag_color;
    public String first_tag_type, second_tag_type, third_tag_type;
    public String first_tag_number, second_tag_number, third_tag_number;
    public List<String> tag_types, tag_locations, tag_colors;
    public List<String> tag_location_id_order, tag_location_display_order;
    public List<String> tag_types_display_order, tag_types_id_order;
    public List<String> tag_color_display_order, tag_color_id_order;
    public Spinner animal_breed_spinner;
    public List<String> animal_breed, animal_breed_id_order, animal_breed_display_order;
    public int  thisanimal_id,  foster_dam_id, surrogate_dam_id;
    public String animal_name, birth_time, weaned_date, death_date, alert, hand_reared, created, modified;
    public String id_deathreasonid, id_managementgroupid, birth_weight, inbreeding, birth_weight_id_unitsid;
    public int  id_birthtypeid, rear_type, sire_id, dam_id, id_breeder_id_contactid;
    public int official_id, birth_order;
    public Spinner age_month_spinner;
    public List<String> age_months;
    public Spinner age_year_spinner;
    public List<String> age_years;
    public int owner_id_contactid , current_breed_id, owner_id_premiseid;
    public int current_age_year, current_age_month;
    public String current_birth_date;
    public int breed_location, owner_location;
    public String id_breeder_id_premiseid, id_owner_id_premiseid ;

    public int default_owner;
    public String default_species, default_sex, default_breed, default_tag_type, this_tag_location;
    public Spinner owner_name_spinner;
    public List<String> possible_owners;
    public List<Integer>  possible_owners_id, possible_owners_id_order;
    public Cursor 	 cursor, animalCursor,tagCursor, breedCursor, ownerCursor, tagLocationCursor, tagColorCursor;
    public Object 	crsr, animalcrsr, tagcrsr, breedcrsr, ownercrsr, taglocationcrsr, tagcolorcrsr;

    public String tag_country_code ;
    ArrayAdapter<String> dataAdapter;
    public Map<String,Integer> animaltrakker_defaults;
    public Date today;

    private AddAnimalBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = AddAnimalBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        View v = null;
        TextView TV;
        String dbfile = getString(R.string.real_database_file);
        Log.i("AddAnimal", " after get database file");
        dbh = new DatabaseHandler(this, dbfile);
        // Get the defaults and fill the defaults list for use later.
        animaltrakker_defaults = DefaultSettingsTable.readAsMapFrom(this, dbh);
        // Fill the animal Breed Spinner
        animal_breed_spinner = binding.animalBreedSpinner;
        animal_breed = new ArrayList<String>();
        animal_breed_id_order = new ArrayList<String>();
        animal_breed_display_order = new ArrayList<String>();
        //todo if use hash table then can reference this by field name not number in the array
        default_species = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.SPECIES_ID));
        Log.i("species default is  ", " is " + default_species);
        //  Select the fields we need from the list of animal breeds
        cmd = String.format("SELECT id_breedid, breed_name, breed_display_order FROM breed_table WHERE id_speciesid = '%s' ORDER BY breed_display_order", default_species);
        Log.i("fill breed spinner", "command is " + cmd);
        breedcrsr = dbh.exec(cmd);
        breedCursor = (Cursor) breedcrsr;
        dbh.moveToFirstRecord();
        animal_breed.add("Select a Breed");
        animal_breed_id_order.add("animal_breed_id");
        animal_breed_display_order.add("animal_breed_display_order");
        for (breedCursor.moveToFirst(); !breedCursor.isAfterLast(); breedCursor.moveToNext()) {
            String breedId = Cursors.getString(breedCursor, BreedTable.Columns.ID);
            animal_breed_id_order.add(breedId);
            Log.i("add animal", "animal_breed_id_order " + breedId);
            String breedName = Cursors.getString(breedCursor, BreedTable.Columns.NAME);
            animal_breed.add(breedName);
            Log.i("add animal", "animal_breed" + breedName);
            String breedDisplayOrder = Cursors.getString(breedCursor, BreedTable.Columns.ORDER);
            animal_breed_display_order.add(breedDisplayOrder);
            Log.i("add animal", "animal_breed_display_order " + breedDisplayOrder);
        }
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, animal_breed);
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
        animal_breed_spinner.setAdapter(dataAdapter);
        //  Need to first get the default which is the actual primary key in the breed_table
        //  Then need to find the spinner number that corresponds to that breed
        //todo if use hash table then can reference this by field name not number in the array
        default_breed = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.BREED_ID));
        Log.i("add animal", " breed default is " + default_breed);
        breed_location = animal_breed_id_order.indexOf(default_breed);
        Log.i("add animal", " breed location is " + breed_location);
        //  Then set the spinner to that number
        animal_breed_spinner.setSelection((breed_location));

        // Fill the Tag Type Spinners
        tag_types = new ArrayList<String>();
        tag_types_id_order = new ArrayList<String>();
        tag_types_display_order = new ArrayList<String>();
        // Select All fields from id types to build the spinner
        cmd = "select * from id_type_table order by id_type_display_order";
        Log.i("add animal", " tag spinner command is " + cmd);
        tagcrsr = dbh.exec(cmd);
        tagCursor = (Cursor) tagcrsr;
        dbh.moveToFirstRecord();
        tag_types.add("Select a Type");
        tag_types_id_order.add("tag_types_id");
        tag_types_display_order.add("tag_type_display_order");
        Log.i("add animal", " fill tag spinner added first option ");
        // looping through all rows and adding to list
        for (tagCursor.moveToFirst(); !tagCursor.isAfterLast(); tagCursor.moveToNext()) {
            String tagTypeId = Cursors.getString(tagCursor, IdTypeTable.Columns.ID);
            tag_types_id_order.add(tagTypeId);
            Log.i("add animal", " tag type id order to add is " + tagTypeId);
            String tagName = Cursors.getString(tagCursor, IdTypeTable.Columns.NAME);
            tag_types.add(tagName);
            Log.i("add animal", " tag type to add is " + tagName);
            String tagDisplayOrder = Cursors.getString(tagCursor, IdTypeTable.Columns.ORDER);
            tag_types_display_order.add(tagDisplayOrder);
            Log.i("add animal", " tag type display order to add is " + tagDisplayOrder);
        }
        first_tag_type_spinner = binding.firstTagTypeSpinner;
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, tag_types);
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
        first_tag_type_spinner.setAdapter(dataAdapter);

        second_tag_type_spinner = binding.secondTagTypeSpinner;
        dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, tag_types);
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
        second_tag_type_spinner.setAdapter(dataAdapter);
// todo fix this with the new primary, secondary and tertiary tag type defaults rather than by species.
        switch (default_species) {
            case "1": // Sheep
                default_sex = "1";
                second_tag_type = "Farm";
//                Log.i("add animal", " in sheep case tag type is " + second_tag_type);
                second_tag_type_id = tag_types.indexOf(second_tag_type);
//                Log.i("add animal", " second tag type id is " + String.valueOf(second_tag_type_id));
                second_tag_type_spinner = binding.secondTagTypeSpinner;
                second_tag_type_spinner.setSelection(second_tag_type_id); // farm tag
//                second_tag_type_spinner.setSelection(2);
            case "2": // Goat
                default_sex = "5";
                second_tag_type = "Farm";
                second_tag_type_id = tag_types_id_order.indexOf(second_tag_type);
                second_tag_type_spinner = binding.secondTagTypeSpinner;
                second_tag_type_spinner.setSelection(second_tag_type_id); // farm tag
//                second_tag_type_spinner.setSelection(2);
            case "3":// Cattle
                default_sex = "9";
                second_tag_type = "Trich";
                second_tag_type_id = tag_types_id_order.indexOf(second_tag_type);
                second_tag_type_spinner.setSelection(second_tag_type_id); // farm Trich
//                second_tag_type_spinner.setSelection(3);
            case "4":// Horse
                default_sex = "13";
                second_tag_type = "Freeze Brand";
                second_tag_type_id = tag_types_id_order.indexOf(second_tag_type);
                second_tag_type_spinner.setSelection(second_tag_type_id); // Freeze Brand
//                second_tag_type_spinner.setSelection(10);
            case "5":// Donkey
                default_sex = "17";
                second_tag_type = "Freeze Brand";
                second_tag_type_id = tag_types_id_order.indexOf(second_tag_type);
                second_tag_type_spinner.setSelection(second_tag_type_id); // Freeze Brand
//                second_tag_type_spinner.setSelection(10);
            case "6":// Pig
                default_sex = "21";
                second_tag_type = "Farm";
                second_tag_type_id = tag_types_id_order.indexOf(second_tag_type);
                second_tag_type_spinner.setSelection(second_tag_type_id); // farm tag
//                second_tag_type_spinner.setSelection(2);
        }

        third_tag_type_spinner = binding.thirdTagTypeSpinner;
        dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, tag_types);
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
        third_tag_type_spinner.setAdapter(dataAdapter);
        switch (default_species) {
            case "1": // Sheep
                default_sex = "1";
                third_tag_type = "Farm";
//                Log.i("add animal", " in sheep case tag type is " + third_tag_type);
                third_tag_type_id = tag_types.indexOf(third_tag_type);
//                Log.i("add animal", " third tag type id is " + String.valueOf(third_tag_type_id));
                third_tag_type_spinner = binding.thirdTagTypeSpinner;
                third_tag_type_spinner.setSelection(third_tag_type_id); // farm tag
//                third_tag_type_spinner.setSelection(2);
            case "2": // Goat
                default_sex = "5";
                third_tag_type = "Farm";
                third_tag_type_id = tag_types_id_order.indexOf(third_tag_type);
                third_tag_type_spinner = binding.thirdTagTypeSpinner;
                third_tag_type_spinner.setSelection(third_tag_type_id); // farm tag
//                third_tag_type_spinner.setSelection(2);
            case "3":// Cattle
                default_sex = "9";
                third_tag_type = "Trich";
                third_tag_type_id = tag_types_id_order.indexOf(third_tag_type);
                third_tag_type_spinner.setSelection(third_tag_type_id); // farm Trich
//                third_tag_type_spinner.setSelection(3);
            case "4":// Horse
                default_sex = "13";
                third_tag_type = "Freeze Brand";
                third_tag_type_id = tag_types_id_order.indexOf(third_tag_type);
                third_tag_type_spinner.setSelection(third_tag_type_id); // Freeze Brand
//                third_tag_type_spinner.setSelection(10);
            case "5":// Donkey
                default_sex = "17";
                third_tag_type = "Freeze Brand";
                third_tag_type_id = tag_types_id_order.indexOf(third_tag_type);
                third_tag_type_spinner.setSelection(third_tag_type_id); // Freeze Brand
//                third_tag_type_spinner.setSelection(10);
            case "6":// Pig
                default_sex = "21";
                third_tag_type = "Farm";
                third_tag_type_id = tag_types_id_order.indexOf(third_tag_type);
                third_tag_type_spinner.setSelection(third_tag_type_id); // farm tag
//                third_tag_type_spinner.setSelection(2);
        }


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            first_tag_type_id = extras.getInt("tagtype");
            first_tag_number = extras.getString("tagnumber");
            TV = binding.firstNewTagNumber;
            TV.setText(first_tag_number);
            first_tag_type_spinner = binding.firstTagTypeSpinner;
            first_tag_type_spinner.setSelection(first_tag_type_id); // Based on the display order not the actual ID
        }
        if (default_species == "3"){
            // todo need to get the preference for autoincrement or not and implement it here
            //  This assumes yes on increment and gets the next trich tag number to use
//            We have cattle so set the second tag to be the next trich tag
            second_tag_type = "Trich";
            TV = binding.secondNewTagNumber;
            second_tag_number = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.TRICH_TAG_NEXT_TAG_NUMBER));
            TV.setText(second_tag_number); // next trich tag number
            Log.i("oncreate", " next trich tag is " + second_tag_number);
        }

        first_tag_type = first_tag_type_spinner.getSelectedItem().toString();
        official_id = 0;
        switch (first_tag_type) {
            case "Federal":
                Log.i("add animal", " current_tag_type Federal");
                this_tag_location = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.FED_TAG_LOCATION));
                this_tag_color = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.FED_TAG_COLOR_MALE));
                official_id = 1;
                Log.i("add animal", " federal tag official_id " + official_id);
                break;
            case "Electronic":
                Log.i("add animal", " current_tag_type Electronic");
                this_tag_location = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.EID_TAG_LOCATION));
                this_tag_color = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.EID_TAG_COLOR_MALE));
                // todo Will need to update this for additional countries.
                tag_country_code = first_tag_number.substring(0, 3);
                Log.i("add animal", " tag_country_code is " + tag_country_code);
                if (tag_country_code.equals("840")){ // USA code
                    official_id = 1;
                    Log.i("add animal", " 840 official_id " + official_id);
                }
                if (tag_country_code.equals("124")){ // Canada code
                    official_id = 1;
                    Log.i("add animal", " 124 official_id " + official_id);
                }
                if (tag_country_code.equals("484")){ // Mexico code
                    official_id = 1;
                    Log.i("add animal", " 484 official_id " + official_id);
                }
                if (tag_country_code.equals("826")){ // UK code
                    official_id = 1;
                    Log.i("add animal", " 826 official_id " + official_id);
                }
                if (tag_country_code.equals("756")) { // Switzerland code
                    official_id = 1;
                    Log.i("add animal", " 756 official_id " + official_id);
                }
                break;
            case "Paint":
                Log.i("add animal", " current_tag_type Paint");
                this_tag_location = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.PAINT_MARK_LOCATION));
                this_tag_color = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.PAINT_MARK_COLOR));
                official_id = 0;
                break;
            case "Farm":
                Log.i("add animal", " current_tag_type Farm");
                this_tag_location = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.FARM_TAG_LOCATION));
                this_tag_color = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.FARM_TAG_COLOR_MALE));
                official_id = 0;
                break;
            case "Tattoo":
                Log.i("add animal", " current_tag_type Tattoo");
                this_tag_location = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.TATTOO_LOCATION));
                this_tag_color = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.TATTOO_COLOR));
                official_id = 0;
                break;
            case "Trich":
                Log.i("add animal", " current_tag_type Trich");
                this_tag_location = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.TRICH_TAG_LOCATION));
                this_tag_color = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.TRICH_TAG_COLOR_MALE));
                official_id = 0;
                break;
            case "NUES":
                Log.i("add animal", " current_tag_type NUES");
                this_tag_location = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.NUES_TAG_LOCATION));
                this_tag_color = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.NUES_TAG_COLOR_MALE));
                official_id = 1;
                break;
            case "Bangs":
                Log.i("add animal", " current_tag_type Bangs");
                this_tag_location = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.BANGS_TAG_LOCATION));
                this_tag_color = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.BANGS_TAG_COLOR_MALE));
                official_id = 1;
                break;
            case "Freeze Brand":
                Log.i("add animal", " current_tag_type Freeze Brand");
                this_tag_location = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.FREEZE_BRAND_LOCATION));
                this_tag_color = "3"; //Set to white for freeze brands
                official_id = 0;
                break;
            case "Sale Order":
                Log.i("add animal", " current_tag_type Sale Order");
                this_tag_location = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.SALE_ORDER_TAG_LOCATION));
                this_tag_color = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.SALE_ORDER_TAG_COLOR_MALE));
                official_id = 0;
                break;
            // todo add name, split and notch ID stuff here
            case "Split":
            case "Name":
            case "Notch":
                Log.i("add animal", " not yet implemented tag types");
                break;
        }
        // Fill the Tag Location Spinners
        tag_locations = new ArrayList<String>();
        tag_location_id_order = new ArrayList<String>();
        tag_location_display_order = new ArrayList<String>();
        // Select All fields from id types to build the spinner
        cmd = "select * from id_location_table order by id_location_display_order";
        Log.i("AddAnimal ", "command is " + cmd);
        taglocationcrsr = dbh.exec(cmd);
        tagLocationCursor = (Cursor) taglocationcrsr;
        dbh.moveToFirstRecord();
        tag_locations.add("Location");
        tag_location_id_order.add("tag_location_id");
        tag_location_display_order.add("tag_location_display_order");
        Log.i("AddAnimal ", "added first option ");
        // looping through all rows and adding to list
        for (tagLocationCursor.moveToFirst(); !tagLocationCursor.isAfterLast(); tagLocationCursor.moveToNext()) {
            tag_location_id_order.add(tagLocationCursor.getString(0));
//            Log.i("AddAnimal", " tag_location_id_order " + tagLocationCursor.getString(0));
            tag_locations.add(tagLocationCursor.getString(1));
//            Log.i("AddAnimal", " tag_location name" + tagLocationCursor.getString(1));
            tag_location_display_order.add(tagLocationCursor.getString(3));
//            Log.i("AddAnimal", " tag_location_display_order " + tagLocationCursor.getString(3));
        }
        first_tag_location_spinner = binding.firstTagLocationSpinner;
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, tag_locations);
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
        first_tag_location_spinner.setAdapter(dataAdapter);
        first_tag_location = tag_location_id_order.indexOf(this_tag_location);
        first_tag_location_spinner.setSelection(first_tag_location);

        // todo get from defaults and handle other species with a different second tag type
        second_tag_location_spinner = binding.secondTagLocationSpinner;
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
        second_tag_location_spinner.setAdapter(dataAdapter);
        second_tag_location_spinner.setSelection(1);

        // todo get from defaults and handle other species with a different third tag type
        third_tag_location_spinner = binding.thirdTagLocationSpinner;
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
        third_tag_location_spinner.setAdapter(dataAdapter);
        third_tag_location_spinner.setSelection(1);
        // Fill the Tag Color Spinner
        tag_colors = new ArrayList<String>();
        tag_color_id_order = new ArrayList<String>();
        tag_color_display_order = new ArrayList<String>();
        // Select All fields from id types to build the spinner
        cmd = "select * from id_color_table order by id_color_display_order";
        Log.i(" add animal", " fill tag color spinner command is " + cmd);
        tagcolorcrsr = dbh.exec(cmd);
        tagColorCursor = (Cursor) tagcolorcrsr;
        dbh.moveToFirstRecord();
        tag_colors.add("Color");
        tag_color_id_order.add("tag_color_id");
        tag_color_display_order.add("tag_color_display_order");
        Log.i("add animal", "fill tag color spinner added first option ");
        // looping through all rows and adding to list
        for (tagColorCursor.moveToFirst(); !tagColorCursor.isAfterLast(); tagColorCursor.moveToNext()) {
            tag_color_id_order.add(tagColorCursor.getString(0));
//            Log.i("UpdateTags", " tag_color_id_order " + tagColorCursor.getString(0));
            tag_colors.add(tagColorCursor.getString(1));
//            Log.i("UpdateTags", " tag_color name" + tagColorCursor.getString(1));
            tag_color_display_order.add(tagColorCursor.getString(3));
//            Log.i("UpdateTags", " tag_color_display_order " + tagColorCursor.getString(3));
        }

        first_tag_color_spinner = binding.firstTagColorSpinner;
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, tag_colors);
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
        first_tag_color_spinner.setAdapter(dataAdapter);
        first_tag_color = tag_color_id_order.indexOf(this_tag_color);
        first_tag_color_spinner.setSelection(first_tag_color);

        // todo fix this to pull from defaults based on what type of tag is the second tag
        second_tag_color_spinner = binding.secondTagColorSpinner;
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, tag_colors);
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
        second_tag_color_spinner.setAdapter(dataAdapter);
        second_tag_color = DefaultSettingsTable.defaultIdColorFromIdTypeId(animaltrakker_defaults, second_tag_type_id);
        second_tag_color = tag_color_id_order.indexOf(String.valueOf(second_tag_color));
        second_tag_color_spinner.setSelection(second_tag_color);

        // todo fix this to pull from defaults based on what type of tag is the third tag
        third_tag_color_spinner = binding.thirdTagColorSpinner;
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, tag_colors);
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
        third_tag_color_spinner.setAdapter(dataAdapter);
        third_tag_color = DefaultSettingsTable.defaultIdColorFromIdTypeId(animaltrakker_defaults, third_tag_type_id);
        third_tag_color = tag_color_id_order.indexOf(String.valueOf(third_tag_color));
        third_tag_color_spinner.setSelection(third_tag_color); // from farm tag color male default

        //  Fill the age years spinner
        //todo Do we need to have a max age in the defaults? Or shoudl we base it on the species?
        age_year_spinner = binding.ageYearSpinner;
        age_years = new ArrayList<String>();
        age_years.add("Years");
        age_years.add("0");
        age_years.add("1");
        age_years.add("2");
        age_years.add("3");
        age_years.add("4");
        age_years.add("5");
        age_years.add("6");
        age_years.add("7");
        age_years.add("8");
        age_years.add("9");
        age_years.add("10");
        age_years.add("11");
        dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item_center, age_years);
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item_center);
        age_year_spinner.setAdapter(dataAdapter);
        age_year_spinner.setSelection(0);

        //  Fill the age months spinner
        age_month_spinner = binding.ageMonthSpinner;
        age_months = new ArrayList<String>();
        age_months.add("Months");
        age_months.add("0");
        age_months.add("1");
        age_months.add("2");
        age_months.add("3");
        age_months.add("4");
        age_months.add("5");
        age_months.add("6");
        age_months.add("7");
        age_months.add("8");
        age_months.add("9");
        age_months.add("10");
        age_months.add("11");
        dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item_center, age_months);
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item_center);
        age_month_spinner.setAdapter(dataAdapter);
        age_month_spinner.setSelection(0);

        // Fill the owner Spinner
        // TODO this will only find owners who are people.
        //  But we need to find owners who are companies too and add them and know which is which.
        //  Need to think how to do this.
        owner_name_spinner = binding.ownerNameSpinner;
        possible_owners = new ArrayList<String>();
        possible_owners_id = new ArrayList<Integer>();
        possible_owners_id_order = new ArrayList<Integer>();
        //     possible_owners_display_order = new ArrayList<String>();
        //  Select the fields we need from the list of owners
        cmd = "SELECT id_contactid, contact_first_name, contact_last_name from contact_table";
        Log.i("fill owner spinner", "command is " + cmd);
        ownercrsr = dbh.exec(cmd);
        ownerCursor = (Cursor) ownercrsr;
        dbh.moveToFirstRecord();
        possible_owners_id.add(0);
        possible_owners.add("Select an Owner");
        possible_owners_id_order.add(0);
        int i = 0;
        for (ownerCursor.moveToFirst(); !ownerCursor.isAfterLast(); ownerCursor.moveToNext()) {
            i=i+1;
            possible_owners_id.add(ownerCursor.getInt(0));
            //TODO This only works for people as owners. Need to fix to handle the company tables as well.
            //           possible_owners.add(ownerCursor.getString(1)+ " " + ownerCursor.getString(2) + " " +ownerCursor.getString(3));
            possible_owners.add(ownerCursor.getString(1)+ " " + ownerCursor.getString(2));
            possible_owners_id_order.add(i);
        }
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, possible_owners);
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
        owner_name_spinner.setAdapter(dataAdapter);
        // TODO: 12/30/21 set the default owner name again this only works for people as owners.
        //TODO: what about the default breeder?
        default_owner = animaltrakker_defaults.get(DefaultSettingsTable.Columns.OWNER_CONTACT_ID);
        owner_name_spinner.setSelection(possible_owners_id.indexOf(default_owner));
        //       owner_name_spinner.setSelection(default_owner);

        Log.i("add animal", " at the end of the on create class");
        binding.buttonPanelTop.show(TopButtonBar.UI_ACTION_UPDATE_DATABASE);
        btn = binding.buttonPanelTop.getMainActionButton();
        btn.setOnClickListener(view -> updateDatabase());
    }

    private void updateDatabase() {
        TextView TV;
        Log.i("add animal", " ready to start the updateDatabase in AddAnimal code");
        // Put empty into all required animal_table fields so the insert query doesn't fail.
//        Log.i("updatedb", " start of updateDB the official_id flag is " + official_id);
//        animal_name = "";
        //            Set the animal name to be the EID tag number
// todo fix these to pull from the defaults  = animaltrakker_defaults.get(46);
        animal_name = first_tag_number.substring (16-5); // last x digits of previous EID tag
        birth_time = "08:00:00";
        weaned_date = "0";
        death_date = "0";
        alert = "";
        birth_weight = "0";
        inbreeding = "0";
        birth_weight_id_unitsid = "0";
        id_birthtypeid = 42;
       // id_birthinghistoryid = "";
        rear_type = 42;
        id_deathreasonid = "0";
        // todo fix this to be generic not Sperry specific
        sire_id = 608; // Hard coded for unknown animal
        dam_id = 608; // Hard coded for unknown animal
        foster_dam_id = 0;
        surrogate_dam_id = 0;
        hand_reared = "0";
        birth_order = 42;

        id_breeder_id_contactid = animaltrakker_defaults.get(DefaultSettingsTable.Columns.BREEDER_CONTACT_ID);

        //todo remove the management group info from here when I update the animal table to the latest schema
        id_managementgroupid = "0";
        id_breeder_id_premiseid = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.BREEDER_PREMISE_ID));
        id_owner_id_premiseid = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.OWNER_PREMISE_ID));

        String mytoday = Utilities.TodayIs();
        String mytime = Utilities.TimeIs();
        Date today = new Date();


// todo This is fine so long as everything is at something other than item 0 in the spinners.
//  It crashes then because the parseInt can't handle the string text of the 0th item in each of my lists
        first_tag_type_spinner = binding.firstTagTypeSpinner;
        first_tag_type_id = first_tag_type_spinner.getSelectedItemPosition();
        Log.i("update DB ", "the tag type position is " + first_tag_type_id);
        first_tag_type_id = Integer.valueOf(tag_types_id_order.get(first_tag_type_id));
        Log.i("update DB ", "the tag type is " + first_tag_type);
        Log.i("update DB ", "the tag type id is " + first_tag_type_id);

        first_tag_location_spinner = binding.firstTagLocationSpinner;
        first_tag_location = first_tag_location_spinner.getSelectedItemPosition();
        first_tag_location = Integer.parseInt(tag_location_id_order.get(first_tag_location));
        Log.i("update DB ", "the tag location is " + first_tag_location);

        first_tag_color_spinner = binding.firstTagColorSpinner;
        first_tag_color = first_tag_color_spinner.getSelectedItemPosition();
        first_tag_color = Integer.parseInt(tag_color_id_order.get(first_tag_color));
        Log.i("update DB ", "the tag color is " + first_tag_color);

        second_tag_type_spinner = binding.secondTagTypeSpinner;
        second_tag_type_id = second_tag_type_spinner.getSelectedItemPosition();
        if (second_tag_type_id == 0){
            Log.i("update DB ", "no second tag listed " );
        }else {
            Log.i("update DB ", "the tag type position is " + second_tag_type_id);
            second_tag_type_id = Integer.valueOf(tag_types_id_order.get(second_tag_type_id));
//        second_tag_type_id = Integer.parseInt(tag_types_id_order.get(second_tag_type_id));
            Log.i("update DB ", "the tag type is " + second_tag_type_id);

            second_tag_location_spinner = binding.secondTagLocationSpinner;
            second_tag_location = second_tag_location_spinner.getSelectedItemPosition();
            second_tag_location = Integer.parseInt(tag_location_id_order.get(second_tag_location));
            Log.i("update DB ", "the tag location is " + second_tag_location);

            second_tag_color_spinner = binding.secondTagColorSpinner;
            second_tag_color = second_tag_color_spinner.getSelectedItemPosition();
            second_tag_color = Integer.parseInt(tag_color_id_order.get(second_tag_color));
            Log.i("update DB ", "the tag color is " + second_tag_color);

            TV = binding.secondNewTagNumber;
            second_tag_number = TV.getText().toString();
            Log.i("update db ", "second tag is " + second_tag_number);
            if (second_tag_type_id == 10) {

                Log.i("update db ", "Trich tag " + second_tag_number);
                int trich_tag_num = Integer.parseInt(TV.getText().toString()) + 1;
                Log.i("update db ", "next trich tag is " + trich_tag_num);
                cmd = String.format("update animaltrakker_default_settings_table set trich_tag_next_tag_number = %s", trich_tag_num);
                Log.i("update db ", "cmd is " + cmd);
                dbh.exec(cmd);
                Log.i("update db ", "after update next trich tag to use ");
            }
        }

        third_tag_type_spinner = binding.thirdTagTypeSpinner;
        third_tag_type_id = third_tag_type_spinner.getSelectedItemPosition();
        if (third_tag_type_id == 0) {
            Log.i("update DB ", "no third tag listed ");
        }else {
            Log.i("update DB ", "the tag type position is " + third_tag_type_id);
            third_tag_type_id = Integer.valueOf(tag_types_id_order.get(third_tag_type_id));
//        third_tag_type_id = Integer.parseInt(tag_types_id_order.get(third_tag_type_id));
            Log.i("update DB ", "the tag type is " + third_tag_type_id);

            third_tag_location_spinner = binding.thirdTagLocationSpinner;
            third_tag_location = third_tag_location_spinner.getSelectedItemPosition();
            third_tag_location = Integer.parseInt(tag_location_id_order.get(third_tag_location));
            Log.i("update DB ", "the tag location is " + third_tag_location);

            third_tag_color_spinner = binding.thirdTagColorSpinner;
            third_tag_color = third_tag_color_spinner.getSelectedItemPosition();
            third_tag_color = Integer.parseInt(tag_color_id_order.get(third_tag_color));
            Log.i("add a bull ", "the tag color is " + third_tag_color);

            TV = binding.thirdNewTagNumber;
            third_tag_number = TV.getText().toString();
            Log.i("update db ", "third tag is " + third_tag_number);
        }
        Log.i("updatedb", " the official_id flag is " + official_id);
        owner_name_spinner = binding.ownerNameSpinner;
        owner_id_contactid = owner_name_spinner.getSelectedItemPosition();
        owner_id_contactid =Integer.valueOf(possible_owners_id.get(owner_id_contactid));
 //       owner_id_contactid =Integer.parseInt(String.valueOf(possible_owners_id.get(owner_id_contactid)));
        Log.i("add a bull ", "the owner id is " + owner_id_contactid);
        id_breeder_id_contactid = owner_id_contactid;

        animal_breed_spinner = binding.animalBreedSpinner;
        current_breed_id = animal_breed_spinner.getSelectedItemPosition();
        current_breed_id = Integer.parseInt(animal_breed_id_order.get(current_breed_id));
        Log.i("add animal ", "the breed is " + current_breed_id);

        age_year_spinner = binding.ageYearSpinner;
        current_age_year = age_year_spinner.getSelectedItemPosition();
        Log.i("update DB ", "the age year is " + current_age_year);

        age_month_spinner = binding.ageMonthSpinner;
        current_age_month = age_month_spinner.getSelectedItemPosition();
        Log.i("update DB ", "the age month is " + current_age_month);
        if (first_tag_type_id == 0 || first_tag_location == 0 || first_tag_color == 0
                || owner_id_contactid == 0 || current_breed_id == 0 ||
                current_age_month == 0 || current_age_year == 0 ) {
// todo need to figure out how to identify on the screen which fields are required
            // Missing data so  display an alert
            AlertDialog.Builder builder = new AlertDialog.Builder( this );
            builder.setMessage( R.string.fill_fields )
                    .setTitle( R.string.fill_fields );
            builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int idx) {
                    // User clicked OK button
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }else {
//       todo Doesn't handle an animal with nothing in either the year or month fields.
//        Somehow it's miscalculating the proper birth date.
            int animalyear = age_year_spinner.getSelectedItemPosition();
            animalyear = animalyear - 1;
            int animalmonth = age_month_spinner.getSelectedItemPosition();
            animalmonth = animalmonth -1;
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(today);

//            int dobYear = 21;
//            int dobMonth = 10;
//            int dobDay = 22;
//
//            LocalDate now = LocalDate.now();
//            LocalDate dob = now.minusYears(dobYear)
//                    .minusMonths(dobMonth)
//                    .minusDays(dobDay);
//
            calendar.add(Calendar.YEAR, - animalyear);
            calendar.add(Calendar.MONTH, - animalmonth);
            calendar.getTime();
            Log.i("add animal", " test birth date is " + calendar.getTime());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-");
            current_birth_date = sdf.format(calendar.getTime())+ "01";
            Log.i("add animal", " test birth date is " + current_birth_date);
            default_sex = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.SEX_ID));
            Log.i("sex default is  ", " is " + default_sex);
            mytime = Utilities.TimeIs();
            created = mytoday + " " + mytime;
            modified = mytoday + " " + mytime;
            // todo This is using the default sex for all added animals . Need to fix to ask for a specific sex.

//            cmd = String.format("INSERT INTO animal_table " +
//                    "(animal_name, id_sexid, birth_date, birth_time, id_birthtypeid, birth_weight, birth_weight_id_unitsid, " +
//                    "birth_order, rear_type, weaned_date, death_date, id_deathreasonid, sire_id, dam_id," +
//                    "foster_dam_id, surrogate_dam_id, hand_reared, id_managementgroupid, alert, created, modified) values " +
//                    "('%s', '%s', '%s', '%s', %s, '%s', '%s', %s, %s, %s, '%s','%s', '%s', %s, %s, %s, %s, '%s', '%s', '%s', '%s', '%s')",
//                    animal_name, default_sex, current_birth_date, birth_time, id_birthtypeid, birth_weight, birth_weight_id_unitsid
//                    , birth_order, rear_type, weaned_date, death_date, id_deathreasonid, sire_id, dam_id, foster_dam_id
//                    , surrogate_dam_id, hand_reared, id_managementgroupid, alert, created, modified);


            cmd = String.format("INSERT INTO animal_table " +
                            "(animal_name, id_sexid, birth_date, birth_time, id_birthtypeid, birth_weight, birth_weight_id_unitsid, " +
                            "birth_order, rear_type, weaned_date, death_date, id_deathreasonid, sire_id, dam_id," +
                            "foster_dam_id, surrogate_dam_id, hand_reared, id_managementgroupid, alert, created, modified) values " +
                            "('%s', '%s', '%s', '%s', '%s', %s, %s, %s, '%s','%s', %s, %s, %s, %s, %s, %s, %s, '%s', '%s', '%s', '%s')",
                    animal_name, default_sex, current_birth_date, birth_time, id_birthtypeid, birth_weight, birth_weight_id_unitsid,
                    birth_order, rear_type, weaned_date, death_date, id_deathreasonid, sire_id, dam_id, foster_dam_id,
                    surrogate_dam_id, hand_reared, id_managementgroupid, alert, created, modified);


            Log.i("update db ", "cmd is " + cmd);
            dbh.exec(cmd);
            Log.i("update db ", "after insert ");
            //	We need to get the id_animalid of this animal for use in the id, ownership and location records
            cmd = String.format("select last_insert_rowid()");
            animalcrsr = dbh.exec(cmd);
            animalCursor = (Cursor) crsr;
            dbh.moveToFirstRecord();
            thisanimal_id = dbh.getInt(0);
            Log.i("update db ", "the id_animalid is " + String.valueOf(thisanimal_id));
            Log.i("updateDB ", "official_id is " + official_id);
            //  Now go add a id record for the tag we got
            cmd = String.format("INSERT INTO animal_id_info_table (id_animalid, id_idtypeid, id_male_id_idcolorid" +
                    ", id_female_id_idcolorid, id_idlocationid, id_date_on, id_time_on, id_date_off, id_time_off, id_number" +
                    ", id_scrapieflockid, official_id, id_idremovereasonid, created, modified)" +
                    " values (%s, %s, %s, %s, %s, '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', '%s', '%s')"
                    , thisanimal_id, first_tag_type_id, first_tag_color, first_tag_color, first_tag_location
                    , mytoday, "", "", "",first_tag_number,"", official_id,"", created, modified);
            Log.i("update db ", "cmd is " + cmd);
            dbh.exec(cmd);
            Log.i("update db ", " after insert EID tag info ");

//todo needs to handle other species where second tag is not a trich tag Must get the type of tag and then add the appropriate insert statement
            if (second_tag_number == null || second_tag_number.isEmpty()||second_tag_number.trim().isEmpty() )
                Log.i("update db ", "no second tag entered ");
            else{
                TV = binding.secondNewTagNumber;
                Log.i("update db ", "second tag is " + second_tag_number);
                cmd = String.format("INSERT INTO animal_id_info_table (id_animalid, id_idtypeid, id_male_id_idcolorid" +
                                ", id_female_id_idcolorid, id_idlocationid, id_date_on, id_time_on, id_date_off, id_time_off, id_number" +
                    ", id_scrapieflockid, official_id, id_idremovereasonid, created, modified )" +
                    " values (%s, %s, %s, %s, %s, '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', '%s', '%s')"
                        , thisanimal_id, second_tag_type_id, second_tag_color, second_tag_color, second_tag_location
                        , mytoday, "", "", "", second_tag_number,"",official_id,"", created, modified);
                Log.i("update db ", "cmd is " + cmd);
                dbh.exec(cmd);
                Log.i("update db ", " after insert second tag info ");
            }
//
            if (third_tag_number == null || third_tag_number.isEmpty()||third_tag_number.trim().isEmpty() )
                    Log.i("update db ", "no third tag entered ");
            else{
                cmd = String.format("INSERT INTO animal_id_info_table (id_animalid, id_idtypeid, id_male_id_idcolorid" +
                                ", id_female_id_idcolorid, id_idlocationid, id_date_on, id_time_on, id_date_off, id_time_off, id_number" +
                                ", id_scrapieflockid, official_id, id_idremovereasonid, created, modified )" +
                                " values (%s, %s, %s, %s, %s, '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', '%s', '%s')"
                        , thisanimal_id, third_tag_type_id, third_tag_color, third_tag_color, third_tag_location
                        , mytoday, "", "", "", third_tag_number,"", official_id,"",  created, modified);
                Log.i("update db ", "cmd is " + cmd);
                dbh.exec(cmd);
                Log.i("update db ", " after insert third tag info ");
            }
            //  todo This makes the assumptions that on a new animal the selected owner is the breeder rather than from the defaults
//todo only handles people as owners not companies
            //  and that the reason for transfer is Natural Addition
            cmd = String.format("INSERT INTO animal_ownership_history_table (id_animalid, transfer_date, from_id_contactid" +
                            ", to_id_contactid, from_id_companyid, to_id_companyid, id_transferreasonid, sell_price, sell_price_id_unitsid, created, modified) " +
                            " values ( %s, '%s', %s, %s, %s, %s, %s, %s, %s, '%s', '%s') "
                            , thisanimal_id, current_birth_date, 0, owner_id_contactid,0,0,4,0.0f,8,created, modified);
            Log.i("update db ", "cmd is " + cmd);
            dbh.exec(cmd);
            Log.i("update db ", " after insert ownership history ");
            cmd = String.format("INSERT INTO animal_location_history_table (id_animalid, movement_date, from_id_premiseid" +
                            ", to_id_premiseid, created, modified) " +
                            " values ( %s, '%s', '%s', %s, '%s', '%s') "
                    , thisanimal_id, current_birth_date, 0, owner_id_premiseid, created, modified);
            Log.i("update db ", "cmd is " + cmd);
            dbh.exec(cmd);
            Log.i("update db ", " after insert ownership history ");
            //  todo this is currently a single breed. Structure in database in place to handle breed percentages but not implemented yet
            cmd = String.format("INSERT INTO animal_breed_table (id_animalid, id_breedid, breed_percentage, created, modified) " +
                            " values ( %s, %s, %s, '%s','%s') "
                    , thisanimal_id, current_breed_id, 100.0f, created, modified);
            Log.i("update db ", "cmd is " + cmd);
            dbh.exec(cmd);
            Log.i("update db ", " after insert breed ");
            dbh.closeDB();
            finish();
        }
    }

    // user clicked the 'back' button
    public void backBtn( View v ) {
        dbh.closeDB();
        //Go back to Bull Breeding Soundness
        finish();
    }
    public void notImplemented (View v ) {
        // Display alerts here
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("This is not yet implemented.")
                .setTitle(R.string.alert_warning);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int idx) {
                // User clicked OK button
                ;
//                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}

