package com.weyr_associates.animaltrakkerfarmmobile.app.animal.add

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.SimpleAddAnimalViewModel.ValidationError.IdEntryRequired
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.SimpleAddAnimalViewModel.ValidationError.IncompleteAnimalEntry
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.SimpleAddAnimalViewModel.ValidationError.InvalidIdCombination
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.SimpleAddAnimalViewModel.ValidationError.InvalidIdNumberFormat
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.SimpleAddAnimalViewModel.ValidationError.PartialIdEntry
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.AutoIncrementNextTrichIdFeature
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.IdEntry
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.IdInput
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.IdInputCompleteness.COMPLETE
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.IdInputCompleteness.PARTIAL
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.IdValidations
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.IdsValidationError
import com.weyr_associates.animaltrakkerfarmmobile.app.core.Result
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.coroutines.flow.combine8
import com.weyr_associates.animaltrakkerfarmmobile.app.model.animalBirthDateFrom
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.LoadActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.model.BirthType
import com.weyr_associates.animaltrakkerfarmmobile.model.Breed
import com.weyr_associates.animaltrakkerfarmmobile.model.DefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.model.IdColor
import com.weyr_associates.animaltrakkerfarmmobile.model.IdLocation
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType
import com.weyr_associates.animaltrakkerfarmmobile.model.Owner
import com.weyr_associates.animaltrakkerfarmmobile.model.Premise
import com.weyr_associates.animaltrakkerfarmmobile.model.Sex
import com.weyr_associates.animaltrakkerfarmmobile.repository.AnimalRepository
import com.weyr_associates.animaltrakkerfarmmobile.repository.BreedRepository
import com.weyr_associates.animaltrakkerfarmmobile.repository.IdColorRepository
import com.weyr_associates.animaltrakkerfarmmobile.repository.IdLocationRepository
import com.weyr_associates.animaltrakkerfarmmobile.repository.IdTypeRepository
import com.weyr_associates.animaltrakkerfarmmobile.repository.OwnerRepository
import com.weyr_associates.animaltrakkerfarmmobile.repository.PremiseRepository
import com.weyr_associates.animaltrakkerfarmmobile.repository.SexRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.LocalDate
import java.time.LocalDateTime

class SimpleAddAnimalViewModel(
    private val savedStateHandle: SavedStateHandle,
    private val databaseHandler: DatabaseHandler,
    private val loadActiveDefaultSettings: LoadActiveDefaultSettings,
    private val animalRepo: AnimalRepository,
    private val breedRepo: BreedRepository,
    private val sexRepo: SexRepository,
    private val ownerRepo: OwnerRepository,
    private val premiseRepo: PremiseRepository,
    private val idTypeRepo: IdTypeRepository,
    private val idColorRepo: IdColorRepository,
    private val idLocationRepo: IdLocationRepository,
    private val idValidations: IdValidations,
    private val autoUpdateTrichId: AutoIncrementNextTrichIdFeature
) : ViewModel() {

    companion object {

    }

    sealed interface Event

    sealed interface InputEvent : Event {
        data object AnimalNameChanged : InputEvent
        data object IdNumber1Changed : InputEvent
        data object IdNumber2Changed : InputEvent
        data object IdNumber3Changed : InputEvent
    }

    sealed interface ValidationError : Event {
        data object IncompleteAnimalEntry : ValidationError
        data object IdEntryRequired : ValidationError
        data object PartialIdEntry : ValidationError

        data class InvalidIdNumberFormat(
            val idEntry: IdEntry
        ) : ValidationError

        data class InvalidIdCombination(
            val error: IdsValidationError
        ) : ValidationError
    }

    sealed interface UpdateDatabaseEvent : Event {
        data class Success(val animalName: String, val animalId: Long) : UpdateDatabaseEvent
        data class Error(val animalName: String) : UpdateDatabaseEvent
    }

    private enum class IdEntryField {
        FIRST,
        SECOND,
        THIRD
    }

    private lateinit var eidIdType: IdType
    private var defaultOwner: Owner? = null

    private var breederId = 0
    private var breederTypeId = 0
    private var birthTypeId = 0
    private var rearTypeId = 0
    private var ownerPremiseId = 0

    private val _animalName = MutableStateFlow("")
    var animalName: String
        get() = _animalName.value
        set(value) { _animalName.update{ value } }

    private val _idNumber1 = MutableStateFlow("")
    var idNumber1: String
        get() = _idNumber1.value
        set(value) { _idNumber1.update { value } }

    private val _idNumber2 = MutableStateFlow("")
    var idNumber2: String
        get() = _idNumber2.value
        set(value) { _idNumber2.update { value } }

    private val _idNumber3 = MutableStateFlow("")
    var idNumber3: String
        get() = _idNumber3.value
        set(value) { _idNumber3.update { value } }

    private val _selectedBreed = MutableStateFlow<Breed?>(null)
    val selectedBreed: StateFlow<Breed?> = _selectedBreed.asStateFlow()

    private val _selectedAgeYears = MutableStateFlow(0)
    val selectedAgeYears = _selectedAgeYears.asStateFlow()

    private val _selectedAgeMonths = MutableStateFlow(0)
    val selectedAgeMonths = _selectedAgeMonths.asStateFlow()

    private val _selectedSex = MutableStateFlow<Sex?>(null)
    val selectedSex = _selectedSex.asStateFlow()

    private val _selectedOwner = MutableStateFlow<Owner?>(null)
    val selectedOwner = _selectedOwner.asStateFlow()

    private val _selectedIdType1 = MutableStateFlow<IdType?>(null)
    val selectedIdType1 = _selectedIdType1.asStateFlow()

    private val _selectedIdColor1 = MutableStateFlow<IdColor?>(null)
    val selectedIdColor1 = _selectedIdColor1.asStateFlow()

    private val _selectedIdLocation1 = MutableStateFlow<IdLocation?>(null)
    val selectedIdLocation1 = _selectedIdLocation1.asStateFlow()

    private val _selectedIdType2 = MutableStateFlow<IdType?>(null)
    val selectedIdType2 = _selectedIdType2.asStateFlow()

    private val _selectedIdColor2 = MutableStateFlow<IdColor?>(null)
    val selectedIdColor2 = _selectedIdColor2.asStateFlow()

    private val _selectedIdLocation2 = MutableStateFlow<IdLocation?>(null)
    val selectedIdLocation2 = _selectedIdLocation2.asStateFlow()

    private val _selectedIdType3 = MutableStateFlow<IdType?>(null)
    val selectedIdType3 = _selectedIdType3.asStateFlow()

    private val _selectedIdColor3 = MutableStateFlow<IdColor?>(null)
    val selectedIdColor3 = _selectedIdColor3.asStateFlow()

    private val _selectedIdLocation3 = MutableStateFlow<IdLocation?>(null)
    val selectedIdLocation3 = _selectedIdLocation3.asStateFlow()

    private val _speciesId = MutableStateFlow(DefaultSettings.SPECIES_ID_DEFAULT)
    val speciesId = _speciesId.asStateFlow()

    val canClearData = combine(
        _animalName,
        _idNumber1,
        _idNumber2,
        _idNumber3
    ) { animalName, idNumber1, idNumber2, idNumber3 ->
        animalName.isNotBlank() || idNumber1.isNotBlank() ||
                idNumber2.isNotBlank() || idNumber3.isNotBlank()
    }.stateIn(viewModelScope, SharingStarted.Lazily, false)

    private val _isUpdatingDatabase = MutableStateFlow(false)

    val canSaveToDatabase = combine8(
        _idNumber1,
        _selectedOwner,
        _selectedBreed,
        _selectedSex,
        _selectedIdType1,
        _selectedIdColor1,
        _selectedIdLocation1,
        _isUpdatingDatabase
    ) { idNumber1, selectedOwner, selectedBreed, selectedSex, selectedIdType1, selectedIdColor1, selectedIdLocation1, isUpdatingDatabase ->
        idNumber1.isNotBlank() && selectedOwner != null &&
        selectedBreed != null && selectedSex != null &&
        selectedIdType1 != null && selectedIdColor1 != null &&
        selectedIdLocation1 != null && !isUpdatingDatabase
    }.stateIn(viewModelScope, SharingStarted.Lazily, false)

    private val _eventChannel = Channel<Event>()
    val events = _eventChannel.receiveAsFlow()

    init {
        idNumber1 = savedStateHandle[AddAnimal.EXTRA_PRIMARY_ID_NUMBER] ?: ""
        viewModelScope.launch {
            loadDefaults()
        }
    }

    fun selectBreed(breed: Breed) {
        _selectedBreed.update { breed }
    }

    fun selectAgeYears(years: Int) {
        _selectedAgeYears.update { years }
    }

    fun selectAgeMonths(months: Int) {
        _selectedAgeMonths.update { months }
    }

    fun selectOwner(owner: Owner) {
        _selectedOwner.update { owner }
    }

    fun selectSex(sex: Sex) {
        _selectedSex.update { sex }
    }

    fun selectIdType1(idType: IdType) {
        _selectedIdType1.update { idType }
        checkAutoPopulateTrich(idType, IdEntryField.FIRST)
    }

    fun selectIdColor1(idColor: IdColor) {
        _selectedIdColor1.update { idColor }
    }

    fun selectIdLocation1(idLocation: IdLocation) {
        _selectedIdLocation1.update { idLocation }
    }

    fun selectIdType2(idType: IdType?) {
        _selectedIdType2.update { idType }
        if (idType == null) {
            clearIdEntryField(IdEntryField.SECOND)
        } else {
            checkAutoPopulateTrich(idType, IdEntryField.SECOND)
        }
    }

    fun selectIdColor2(idColor: IdColor?) {
        _selectedIdColor2.update { idColor }
    }

    fun selectIdLocation2(idLocation: IdLocation?) {
        _selectedIdLocation2.update { idLocation }
    }

    fun selectIdType3(idType: IdType?) {
        _selectedIdType3.update { idType }
        if (idType == null) {
            clearIdEntryField(IdEntryField.THIRD)
        } else {
            checkAutoPopulateTrich(idType, IdEntryField.THIRD)
        }
    }

    fun selectIdColor3(idColor: IdColor?) {
        _selectedIdColor3.update { idColor }
    }

    fun selectIdLocation3(idLocation: IdLocation?) {
        _selectedIdLocation3.update { idLocation }
    }

    fun onEIDScanned(eidString: String) {
        idNumber1 = eidString
        _selectedIdType1.update { eidIdType }
        postEvent(InputEvent.IdNumber1Changed)
    }

    fun clearData() {
        if (canClearData.value) {
            executeClearData()
        }
    }

    fun saveToDatabase() {
        if (canSaveToDatabase.value) {
            viewModelScope.launch {
                updateDatabase()
            }
        }
    }

    override fun onCleared() {
        databaseHandler.close()
        super.onCleared()
    }

    private suspend fun loadDefaults() {

        val defaults = loadActiveDefaultSettings()

        breederId = defaults.breederId ?: 0
        breederTypeId = defaults.breederType ?: 0
        birthTypeId = defaults.birthTypeId
        rearTypeId = defaults.rearTypeId
        ownerPremiseId = defaults.ownerPremiseId

        autoUpdateTrichId.configureFromSettings(defaults)

        val eidIdTypeDeferred = viewModelScope.async {
            idTypeRepo.queryForIdType(IdType.ID_TYPE_ID_EID)
        }
        val breedDeferred = viewModelScope.async {
            breedRepo.queryBreed(defaults.breedId)
        }
        val ownerDeferred = viewModelScope.async {
            val ownerId = defaults.ownerId
            val ownerType = defaults.ownerType
            when {
                ownerId != null && ownerType != null -> {
                    ownerRepo.queryOwner(ownerId, ownerType)
                }
                else -> null
            }
        }
        val sexDeferred = viewModelScope.async {
            sexRepo.querySex(defaults.sexId)
        }

        val idType1Deferred = viewModelScope.async {
            idTypeRepo.queryForIdType(
                savedStateHandle[AddAnimal.EXTRA_PRIMARY_ID_TYPE_ID]
                    ?: defaults.idTypeIdPrimary
            )
        }
        val idColor1Deferred = viewModelScope.async {
            idColorRepo.queryIdColor(
                defaults.defaultIdColorFromIdType(defaults.idTypeIdPrimary)
            )
        }
        val idLocation1Deferred = viewModelScope.async {
            idLocationRepo.queryIdLocation(
                defaults.defaultIdLocationFromIdType(defaults.idTypeIdPrimary)
            )
        }

        val idType2Deferred = viewModelScope.async {
            idTypeRepo.queryForIdType(defaults.idTypeIdSecondary)
        }
        val idColor2Deferred = viewModelScope.async {
            idColorRepo.queryIdColor(
                defaults.defaultIdColorFromIdType(defaults.idTypeIdSecondary)
            )
        }
        val idLocation2Deferred = viewModelScope.async {
            idLocationRepo.queryIdLocation(
                defaults.defaultIdLocationFromIdType(defaults.idTypeIdSecondary)
            )
        }

        val idType3Deferred = viewModelScope.async {
            idTypeRepo.queryForIdType(defaults.idTypeIdTertiary)
        }
        val idColor3Deferred = viewModelScope.async {
            idColorRepo.queryIdColor(
                defaults.defaultIdColorFromIdType(defaults.idTypeIdTertiary)
            )
        }
        val idLocation3Deferred = viewModelScope.async {
            idLocationRepo.queryIdLocation(
                defaults.defaultIdLocationFromIdType(defaults.idTypeIdTertiary)
            )
        }

        awaitAll(
            eidIdTypeDeferred,
            breedDeferred,
            ownerDeferred,
            sexDeferred,
            idType1Deferred,
            idType2Deferred,
            idType3Deferred,
            idColor1Deferred,
            idColor2Deferred,
            idColor3Deferred,
            idLocation1Deferred,
            idLocation2Deferred,
            idLocation3Deferred,
        )

        eidIdType = requireNotNull(eidIdTypeDeferred.await())

        _selectedBreed.update { breedDeferred.await() }
        _selectedSex.update { sexDeferred.await() }
        _selectedOwner.update {
            ownerDeferred.await().also { defaultOwner = it }
        }

        val idType1 = idType1Deferred.await()
        _selectedIdType1.update { idType1 }
        _selectedIdColor1.update { idColor1Deferred.await() }
        _selectedIdLocation1.update { idLocation1Deferred.await() }
        checkAutoPopulateTrich(idType1, IdEntryField.FIRST)

        val idType2 = idType2Deferred.await()
        _selectedIdType2.update { idType2 }
        _selectedIdColor2.update { idColor2Deferred.await() }
        _selectedIdLocation2.update { idLocation2Deferred.await() }
        checkAutoPopulateTrich(idType2, IdEntryField.SECOND)

        val idType3 = idType3Deferred.await()
        _selectedIdType3.update { idType3 }
        _selectedIdColor3.update { idColor3Deferred.await() }
        _selectedIdLocation3.update { idLocation3Deferred.await() }
        checkAutoPopulateTrich(idType3, IdEntryField.THIRD)
    }

    private fun clearIdEntryField(idEntryField: IdEntryField) {
        when (idEntryField) {
            IdEntryField.FIRST -> {
                _selectedIdColor1.update { null }
                _selectedIdLocation1.update { null }
                idNumber1 = ""
                postEvent(InputEvent.IdNumber1Changed)
            }
            IdEntryField.SECOND -> {
                _selectedIdColor2.update { null }
                _selectedIdLocation2.update { null }
                idNumber2 = ""
                postEvent(InputEvent.IdNumber2Changed)
            }
            IdEntryField.THIRD -> {
                _selectedIdColor3.update { null }
                _selectedIdLocation3.update { null }
                idNumber3 = ""
                postEvent(InputEvent.IdNumber3Changed)
            }
        }
    }

    private fun checkAutoPopulateTrich(idType: IdType?, idEntryField: IdEntryField) {
        if (idType == null) return
        if (autoUpdateTrichId.shouldAutoPopulateTrichNumber(idType)) {
            val nextTrichNumberString = autoUpdateTrichId.nextTrichNumber.toString()
            when (idEntryField) {
                IdEntryField.FIRST -> {
                    idNumber1 = nextTrichNumberString
                    postEvent(InputEvent.IdNumber1Changed)
                }
                IdEntryField.SECOND -> {
                    idNumber2 = nextTrichNumberString
                    postEvent(InputEvent.IdNumber2Changed)
                }
                IdEntryField.THIRD -> {
                    idNumber3 = nextTrichNumberString
                    postEvent(InputEvent.IdNumber3Changed)
                }
            }
        }
    }

    private fun executeClearData() {
        animalName = ""
        idNumber1 = ""
        idNumber2 = ""
        idNumber3 = ""
        postEvents(
            InputEvent.AnimalNameChanged,
            InputEvent.IdNumber1Changed,
            InputEvent.IdNumber2Changed,
            InputEvent.IdNumber3Changed
        )
    }

    private suspend fun updateDatabase() {
        try {

            _isUpdatingDatabase.update { true }

            val selectedOwnerId = selectedOwner.value?.id
            val selectedOwnerTypeId = selectedOwner.value?.type
            val selectedBreedId = selectedBreed.value?.id
            val selectedSexId = selectedSex.value?.id

            if (selectedOwnerId == null ||
                selectedOwnerTypeId == null ||
                selectedBreedId == null ||
                selectedSexId == null
            ) {
                postEvent(IncompleteAnimalEntry)
                return
            }

            //Capture ID input values for each ID.

            val idInput1 = IdInput(
                idNumber1,
                selectedIdType1.value,
                selectedIdColor1.value,
                selectedIdLocation1.value
            )

            val idInput2 = IdInput(
                idNumber2,
                selectedIdType2.value,
                selectedIdColor2.value,
                _selectedIdLocation2.value
            )

            val idInput3 = IdInput(
                idNumber3,
                selectedIdType3.value,
                selectedIdColor3.value,
                selectedIdLocation3.value
            )

            //Check to make sure that ID 1 is completely entered, and that ID 2 and ID 3
            //are either completely entered or left completely empty.

            val idInput1Completeness = idValidations.checkIdInputCompleteness(idInput1)
            val idInput2Completeness = idValidations.checkIdInputCompleteness(idInput2)
            val idInput3Completeness = idValidations.checkIdInputCompleteness(idInput3)

            if (COMPLETE != idInput1Completeness) {
                postEvent(IdEntryRequired)
                return
            }

            if (PARTIAL == idInput2Completeness) {
                postEvent(PartialIdEntry)
                return
            }

            if (PARTIAL == idInput3Completeness) {
                postEvent(PartialIdEntry)
                return
            }

            //Capture ID entry values as non-optional
            //in the case where complete entries have been made.

            var idEntry1 = idInput1.toEntry()
            var idEntry2 = idInput2.toOptEntry()
            var idEntry3 = idInput3.toOptEntry()

            //Check to make sure the ID number entries for each ID are
            //properly formatted for their associated ID Type.

            val idFormatCheck1 = idValidations.checkIdNumberFormat(idEntry1)
            val idFormatCheck2 = idEntry2?.let { idValidations.checkIdNumberFormat(it) }
            val idFormatCheck3 = idEntry3?.let { idValidations.checkIdNumberFormat(it) }

            listOf(idFormatCheck1, idFormatCheck2, idFormatCheck3).forEach { formatCheck ->
                if (formatCheck is Result.Failure) {
                    postEvent(InvalidIdNumberFormat(formatCheck.error.idEntry))
                    return
                }
            }

            //Determine whether or not each ID entry should be considered an official ID.

            idEntry1 = idEntry1.copy(isOfficial = idValidations.checkIdEntryIsOfficial(idEntry1))
            idEntry2 = idEntry2?.let { it.copy(isOfficial = idValidations.checkIdEntryIsOfficial(it)) }
            idEntry3 = idEntry3?.let { it.copy(isOfficial = idValidations.checkIdEntryIsOfficial(it)) }

            //Create list of actual id entries that will be added to the animal

            val idEntries = buildList {
                add(idEntry1)
                idEntry2?.let { add(it) }
                idEntry3?.let { add(it) }
            }

            //Check to see if any EID entries duplicate EIDs amongst themselves
            //or against any existing EIDs in the database.

            val idEntryDuplicateEIDCheck = idValidations.checkEIDsNotDuplicated
                .withAdditionOf(idEntries)

            if (idEntryDuplicateEIDCheck is Result.Failure) {
                postEvent(InvalidIdCombination(idEntryDuplicateEIDCheck.error))
                return
            }

            //Check to see if the combination of IDs violates the rules for which
            //and how many of each ID type can be on an animal at the same time.

            val idEntryComboCheck = idValidations.checkIdCombinationValidity
                .whenAddingAnimal(idEntries)

            if (idEntryComboCheck is Result.Failure) {
                postEvent(InvalidIdCombination(idEntryComboCheck.error))
                return
            }

            //Prepare data and some defaults before adding animal and ID entries to the database.

            //Capturing official ID number in lieu of a an entered name
            //requires that validation guarantees at least one official ID
            //above.
            val animalName = animalName.takeIf { it.isNotBlank() }
                ?: idEntries.first { it.isOfficial }.number

            val todayDateTime = LocalDateTime.now()

            val animalAgeYears = selectedAgeYears.value
            val animalAgeMonths = selectedAgeMonths.value
            val animalBirthDate = todayDateTime.animalBirthDateFrom(animalAgeYears, animalAgeMonths)

            //Look up the owner premise as the first physical or "both" premise for the selected owner
            //if it is not the default owner, otherwise use the default ownerPremiseId from active defaults.
            val ownerPremiseId = ownerPremiseId.takeIf { it != 0 && selectedOwnerId == defaultOwner?.id }
                ?: premiseRepo.queryPhysicalPremiseForOwner(selectedOwnerId, selectedOwnerTypeId)?.id
                ?: Premise.ID_PREMISE_UNKNOWN

            val birthTypeId = birthTypeId.takeIf { it != 0 } ?: BirthType.ID_UNKNOWN

            //Setup a transaction and add the animal and its IDs
            val animalId: Long = saveAnimalAndIdsToDatabase(
                animalName,
                selectedBreedId,
                selectedSexId,
                animalBirthDate,
                birthTypeId,
                rearTypeId,
                selectedOwnerId,
                selectedOwnerTypeId,
                ownerPremiseId,
                breederId,
                breederTypeId,
                idEntries,
                todayDateTime
            )

            val succeeded = animalId != -1L

            postEvent(
                if (succeeded) UpdateDatabaseEvent.Success(animalName, animalId)
                else UpdateDatabaseEvent.Error(animalName)
            )

            if (succeeded) {
                executeClearData()
            }

        } finally {
            _isUpdatingDatabase.update { false }
        }
    }

    private suspend fun saveAnimalAndIdsToDatabase(
        animalName: String,
        breedId: Int,
        sexId: Int,
        birthDate: LocalDate,
        birthTypeId: Int,
        rearTypeId: Int,
        ownerId: Int,
        ownerType: Owner.Type,
        ownerPremiseId: Int,
        breederId: Int,
        breederTypeId: Int,
        idEntries: List<IdEntry>,
        timeStamp: LocalDateTime
    ): Long {
        //Run the whole transaction from dispatch to IO thread to prevent
        // layered transactions from blocking each other.
        return withContext(Dispatchers.IO) {
            databaseHandler.writableDatabase.beginTransaction()
            try {
                //Add the animal
                val animalId = animalRepo.addAnimal(
                    animalName,
                    breedId,
                    sexId,
                    birthDate,
                    birthTypeId,
                    rearTypeId,
                    ownerId,
                    ownerType,
                    ownerPremiseId,
                    breederId,
                    breederTypeId,
                    timeStamp
                )
                //Add its IDs
                idEntries.forEach { idEntry ->
                    animalRepo.addIdToAnimal(
                        animalId,
                        idEntry.type.id,
                        idEntry.color.id,
                        idEntry.location.id,
                        idEntry.number,
                        idEntry.isOfficial,
                        timeStamp
                    )
                    //Update next trich number if any IDs require it
                    autoUpdateTrichId.autoIncrementIfRequired(idEntry)
                }
                databaseHandler.writableDatabase.setTransactionSuccessful()
                animalId
            } catch (ex: Exception) {
                -1
            } finally {
                databaseHandler.writableDatabase.endTransaction()
            }
        }
    }

    private fun postEvent(event: Event) {
        viewModelScope.launch {
            _eventChannel.send(event)
        }
    }

    private fun postEvents(vararg events: Event) {
        viewModelScope.launch {
            events.forEach { _eventChannel.send(it) }
        }
    }
}
