package com.weyr_associates.animaltrakkerfarmmobile.app.animal.id

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.viewmodel.CreationExtras
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.weyr_associates.animaltrakkerfarmmobile.BluetoothWatcher
import com.weyr_associates.animaltrakkerfarmmobile.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.RequiredPermissions
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.AnimalBasicInfoHeaderPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.AnimalDialogs
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.AddAnimal
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.ManageAnimalIdsViewModel.AnimalInfoState.AnimalInfoLoaded
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.note.TakeNotesActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.setupForAnimalNotFound
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.setupForAnimalWithEIDNotFound
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.setupForInitialLookup
import com.weyr_associates.animaltrakkerfarmmobile.app.core.hideKeyboard
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.coroutines.flow.observeOneTimeEvents
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.takeAs
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.ItemSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.SelectItem
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.selectedItem
import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.TopButtonBar
import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.recyclerview.OutlineDividerDecoration
import com.weyr_associates.animaltrakkerfarmmobile.app.main.MainActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.model.itemCallbackUsingOnlyIdentity
import com.weyr_associates.animaltrakkerfarmmobile.app.select.SelectAnimal
import com.weyr_associates.animaltrakkerfarmmobile.app.select.SelectAnimalActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.select.SelectIdRemoveReasonDialogFragment
import com.weyr_associates.animaltrakkerfarmmobile.app.select.idColorSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.idLocationSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.idTypeSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.ActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.LoadActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.baacodeService
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ActivityManageAnimalIdsBinding
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ItemAnimalIdEditableBinding
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ViewNoAnimalIdsFoundBinding
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderService
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderStatePresenter
import com.weyr_associates.animaltrakkerfarmmobile.model.IdBasicInfo
import com.weyr_associates.animaltrakkerfarmmobile.model.IdColor
import com.weyr_associates.animaltrakkerfarmmobile.model.IdLocation
import com.weyr_associates.animaltrakkerfarmmobile.model.IdRemoveReason
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.AnimalRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.repository.database.DefaultSettingsRepositoryImpl
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class ManageAnimalIdsActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_SELECTED_ID = "EXTRA_SELECTED_ANIMAL_ID"
        private const val TAG_FRAGMENT_SELECT_ID_REMOVE_REASON = "TAG_FRAGMENT_SELECT_ID_REMOVE_REASON"
        private const val REQUEST_CODE_LOOKUP_ANIMAL = 200
        private const val REQUEST_CODE_ADD_AND_SELECT_ANIMAL = 201
        private const val REQUEST_CODE_EDIT_ANIMAL_ID = 202
    }

    private val viewModel: ManageAnimalIdsViewModel
        by viewModels { ManageAnimalIdsViewModelFactory(this) }

    private val binding: ActivityManageAnimalIdsBinding by lazy {
        ActivityManageAnimalIdsBinding.inflate(layoutInflater)
    }

    private lateinit var eidReaderStatePresenter: EIDReaderStatePresenter

    private val animalInfoHeaderPresenter = AnimalBasicInfoHeaderPresenter()

    private lateinit var idTypePresenter: ItemSelectionPresenter<IdType>
    private lateinit var idColorPresenter: ItemSelectionPresenter<IdColor>
    private lateinit var idLocationPresenter: ItemSelectionPresenter<IdLocation>

    private val animalIdsAdapter = EditAnimalIdsAdapter(::onEditId, ::onRemoveId)

    private var mService: Messenger? = null
    private var mIsBound: Boolean = false
    private val mMessenger: Messenger = Messenger(IncomingHandler())

    private val mConnection: ServiceConnection = EidReaderServiceConnection()

    private val bluetoothWatcher = BluetoothWatcher(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        with(binding.buttonPanelTop) {
            show(
                TopButtonBar.UI_SCANNER_STATUS or
                TopButtonBar.UI_SCAN_EID or
                TopButtonBar.UI_LOOKUP_ANIMAL or
                TopButtonBar.UI_SHOW_ALERT or
                TopButtonBar.UI_TAKE_NOTE or
                TopButtonBar.UI_CLEAR_DATA
            )

            eidReaderStatePresenter = EIDReaderStatePresenter(
                this@ManageAnimalIdsActivity, imageScannerStatus
            )
            scanEIDButton.setOnClickListener { onScanEID() }
            lookupAnimalButton.setOnClickListener { onLookupAnimal() }
            clearDataButton.setOnClickListener { viewModel.clearData() }
        }

        animalInfoHeaderPresenter.binding = binding.animalInfoHeader

        idTypePresenter = idTypeSelectionPresenter(
            button = binding.inputAnimalId.spinnerIdType
        ) { idType ->
            viewModel.selectIdType(idType)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdType)
        }

        idLocationPresenter = idLocationSelectionPresenter(
            button = binding.inputAnimalId.spinnerIdLocation
        ) { idLocation ->
            viewModel.selectIdLocation(idLocation)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdLocation)
        }

        idColorPresenter = idColorSelectionPresenter(
            button = binding.inputAnimalId.spinnerIdColor
        ) { idColor ->
            viewModel.selectIdColor(idColor)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdColor)
        }

        with(binding.inputAnimalId) {
            inputIdNumber.setText(viewModel.idNumber)
            inputIdNumber.addTextChangedListener {
                viewModel.idNumber = it.toString()
            }
            buttonAddId.setOnClickListener {
                hideKeyboard()
                viewModel.addId()
            }
        }

        with(binding.recyclerAnimalIds) {
            adapter = animalIdsAdapter
            layoutManager = LinearLayoutManager(
                this@ManageAnimalIdsActivity,
                RecyclerView.VERTICAL,
                false
            )
            itemAnimator = null
            addItemDecoration(OutlineDividerDecoration(this@ManageAnimalIdsActivity))
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.canClearData.collectLatest { canClear ->
                    binding.buttonPanelTop.clearDataButton.isEnabled = canClear
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.canAddId.collectLatest { canAdd ->
                    binding.inputAnimalId.buttonAddId.isEnabled = canAdd
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.animalInfoState.collectLatest {
                    updateTopBarAnimalActions(it)
                    updateAnimalInfoDisplay(it)
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.events.observeOneTimeEvents {
                    handleEvent(it)
                }
            }
        }

        supportFragmentManager.setFragmentResultListener(
            SelectIdRemoveReasonDialogFragment.REQUEST_KEY_SELECT_ID_REMOVE_REASON,
            this
        ) { _, result ->
            val removeReason: IdRemoveReason = result.selectedItem()
            val idBasicInfo: IdBasicInfo = requireNotNull(
                result.getBundle(SelectItem.EXTRA_ASSOCIATED_DATA)
                    ?.getParcelable(EXTRA_SELECTED_ID)
            )
            onRemoveReasonSelected(idBasicInfo, removeReason)
        }
    }

    override fun onResume() {
        super.onResume()
        if (checkRequiredPermissions()) {
            checkIfServiceIsRunning()
        }
        bluetoothWatcher.startWatching()
    }

    public override fun onPause() {
        super.onPause()
        Log.i("Breeding", " OnPause")
        doUnbindService()
        bluetoothWatcher.stopWatching()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE_LOOKUP_ANIMAL) {
            if (resultCode == RESULT_OK && data != null && data.hasExtra(SelectAnimal.EXTRA_SELECTED_ANIMAL_ID)) {
                val selectedAnimalId = data.getIntExtra(SelectAnimal.EXTRA_SELECTED_ANIMAL_ID, -1)
                if (selectedAnimalId != -1) {
                    viewModel.onAnimalSelected(selectedAnimalId)
                }
            }
        } else if (requestCode == REQUEST_CODE_ADD_AND_SELECT_ANIMAL) {
            if (resultCode == RESULT_OK && data != null && data.hasExtra(AddAnimal.EXTRA_RESULTING_ANIMAL_ID)) {
                val resultingAnimalId = data.getIntExtra(AddAnimal.EXTRA_RESULTING_ANIMAL_ID, -1)
                if (resultingAnimalId != -1) {
                    viewModel.onAnimalSelected(resultingAnimalId)
                }
            }
        }
        else if (requestCode == REQUEST_CODE_EDIT_ANIMAL_ID) {
            if (resultCode == RESULT_OK) {
                viewModel.onIdEdited()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun checkRequiredPermissions(): Boolean {
        if (!RequiredPermissions.areFulfilled(this)) {
            MainActivity.returnFrom(this)
            return false
        }
        return true
    }

    //  Really should be check if services are running...
    //  Start the service if it is not already running and bind to it
    //  Should be one each for eid, scale and baacode
    private fun checkIfServiceIsRunning() {
        //If the service is running when the activity starts, we want to automatically bind to it.
        Log.i("Breeding", "At eid isRunning?.")
        if (EIDReaderService.isRunning()) {
//			Log.i("Breeding", " eidService is running");
            doBindService()
        } else {
//			Log.i("Breeding", " eidService is not running, start it");
            startService(Intent(this, EIDReaderService::class.java))
            doBindService()
        }
    }

    private fun onLookupAnimal() {
        startActivityForResult(
            Intent(this, SelectAnimalActivity::class.java),
            REQUEST_CODE_LOOKUP_ANIMAL
        )
    }

    private fun onScanEID() {
        mService?.let { service ->
            try {
                //Start eidService sending tags
                val msg = Message.obtain(null, EIDReaderService.MSG_SEND_ME_TAGS)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
    }

    private fun onEIDScanned(eidNumber: String) {
        viewModel.onEIDScanned(eidNumber)
    }

    private fun onEditId(idBasicInfo: IdBasicInfo) {
        val animalInfo = viewModel.animalInfoState.value.takeAs<AnimalInfoLoaded>()
            ?.animalBasicInfo
            ?: return
        startActivityForResult(
            EditAnimalIdActivity.newIntent(
                this,
                idBasicInfo,
                animalInfo
            ),
            REQUEST_CODE_EDIT_ANIMAL_ID
        )
    }

    private fun onRemoveId(idBasicInfo: IdBasicInfo) {
        promptForRemoveReason(idBasicInfo)
    }

    private fun onRemoveReasonSelected(idBasicInfo: IdBasicInfo, removeReason: IdRemoveReason) {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title_confirm_remove_animal_id)
            .setMessage(
                getString(
                    R.string.dialog_message_confirm_remove_animal_id,
                    idBasicInfo.typeName,
                    idBasicInfo.number,
                    removeReason.text
                )
            )
            .setPositiveButton(R.string.yes_label) { _, _ ->
                viewModel.removeId(idBasicInfo.id, removeReason.id)
            }
            .setNegativeButton(R.string.no_label) { _, _ -> /*NO-OP*/ }
            .create()
            .show()
    }

    private fun updateTopBarAnimalActions(animalInfoState: ManageAnimalIdsViewModel.AnimalInfoState) {
        when (animalInfoState) {
            is AnimalInfoLoaded -> {
                with(binding.buttonPanelTop) {
                    with(takeNoteButton) {
                        isEnabled = true
                        setOnClickListener {
                            startActivity(
                                TakeNotesActivity.newIntent(
                                    this@ManageAnimalIdsActivity,
                                    animalInfoState.animalBasicInfo.id
                                )
                            )
                        }
                    }
                    with(showAlertButton) {
                        val alertText = animalInfoState.animalBasicInfo.alert
                        when {
                            alertText.isNullOrBlank() -> {
                                isEnabled = false
                                setOnClickListener(null)
                            }
                            else -> {
                                isEnabled = true
                                setOnClickListener {
                                    AnimalDialogs.showAnimalAlert(this@ManageAnimalIdsActivity, alertText)
                                }
                            }
                        }
                    }
                }
            }
            else -> {
                with(binding.buttonPanelTop) {
                    with(takeNoteButton) {
                        isEnabled = false
                        setOnClickListener(null)
                    }
                    with(showAlertButton) {
                        isEnabled = false
                        setOnClickListener(null)
                    }
                }
            }
        }
    }

    private fun updateAnimalInfoDisplay(animalInfoState: ManageAnimalIdsViewModel.AnimalInfoState) {
        when (animalInfoState) {
            ManageAnimalIdsViewModel.AnimalInfoState.Initial -> {
                animalInfoHeaderPresenter.animalBasicInfo = null
                binding.animalInfoHeader.root.isGone = true
                binding.containerNoAnimal.root.isVisible = true
                binding.containerNoAnimal.setupForInitialLookup()
                binding.inputAnimalId.root.isGone = true
                binding.recyclerAnimalIds.isGone = true
            }
            is AnimalInfoLoaded -> {
                binding.animalInfoHeader.root.isVisible = true
                binding.containerNoAnimal.root.isGone = true
                binding.inputAnimalId.root.isVisible = true
                binding.recyclerAnimalIds.isVisible = true
                animalInfoHeaderPresenter.animalBasicInfo = animalInfoState.animalBasicInfo
                animalIdsAdapter.submitList(animalInfoState.animalBasicInfo.ids)
            }
            is ManageAnimalIdsViewModel.AnimalInfoState.AnimalInfoNotFound -> {
                animalInfoHeaderPresenter.animalBasicInfo = null
                binding.animalInfoHeader.root.isGone = true
                binding.containerNoAnimal.root.isVisible = true
                binding.inputAnimalId.root.isGone = true
                binding.recyclerAnimalIds.isGone = true
                when (val lookup = animalInfoState.lookup) {
                    is ManageAnimalIdsViewModel.Lookup.ByAnimalId -> {
                        binding.containerNoAnimal.setupForAnimalNotFound()
                    }
                    is ManageAnimalIdsViewModel.Lookup.ByScannedEID -> {
                        binding.containerNoAnimal.setupForAnimalWithEIDNotFound(
                            this,
                            lookup.scannedEID,
                            REQUEST_CODE_ADD_AND_SELECT_ANIMAL
                        )
                    }
                }
            }
        }
    }

    private fun promptForRemoveReason(idBasicInfo: IdBasicInfo) {
        SelectIdRemoveReasonDialogFragment.newInstance(
            associatedData = Bundle().apply {
                putParcelable(EXTRA_SELECTED_ID, idBasicInfo)
            }
        ).show(supportFragmentManager, TAG_FRAGMENT_SELECT_ID_REMOVE_REASON)
    }

    private fun handleEvent(event: ManageAnimalIdsViewModel.Event) {
        when (event) {
            ManageAnimalIdsViewModel.IdNumberChanged -> {
                binding.inputAnimalId.inputIdNumber.setText(viewModel.idNumber)
            }
            is ManageAnimalIdsViewModel.PromptForEIDUsage -> {
                promptForEIDUsage(event.eidNumber)
            }
            ManageAnimalIdsViewModel.IdAdditionFailed -> {
                showIdAdditionFailed()
            }
            ManageAnimalIdsViewModel.IdRemovalFailed -> {
                showIdRemovalFailed()
            }
            ManageAnimalIdsViewModel.ValidationError.IdEntryRequired -> {
                IdValidationErrorDialog.showIdEntryIsRequiredError(this)
            }
            ManageAnimalIdsViewModel.ValidationError.PartialIdEntry -> {
                IdValidationErrorDialog.showPartialIdEntryError(this)
            }
            is ManageAnimalIdsViewModel.ValidationError.InvalidIdNumberFormat -> {
                IdValidationErrorDialog.showIdNumberFormatError(this, event.idEntry)
            }
            is ManageAnimalIdsViewModel.ValidationError.InvalidIdCombination -> {
                IdValidationErrorDialog.showIdCombinationError(this, event.error)
            }
            is ManageAnimalIdsViewModel.AnimalAlert -> {
                AnimalDialogs.showAnimalAlert(this, event.alertText)
            }
        }
    }

    private fun promptForEIDUsage(eidNumber: String) {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title_prompt_scanned_eid_usage)
            .setMessage(getString(R.string.dialog_message_prompt_scanned_eid_usage, eidNumber))
            .setPositiveButton(R.string.text_lookup_animal) { _, _ ->
                viewModel.lookupAnimalByEID(eidNumber)
            }
            .setNegativeButton(R.string.text_add_eid) { _, _ ->
                viewModel.setIdNumberFromEIDScan(eidNumber)
            }
            .create()
            .show()
    }

    private fun showIdAdditionFailed() {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title_id_addition_failed)
            .setMessage(R.string.dialog_message_id_addition_failed)
    }

    private fun showIdRemovalFailed() {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title_id_removal_failed)
            .setMessage(R.string.dialog_message_id_removal_failed)
            .setPositiveButton(R.string.ok) { _, _ -> /*NO-OP*/ }
            .create()
            .show()
    }

    //  Bind services, should be one set of bind & unbind methods for eid, scale and baacode
    //  First is the eid service bind
    private fun doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
//		Log.i("Breeding", "At doBind1.");
        bindService(Intent(this, EIDReaderService::class.java), mConnection, BIND_AUTO_CREATE)
        //		Log.i("Breeding", "At doBind2.");
        mIsBound = true
        val service = mService
        if (service != null) {
//			Log.i("Breeding", "At doBind3.");
            try {
                //Request status update
                var msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_STATUS, 0, 0)
                msg.replyTo = mMessenger
                service.send(msg)
                //				Log.i("Breeding", "At doBind4.");
                //Request full log from service.
                msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_LOG_FULL, 0, 0)
                service.send(msg)
            } catch (e: RemoteException) {
                //NO-OP
            }
        }
        //		Log.i("Breeding", "At doBind5.");
    }

    //     Next the unbind eid servides
    private fun doUnbindService() {
//		Log.i("Breeding", "At DoUnbindservice");
        val service = mService
        if (service != null) {
            try {
                //Stop eidService from sending tags
                val msg = Message.obtain(null, EIDReaderService.MSG_NO_TAGS_PLEASE)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        if (mIsBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (service != null) {
                try {
                    val msg = Message.obtain(null, EIDReaderService.MSG_UNREGISTER_CLIENT)
                    msg.replyTo = mMessenger
                    service.send(msg)
                } catch (e: RemoteException) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection)
            mIsBound = false
        }
    }

    inner class EidReaderServiceConnection : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, binder: IBinder) {
            val service = Messenger(binder).also { mService = it }
            try {
                //Register client with service
                val msg = Message.obtain(null, EIDReaderService.MSG_REGISTER_CLIENT)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }

        //  *** This is the eid service disconnection
        override fun onServiceDisconnected(className: ComponentName) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            Log.i("Breeding", "At eid Service Disconnected.")
            mService = null
        }
    }

    internal inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                EIDReaderService.MSG_UPDATE_STATUS -> {
                    val b1 = msg.data
                    val state = b1.getString("stat")
                    if (state != null) {
                        eidReaderStatePresenter.updateReaderState(state)
                    }
                }

                EIDReaderService.MSG_NEW_EID_FOUND -> {
                    val bundle = msg.data
                    val eid = bundle.getString("info1")
                    // We have a good whole EID number
                    //TODO: Add format validation here.
                    if (eid != null) {
                        onEIDScanned(eid)
                    }
                }

                EIDReaderService.MSG_UPDATE_LOG_APPEND -> {}
                baacodeService.MSG_UPDATE_LOG_APPEND -> {
                    val b5 = msg.data
                }

                EIDReaderService.MSG_UPDATE_LOG_FULL -> {}
                EIDReaderService.MSG_THREAD_SUICIDE -> {
                    doUnbindService()
                    stopService(Intent(this@ManageAnimalIdsActivity, EIDReaderService::class.java))
                }

                else -> super.handleMessage(msg)
            }
        }
    }
}

private class ManageAnimalIdsViewModelFactory(context: Context) : ViewModelProvider.Factory {

    private val appContext = context.applicationContext

    override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
        return when (modelClass) {
            ManageAnimalIdsViewModel::class.java -> {
                val databaseHandler = DatabaseHandler.create(appContext)
                val defSettingsRepo = DefaultSettingsRepositoryImpl(databaseHandler)
                val activeDefaultSettings = ActiveDefaultSettings(
                    PreferenceManager.getDefaultSharedPreferences(appContext)
                )
                val loadActiveDefaultSettings = LoadActiveDefaultSettings(
                    activeDefaultSettings = activeDefaultSettings,
                    defaultSettingsRepo = defSettingsRepo
                )
                val animalRepo = AnimalRepositoryImpl(databaseHandler)
                @Suppress("UNCHECKED_CAST")
                ManageAnimalIdsViewModel(
                    animalRepo = animalRepo,
                    idValidations = IdValidations(animalRepo),
                    autoUpdateTrichId = AutoIncrementNextTrichIdFeature(
                        loadActiveDefaultSettings = loadActiveDefaultSettings,
                        defaultSettingsRepository = defSettingsRepo
                    )
                ) as T
            }
            else -> {
                throw IllegalStateException("${modelClass.simpleName} is not supported.")
            }
        }
    }
}

private class EditAnimalIdsAdapter(
    private val onEditId: (IdBasicInfo) -> Unit,
    private val onRemoveId: (IdBasicInfo) -> Unit
) : ListAdapter<IdBasicInfo, EditAnimalIdViewHolder>(
    IdBasicInfo.Differ
) {

    companion object {
        private const val VIEW_TYPE_NO_IDS = 0
        private const val VIEW_TYPE_EDIT_ID = 1
    }

    override fun getItemCount(): Int {
        return currentList.size.takeIf { 0 < it } ?: 1
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            currentList.isEmpty() -> VIEW_TYPE_NO_IDS
            else -> VIEW_TYPE_EDIT_ID
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EditAnimalIdViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_TYPE_NO_IDS -> NoAnimalIdsViewHolder(
                ViewNoAnimalIdsFoundBinding.inflate(layoutInflater, parent, false)
            )
            else -> AnimalIdViewHolder(
                ItemAnimalIdEditableBinding.inflate(layoutInflater, parent, false),
                onEditId,
                onRemoveId
            )
        }
    }

    override fun onBindViewHolder(holder: EditAnimalIdViewHolder, position: Int) {
        if (currentList.isNotEmpty()) {
            holder.takeAs<AnimalIdViewHolder>()?.bind(currentList[position])
        }
    }
}

private abstract class EditAnimalIdViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

private class NoAnimalIdsViewHolder(binding: ViewNoAnimalIdsFoundBinding) : EditAnimalIdViewHolder(binding.root)

private class AnimalIdViewHolder(
    private val binding: ItemAnimalIdEditableBinding,
    private val onEditId: (IdBasicInfo) -> Unit,
    private val onRemoveId: (IdBasicInfo) -> Unit
) : EditAnimalIdViewHolder(binding.root) {
    fun bind(item: IdBasicInfo) {
        binding.textIdTypeName.text = item.typeName
        binding.textIdNumber.text = item.number
        binding.textIdColorName.text = item.colorName
        binding.textIdLocationName.text = item.locationName
        with(binding.editControls) {
            buttonEditId.setOnClickListener { onEditId(item) }
            buttonRemoveId.setOnClickListener { onRemoveId(item) }
        }
    }
}
