package com.weyr_associates.animaltrakkerfarmmobile.app.label

import android.content.Context
import android.content.Intent

object PrintLabel {

    const val ACTION_ENCODE_LABEL_DATA = "weyr.LT.ENCODE"
    const val EXTRA_ENCODE_FORMAT = "ENCODE_FORMAT"
    const val EXTRA_ENCODE_SHOW_CONTENTS = "ENCODE_SHOW_CONTENTS"
    const val EXTRA_ENCODE_AUTOPRINT = "ENCODE_AUTOPRINT"
    const val EXTRA_ENCODE_SHEEPNAME = "ENCODE_SHEEPNAME"
    const val EXTRA_ENCODE_DATA = "ENCODE_DATA"
    const val EXTRA_ENCODE_DATA1 = "ENCODE_DATA1"
    const val EXTRA_ENCODE_DATE = "ENCODE_DATE"

    const val ENCODE_FORMAT_CODE_128 = "CODE_128"

    const val PREFS_KEY_PRINT_LABEL_TEXT = "label"
    const val DEFAULT_PRINT_LABEL_TEXT = "text"

    fun newIntentToEncode(printLabelData: PrintLabelData, autoPrint: Boolean): Intent {
        return Intent().apply {
            action = ACTION_ENCODE_LABEL_DATA
            addCategory(Intent.CATEGORY_DEFAULT)
            putExtra(EXTRA_ENCODE_FORMAT, ENCODE_FORMAT_CODE_128)
            putExtra(EXTRA_ENCODE_SHOW_CONTENTS, false)
            putExtra(EXTRA_ENCODE_AUTOPRINT, if (autoPrint) "true" else "false")
            with(PrintLabelFormatter(printLabelData)) {
                putExtra(EXTRA_ENCODE_DATA, formatData())
                putExtra(EXTRA_ENCODE_DATA1, formatData1())
                putExtra(EXTRA_ENCODE_SHEEPNAME, formatSheepName())
                putExtra(EXTRA_ENCODE_DATE, formatDateTime())
            }
        }
    }
}
