package com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation

import android.widget.EditText
import android.widget.RatingBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.weyr_associates.animaltrakkerfarmmobile.app.core.FragmentResultListenerRegistrar
import com.weyr_associates.animaltrakkerfarmmobile.app.core.asFragmentResultListenerRegistrar
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.coroutines.flow.observeOneTimeEvents
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.ItemSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.evalTraitOptionSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.optionalEvalTraitOptionSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ViewEvaluationEditorBinding
import com.weyr_associates.animaltrakkerfarmmobile.model.EvalTraitOption
import com.weyr_associates.animaltrakkerfarmmobile.model.UnitOfMeasure
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class EvaluationEditorPresenter(private val binding: ViewEvaluationEditorBinding) {

    companion object {
        private const val REQUEST_KEY_SELECT_TRAIT_16_OPTION = "REQUEST_KEY_SELECT_TRAIT_16_OPTION"
        private const val REQUEST_KEY_SELECT_TRAIT_17_OPTION = "REQUEST_KEY_SELECT_TRAIT_17_OPTION"
        private const val REQUEST_KEY_SELECT_TRAIT_18_OPTION = "REQUEST_KEY_SELECT_TRAIT_18_OPTION"
        private const val REQUEST_KEY_SELECT_TRAIT_19_OPTION = "REQUEST_KEY_SELECT_TRAIT_19_OPTION"
        private const val REQUEST_KEY_SELECT_TRAIT_20_OPTION = "REQUEST_KEY_SELECT_TRAIT_20_OPTION"
    }

    private var collectEventsJob: Job? = null
    private var trait16BindingJob: Job? = null
    private var trait17BindingJob: Job? = null
    private var trait18BindingJob: Job? = null
    private var trait19BindingJob: Job? = null
    private var trait20BindingJob: Job? = null

    private var trait16Presenter: ItemSelectionPresenter<EvalTraitOption>? = null
    private var trait17Presenter: ItemSelectionPresenter<EvalTraitOption>? = null
    private var trait18Presenter: ItemSelectionPresenter<EvalTraitOption>? = null
    private var trait19Presenter: ItemSelectionPresenter<EvalTraitOption>? = null
    private var trait20Presenter: ItemSelectionPresenter<EvalTraitOption>? = null

    fun bindToEditorIn(activity: AppCompatActivity, editor: EvaluationEditor) {
        bindToEditor(
            activity.asFragmentResultListenerRegistrar(),
            activity,
            activity.lifecycleScope,
            editor
        )
    }

    fun bindToEditorIn(fragment: Fragment, editor: EvaluationEditor) {
        bindToEditor(
            fragment.asFragmentResultListenerRegistrar(),
            fragment,
            fragment.lifecycleScope,
            editor
        )
    }

    fun unbind() {
        cancelAllJobs()
        dereferenceAllJobs()
        dereferenceAllPresenters()
    }

    private fun bindToEditor(
        fragmentResultListenerRegistrar: FragmentResultListenerRegistrar,
        lifecycleOwner: LifecycleOwner,
        lifecycleScope: LifecycleCoroutineScope,
        editor: EvaluationEditor
    ) {
        unbind()
        collectEvents(
            lifecycleOwner,
            lifecycleScope,
            editor
        )
        bindToOptionsTraits(
            fragmentResultListenerRegistrar,
            lifecycleOwner,
            lifecycleScope,
            editor
        )
        configureTraitDisplays(editor)
    }

    private fun collectEvents(
        lifecycleOwner: LifecycleOwner,
        lifecycleScope: LifecycleCoroutineScope,
        editor: EvaluationEditor
    ) {
        collectEventsJob?.cancel()
        collectEventsJob = lifecycleScope.launch {
            lifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                editor.events.observeOneTimeEvents {
                    handleEvent(editor, it)
                }
            }
        }
    }

    private fun bindToOptionsTraits(
        fragmentResultListenerRegistrar: FragmentResultListenerRegistrar,
        lifecycleOwner: LifecycleOwner,
        lifecycleScope: LifecycleCoroutineScope,
        editor: EvaluationEditor
    ) {
        trait16Presenter = optionalEvalTraitOptionSelectionPresenter(
            fragmentResultListenerRegistrar,
            { editor.trait16Options },
            button = binding.inputTrait16.spinnerEvalTraitOptions,
            requestKey = REQUEST_KEY_SELECT_TRAIT_16_OPTION
        ) {
            editor.setTrait16(it)
        }.also {
            trait16BindingJob?.cancel()
            it.bindToFlow(lifecycleOwner, lifecycleScope, editor.trait16)
        }

        trait17Presenter = optionalEvalTraitOptionSelectionPresenter(
            fragmentResultListenerRegistrar,
            { editor.trait17Options },
            button = binding.inputTrait17.spinnerEvalTraitOptions,
            requestKey = REQUEST_KEY_SELECT_TRAIT_17_OPTION
        ) {
            editor.setTrait17(it)
        }.also {
            trait17BindingJob?.cancel()
            it.bindToFlow(lifecycleOwner, lifecycleScope, editor.trait17)
        }

        trait18Presenter = optionalEvalTraitOptionSelectionPresenter(
            fragmentResultListenerRegistrar,
            { editor.trait18Options },
            button = binding.inputTrait18.spinnerEvalTraitOptions,
            requestKey = REQUEST_KEY_SELECT_TRAIT_18_OPTION
        ) {
            editor.setTrait18(it)
        }.also {
            trait18BindingJob?.cancel()
            it.bindToFlow(lifecycleOwner, lifecycleScope, editor.trait18)
        }

        trait19Presenter = optionalEvalTraitOptionSelectionPresenter(
            fragmentResultListenerRegistrar,
            { editor.trait19Options },
            button = binding.inputTrait19.spinnerEvalTraitOptions,
            requestKey = REQUEST_KEY_SELECT_TRAIT_19_OPTION
        ) {
            editor.setTrait19(it)
        }.also {
            trait19BindingJob?.cancel()
            it.bindToFlow(lifecycleOwner, lifecycleScope, editor.trait19)
        }

        trait20Presenter = optionalEvalTraitOptionSelectionPresenter(
            fragmentResultListenerRegistrar,
            { editor.trait20Options },
            button = binding.inputTrait20.spinnerEvalTraitOptions,
            requestKey = REQUEST_KEY_SELECT_TRAIT_20_OPTION
        ) {
            editor.setTrait20(it)
        }.also {
            trait20BindingJob?.cancel()
            it.bindToFlow(lifecycleOwner, lifecycleScope, editor.trait20)
        }
    }

    private fun handleEvent(editor: EvaluationEditor, event: EvaluationEditor.Event) {
        when (event) {
            EvaluationEditor.EvaluationLoaded -> {
                configureTraitDisplays(editor)
            }
            EvaluationEditor.FieldValuesCleared -> {
                handleFieldsCleared(editor)
            }
            is EvaluationEditor.FieldValueChanged -> {
                handleFieldValueChanged(editor, event.field)
            }
        }
    }

    private fun handleFieldValueChanged(editor: EvaluationEditor, field: EvaluationField) {
        when (field) {
            EvaluationField.TRAIT_01,
            EvaluationField.TRAIT_02,
            EvaluationField.TRAIT_03,
            EvaluationField.TRAIT_04,
            EvaluationField.TRAIT_05,
            EvaluationField.TRAIT_06,
            EvaluationField.TRAIT_07,
            EvaluationField.TRAIT_08,
            EvaluationField.TRAIT_09,
            EvaluationField.TRAIT_10,
            EvaluationField.TRAIT_11,
            EvaluationField.TRAIT_12,
            EvaluationField.TRAIT_13,
            EvaluationField.TRAIT_14,
            EvaluationField.TRAIT_15 -> {
                updateTraitValue(editor, field)
            }
            EvaluationField.TRAIT_16,
            EvaluationField.TRAIT_17,
            EvaluationField.TRAIT_18,
            EvaluationField.TRAIT_19,
            EvaluationField.TRAIT_20 -> Unit //NO-OP for options which are bound through Flows.
        }
    }

    private fun handleFieldsCleared(editor: EvaluationEditor) {
        updateTraitValue(editor, EvaluationField.TRAIT_01)
        updateTraitValue(editor, EvaluationField.TRAIT_02)
        updateTraitValue(editor, EvaluationField.TRAIT_03)
        updateTraitValue(editor, EvaluationField.TRAIT_04)
        updateTraitValue(editor, EvaluationField.TRAIT_05)
        updateTraitValue(editor, EvaluationField.TRAIT_06)
        updateTraitValue(editor, EvaluationField.TRAIT_07)
        updateTraitValue(editor, EvaluationField.TRAIT_08)
        updateTraitValue(editor, EvaluationField.TRAIT_09)
        updateTraitValue(editor, EvaluationField.TRAIT_10)
        updateTraitValue(editor, EvaluationField.TRAIT_11)
        updateTraitValue(editor, EvaluationField.TRAIT_12)
        updateTraitValue(editor, EvaluationField.TRAIT_13)
        updateTraitValue(editor, EvaluationField.TRAIT_14)
        updateTraitValue(editor, EvaluationField.TRAIT_15)
        //Options traits are bound to value flows, do not update here.
    }

    private fun configureTraitDisplays(editor: EvaluationEditor) {
        configureTraitVisibility(editor)
        configureTraitNames(editor)
        configureTraitUnits(editor)
        configureScoredTraitsHandlers(editor)
        configureUnitsTraitsHandlers(editor)
    }

    private fun configureTraitVisibility(editor: EvaluationEditor) {

        // Show traits only if we are collecting them.

        binding.inputTrait01.root.isGone = editor.trait01Entry.isUncollected
        binding.inputTrait02.root.isGone = editor.trait02Entry.isUncollected
        binding.inputTrait03.root.isGone = editor.trait03Entry.isUncollected
        binding.inputTrait04.root.isGone = editor.trait04Entry.isUncollected
        binding.inputTrait05.root.isGone = editor.trait05Entry.isUncollected
        binding.inputTrait06.root.isGone = editor.trait06Entry.isUncollected
        binding.inputTrait07.root.isGone = editor.trait07Entry.isUncollected
        binding.inputTrait08.root.isGone = editor.trait08Entry.isUncollected
        binding.inputTrait09.root.isGone = editor.trait09Entry.isUncollected
        binding.inputTrait10.root.isGone = editor.trait10Entry.isUncollected
        binding.inputTrait11.root.isGone = editor.trait11Entry.isUncollected
        binding.inputTrait12.root.isGone = editor.trait12Entry.isUncollected
        binding.inputTrait13.root.isGone = editor.trait13Entry.isUncollected
        binding.inputTrait14.root.isGone = editor.trait14Entry.isUncollected
        binding.inputTrait15.root.isGone = editor.trait15Entry.isUncollected
        binding.inputTrait16.root.isGone = editor.trait16Entry.isUncollected
        binding.inputTrait17.root.isGone = editor.trait17Entry.isUncollected
        binding.inputTrait18.root.isGone = editor.trait18Entry.isUncollected
        binding.inputTrait19.root.isGone = editor.trait19Entry.isUncollected
        binding.inputTrait20.root.isGone = editor.trait20Entry.isUncollected
    }

    private fun configureTraitNames(editor: EvaluationEditor) {
        binding.inputTrait01.textEvalTraitName.text = editor.trait01Name
        binding.inputTrait02.textEvalTraitName.text = editor.trait02Name
        binding.inputTrait03.textEvalTraitName.text = editor.trait03Name
        binding.inputTrait04.textEvalTraitName.text = editor.trait04Name
        binding.inputTrait05.textEvalTraitName.text = editor.trait05Name
        binding.inputTrait06.textEvalTraitName.text = editor.trait06Name
        binding.inputTrait07.textEvalTraitName.text = editor.trait07Name
        binding.inputTrait08.textEvalTraitName.text = editor.trait08Name
        binding.inputTrait09.textEvalTraitName.text = editor.trait09Name
        binding.inputTrait10.textEvalTraitName.text = editor.trait10Name
        binding.inputTrait11.textEvalTraitName.text = editor.trait11Name
        binding.inputTrait12.textEvalTraitName.text = editor.trait12Name
        binding.inputTrait13.textEvalTraitName.text = editor.trait13Name
        binding.inputTrait14.textEvalTraitName.text = editor.trait14Name
        binding.inputTrait15.textEvalTraitName.text = editor.trait15Name
        binding.inputTrait16.textEvalTraitName.text = editor.trait16Name
        binding.inputTrait17.textEvalTraitName.text = editor.trait17Name
        binding.inputTrait18.textEvalTraitName.text = editor.trait18Name
        binding.inputTrait19.textEvalTraitName.text = editor.trait19Name
        binding.inputTrait20.textEvalTraitName.text = editor.trait20Name
    }

    private fun configureTraitUnits(editor: EvaluationEditor) {
        binding.inputTrait11.textEvalTraitUnits.text = editor.trait11Units.abbreviation
        binding.inputTrait12.textEvalTraitUnits.text = editor.trait12Units.abbreviation
        binding.inputTrait13.textEvalTraitUnits.text = editor.trait13Units.abbreviation
        binding.inputTrait14.textEvalTraitUnits.text = editor.trait14Units.abbreviation
        binding.inputTrait15.textEvalTraitUnits.text = editor.trait15Units.abbreviation
    }

    private fun configureScoredTraitsHandlers(editor: EvaluationEditor) {
        binding.inputTrait01.ratingEvalTraitValue.setOnTraitScoreChangedListener { _, score ->
            editor.setTrait01(score)
        }
        binding.inputTrait02.ratingEvalTraitValue.setOnTraitScoreChangedListener { _, score ->
            editor.setTrait02(score)
        }
        binding.inputTrait03.ratingEvalTraitValue.setOnTraitScoreChangedListener { _, score ->
            editor.setTrait03(score)
        }
        binding.inputTrait04.ratingEvalTraitValue.setOnTraitScoreChangedListener { _, score ->
            editor.setTrait04(score)
        }
        binding.inputTrait05.ratingEvalTraitValue.setOnTraitScoreChangedListener { _, score ->
            editor.setTrait05(score)
        }
        binding.inputTrait06.ratingEvalTraitValue.setOnTraitScoreChangedListener { _, score ->
            editor.setTrait06(score)
        }
        binding.inputTrait07.ratingEvalTraitValue.setOnTraitScoreChangedListener { _, score ->
            editor.setTrait07(score)
        }
        binding.inputTrait08.ratingEvalTraitValue.setOnTraitScoreChangedListener { _, score ->
            editor.setTrait08(score)
        }
        binding.inputTrait09.ratingEvalTraitValue.setOnTraitScoreChangedListener { _, score ->
            editor.setTrait09(score)
        }
        binding.inputTrait10.ratingEvalTraitValue.setOnTraitScoreChangedListener { _, score ->
            editor.setTrait10(score)
        }
    }

    private fun configureUnitsTraitsHandlers(editor: EvaluationEditor) {
        binding.inputTrait11.inputEvalTraitValue.addTextChangedListener {
            editor.setTrait11(it.toString().toFloatOrNull())
        }
        binding.inputTrait12.inputEvalTraitValue.addTextChangedListener {
            editor.setTrait12(it.toString().toFloatOrNull())
        }
        binding.inputTrait13.inputEvalTraitValue.addTextChangedListener {
            editor.setTrait13(it.toString().toFloatOrNull())
        }
        binding.inputTrait14.inputEvalTraitValue.addTextChangedListener {
            editor.setTrait14(it.toString().toFloatOrNull())
        }
        binding.inputTrait15.inputEvalTraitValue.addTextChangedListener {
            editor.setTrait15(it.toString().toFloatOrNull())
        }
    }

    private fun updateTraitValue(editor: EvaluationEditor, field: EvaluationField) {
        when (field) {
            EvaluationField.TRAIT_01 -> updateScoredTrait(
                binding.inputTrait01.ratingEvalTraitValue, editor.trait01.value
            )
            EvaluationField.TRAIT_02 -> updateScoredTrait(
                binding.inputTrait02.ratingEvalTraitValue, editor.trait02.value
            )
            EvaluationField.TRAIT_03 -> updateScoredTrait(
                binding.inputTrait03.ratingEvalTraitValue, editor.trait03.value
            )
            EvaluationField.TRAIT_04 -> updateScoredTrait(
                binding.inputTrait04.ratingEvalTraitValue, editor.trait04.value
            )
            EvaluationField.TRAIT_05 -> updateScoredTrait(
                binding.inputTrait05.ratingEvalTraitValue, editor.trait05.value
            )
            EvaluationField.TRAIT_06 -> updateScoredTrait(
                binding.inputTrait06.ratingEvalTraitValue, editor.trait06.value
            )
            EvaluationField.TRAIT_07 -> updateScoredTrait(
                binding.inputTrait07.ratingEvalTraitValue, editor.trait07.value
            )
            EvaluationField.TRAIT_08 -> updateScoredTrait(
                binding.inputTrait08.ratingEvalTraitValue, editor.trait08.value
            )
            EvaluationField.TRAIT_09 -> updateScoredTrait(
                binding.inputTrait09.ratingEvalTraitValue, editor.trait09.value
            )
            EvaluationField.TRAIT_10 -> updateScoredTrait(
                binding.inputTrait10.ratingEvalTraitValue, editor.trait10.value
            )
            EvaluationField.TRAIT_11 -> updateUnitsTrait(
                binding.inputTrait11.inputEvalTraitValue,
                editor.trait11.value,
                editor.trait11Units
            )
            EvaluationField.TRAIT_12 -> updateUnitsTrait(
                binding.inputTrait12.inputEvalTraitValue,
                editor.trait12.value,
                editor.trait12Units
            )
            EvaluationField.TRAIT_13 -> updateUnitsTrait(
                binding.inputTrait13.inputEvalTraitValue,
                editor.trait13.value,
                editor.trait13Units
            )
            EvaluationField.TRAIT_14 -> updateUnitsTrait(
                binding.inputTrait14.inputEvalTraitValue,
                editor.trait14.value,
                editor.trait14Units
            )
            EvaluationField.TRAIT_15 -> updateUnitsTrait(
                binding.inputTrait15.inputEvalTraitValue,
                editor.trait15.value,
                editor.trait15Units
            )
            EvaluationField.TRAIT_16 -> updateOptionsTrait(
                trait16Presenter,
                editor.trait16.value
            )
            EvaluationField.TRAIT_17 -> updateOptionsTrait(
                trait17Presenter,
                editor.trait17.value
            )
            EvaluationField.TRAIT_18 -> updateOptionsTrait(
                trait18Presenter,
                editor.trait18.value
            )
            EvaluationField.TRAIT_19 -> updateOptionsTrait(
                trait19Presenter,
                editor.trait19.value
            )
            EvaluationField.TRAIT_20 -> updateOptionsTrait(
                trait20Presenter,
                editor.trait20.value
            )
        }
    }

    private fun updateScoredTrait(ratingBar: RatingBar, score: Int) {
        ratingBar.rating = score.toFloat()
    }

    private fun updateUnitsTrait(input: EditText, value: Float?, units: UnitOfMeasure) {
        input.setText(value?.toString() ?: "") //TODO: Use units if necessary for Float -> String conversion.
    }

    private fun updateOptionsTrait(
        presenter: ItemSelectionPresenter<EvalTraitOption>?,
        option: EvalTraitOption?
    ) {
        presenter?.displaySelectedItem(option)
    }

    private fun cancelAllJobs() {
        listOf(
            collectEventsJob,
            trait16BindingJob,
            trait17BindingJob,
            trait18BindingJob,
            trait19BindingJob,
            trait20BindingJob,
        ).forEach { it?.cancel() }
    }

    private fun dereferenceAllJobs() {
        collectEventsJob = null
        trait16BindingJob = null
        trait17BindingJob = null
        trait18BindingJob = null
        trait19BindingJob = null
        trait20BindingJob = null
    }

    private fun dereferenceAllPresenters() {
        trait16Presenter = null
        trait17Presenter = null
        trait18Presenter = null
        trait19Presenter = null
        trait20Presenter = null
    }

    private fun RatingBar.setOnTraitScoreChangedListener(listener: (RatingBar?, Int) -> Unit) {
        setOnRatingBarChangeListener { ratingBar, value, fromUser ->
            if (fromUser) {
                listener(ratingBar, value.toInt())
            }
        }
    }
}
