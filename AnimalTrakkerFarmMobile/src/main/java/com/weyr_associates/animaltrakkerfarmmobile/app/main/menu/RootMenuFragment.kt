package com.weyr_associates.animaltrakkerfarmmobile.app.main.menu

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.LegacyAddAnimalActivity
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.ScanAndPrintActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.core.checkDatabaseIsPresentThen
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.DatabaseManagementActivity
import com.weyr_associates.animaltrakkerfarmmobile.databinding.FragmentMenuRootBinding

class RootMenuFragment : Fragment(R.layout.fragment_menu_root) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(FragmentMenuRootBinding.bind(view)) {
            btnScanPrint.setOnClickListener {
                checkDatabaseIsPresentThen {
                    startActivity(Intent(requireActivity(), ScanAndPrintActivity::class.java))
                }
            }
            btnManageAnimals.setOnClickListener {
                findNavController().navigate(R.id.nav_dst_menu_manage_animals)
            }
            btnAnimalEvaluation.setOnClickListener {
                findNavController().navigate(R.id.nav_dst_menu_animal_evaluation)
            }
            btnAnimalCare.setOnClickListener {
                findNavController().navigate(R.id.nav_dst_menu_animal_care)
            }
            btnBirthing.setOnClickListener {
                findNavController().navigate(R.id.nav_dst_menu_birthing)
            }
            btnAnimalMovements.setOnClickListener {
                findNavController().navigate(R.id.nav_dst_menu_animal_movements)
            }
            btnAnimalHistory.setOnClickListener {
                checkDatabaseIsPresentThen {
                    startActivity(Intent(requireActivity(), LegacyAddAnimalActivity::class.java))
                }
            }
            btnSetup.setOnClickListener {
                findNavController().navigate(R.id.nav_dst_menu_setup)
            }
            btnManageDatabase.setOnClickListener {
                startActivity(Intent(requireActivity(), DatabaseManagementActivity::class.java))
            }
            btnQuitApp.setOnClickListener {
                requireActivity().finishAffinity()
            }

            //TODO: Enable these menu options as they become available.
            listOf(
                btnBirthing,
                btnAnimalCare,
                btnAnimalMovements,
                btnAnimalHistory,
            ).forEach { it.deactivate() }
        }
    }
}
