package com.weyr_associates.animaltrakkerfarmmobile;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.weyr_associates.animaltrakkerfarmmobile.app.main.MainActivity;
import com.weyr_associates.animaltrakkerfarmmobile.database.DefaultSettingsTable;
import com.weyr_associates.animaltrakkerfarmmobile.databinding.RaceReaderBinding;
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderService;
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderStatePresenter;

import java.util.Map;

import kotlin.Unit;

public class Counter extends AppCompatActivity {

    public int numAnimals = 0;

    public Map<String,Integer> animaltrakker_defaults;

    public String LastEID ;
    public String  FileName;

    Messenger mService = null;
    boolean mIsBound;

    final Messenger mMessenger = new Messenger(new Counter.IncomingHandler());

    private final BluetoothWatcher bluetoothWatcher = new BluetoothWatcher(this);

    private RaceReaderBinding binding;
    private EIDReaderStatePresenter eidReaderStatePresenter;

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case EIDReaderService.MSG_UPDATE_STATUS:
                    Bundle b1 = msg.getData();
                    String State;
                    State = (b1.getString("stat"));
                    if (State != null) {
                        eidReaderStatePresenter.updateReaderState(State);
                    }
                    break;
                case EIDReaderService.MSG_NEW_EID_FOUND:
                    Bundle b2 = msg.getData();

                    LastEID = (b2.getString("info1"));
                    FileName = (b2.getString("stat"));
                    //We have a good whole EID number
         //           Log.i ("Counter" , "got eid of " + LastEID);
                    gotEID ();
                    break;
                case EIDReaderService.MSG_UPDATE_LOG_APPEND:
                    //Bundle b3 = msg.getData();
                    //Log.i("Counter", "Add to Log.");
                    break;
                case EIDReaderService.MSG_UPDATE_LOG_FULL:
                    //Log.i("Counter", "Log Full.");
                    break;
                case EIDReaderService.MSG_THREAD_SUICIDE:
                    Log.i("Counter", "Service informed Activity of Suicide.");
                    doUnbindService();
                    stopService(new Intent(Counter.this, EIDReaderService.class));
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    public ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);
            Log.i("Counter", "At Service Connected");
            try {
                //Register client with service
                Message msg = Message.obtain(null, EIDReaderService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);

            } catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            mService = null;
        }
    };

    private void CheckIfServiceIsRunning() {
        //If the service is running when the activity starts, we want to automatically bind to it.
        Log.i("Counter", "At isRunning?.");
        if (EIDReaderService.isRunning()) {
            //Log.i("Counter", "is.");
            doBindService();
        } else {
            //Log.i("Counter", "is not, start it");
            startService(new Intent(Counter.this, EIDReaderService.class));
            doBindService();
        }

    }

    void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        //Log.i("Counter", "At doBind1.");
        bindService(new Intent(this, EIDReaderService.class), mConnection, Context.BIND_AUTO_CREATE);
        //Log.i("Counter", "At doBind2.");

        mIsBound = true;

        if (mService != null) {
            //Log.i("Counter", "At doBind3.");
            try {
                //Request reload preferences
                Message msg = Message.obtain(null, EIDReaderService.MSG_RELOAD_PREFERENCES);
                msg.replyTo = mMessenger;
                mService.send(msg);
                //Request status update
                msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_STATUS, 0, 0);
                msg.replyTo = mMessenger;
                mService.send(msg);
       //         Log.i("Counter", "At doBind4.");
                //Request full log from service.
                msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_LOG_FULL, 0, 0);
                mService.send(msg);
            } catch (RemoteException e) { /* NO-OP */ }
        }
    }

    void doUnbindService() {
        //Log.i("Counter", "At DoUnbindservice");
        if (mService != null) {
            try {
                //Stop eidService from sending tags
                Message msg = Message.obtain(null, EIDReaderService.MSG_NO_COUNTER_TAGS_PLEASE);
                msg.replyTo = mMessenger;
                mService.send(msg);

            } catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        if (mIsBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, EIDReaderService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection);
            mIsBound = false;
        }
    }

    public void gotEID( ) {

        binding.tagNumberEID.setText(LastEID);
        Log.i("Counter", " with LastEID of " + LastEID);
        numAnimals = numAnimals + 1;
        binding.numAnimals.setText(Integer.toString(numAnimals));
        binding.fileName.setText(FileName);

        //  Set Stop Counter button Green
        binding.btnStopCounter.setEnabled(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = RaceReaderBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Log.i("Counter", " after set content view");

        // Get the defaults and fill the defaults list for use later.
        animaltrakker_defaults = DefaultSettingsTable.readAsMap(this);

        CheckIfServiceIsRunning();
        Log.i("Counter", "back from isRunning");

        Button btn = binding.btnStartCounter;
        btn.setOnClickListener(this::startCounter);

        btn = binding.btnStopCounter;
        btn.setOnClickListener(this::stopCounter);
        btn.setEnabled(false);

        eidReaderStatePresenter = new EIDReaderStatePresenter(this, binding.imageScannerStatus);

        bluetoothWatcher.setOnActivationChanged(enabled -> {
            onBluetoothActivationChanged();
            return Unit.INSTANCE;
        });
    }

    //  user clicked 'Start Counter' button
    private void startCounter(View v){
        // Here is where scanning starts and tags scanned are put the data into the variable LastEID
        if (mService != null) {
            try {
                //Request reload preferences
                Message msg = Message.obtain(null, EIDReaderService.MSG_RELOAD_PREFERENCES);
                msg.replyTo = mMessenger;
                mService.send(msg);
                //Start eidService sending tags
                msg = Message.obtain(null, EIDReaderService.MSG_SEND_ME_COUNTER_TAGS);
                msg.replyTo = mMessenger;
                mService.send(msg);
                numAnimals = 0;
                binding.numAnimals.setText(Integer.toString(numAnimals));
                binding.fileName.setText("");
                binding.tagNumberEID.setText("");
                //	make the buttons green
                binding.btnStartCounter.setText(R.string.btn_reset_counter);
                binding.btnStopCounter.setEnabled(true);
            } catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
    }

    //  user clicked 'Stop Counter' button
    private void stopCounter(View v) {
        if (mService != null) {
            try {
                //Stop eidService sending tags
                Message msg = Message.obtain(null, EIDReaderService.MSG_NO_COUNTER_TAGS_PLEASE);
                msg.replyTo = mMessenger;
                mService.send(msg);
                binding.btnStartCounter.setText(R.string.btn_start_counter);
                binding.btnStopCounter.setEnabled(false);
            } catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        Log.i("Counter", " Stop Counter" );
    }

    @Override
    public void onResume (){
        super.onResume();
        Log.i("Counter", " OnResume");
        if (checkRequiredPermissions()) {
            CheckIfServiceIsRunning();
            Log.i("Counter", " OnResume after Check ");
        }
        bluetoothWatcher.startWatching();
    }

    @Override
    protected void onPause() {
        super.onPause();
        bluetoothWatcher.stopWatching();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode==KeyEvent.KEYCODE_BACK && event.getRepeatCount()==0){
            Log.i("Counter", "Back clicked ");
            finish();
        }
        return false;
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        doUnbindService();
    }

    private boolean checkRequiredPermissions() {
        if (!RequiredPermissions.areFulfilled(this)) {
            MainActivity.returnFrom(this);
            return false;
        }
        return true;
    }

    private void onBluetoothActivationChanged() {
        checkRequiredPermissions();
    }
}
