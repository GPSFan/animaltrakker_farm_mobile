package com.weyr_associates.animaltrakkerfarmmobile;

public class Coalesce {
    private Coalesce() {}

    public static int value(Integer value, int defaultValue) {
        return (value != null) ? value : defaultValue;
    }
}
