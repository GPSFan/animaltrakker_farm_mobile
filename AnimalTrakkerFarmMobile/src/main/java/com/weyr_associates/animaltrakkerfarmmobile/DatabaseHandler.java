package com.weyr_associates.animaltrakkerfarmmobile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.weyr_associates.animaltrakkerfarmmobile.database.Cursors;
import com.weyr_associates.animaltrakkerfarmmobile.database.DefaultSettingsTable;
import com.weyr_associates.animaltrakkerfarmmobile.database.Sql;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.StringTokenizer;

/**
 * This class provides a set of methods to facilitate using the
 * SQLite database under Android. 
 * @author ww
 */
@SuppressLint("DefaultLocale")

public class DatabaseHandler extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 5;
    
    // active table
    private String activeTable = null;
    
    // build table sql
    private String buildTable = null;
    
    // the CSV file to be used
    private String theCSVFile = null;
    
    // the context
    private Context context;
    
    // cursor associated with last exec() call
    private Cursor currentCursor;
    
    // the active database
    private SQLiteDatabase db = null;

	//TODO: There is likely a more performant check to execute here.
	private static final String DB_PRESENCE_CHECK_SQL = "SELECT * FROM animaltrakker_default_settings_table";

	/**
     * Constructor for the DatabaseHandler
     * @param context the context of the caller (usually 'this')
     * @param dbName  the name of the database to open
	 *
     */
    public DatabaseHandler(Context context, String dbName) {
        super(context, dbName, null, DATABASE_VERSION);
        this.context = context;
	}
    
    /**
     * Required by Android, not used in this class
     * @param db a SQLite database object
     */
	@Override
    public void onCreate(SQLiteDatabase db) {
	}

	public static boolean isDatabasePresent(Context context) {
		try (DatabaseHandler dbh = new DatabaseHandler(context, getDatabaseName(context))) {
			try { dbh.exec(DB_PRESENCE_CHECK_SQL); }
			catch (Exception ex) { return false; }
			return true;
		}
	}

	public static DatabaseHandler create(Context context) {
		return new DatabaseHandler(context, getDatabaseName(context));
	}

	public static String getDatabaseName(Context context) {
		//TODO: move this string out of resources to a constant.
		return context.getString(R.string.real_database_file);
	}

	public long queryUserVersion() {
		try (Cursor cursor = getReadableDatabase().rawQuery(Sql.QUERY_SQLITE_USER_VERSION, new String[]{})) {
			return (cursor.moveToFirst()) ? cursor.getLong(0) : -1;
		}
	}

	public int querySerialNumber(long activeDefaultsId) {
		try (Cursor cursor = getReadableDatabase().rawQuery(
				Sql.QUERY_ATRKKR_SERIAL_NUMBER, new String[] { String.valueOf(activeDefaultsId) })) {
			return (cursor.moveToFirst()) ? Cursors.getInt(cursor, DefaultSettingsTable.Columns.USER_SYSTEM_SERIAL_NUMBER) : 0;
		}
	}

    /**
     * Create a table in the database from a CSV file. 
     * @param tableName the name of the table to create
     * @param createTableSQL the SQL required to actually *create* the table
     * @param csvFile the name of the file to be used as the CSV source (in 'assets/')
     * @return int (number of records in table)
     */
    public int createTable( String tableName, String createTableSQL, String csvFile )
        {
        String line;
        
        activeTable = tableName;
        buildTable  = createTableSQL;
        theCSVFile  = csvFile;
//        Log.i("in DBH", " createTable the active table is: " + activeTable);
//        Log.i("in DBH", " createTable the SQL command is: " +buildTable);
//        Log.i("in DBH", " createTable the CSV file is: " + theCSVFile);
//        Log.i("in DBH", " createTable the database is: " + String.valueOf(db));
        if( db == null ){
            db = this.getWritableDatabase();
        }
        Log.i("in DBH ", "the writable database is: " + String.valueOf(db));
        if( db.isOpen()){
        	db.close();
        }
        db.execSQL( "drop table if exists " + tableName );
        Log.i("DBH ", "Before the create table command");
        db.execSQL( createTableSQL );
        
        // load from .csv file in assets/
        if( csvFile != null )
            {
        	try {
	            InputStream       is     = context.getAssets().open( csvFile );
	            InputStreamReader isr    = new InputStreamReader( is );
	            BufferedReader    in     = new BufferedReader(isr);
	            String            header = in.readLine();
	            String            cmd    = String.format( "insert into %s(%s) ", tableName, header );
	            
	            try {
	                while(  (line = in.readLine()) != null )
	                    {
	                    if( line.length() == 0 )
	                        continue;
	                    
	                    StringBuilder sb = new StringBuilder();
	                    
	                    // get the data
	                    String[] values = line.split( "," );
	                    
	                    sb.append( "values(" );
	                    
	                    for( int i = 0; i < values.length; i++ )
	                        {
	                        sb.append( "'" );
	                        sb.append( values[i] );
	                        sb.append( "'," );
	                        }
	                    
	                    String vals = sb.toString();
	                    int lastIx  = vals.lastIndexOf( "," );
	                    vals        = vals.substring( 0, lastIx );
	                    String sql  = cmd + vals + ")";
	                    db.execSQL( sql );
	                    }
	                }
	            
	            finally
	                {
	                in.close();
	                }
        		}
        	
        	catch( Exception ex )
        		{
        		Log.e( "DatabaseHandler", ex.getMessage() );
        		return -1;
        		}
        	}
        
        String cmd = String.format( "select count(*) from %s", tableName );
        Cursor crsr = (Cursor)exec( cmd );
        crsr.moveToFirst();
        return crsr.getInt( 0 );
        }

    /**
     * Execute an SQL command. There are two cases:
     *     1) a 'select' (which should create a result set of data)
     *     2) all other SQL commands (e.g. delete, update, drop, alter, ...)
     * @param sqlStmt the SQL statement to be executed
     * @return a Cursor object in the case of 'select'; an Integer otherwise
     */
    @SuppressLint("UseValueOf")
	public Object exec( String sqlStmt )
        {
    	StringTokenizer tok = new StringTokenizer(sqlStmt, " " );
    	String          cmd = tok.nextToken();
        
 //   	Log.i("DBH Exec", "token is: " + tok);
        if( db == null )
            db = this.getWritableDatabase();
        
        if( cmd.toLowerCase().equals("select") )
            {
            currentCursor = db.rawQuery( sqlStmt, null );
            return currentCursor;
            }
        
        else    // non-select commands (do not return any 'data' but a count)
            {
            db.execSQL( sqlStmt );
            Cursor crsr = db.rawQuery( "select changes()", null );
            crsr.moveToFirst();
            int nr        = crsr.getInt( 0 );
            currentCursor = null;
            return new Integer( nr );
            }
        }
    
    /**
     * Gets the names of the columns, in the same order that they were defined
     * for the table.
     * @param tableName the name of the table
     * @return array of table column names
     */
    public String[] getColumnNames( String tableName )
    	{
        if( db == null )
            db = this.getWritableDatabase();
        
    	String cmd  = String.format( "select * from %s limit 1", tableName );
        Cursor crsr = db.rawQuery( cmd, null );
        return crsr.getColumnNames();
        }
    
    /**
     *
     * @return
     */
    public String[] getColumnNames()
    {
    	if( currentCursor != null )
    		return currentCursor.getColumnNames();
    	
    	throw new NullPointerException( "getColumnNames: No cursor from last exec()" );

    }
    /**
     *
     * @param colIndex
     * @return
     */
    public int getInt( int colIndex )
    	{
    	if( currentCursor != null )
    		return currentCursor.getInt(colIndex);
    	
    	throw new NullPointerException( "getInt: No cursor from last exec()" );
    	}
    
    /**
     *
     * @param colName
     * @return
     */
    public int getInt( String colName )
    	{
    	if( currentCursor != null )
    		return currentCursor.getInt( colIndexFromName(colName) );
    	
    	throw new NullPointerException( "getInt: No cursor from last exec()" );
    	}
    
    /**
     *
     * @param colIndex
     * @return
     */
    public String getStr( int colIndex )
    {
    	if( currentCursor != null ) {
    		String workingString = currentCursor.getString(colIndex);
    		
    		// Don't return null string pointers, return empty strings instead.
    		if (workingString == null)
    			workingString = "";
    		
    		return workingString;
    	}
    	throw new NullPointerException( "getStr: No cursor from last exec()" );
    	}
    	
    
    /**
     *
     * @param colName
     * @return
     */
    public String getStr( String colName  )
    	{
    	if( currentCursor != null ) {
    		String workingString = currentCursor.getString( colIndexFromName(colName) );
    		
    		// Don't return null string pointers, return empty strings instead.
    		if (workingString == null)
    			workingString = "";
    		
    		return workingString;
    	}
    	throw new NullPointerException( "getStr: No cursor from last exec()" );
    	}
    
    /**
     *
     * @param colIndex
     * @return
     */
    public float getReal( int colIndex )
    	{
    	if( currentCursor != null )
    		return currentCursor.getFloat(colIndex);
    	
    	throw new NullPointerException( "getReal: No cursor from last exec()" );
    	}
    
    /**
     *
     * @param colName
     * @return
     */
    public float getReal( String colName )
    	{
    	if( currentCursor != null )
    		return currentCursor.getFloat( colIndexFromName(colName) );
    	
    	throw new NullPointerException( "getReal: No cursor from last exec()" );
    	}
    
    /**
     *
     * @return
     */
    public int getSize()
    	{
    	if( currentCursor != null )
    		return currentCursor.getCount();
    	
    	throw new NullPointerException( "getSize: No cursor from last exec()" );
    	}
    
    /**
     *
     * @return
     */
    public int getNrcols()
    	{
    	if( currentCursor != null )
    		return currentCursor.getColumnCount();
    	
    	throw new NullPointerException( "getNrCols: No cursor from last exec()" );

    	}
    
    /**
     *
     * @param name
     * @return
     */
    public int colIndexFromName( String name )
    {
    	if( currentCursor != null )
    		return currentCursor.getColumnIndex( name );
    	
    	throw new NullPointerException( "colIndexFromName: No cursor from last exec()" );
    }
    
    /**
     *
     * @return
     */
    public Cursor getCursor()
    	{
    	return currentCursor;
    	}
    
    /**
     *
     * @return
     */
    public boolean advanceCursor()
    	{
    	if( currentCursor != null )
    		return currentCursor.moveToNext();
    	
    	throw new NullPointerException( "advanceCursor: No cursor from last exec()" );
    	}
    
    /**
     *
     * @return
     */
    public boolean moveToFirstRecord()
    	{
    	if( currentCursor != null )
    		return currentCursor.moveToFirst();
    	
    	throw new NullPointerException( "moveToFirstRecord: No cursor from last exec()" );
    	}
    
    /**
     *
     * @param fileName
     * @return
     */
    public boolean backup(String fileName )
    	{
    	int            length;
    	ContextWrapper ctxWrapper = new ContextWrapper( context );
    	
        // Local database
        if( db == null )
            db = this.getWritableDatabase();
        
    	String  from  = db.getPath();
        String  dPath = (ctxWrapper.getDir( "data", 'w' )).getPath();	// has terminating '/' ?
        Log.d( "DatabaseHandler", "dPath=" + dPath );
        
        try {
            InputStream input = new FileInputStream( from );
            
            // Path to the external backup
//            OutputStream output = new FileOutputStream( dPath + "/" + fileName );	// guessing no terminating '/'
			OutputStream output = new FileOutputStream( dPath + fileName );
            // transfer bytes from the Input File to the Output File
            byte[] buffer = new byte[1024];

            while( (length = input.read(buffer)) > 0 )
            	{
                output.write(buffer, 0, length);
            	}

            output.flush();
            output.close();
            input.close();        	
        	}
        
        catch( FileNotFoundException fnf )
        	{
        	Log.e("DatabaseHandler", "File not found: " + fileName );
        	Log.e("DatabaseHandler", "  " + fnf.getLocalizedMessage() );
        	return false;
        	}
        
        catch( IOException ioe )
        	{
        	Log.e("DatabaseHandler", "I/O error: " + ioe.getLocalizedMessage() );
        	return false;
        	}
        
        return true;
    	}

    public boolean restore()
    	{
    	// bring up file selection dialog
    	// get path to selected file
    	// get path to database location
    	// close the active db if necessary
    	// rename the target db file to something (e.g. currendb.ren)
    	// copy backup to database location
    	// rename backup file to name of original db file
    	// if copy succeeded delete the renamed database file, reopen a connection, return true
    	// if copy failed restore renamed db file to original name and return false
    	return false;
    	}
    

    public String dumpTable( String tableName )
    	{
    	String[] cols = getColumnNames( tableName );
    	
    	if( cols == null || cols.length == 0 )
    		{
    		return "No column names for '" + tableName + "'";
    		}
    	
    	StringBuilder sb   = new StringBuilder();
    	String        cmd  = String.format( "select * from %s", tableName );
    	Cursor        crsr = (Cursor) exec( cmd );
    	int           n    = cols.length;
    	int           nRec = 0;
    	
    	crsr.moveToFirst();
    	
    	do  {
    		nRec += 1;
    		String line = String.format( "Record %d:\n", nRec );
			sb.append( line );
			Log.d( "DatabaseHandler", line );
    		
    		for( int i = 0; i < n; i++ )
    			{
    			line = String.format( "  %s: %s\n", cols[i], crsr.getString(i) );
    			sb.append( line );
    			Log.d( "DatabaseHandler", line );
    			}
    		} while( crsr.moveToNext() );
    	
    	return sb.toString();
    	}

    public void onUpgrade( SQLiteDatabase db, int oldVersion, int newVersion )
    	{
        Log.w( "DatabaseHandler", "Upgrading database from version " + oldVersion + " to " + 
        		newVersion + " (destroys all current data)" );

        // rebuild the table
        if( activeTable != null &&
        	buildTable  != null &&
        	theCSVFile  != null )
        	createTable( activeTable, buildTable, theCSVFile );
    	}
    
    /**
     *
     * @param tblName
     * @param tblSQL
     * @param tblCSV
     */
    public void setTableInfo( String tblName, String tblSQL, String tblCSV )
    	{
    	// use this before doing an 'onUpgrade'
    	activeTable = tblName;
    	buildTable  = tblSQL;
    	theCSVFile  = tblCSV;
    	}

    public void closeDB()
        {
        if( db == null )
            return;
        
        db.close();
        db = null;
        }

    public String fixApostrophes( String src )
    	{
    	return src.replaceAll( "'", "''" );
    	}
    
    public void copyRealDataBase(String dbName) throws IOException
    {
    	if( db == null ){
            db = this.getWritableDatabase();
        }
    	Log.i("DBH ", "filename= " + dbName);
        InputStream myInput = context.getAssets().open(dbName);
        // Path to the just created empty db
		// TODO: 12/30/21 Figure out why the string reference doesn’t work in this instance
		String outFileName = "/data/data/com.weyr_associates.animaltrakkermobile/databases/" + "animaltrakker_db.sqlite";
//		String outFileName = getString(R.string.database_path) + getString(R.string.real_database_file);
         //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);
         //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        length = 0;
         while ((length = myInput.read(buffer))>0)
        {
            myOutput.write(buffer, 0, length);
        }
        //Close the streams
         
        myOutput.flush();
        myOutput.close();
        myInput.close();
        closeDB();
    }
    
    /////////////////////
    public void copy(File src, File dst) throws IOException {
    	
	    	Log.i("Copy From", " source file " + src);
	    	Log.i("Copy To", " destination file " + dst);
	    	if( db == null ){
				Log.i("inDBH", " in the if db == null section before get writable db ");
	            db = this.getWritableDatabase();
	        }
			Log.i("inDBH", " should have a writable DB now ");
	    	InputStream in;
	    	OutputStream out;
			try {
		        in = new FileInputStream(src);
			} catch (FileNotFoundException e) {
				Log.i("DBH", "Input database file not found " + src);
				return;
			}
			try {
			    out = new FileOutputStream(dst);
			} catch (FileNotFoundException e) {
				Log.i("DBH", "Output database file not found " + dst);
				in.close();
				return;
			}
			Utilities.copy(in, out);
			closeDB();
		}

		public void copy(InputStream inputStream, File dst) throws IOException {

			Log.i("Copy From", " input stream");
			Log.i("Copy To", " destination file " + dst);
			if( db == null ){
				Log.i("inDBH", " in the if db == null section before get writable db ");
				db = this.getWritableDatabase();
			}
			Log.i("inDBH", " should have a writable DB now ");
			OutputStream out;
			try {
				out = new FileOutputStream(dst);
			} catch (FileNotFoundException e) {
				Log.i("DBH", "Output database file not found " + dst);
				inputStream.close();
				return;
			}
			Utilities.copy(inputStream, out);
			closeDB();
		}
	}
