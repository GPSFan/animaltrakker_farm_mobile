package com.weyr_associates.animaltrakkerfarmmobile.eid

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import java.lang.ref.WeakReference

class EIDReaderStatePresenter(private val context: Context, view: View? = null) {

    private var _view: WeakReference<View?> = WeakReference(view)
    private val colorCache = mutableMapOf<String, ColorStateList>()
    private val iconCache = mutableMapOf<String, Drawable>()

    private var _state: String = EIDReaderService.STATE_STRING_NONE
        set(value) {
            if (value != field) {
                field = value
                updateDisplay()
            }
        }

    init {
        updateDisplay()
    }

    var view: View?
        get() = _view.get()
        set(value) {
            _view = WeakReference(value)
            updateDisplay()
        }

    fun updateReaderState(state: String) {
        _state = when (state) {
            EIDReaderService.STATE_STRING_NONE,
            EIDReaderService.STATE_STRING_LISTEN,
            EIDReaderService.STATE_STRING_CONNECTING,
            EIDReaderService.STATE_STRING_CONNECTED,
            EIDReaderService.STATE_STRING_SCANNING -> {
                state
            }
            else -> {
                EIDReaderService.STATE_STRING_NONE
            }
        }
    }

    private fun updateDisplay() {
        val view = _view.get() ?: return
        view.backgroundTintList = colorCache.computeIfAbsent(_state) { state ->
            ColorStateList.valueOf(
                ContextCompat.getColor(
                    context,
                    EIDReaderState.colorForState(state)
                )
            )
        }
        if (view is ImageView) {
            view.setImageDrawable(
                iconCache.computeIfAbsent(_state) { state ->
                    requireNotNull(
                        EIDReaderState.iconForState(state).let {
                            AppCompatResources.getDrawable(context, it)
                        }
                    )
                }
            )
        }
    }
}
