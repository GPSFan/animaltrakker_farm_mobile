package com.weyr_associates.animaltrakkerfarmmobile.eid;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import android.media.AudioManager;
import android.media.ToneGenerator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.HashSet;
import java.util.LinkedList;


import com.handheldgroup.serialport.SerialPort;
import com.weyr_associates.animaltrakkerfarmmobile.AppDirectories;
import com.weyr_associates.animaltrakkerfarmmobile.BuildConfig;
import com.weyr_associates.animaltrakkerfarmmobile.R;
import com.weyr_associates.animaltrakkerfarmmobile.Sdk;


public class EIDReaderService extends Service {

	private static final String TEST_EID = "940_100000286625";
	//Thread nThread;
	private SerialPort mSerialPort;
	protected OutputStream mOutputStream;
	private InputStream mInputStream;
	private ReadThread mReadThread;

	private BluetoothAdapter mBluetoothAdapter = null;
	private BTConnectThread mBTConnectThread;
	private BTConnectedThread mBTConnectedThread;

	private int mBTState;
	public static final int STATE_NONE = 0;       // we're doing nothing
	public static final int STATE_LISTEN = 1;     // now listening for incoming connections
	public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
	public static final int STATE_CONNECTED = 3;  // now connected to a remote device

	public static final String STATE_STRING_NONE = "None";
	public static final String STATE_STRING_LISTEN = "Listen";
	public static final String STATE_STRING_CONNECTING = "Connecting";
	public static final String STATE_STRING_CONNECTED = "Connected";
	public static final String STATE_STRING_SCANNING = "Scanning";

	private static final int WINDOW_SIZE = 4;  // Size of the sliding window
	private final LinkedList<String> window = new LinkedList<>();
	private final HashSet<String> dedupSet = new HashSet<>();

	private Boolean BTShouldAutoSwitch = false;
	private Boolean BTwasDisabled = false;
	private Boolean UseSerialEID = false;
	private Boolean De_Duplicate = false;
	private Boolean Is_RaceReader = false;
	static boolean isRunning = false;
	static boolean sendTagsOK = false;
	static boolean sendCounterTagsOK = false;
	static String FileNameMod = "0";
	private Timer timer = new Timer();
	private String logmsgs = "";
	private int DisplayMsgType = 0;

	ArrayList<Messenger> mClients = new ArrayList<Messenger>(); // Keeps track of all current registered clients.
	int mValue = 0; // Holds last value set by a client.
	public static final int MSG_THREAD_SUICIDE = 997;
	public static final int MSG_REGISTER_CLIENT = 1;
	public static final int MSG_UNREGISTER_CLIENT = 2;
	public static final int MSG_UPDATE_STATUS = 3;
	public static final int MSG_NEW_EID_FOUND = 4;
	public static final int MSG_UPDATE_LOG_APPEND = 5;
	public static final int MSG_UPDATE_LOG_FULL = 6;
	public static final int MSG_TOGGLE_LOG_TYPE = 7;
	public static final int MSG_RELOAD_PREFERENCES = 8;
	public static final int MSG_SEND_ME_TAGS = 9;
	public static final int MSG_NO_TAGS_PLEASE = 10;
	public static final int MSG_SEND_ME_COUNTER_TAGS = 39;
	public static final int MSG_NO_COUNTER_TAGS_PLEASE = 40;

	public static final int MSG_TIMER_TICK = 100;
	public static final int MSG_BT_LOG_MESSAGE = 200;
	public static final int MSG_BT_GOT_DATA = 201;
	public static final int MSG_READER_CONNECTED= 202;
	public static final int MSG_READER_CONNECTING= 203;
	public static final int MSG_BT_FINISHED = 299;

	// to pass the last eid stuff for passing to do sheep task
//	public final static String LASTEID = "com.weyr_associates.lambtracker.LASTEID";

	//Target we publish for clients to send messages to IncomingHandler.
	final Messenger mMessenger = new Messenger(new IncomingHandler());

	private String MACAddress = "00:00:00:00:00:00";
	private String[] recentData[];
	private Boolean UseHTCConnectionWorkaround = false;
	private byte[] BinaryDataToSave = new byte[4096];
	private String EIDDataToSave = "";
	private String EID = "";
	private String LastEID = "zzz";
	private boolean completeline;

	private int TicksSinceLastStatusSent = 0;

	boolean mByteReceivedBack;
	final Object mByteReceivedBackSemaphore = new Object();

	private static ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);

	@Override
	public IBinder onBind(Intent intent) {
		return mMessenger.getBinder();
	}

	class IncomingHandler extends Handler { // handler of incoming messages from clients
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case MSG_REGISTER_CLIENT:
					mClients.add(msg.replyTo);
					break;
				case MSG_UNREGISTER_CLIENT:
					mClients.remove(msg.replyTo);
					break;
				case MSG_UPDATE_STATUS:
					sendStatusMessageToUI(); // Client requested a status update
					break;
				case MSG_RELOAD_PREFERENCES:
					LoadPreferences(true); // Client requested that the service reload the shared preferences
					break;
				case MSG_UPDATE_LOG_FULL:
					sendAllLogMessagesToUI(); // Client requested all of the log messages.
					if (!isRunning) {
						InformActivityOfThreadSuicide();
					}
					break;
				case MSG_TOGGLE_LOG_TYPE:
					if (DisplayMsgType == 0) {
						SetDisplayMsgType(1);
					} else {
						SetDisplayMsgType(0);
					}
					break;
				case MSG_SEND_ME_TAGS:
					sendTagsOK = true; // Client requested Tags
					if (BuildConfig.EID_READER_SERVICE_AUTO_POST_TEST_EID_ON_SCAN) {
						LastEID = TEST_EID;
						sendNewEIDMessageToUI();
					}
//				Log.i("eidService", "sendTagsOK.");
					break;
				case MSG_NO_TAGS_PLEASE:
					sendTagsOK = false; // Client requested No Tags
//				Log.i("eidService", "No Tags.");
					break;
				case MSG_SEND_ME_COUNTER_TAGS:
					sendCounterTagsOK = true; // Client requested Tags
//				Log.i("eidService", "sendCounterTagsOK.");
					break;
				case MSG_NO_COUNTER_TAGS_PLEASE:
					sendCounterTagsOK = false; // Client requested No Tags
					ClearDupSet();  // Clear the dup set and window
					UpdateFileNameMod();
//				Log.i("eidService", "No Counter Tags.");
					break;

				default:
					super.handleMessage(msg);
			}
//			Log.i("eidService", "after case statement re messages");
		}
	}


	public void UpdateFileNameMod() {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		FileNameMod = preferences.getString("filenamemod", "0");
		SharedPreferences.Editor editor= preferences.edit();
		int mod = Integer.parseInt(preferences.getString("filenamemod","0"));
        mod = mod + 1;
		editor.putString("filenamemod",Integer.toString(mod));
		editor.commit();
		LoadPreferences(false);
	}

	public static void Beep() {
		toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
	}

	public void ClearDupSet() {
		window.clear();
		dedupSet.clear();
	}

	private class ReadThread extends Thread {

		@Override
		public void run() {
			super.run();
			byte[] buffer1 = new byte[2000];
			while (!isInterrupted()) {
				int size, size1, size2;
				try {
					byte[] buffer = new byte[64];
					if (mInputStream == null) return;
					size = mInputStream.read(buffer);
					if (size > 0) {
						onDataReceived(buffer, size);
					}
				} catch (IOException e) {
					e.printStackTrace();
					return;
				}
			}
		}
	}


	private void InformActivityOfThreadSuicide() {
		for (int i = mClients.size() - 1; i >= 0; i--) {
			try {
				mClients.get(i).send(Message.obtain(null, MSG_THREAD_SUICIDE, 0, 0));
				Log.i("eidService", "Service informed Activity of Suicide. +");
			} catch (RemoteException e) {
				// The client is dead. Remove it from the list; we are going
				// through the list from back to front so this is safe to do
				// inside the loop.
				mClients.remove(i);
				Log.i("eidService", "Service informed Activity of Suicide. -");
			}
		}
		Log.i("eidService", "Service informed Activity of Suicide.");
	}

	private void LogMessage(String m) {
		// Check if log is too long, shorten if necessary.
		if (logmsgs.length() > 1000) {
			int tempi = logmsgs.length();
			tempi = logmsgs.indexOf("\n", tempi - 500);
			logmsgs = logmsgs.substring(tempi + 1);
		}

		// Append new message to the log.
		logmsgs += "\n" + TheDateTimeIs() + m;

		if (DisplayMsgType == 0) {
			// Build bundle
			Bundle b = new Bundle();
			b.putString("logappend", TheDateTimeIs() + m);
			for (int i = mClients.size() - 1; i >= 0; i--) {
				try {
					Message msg = Message.obtain(null, MSG_UPDATE_LOG_APPEND);
					msg.setData(b);
					mClients.get(i).send(msg);
				} catch (RemoteException e) {
					// The client is dead. Remove it from the list; we are going
					// through the list from back to front so this is safe to do
					// inside the loop.
					mClients.remove(i);
				}
			}
		}
	}

	private void sendAllLogMessagesToUI() {
		Bundle b = new Bundle();
		b.putString("logfull", logmsgs);

		for (int i = mClients.size() - 1; i >= 0; i--) {
			try {
				Message msg = Message.obtain(null, MSG_UPDATE_LOG_FULL);
				msg.setData(b);
				mClients.get(i).send(msg);
			} catch (RemoteException e) {
				// The client is dead. Remove it from the list; we are going
				// through the list from back to front so this is safe to do
				// inside the loop.
				mClients.remove(i);
			}
		}
	}

	private void SetDisplayMsgType(int MsgType) {
		MsgType = 0;

		if (DisplayMsgType != MsgType) { //Type changed. Need to re-send everything
			DisplayMsgType = MsgType;
			sendAllLogMessagesToUI();
		}
	}

	private void sendStatusMessageToUI() {
		// Build bundle
		String Stat;

		switch (mBTState) {
			case STATE_NONE:
				Stat = STATE_STRING_NONE;
				break;
			case STATE_LISTEN:
				Stat = STATE_STRING_LISTEN;
				break;
			case STATE_CONNECTED:
				Stat = STATE_STRING_CONNECTED;
				break;
			case STATE_CONNECTING:
				Stat = STATE_STRING_CONNECTING;
				break;
			default:
				Stat = STATE_STRING_SCANNING;
		}

		Bundle b = new Bundle();
		b.putString("stat", Stat);
		b.putString("info1", LastEID);
		b.putString("info2", TheTimeIs());

		for (int i = mClients.size() - 1; i >= 0; i--) {
			try {
				Message msg = Message.obtain(null, MSG_UPDATE_STATUS);
				msg.setData(b);
				mClients.get(i).send(msg);
			} catch (RemoteException e) {
				// The client is dead. Remove it from the list; we are going
				// through the list from back to front so this is safe to do
				// inside the loop.
				mClients.remove(i);
			}
		}
		TicksSinceLastStatusSent = 0; // Reset to zero
	}

	private void sendNewEIDMessageToUI() {  // tell non counter clients when a new EID is available, save to EID-TAGS folder
		if ( sendTagsOK == true) {
			// Build bundle
			String Stat;
			Stat = "Scanning";

			Bundle b = new Bundle();
			b.putString("stat", Stat);
			b.putString("info1", LastEID);
			b.putString("info2", TheTimeIs());

			for (int i = mClients.size() - 1; i >= 0; i--) {
				try {
					Message msg = Message.obtain(null, MSG_NEW_EID_FOUND);
					msg.setData(b);
					mClients.get(i).send(msg);
				} catch (RemoteException e) {
					// The client is dead. Remove it from the list; we are going
					// through the list from back to front so this is safe to do
					// inside the loop.
					mClients.remove(i);
				}
			}

			if (!Is_RaceReader) {
				SaveEIDLineToFile(LastEID + "\r");
				if (UseSerialEID) {
					Beep();
				}
				sendTagsOK = false; // turn off tags unless this is a race reader
				//		Log.i("eidService", "No Tags1.");
			} else {
  // something should go here... for race reader
			}
		}
    }

	private void SaveEIDToFile() {  // tell counter clients when a new EID is available save to RACE-TAGS folder
		if ( sendCounterTagsOK == true) {
			// De Duplicate
			if (De_Duplicate == true) {

				if (LastEID.length() != 16) { // look out for too many tags read at once, usually a race reader problem
					return;
				}
				if (!dedupSet.contains(LastEID)) {

					// Remove the oldest entry if the window size exceeds 5
					if (window.size() >= WINDOW_SIZE) {
						String removed = window.removeFirst(); // remove first element from window list
						dedupSet.remove(removed); // remove the
						//			}
					}
					// Check if the line is a duplicate within the window
					//		if (!dedupSet.contains(LastEID)) {
					// if unique Add the line to the window and the dedupSet
					window.addLast(LastEID);
					dedupSet.add(LastEID);
				} else {
					return;
				}
			}
			// Build bundle
			String Stat;
		//	Stat = "Scanning";

			Stat = SaveEIDLineToCounterFile(LastEID + "\r");
			Bundle b = new Bundle();
			b.putString("stat", Stat);
			b.putString("info1", LastEID);
			b.putString("info2", TheTimeIs());

			for (int i = mClients.size() - 1; i >= 0; i--) {
				try {
					Message msg = Message.obtain(null, MSG_NEW_EID_FOUND);
					msg.setData(b);
					mClients.get(i).send(msg);
				} catch (RemoteException e) {
					// The client is dead. Remove it from the list; we are going
					// through the list from back to front so this is safe to do
					// inside the loop.
					mClients.remove(i);
				}
			}

	//			SaveEIDLineToCounterFile(LastEID + "\r");
	//			if (UseSerialEID) {
					Beep();
	//			}
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();
//		Log.i("eidService", "Service Started.");

		isRunning = true;
		LoadPreferences(false);

///		Notification lNotification = new Notification(R.drawable.reader,getText(R.string.service_started), System.currentTimeMillis());
///		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
///		lNotification.setLatestEventInfo(this, getText(R.string.service_label), getText(R.string.service_started), contentIntent);
///		startForeground(5213585, lNotification);

		if (!UseSerialEID) {
			Log.i("SerialEID", "Generic Bluetooth Reader");

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		timer.scheduleAtFixedRate(new TimerTask(){ public void run()
		{onTimerTick_TimerThread();}}, 0, 500L);
		}else {
			Log.i("SerialEID", "X6P Serial Reader");
			try {
				SerialPort.setDevicePower(getApplicationContext(), true);
			} catch (SecurityException e) {
				Log.e("SecurityException", e.getMessage());
			}
			Log.e("No SecurityException","Port power ON");

			try {
				SerialPort.setNx6pBackNode("/sys/class/ext_dev/function/pin10_en", "1");
			} catch (SecurityException e) {
				Log.e("SecurityException", e.getMessage());
			}
			Log.e("No SecurityException","enable pin 10");

			try {
				File path = new File(SerialPort.getSerialPath()); // --> /dev/ttyHSL1
				mSerialPort = new SerialPort(path, 9600, 0x02);
				mOutputStream = mSerialPort.getOutputStream();
				mInputStream = mSerialPort.getInputStream();

				/* Create a receiving thread */
				mReadThread = new ReadThread();
				mReadThread.start();

			} catch (SecurityException e) {
				Log.e("SecurityException", e.getMessage());
			} catch (IOException e) {
				Log.e("IOException", e.getMessage());
			} catch (InvalidParameterException e) {
				Log.e("InvalidParameterException", e.getMessage());
			}

	//		Log.e("No SecurityException","Serial port open");

			for (int i = mClients.size() - 1; i >= 0; i--) {
				try {
					Message msg = Message.obtain(null, MSG_READER_CONNECTED);
					mClients.get(i).send(msg);
				} catch (RemoteException e) {
					// The client is dead. Remove it from the list; we are going
					// through the list from back to front so this is safe to do
					// inside the loop.
					mClients.remove(i);
				}
			}

			// do more serial stuff here ? ... maybe not ...
		}

	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
//		Log.i("eidService", "Received start id " + startId + ": " + intent);
		return START_STICKY; // run until explicitly stopped.
	}

	private void LoadPreferences(Boolean NotifyOfChanges) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
//		SharedPreferences preferences = getActivity().getPreferences(getBaseContext());
		UseSerialEID = preferences.getBoolean("useserialeid", false);
		De_Duplicate = preferences.getBoolean("deduplicate", false);
		Is_RaceReader = preferences.getBoolean("isracereader", false);
		FileNameMod = preferences.getString("filenamemod", "0");

		try {

			BTShouldAutoSwitch = preferences.getBoolean("autoswitchbluetooth", false);
			UseHTCConnectionWorkaround = preferences.getBoolean("htcconnectworkaround", false);
						
			String newMACAddress = preferences.getString("bluetooth_mac", "00:00:00:00:00:00");
			if (newMACAddress != MACAddress) {
				if (!MACAddress.equals("00:00:00:00:00:00")) {
					LogMessage("BT: Target Device Changed. You will need to Disconnet/Reconnect.");
				}
				MACAddress = newMACAddress;
				BTstop();
			}
			
		} catch (NumberFormatException nfe) {}

	}
/*	public boolean isInteger(String input) {
		try {
			Integer.parseInt(input);
			return true;
		} catch (Exception e) {
			return false;
		}
	}*/
	
	public static boolean isRunning(){
		return isRunning;
	}
	
	private String TheDateTimeIs() {  // maybe move this to its own class ?
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minutes = calendar.get(Calendar.MINUTE);
		int seconds = calendar.get(Calendar.SECOND);
		return Make2Digits(month + 1) + "/" +  Make2Digits(day) + "/" + year + ":" + Make2Digits(hours) + ":" + Make2Digits(minutes) + ":"
				+ Make2Digits(seconds) + " ";
	}

	private String TheTimeIs() { // maybe move this too ?
		Calendar calendar = Calendar.getInstance();
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minutes = calendar.get(Calendar.MINUTE);
		int seconds = calendar.get(Calendar.SECOND);
		return Make2Digits(hours) + ":" + Make2Digits(minutes) + ":"
				+ Make2Digits(seconds) + " ";
	}

	private String Make2Digits(int i) { // maybe move this too ?
		if (i < 10) {
			return "0" + i;
		} else {
			return Integer.toString(i);
		}
	}
	private void onTimerTick_TimerThread() {
		// This is running on a separate thread. Cannot do UI stuff from here.
		// Send a message to the handler to do that stuff on the main thread.
		handler.sendMessage(handler.obtainMessage(MSG_TIMER_TICK));
	}

	private void onTimerTick() { // Back on the main thread.
		TicksSinceLastStatusSent++;
		if (TicksSinceLastStatusSent > 0) {
			sendStatusMessageToUI();
		}

		if (getBTState() == STATE_NONE) { //We're not connected, try to start.
			if (mBluetoothAdapter == null) { //No adapter. Fail
				//Log.e("Bluetooth", "getDefaultAdapter returned null");
				Toast.makeText(this, "This device does not support Bluetooth.", Toast.LENGTH_SHORT).show();
				LogMessage("Bluetooth is NOT supported.");
				haltService();
			} else {
				if (!mBluetoothAdapter.isEnabled()) { //Bluetooth disabled
					if (BTShouldAutoSwitch) { //At least auto-switch is enabled
						//We need to turn on Bluetooth
						if (!BTwasDisabled && Sdk.allowsProgrammaticActivationOfBluetooth()) {
							//Log.i("Bluetooth", "Turning on");
							try {
								mBluetoothAdapter.enable();
								BTwasDisabled = true;
								LogMessage("Waiting for Bluetooth...");
							} catch (SecurityException e) {
								Toast.makeText(this,
										R.string.toast_auto_bluetooth_activation_failure_permissions,
										Toast.LENGTH_LONG).show();
								haltService();
							}
						} else { //We need to wait until bluetooth comes online
							//Log.i("Bluetooth", "Waiting for for Bluetooth to turn on");
						}
					} else { //and auto-switch is disabled. Fail
						//Log.e("Bluetooth", "Bluetooth is Disabled and we can't autoswitch it on");
						Toast.makeText(this, "Bluetooth is Disabled", Toast.LENGTH_SHORT).show();
						LogMessage("Bluetooth is Disabled");
						haltService();
					}
				} else {
					BTstart();
				}
			}
		}
	}

	private void haltService() {
		isRunning = false;
		InformActivityOfThreadSuicide();
		if (timer != null) {timer.cancel();}
		this.stopSelf();
	}

	// Bluetooth Reader Stuff
	
	private synchronized void setBTState(int state) {
//        Log.i("eidService", "setBTState() " + mBTState + " -> " + state);
        mBTState = state;
    }

	public synchronized int getBTState() {
	        return mBTState;
	    }

	public synchronized void BTstart() {
		SetDisplayMsgType(0);
//		Log.i("eidService", "BTstart");
        // Cancel any thread attempting to make a connection
        if (mBTConnectThread != null) {mBTConnectThread.cancel(); mBTConnectThread = null;}
        // Cancel any thread currently running a connection
        if (mBTConnectedThread != null) {mBTConnectedThread.cancel(); mBTConnectedThread = null;}
        
        if (!BluetoothAdapter.checkBluetoothAddress(MACAddress)) {
        	LogMessage("Invalid Bluetooth MAC Address: \"" + MACAddress + "\"");
        	InformActivityOfThreadSuicide();
        } else if (MACAddress.equals("00:00:00:00:00:00")) {
        	LogMessage("Error: No Bluetooth device has been selected.");
        	isRunning = false;
			InformActivityOfThreadSuicide();
			if (timer != null) {timer.cancel();}
			this.stopSelf();
        } else {
        	setBTState(STATE_LISTEN);
            BluetoothDevice btdevice = mBluetoothAdapter.getRemoteDevice(MACAddress);
            BTconnect(btdevice);	
        }
    }

	public synchronized void BTconnect(BluetoothDevice device) {
        LogMessage("Device: " + device.getAddress());
		// Cancel any thread attempting to make a connection
        if (mBTState == STATE_CONNECTING) {
            if (mBTConnectThread != null) {mBTConnectThread.cancel(); mBTConnectThread = null;}
        }
        // Cancel any thread currently running a connection
        if (mBTConnectedThread != null) {mBTConnectedThread.cancel(); mBTConnectedThread = null;}

        // Start the thread to connect with the given device
        mBTConnectThread = new BTConnectThread(device, UseHTCConnectionWorkaround);
        mBTConnectThread.start();
        setBTState(STATE_CONNECTING);

		for (int i = mClients.size() - 1; i >= 0; i--) {
			try {
				Message msg = Message.obtain(null, MSG_READER_CONNECTING);
				mClients.get(i).send(msg);
			} catch (RemoteException e) {
				// The client is dead. Remove it from the list; we are going
				// through the list from back to front so this is safe to do
				// inside the loop.
				mClients.remove(i);
			}
		}
    }

	private class BTConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public BTConnectThread(BluetoothDevice device, boolean IsAnHTCDevice) {
        	mmDevice = device;
            BluetoothSocket tmp = null;

            // Get a BluetoothSocket for a connection with the given BluetoothDevice
            if (IsAnHTCDevice) {
				try {
					Method m = device.getClass().getMethod("createRfcommSocket", new Class[] {int.class});
					tmp = (BluetoothSocket) m.invoke(device, Integer.valueOf(1));
				} catch (Exception e) {
					handleExceptionForSocketCreation(e);
				}
			} else {
				try {
					UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
					tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
				} catch (SecurityException e) {
					handleExceptionForSocketCreation(e);
					//Prevent lint from flagging duplicate catch block for Exception.
					//noinspection
				} catch (Exception e) {
					handleExceptionForSocketCreation(e);
				}
			}
            
            mmSocket = tmp;
        }

        public void run() {
            //Log.i("eidService", "BEGIN BTConnectThread");
            handler.sendMessage(handler.obtainMessage(MSG_BT_LOG_MESSAGE, "Trying to Connect..."));

			// Always try to cancel discovery because it will slow down a connection
			try {
				mBluetoothAdapter.cancelDiscovery();
			} catch (SecurityException e) {
				// No BLUETOOTH_SCAN permission. Shouldn't happen, but it could.
				// NO-OP since there is nothing we can really do here except abort
				// and connections can still be made with discovery in progress...
				// just slowly...
				e.printStackTrace();
			}

            // Make a connection to the BluetoothSocket
            try {
                // This is a blocking call and will only return on a successful connection or an exception
                mmSocket.connect();
            } catch (SecurityException|IOException e) {
				handleExceptionOnConnectionFailure(e);
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (EIDReaderService.this) {
            	mBTConnectThread = null;
            }

            // Start the connected thread
            BTconnected(mmSocket, mmDevice);
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                //Log.e("EidService", "close() of connect socket failed", e);
            }
        }

		private void handleExceptionForSocketCreation(Exception e) {
			//Log.e("EidService", "Error at createRfcommSocketToServiceRecord: " + e);
			e.printStackTrace();
			handler.sendMessage(handler.obtainMessage(MSG_BT_LOG_MESSAGE, "Exception creating socket: " + e));
		}

		private void handleExceptionOnConnectionFailure(Exception e) {
			// Close the socket
			try {
				mmSocket.close();
			} catch (IOException e2) {
				//Log.e("EidService", "unable to close() socket during connection failure", e2);
			}
			//Log.e("EidService", "unable to connect() socket. Error: ", e);
			handler.sendMessage(handler.obtainMessage(MSG_BT_LOG_MESSAGE, "Failed to Connect: " + e));
			handler.sendMessage(handler.obtainMessage(MSG_BT_FINISHED));
		}
    }

	public synchronized void BTconnected(BluetoothSocket socket, BluetoothDevice device) {
        //Log.i("eidService", "Connected");
        // Cancel the thread that completed the connection
        if (mBTConnectThread != null) {mBTConnectThread.cancel(); mBTConnectThread = null;}
        // Cancel any thread currently running a connection
        if (mBTConnectedThread != null) {mBTConnectedThread.cancel(); mBTConnectedThread = null;}

        // Start the thread to manage the connection and perform transmissions
        mBTConnectedThread = new BTConnectedThread(socket);
        mBTConnectedThread.start();

        setBTState(STATE_CONNECTED);

			for (int i = mClients.size() - 1; i >= 0; i--) {
				try {
					Message msg = Message.obtain(null, MSG_READER_CONNECTED);
					mClients.get(i).send(msg);
				} catch (RemoteException e) {
					// The client is dead. Remove it from the list; we are going
					// through the list from back to front so this is safe to do
					// inside the loop.
					mClients.remove(i);
				}
			}

        sendTagsOK = true; // Lets get some tags
    }

	private class BTConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        
        public BTConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            
            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                //Log.e("EidService", "temp sockets not created", e);
                handler.sendMessage(handler.obtainMessage(MSG_BT_LOG_MESSAGE, "Could not create Streams"));
                handler.sendMessage(handler.obtainMessage(MSG_BT_FINISHED));
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            //Log.i("eidService", "BEGIN BTConnectedThread");
            handler.sendMessage(handler.obtainMessage(MSG_BT_LOG_MESSAGE, "Bluetooth Device Connected"));
			// maybe add an on screen indicator here that we have a connected Bluetooth device

            byte[] buffer = new byte[1024];
            int bytesread;

            // Keep listening to the InputStream while connected
            while (true) {
                try {
                	bytesread = mmInStream.read(buffer); //This is a blocking call
                    byte[] tempdata = new byte[bytesread];
                    System.arraycopy(buffer, 0, tempdata, 0, bytesread);
//                    Log.d("EidService", "Got Data: " + new String(tempdata));
                    handler.sendMessage(handler.obtainMessage(MSG_BT_GOT_DATA, tempdata));
                    //mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer).sendToTarget();
                } catch (IOException e) {
                    //Log.e("EidService", "ConnectionLost. Error: " + e);
                    handler.sendMessage(handler.obtainMessage(MSG_BT_FINISHED));
                    break;
                }
            }
        }

        public void write(byte[] buffer) {
            try {
                mmOutStream.write(buffer);
            } catch (IOException e) {
                //Log.e("EidService", "Exception during write", e);
            }
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                //Log.e("EidService", "close() of connected socket failed", e);
            }
        }
    }

	public synchronized void BTstop() {
//        Log.i("eidService", "BTstop");
        if (mBTConnectThread != null) {mBTConnectThread.cancel(); mBTConnectThread = null;}
        if (mBTConnectedThread != null) {mBTConnectedThread.cancel(); mBTConnectedThread = null;}
        setBTState(STATE_NONE);
    }
	public void SendDataToBluetooth(byte[] buffer) { // You run this from the main thread.
		// Create temporary object
		BTConnectedThread r;
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mBTState != STATE_CONNECTED) return;
            r = mBTConnectedThread;
            //Log.d("EidService", "Sent Data");
        }
        // Perform the write unsynchronized
        r.write(buffer);
	}
	private void ParseBTDataStream(byte[] buffer) {
	//	Log.e("eidservice","bytes from bt:" + buffer.length + ", " + new String(buffer));
		if (buffer.length<=18) {
			ParseEIDStream(new String(buffer));
		}
// Save all data to file
//			SaveRawDataToFile(buffer); // helpful for debugging 
	}
	
	private void ParseEIDStream(String newdata) {
		
		EID += newdata;
//		String[] lines = EID.split("\\r?\\n"); //works for Priority1
//		String[] lines = EID.split("\r"); //works for y-tex
		String[] lines = EID.split("\n"); // works for both

		Log.e("EidService", EID);
	//	Log.e("EidService",Integer.toString( lines.length));
	//	Log.e("In Parse1" + " " + lines.length + ", " + new String(newdata) + ", " + EID);
		if (lines.length > 0) {
			for (int i = 0; i < lines.length; i++) {
				completeline = false; // Reset this
				if (lines.length > 0) { // There is some data here
//					LogMessage("In Parse2" + " " + i + " " + lines[i].length() + " " + lines[i].lastIndexOf("\r"));
					if (lines[i].lastIndexOf("\r") + 1 == lines[i].length()) { // Line ends with a \r
						completeline = true;
						if (lines[i].substring(3, 4).equals("_")) {
//								LogMessage("eidService0, Priority1");
						} else
							if (lines[i].substring(3, 4).equals(" ")) {
//									LogMessage("eidService0, Y-Tex Panel");
									EID = EID.substring(0, 3) + "_" + EID.substring(4,EID.length()-1);	
							} else {
								  if (lines[i].substring(0, 1).equals("0")) {
		//						    Log.i("eidService", "ShearWell Wand/Panel");
								    EID = EID.substring(1, 4) + "_" + EID.substring(4,EID.length());
									EID = EID.substring(0, EID.length() - 1);  //prune off \n
								  } else {
				//				         LogMessage("eidService0, Y-Tex Wand");
								          EID = EID.substring(0, 3) + "_" + EID.substring(3,EID.length()-1);
								  }
							}
//						LogMessage("eidService1, completeline true");				
						LastEID = EID.substring(0, EID.length() - 1);  //prune off end of line for the database
						if (Is_RaceReader) {
							SaveEIDToFile(); // send the EID to counter clients and save to RACE-TAGS folder
		//					sendTagsOK = false; // turn off tags unless this is a race reader
		//							Log.i("eidService", "SaveEIDToFile");
						} else {
							sendNewEIDMessageToUI();  // send the EID to non counter clients and save to EID-TAGS folder
							Log.i("eidService", "sendNewEIDMessageToUI");
						}
						EID = ""; // Clear out EID for next one
					}
				}
			}

			if (!completeline) { // Last line wasn't complete, put last incomplete line back
//				LogMessage("eidService2, completeline false but more data to come");
				if (lines[lines.length - 1].length() < 1000) { // Only if less than 1000 characters long.
					EID = lines[lines.length - 1];
				}
			}
		}
	}

	private String SaveEIDLineToCounterFile(String line) {  // Saves EID to RACE-TAGS folder and returns name of the file
		EIDDataToSave += TheDateTimeIs() + line;
		String filename = "zzz";
		if (EIDDataToSave.length() > 15) {
			try {
				String state = Environment.getExternalStorageState();
				if (Environment.MEDIA_MOUNTED.equals(state)) { // We can read and write the media
					File dir = AppDirectories.raceTagsDirectory(this);
					dir.mkdirs();

					Calendar calendar = Calendar.getInstance();
					int year = calendar.get(Calendar.YEAR);
					int month = calendar.get(Calendar.MONTH) + 1;
					int day = calendar.get(Calendar.DAY_OF_MONTH);

					filename = year + "-" + Make2Digits(month) + "-" + Make2Digits(day) + "_" + FileNameMod +".txt";
					File file = new File(dir, filename);
					FileWriter writer = new FileWriter(file, true);
					writer.append(EIDDataToSave);
					writer.flush();
					writer.close();
					EIDDataToSave = "";
				}
			} catch (Exception e) {
				//Log.d("SaveEIDChunk", e.getMessage());
			}
		}	return filename;
	}

	private void SaveEIDLineToFile(String line) {
		EIDDataToSave += TheDateTimeIs() + line;
		if (EIDDataToSave.length() > 15) {
			SaveEIDChunk();
		}
	}

	private void SaveEIDChunk() {
		if (EIDDataToSave.length() > 0) {
				try {
				String state = Environment.getExternalStorageState();
				if (Environment.MEDIA_MOUNTED.equals(state)) { // We can read and write the media
					File dir = AppDirectories.eidTagsDirectory(this);
					dir.mkdirs();

					Calendar calendar = Calendar.getInstance();
					int year = calendar.get(Calendar.YEAR);
					int month = calendar.get(Calendar.MONTH) + 1;
					int day = calendar.get(Calendar.DAY_OF_MONTH);
					String filename = year + "-" + Make2Digits(month) + "-" + Make2Digits(day) + ".txt";

					File file = new File(dir, filename);
					FileWriter writer = new FileWriter(file, true);
					writer.append(EIDDataToSave);
					writer.flush();
					writer.close();
					EIDDataToSave = "";
				}
			} catch (Exception e) {
				//Log.d("SaveEIDChunk", e.getMessage());
			}
		}
	}

	public Handler handler = new Handler() { // Handler for data coming from bluetooth sockets
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_TIMER_TICK:
				onTimerTick();
				break;
			case MSG_BT_GOT_DATA:
				 Log.i("handleMessage", "MSG_BT_GOT_DATA");
				byte[] buffer2 = (byte[]) msg.obj;
				ParseBTDataStream(buffer2);
				break;
			case MSG_BT_LOG_MESSAGE:
				LogMessage((String) msg.obj);
				break;
			case MSG_BT_FINISHED:
//				Log.i("handleMessage", "MSG_BT_FINISHED");
				BTstop();
				break;
			default:
				super.handleMessage(msg);
			}
		}
	};

//	protected abstract void onDataReceived(final byte[] buffer, final int size);

protected void onDataReceived(byte[] buffer, int size) {

	synchronized (mByteReceivedBackSemaphore) {

		byte[] tempdata = new byte[size];
		System.arraycopy(buffer, 0, tempdata, 0, size);
		handler.sendMessage(handler.obtainMessage(MSG_BT_GOT_DATA, tempdata));
//		Log.e("bytes from SERIAL ", "size="+ size);
//		Log.e("bytes from SERIAL ",  new String(buffer));
	}
}

	@Override
	public void onDestroy() {
		super.onDestroy();

		SaveEIDChunk();

		// Kill threads
		if (timer != null) {timer.cancel();}

		if (!UseSerialEID) {
//		Log.i("eidService", "onDestroy.");
			BTstop();
			// Turn BT off
			if (BTShouldAutoSwitch && BTwasDisabled && Sdk.allowsProgrammaticActivationOfBluetooth()) {
				//Log.i("eidService", "ON DESTROY: Turning BT back off");
				try {
					mBluetoothAdapter.disable();
					Toast.makeText(this, "Disabling Bluetooth...", Toast.LENGTH_SHORT).show();
				} catch (SecurityException e) {
					Toast.makeText(this,
							R.string.toast_auto_bluetooth_deactivation_failure_permissions,
							Toast.LENGTH_LONG).show();
				}
			}
		}else{
			try {
				SerialPort.setNx6pBackNode("/sys/class/ext_dev/function/pin10_en", "0");
			} catch (SecurityException e) {
				Log.e("SecurityException", e.getMessage());
			}
//			Log.e("No SecurityException","disable pin 10");

			try {
				mSerialPort.close();
			} catch (SecurityException e) {
				Log.e("SecurityException", e.getMessage());
			}
	//		Log.e("No SecurityException","Serial port closed");

			for (int i = mClients.size() - 1; i >= 0; i--) {
				try {
					Message msg = Message.obtain(null, MSG_READER_CONNECTING);
					mClients.get(i).send(msg);
				} catch (RemoteException e) {
					// The client is dead. Remove it from the list; we are going
					// through the list from back to front so this is safe to do
					// inside the loop.
					mClients.remove(i);
				}
			}

			try {
				SerialPort.setDevicePower(getApplicationContext(), false);
			} catch (SecurityException e) {
				Log.e("SecurityException", e.getMessage());
			}
	//		Log.e("No SecurityException","Port power OFF");
		}

		stopForeground(true);
//		Log.i("eidService", "Service Stopped.");
		isRunning = false;
	}
}