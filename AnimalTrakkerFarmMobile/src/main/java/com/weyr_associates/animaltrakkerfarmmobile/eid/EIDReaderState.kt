package com.weyr_associates.animaltrakkerfarmmobile.eid

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import com.weyr_associates.animaltrakkerfarmmobile.R

object EIDReaderState {
    @JvmStatic
    @DrawableRes
    fun iconForState(state: String): Int = when (state) {
        EIDReaderService.STATE_STRING_NONE -> R.drawable.ic_bluetooth_disabled
        EIDReaderService.STATE_STRING_LISTEN,
        EIDReaderService.STATE_STRING_SCANNING,
        EIDReaderService.STATE_STRING_CONNECTING -> R.drawable.ic_bluetooth_searching
        EIDReaderService.STATE_STRING_CONNECTED -> R.drawable.ic_bluetooth_connected
        else -> R.drawable.ic_bluetooth_disabled
    }

    @JvmStatic
    @ColorRes
    fun colorForState(state: String): Int = when (state) {
        EIDReaderService.STATE_STRING_CONNECTED -> R.color.status_ok
        else -> R.color.status_error
    }
}
