package com.weyr_associates.animaltrakkerfarmmobile;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Write a description of class Utilities here.
 * 
 * @author ww 
 * em added the take note utility
 * @version 2014-05-11
 */
public class Utilities 
{
    public static int    JGREG        = 15 + 31*(10+12*1582);   // 15 October 1582
    public static double SECS_PER_DAY = 86400.0;
    public static double HALFSECOND   = 0.5;

    public static Spinner predefined_note_spinner01;
	public static Spinner predefined_note_spinner02;
	public static Spinner predefined_note_spinner03;
	public static Spinner predefined_note_spinner04;
	public static Spinner predefined_note_spinner05;
	public static List<String> predefined_notes;
	public int 		thisanimal_id;
	private static DatabaseHandler dbh;
	public static Cursor 	cursor5;
	public static Object	crsr;
	static ArrayAdapter<String> dataAdapter;
	static String     	cmd;
    /**
     * Constructor for objects of class Utilities
     */
    public Utilities()
    {
    }

public static double toJulian( int[] ymd )
    {
    int year       = ymd[0]; // yyyy
    int month      = ymd[1]; // jan=1, feb=2,...
    int day        = ymd[2]; // 1 - 31
    int julianYear = year;
    
    if( year < 0 )  // 10 BCE => -10
        julianYear++;
    
    int julianMonth = month;
    
    if( month > 2 )
        julianMonth++;
    
    else
        {
        julianYear--;
        julianMonth += 13;
        }

    double julian = (Math.floor(365.25 * julianYear)
                    + Math.floor(30.6001*julianMonth) + day + 1720995.0);
    
    // Gregorian Calendar adopted Oct. 15, 1582 (2299161)
    if( day + 31 * (month + 12 * year) >= JGREG )
        {
        // change over to Gregorian calendar
        int ja  = (int)(0.01 * julianYear);
        julian += 2 - ja + (0.25 * ja);
        }
    
    return Math.floor(julian) - 0.5;    // start of civil day 0h UTC
    }

public double toJulianWithTime( int[] ymdhms )
   {
   double jd  = toJulian( ymdhms );
   int    hh  = ymdhms[3],
          mm  = ymdhms[4],
          ss  = ymdhms[5];
   return jd + (hh * 3600 + mm * 60 + ss) / SECS_PER_DAY;
   }


 /**
* Converts a Julian day to a calendar date
* ref :
* Numerical Recipes in C, 2nd ed., Cambridge University Press 1992
*/
public static int[] fromJulian( double injulian )
    {
    int    jalpha, ja, jb, jc, jd, je, year,month, day;
    double julian = injulian + (HALFSECOND / 86400.0);
    ja = (int) (julian + 0.5);
    
    if( ja >= JGREG )
        {    
        jalpha = (int) (((ja - 1867216) - 0.25) / 36524.25);
        ja = ja + 1 + jalpha - jalpha / 4;
        }

    jb    = ja + 1524;
    jc    = (int) (6680.0 + ((jb - 2439870) - 122.1) / 365.25);
    jd    = 365 * jc + jc / 4;
    je    = (int) ((jb - jd) / 30.6001);
    day   = jb - jd - (int) (30.6001 * je);
    month = je - 1;
    
    if( month > 12 )
        month = month - 12;
    
    year = jc - 4715;
    
    if( month > 2 )
        year--;
    
    if( year <= 0 )
        year--;

    return new int[] {year, month, day};
    }

public int[] fromJulianWithTime( double julian )
    {
    double jd = julian + (HALFSECOND / 86400.0);
    jd = Math.floor( julian + 0.5 ) - 0.5;
    double time = (julian + (HALFSECOND / 86400.0)) - jd;
    int[]  date = fromJulian( jd );
    int    secs = (int) Math.floor( SECS_PER_DAY * time );
    int    hrs  = secs / 3600;
    secs -= hrs * 3600;
    int mins = secs / 60;
    secs -= mins * 60;
    return new int[] { date[0], date[1], date[2], hrs, mins, secs };
    }


public static String takeNote(View v, final Integer thisanimal_id, final Context context )
{
	final List<String> predefined_notes_id_order, predefined_notes_display_order;
	String 	dbfile = context.getString (R.string.real_database_file) ;
    Log.i("takeNote", " after get database file in Utilities");
	dbh = new DatabaseHandler( context, dbfile );
	Log.i ("takeNote", " in beginning of take a note the animal id is " + String.valueOf(thisanimal_id));
	if (thisanimal_id == 0) {
		Log.i ("takeNote", " no animal selected " + String.valueOf(thisanimal_id));
		return "no animal";
	}
	else {
		//	First fill the predefined note spinner with possibilities
    	predefined_notes = new ArrayList<String>();
		predefined_notes_id_order = new ArrayList<String>();
		predefined_notes_display_order = new ArrayList<String>();
    	// Fill first position
		predefined_notes.add("Select a Predefined Note");
		// Set the first item so that the later if statements work if not all notes are filled in
		predefined_notes_id_order.add("0");
		predefined_notes_display_order.add("Predefined Note Display Order");

		Log.i ("takeNote", " after adding Select a Predefined Note");
    	// Select All fields from predefined_notes_table to build the spinner
		// todo Fix this to only show notes from the current owner and handle Geri vs Renee and other specific notes issues
        cmd = "select * from predefined_notes_table order by predefined_note_display_order";
        Log.i ("takeNote", " cmd is " + cmd);
        crsr = dbh.exec( cmd ); 
        Log.i ("takeNote", " after executing the dbh command " + cmd);
        cursor5   = ( Cursor ) crsr;
    	dbh.moveToFirstRecord();
         // looping through all rows and adding to list
    	for (cursor5.moveToFirst(); !cursor5.isAfterLast(); cursor5.moveToNext()){
			predefined_notes_id_order.add(cursor5.getString(0));
    		predefined_notes.add(cursor5.getString(1));
			predefined_notes_display_order.add(cursor5.getString(2));
    		Log.i ("takeNote", " in for loop predefined note id is " + String.valueOf(cursor5.getString(1)));
    	}
    	cursor5.close();    
    	Log.i ("takeNote", " after set the predefined note spinner ");
    	Log.i ("takeNote", " this animal is " + String.valueOf(thisanimal_id));
//		Log.i ("takeNote", " got a sheep, need to get a note to add");
		Builder alertDialogBuilder = new Builder(context);
//		Log.i ("takeNote", " after getting new alertdialogbuilder");
		
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.note_prompt, null);
//		Log.i ("takeNote", " after inflating layout");	

		// set view note_prompt to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		Log.i ("takeNote", " after setting view");
	   	// Creating adapter for predefined notes spinners
    	dataAdapter = new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item, predefined_notes);
    	dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	predefined_note_spinner01 = (Spinner) promptsView.findViewById(R.id.predefined_note_spinner01);
    	predefined_note_spinner01.setAdapter (dataAdapter);
		predefined_note_spinner01.setSelection(0);
		
    	predefined_note_spinner02 = (Spinner) promptsView.findViewById(R.id.predefined_note_spinner02);
    	predefined_note_spinner02.setAdapter (dataAdapter);
		predefined_note_spinner02.setSelection(0);

    	predefined_note_spinner03 = (Spinner) promptsView.findViewById(R.id.predefined_note_spinner03);
    	predefined_note_spinner03.setAdapter (dataAdapter);
		predefined_note_spinner03.setSelection(0);

    	predefined_note_spinner04 = (Spinner) promptsView.findViewById(R.id.predefined_note_spinner04);
    	predefined_note_spinner04.setAdapter (dataAdapter);
		predefined_note_spinner04.setSelection(0);

    	predefined_note_spinner05 = (Spinner) promptsView.findViewById(R.id.predefined_note_spinner05);
    	predefined_note_spinner05.setAdapter (dataAdapter);
		predefined_note_spinner05.setSelection(0);

		Log.i ("takeNote", " after setting spinners");

		final EditText userInput = (EditText) promptsView
				.findViewById(R.id.note_text);

		// set dialog message
		alertDialogBuilder
			.setCancelable(false)
			.setPositiveButton("Save Note",
			  new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
					int temploc, predefined_note02, predefined_note03, predefined_note04, predefined_note05;
					int predefined_note01 ;

					// get user input and set it to result
				// edit text
				String note_text = dbh.fixApostrophes(String.valueOf(userInput.getText()));
				Log.i("update notes ", "note text is " + note_text);

				//	Get id_predefinednotesid from all the spinners here
					temploc = predefined_note_spinner01.getSelectedItemPosition();
					Log.i("update notes ", "got first note spinner location is " + String.valueOf(temploc) );
					predefined_note01 = Integer.valueOf(predefined_notes_id_order.get(temploc));
					Log.i("update notes ", "got first note id is " + String.valueOf(predefined_note01) );

					temploc = predefined_note_spinner02.getSelectedItemPosition();
					Log.i("update notes ", "got 2nd note spinner location is " + String.valueOf(temploc) );
					predefined_note02 = Integer.valueOf(predefined_notes_id_order.get(temploc));
					Log.i("update notes ", "got 2nd note id is " + String.valueOf(predefined_note02) );
//					Log.i("update notes ", "got 2nd note spinner " );

					temploc = predefined_note_spinner03.getSelectedItemPosition();
					predefined_note03 = Integer.valueOf(predefined_notes_id_order.get(temploc));
					Log.i("update notes ", "got 3rd note spinner " );

					temploc = predefined_note_spinner04.getSelectedItemPosition();
					predefined_note04 = Integer.valueOf(predefined_notes_id_order.get(temploc));
					Log.i("update notes ", "got 4th note spinner " );

					temploc = predefined_note_spinner05.getSelectedItemPosition();
					predefined_note05 = Integer.valueOf(predefined_notes_id_order.get(temploc));
					Log.i("update notes ", "got 5th note spinner " );
				String create_modified = TodayIs()+ " " + TimeIs();
				// Update the notes table with the data
				if (predefined_note01 > 0) {
					Log.i("in if ", "before create cmd ");
					cmd = String.format("INSERT INTO animal_note_table (id_animalid, note_text, note_date, note_time, id_predefinednotesid, created, modified) " +
							"values ( %s, '%s', '%s', '%s', '%s', '%s', '%s') ",
							thisanimal_id, note_text, TodayIs(), TimeIs(), predefined_note01,  create_modified, create_modified);
	    			Log.i("update notes ", "before cmd " + cmd);
	    			dbh.exec( cmd );	
	    			note_text = "";
	    			Log.i("update notes ", "after cmd exec");
	    			Log.i("take note","first note written with predefined note");
			    }else{
			    	//	no predefined note so write one without it
			    	cmd = String.format("insert into animal_note_table (id_animalid, note_text, note_date, note_time, id_predefinednotesid, created, modified) " +
 							"values ( %s, '%s', '%s', '%s', '', '%s', '%s')",
							thisanimal_id, note_text, TodayIs(), TimeIs(), create_modified, create_modified);
	    			Log.i("update notes ", "before cmd " + cmd);
	    			dbh.exec( cmd );	
	    			Log.i("update notes ", "after cmd exec");
	    			note_text = "";
			    }
    			if (predefined_note02 > 0) {
					cmd = String.format("insert into animal_note_table (id_animalid, note_text, note_date, note_time, " +
									"id_predefinednotesid, created, modified) " +
									"values ( %s, '%s', '%s', '%s', %s, '%s', '%s')",
							thisanimal_id, note_text, TodayIs(), TimeIs(), predefined_note02, create_modified, create_modified );
 	    			Log.i("update notes ", "before cmd " + cmd);
 	    			dbh.exec( cmd );
    	 		}
    			if (predefined_note03 > 0) {
    	 			Log.i("take note","third note written");
					cmd = String.format("insert into animal_note_table (id_animalid, note_text, note_date, note_time, " +
									"id_predefinednotesid, created, modified) " +
									"values ( %s, '%s', '%s', '%s', %s, '%s', '%s')",
							thisanimal_id, note_text, TodayIs(), TimeIs(), predefined_note03, create_modified, create_modified );
 	    			Log.i("update notes ", "before cmd " + cmd);
 	    			dbh.exec( cmd );	
    	 		}
    			if (predefined_note04 > 0) {
    	 			Log.i("take note","fourth note written");
    	 			cmd = String.format("insert into animal_note_table (id_animalid, note_text, note_date, note_time, " +
 							"id_predefinednotesid, created, modified) " +
 							"values ( %s, '%s', '%s', '%s', %s, '%s', '%s')",
							thisanimal_id, note_text, TodayIs(), TimeIs(), predefined_note04, create_modified, create_modified );
 	    			Log.i("update notes ", "before cmd " + cmd);
 	    			dbh.exec( cmd );	
    	 		}
    			if (predefined_note05 > 0) {
    	 			Log.i("take note","fifth note written");
					cmd = String.format("insert into animal_note_table (id_animalid, note_text, note_date, note_time, " +
									"id_predefinednotesid, created, modified) " +
									"values ( %s, '%s', '%s', '%s', %s, '%s', '%s')",
							thisanimal_id, note_text, TodayIs(), TimeIs(), predefined_note05, create_modified, create_modified );
 	    			Log.i("update notes ", "before cmd " + cmd);
 	    			dbh.exec( cmd );	
    	 		}
			    }
			  })
			.setNegativeButton("Cancel",
			  new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
			    }
			  });
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	} 
	return "got note";
}
public static String TimeIs() {
	Calendar calendar = Calendar.getInstance();
    // 12 hour format
    //	int hour = cal.get(Calendar.HOUR);
    //  24 hour format
	int hourofday = calendar.get(Calendar.HOUR_OF_DAY);
	int minute = calendar.get(Calendar.MINUTE);
	int second = calendar.get(Calendar.SECOND);
	  
	return Make2Digits(hourofday) + ":" + Make2Digits(minute) + ":" + Make2Digits(second) ;
}
public static String TodayIs() {
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		return year + "-" + Make2Digits(month + 1) + "-" +  Make2Digits(day) ;
	}
 public static String Make2Digits(int i) {
		if (i < 10) {
			return "0" + i;
		} else {
			return Integer.toString(i);
		}
	}
 public static String YearIs() {
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		return Integer.toString(year) ;
	}
 
 public static void alertDialogShow(Context context, String title, String message) {
		Builder builder = new Builder( context );
		builder.setMessage( message)
	           .setTitle( title );
		builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int idx) {
	               // User clicked OK button 
	               }
	       });		
		AlertDialog dialog = builder.create();
		dialog.show();		 
 }
 
 public static double GetJulianDate(){
	 Calendar calendarDate = Calendar.getInstance();
	   int year = calendarDate.get(Calendar.YEAR);
	   int month = calendarDate.get(Calendar.MONTH) + 1;
	   int day = calendarDate.get(Calendar.DAY_OF_MONTH);
	   double hour = calendarDate.get(Calendar.HOUR_OF_DAY);
	   double minute = calendarDate.get(Calendar.MINUTE);
	   double second = calendarDate.get(Calendar.SECOND);
	   int isGregorianCal = 1;
	   int A;
	   int B;
	   int C;
	   int D;
	   double fraction = day + ((hour + (minute / 60) + (second / 60 / 60)) / 24);
	        
	   if (year < 1582)
	   {
	      isGregorianCal = 0;
	   }
	        
	   if (month < 3)
	   {
	      year = year - 1;
	      month = month + 12;
	   }

	   A = year / 100;
	   B = (2 - A + (A / 4)) * isGregorianCal;
	        
	   if (year < 0)
	   {
	      C = (int)((365.25 * year) - 0.75);
	   }
	   else
	   {
	      C = (int)(365.25 * year);
	   }
	        
	   D = (int)(30.6001 * (month + 1));
	   double JD = B + C + D + 1720994.5 + fraction;

	   return JD;
	}

	public static void copy(InputStream in, OutputStream out) throws IOException {
		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		out.flush();
		out.close();
		in.close();
	}
}
 
 