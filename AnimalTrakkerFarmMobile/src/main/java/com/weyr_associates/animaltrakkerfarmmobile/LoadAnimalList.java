package com.weyr_associates.animaltrakkerfarmmobile;

import static android.content.Intent.ACTION_OPEN_DOCUMENT;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.weyr_associates.animaltrakkerfarmmobile.database.DefaultSettingsTable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;


public class LoadAnimalList extends AppCompatActivity {
    String pkgversion;
    public String dbversion;
    private static final int REQUEST_CODE_OPEN_DATABASE_FILE = 1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.load_animal_database    );
    }

    public void allDone( View v )
    {
        finish();
    }

    public void copyRealDB( View v )
    {
        String temp;
        TextView        txtView = (TextView) findViewById( R.id.editText1 );
//    	Resources res = getResources();
        String 	dbfile = getString(R.string.real_database_file) ;
        Log.i("LoadAnimalList ", " got this as a file " + dbfile);
        // Moved this below the copy per note from Eric Coker on DXR
        DatabaseHandler dbh = new DatabaseHandler( this, dbfile );
        try {
            Log.i("in try ", " before going to DBH");
            dbh.copyRealDataBase(dbfile);
        }
        catch (IOException e) {
            Log.i("in try ", " got an exception ");
        }
        temp = "Created Database from Copy in Assets.";
        String cmd = String.format( "select count(*) from %s", "animal_table" );
//        try {
            Cursor crsr = ((Cursor) dbh.exec(cmd));
            crsr.moveToFirst();
            temp = String.format(temp + "\n" + "Records created in animal_table = " + String.valueOf(crsr.getInt(0)));
            txtView.setText(temp);
            dbh.closeDB();
//        }
//        catch (Exception e) {
//            // Missing data so  display an alert
//            AlertDialog.Builder builder = new AlertDialog.Builder( this );
//            builder.setMessage( R.string.please_install_database )
//                    .setTitle( R.string.please_install_database );
//            builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int idx) {
//                    // User clicked OK button
//                }
//            });
//            AlertDialog dialog = builder.create();
//            dialog.show();
//        }
    }

    public void selectFileDB(View v) {
        Log.i("in try ", " File DB");
        Intent intent = new Intent(ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_MIME_TYPES,
                new String[] { "application/vnd.sqlite3", "application/octet-stream" });
        startActivityForResult(intent, REQUEST_CODE_OPEN_DATABASE_FILE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result){
        if(requestCode == REQUEST_CODE_OPEN_DATABASE_FILE) {
            if(resultCode == RESULT_OK){
                String temp;
                TextView txtView = (TextView) findViewById( R.id.editText1 );

                Uri inUri = result.getData();

                if (inUri == null) {
                    return;
                }

                String outFileName = getString(R.string.database_path) + getString(R.string.real_database_file);
                File dbOutFile = new File(outFileName);

                DatabaseHandler dbh = new DatabaseHandler( this, outFileName );
                try {
                    Log.i("in try ", " before going to DBH");
                    InputStream inputStream = getContentResolver().openInputStream(inUri);
                    dbh.copy(inputStream, dbOutFile);
                }
                catch (IOException e) {
                    Log.i("in try ", " got an exception ");
                }
                DocumentMetadata docMetadata = Content.queryDocumentMetadata(this, inUri);
                String inDisplayName = (docMetadata != null) ? docMetadata.getDisplayName() : inUri.toString();
                temp = "Created Database from selected file " + inDisplayName;

//                Log.i("loading data ", temp);
                String dbname = getString(R.string.real_database_file);
//                Log.i("loading data ", dbname);
                String cmd = String.format( "select count(*) from %s", "animal_table" );
//                Log.i("command is ", cmd);
                Cursor crsr = ((Cursor) dbh.exec( cmd ));
                crsr.moveToFirst();
                temp = temp + "\n" + "Records created in animal_table = " + crsr.getInt( 0 );
                txtView.setText(temp);
                dbh.closeDB();
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, result);
        }
    }

    public void showRealDB( View v )
    {
        String 	dbfile = getString(R.string.real_database_file) ;
        DatabaseHandler dbh     = new DatabaseHandler( this, dbfile );
        TextView        txtView = (TextView) findViewById( R.id.editText1 );
//	todo Figure out how to show the entire real LambTracker database by selecting a table to show
//	and then displaying it
// 	Decided to just dump the animal table as a first cut
        String          theDump = dbh.dumpTable( "animal_table" );
        txtView.setText( theDump );
        dbh.closeDB();
    }
    private String SetDefaultStatusText() {
        String t = "Contact: Support@AnimalTrakker.com";
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return t;
        }
    }

    public void backupRealDB( View v ) throws IOException{
        DatabaseHandler dbh;
        Map<String,Integer> animaltrakker_defaults;
        String cmd;
        Cursor cursor;
        Object crsr;
        String 	dbfile = getString(R.string.real_database_file) ;
        String 	dbpath = getString(R.string.database_path) ;

        String	fullPath = dbpath + dbfile;
        File dbInFile = new File(fullPath);
        FileInputStream fis;
        try {
            fis = new FileInputStream(dbInFile);
        } catch (FileNotFoundException e) {
            Log.i("backup", "input database file not found" + fullPath);
            // Missing database so  display an alert
            AlertDialog.Builder builder = new AlertDialog.Builder( this );
            builder.setMessage( R.string.no_database )
                    .setTitle( R.string.no_database );
            builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int idx) {
                    // User clicked OK button
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
            return;
        }
        String today = TodayIs();
        String currentTimeString = new SimpleDateFormat("HH-mm-ss").format(new Date());
        // This is the version of the system code
        pkgversion = (SetDefaultStatusText());
        Log.i("AddDefault", "code version " + pkgversion);
        //todo Need to implement a more strong coordination between DB version and the code version that support it.
        dbh = new DatabaseHandler(this, dbfile);
        cmd = "SELECT * FROM pragma_user_version";
        crsr = dbh.exec(cmd);
        cursor = (Cursor) crsr;
        dbh.moveToFirstRecord();
        dbversion = cursor.getString(0);
        //dbversion = "4";
        Log.i("AddDefault", "DB version " + dbversion);

        dbh = new DatabaseHandler(this, dbfile);
        // Get the defaults and fill the defaults list for use later.
        animaltrakker_defaults = DefaultSettingsTable.readAsMapFrom(this, dbh);
        //todo Fix this to not be hard coded to the location in the defaults table
        String user_system_serial_number = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.USER_SYSTEM_SERIAL_NUMBER));
        Log.i("AddDefault", "serial Num " + user_system_serial_number);
        //todo Change this to store the DB BU in the downloads directory
        // Changed the order of the serial number and date for easier sorting by the users
        String outFileName = AppDirectories.databaseBackupsDirectory(this)+"/"+today+"_"+currentTimeString+"_AT_SERIAL_"+user_system_serial_number+"_DB"+dbversion+"_VER_"+pkgversion+".sqlite";
//        String outFileName = AppDirectories.getDatabaseBackupsDirectory()+"/animaltrakker_SERIAL"+user_system_serial_number+"_DB"+dbversion+"_VER"+pkgversion+".sqlite";
        Log.i("backup", outFileName);
        // Open the empty db as the output stream
        OutputStream output;
        try {
            output = new FileOutputStream(outFileName);
        } catch (FileNotFoundException e) {
            Log.i("backup", "output database file not found");
            fis.close();
            return;
        }

        // Transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = fis.read(buffer))>0){
            output.write(buffer, 0, length);
        }

        // Close the streams
        output.flush();
        output.close();
        fis.close();

        TextView        txtView = (TextView) findViewById( R.id.editText1 );
        txtView.setText("Real Data backed up to External Storage as file "+ outFileName );
    }
    private String TodayIs() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        return year + "-" + Make2Digits(month + 1) + "-" +  Make2Digits(day) ;
    }
    private String Make2Digits(int i) {
        if (i < 10) {
            return "0" + i;
        } else {
            return Integer.toString(i);
        }
    }

}
