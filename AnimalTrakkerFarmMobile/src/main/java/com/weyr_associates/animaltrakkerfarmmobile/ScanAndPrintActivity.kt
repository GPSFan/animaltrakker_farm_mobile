package com.weyr_associates.animaltrakkerfarmmobile

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import android.preference.PreferenceManager
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.weyr_associates.animaltrakkerfarmmobile.RequiredPermissions.areFulfilled
import com.weyr_associates.animaltrakkerfarmmobile.app.core.getAnnotatedText
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.ItemSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.TopButtonBar
import com.weyr_associates.animaltrakkerfarmmobile.app.main.MainActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.select.optionalIdColorSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.optionalIdTypeSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ActivityScanAndPrintBinding
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderService
import com.weyr_associates.animaltrakkerfarmmobile.eid.EIDReaderStatePresenter
import com.weyr_associates.animaltrakkerfarmmobile.model.IdColor
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType

class ScanAndPrintActivity : AppCompatActivity() {

    private lateinit var dbh: DatabaseHandler

    //TODO need to fix the type of tags based on defaults to handle multiple species No trich tags for rams
    private var readyToPrint = false
    private var autoPrint = false
    private var labelText: String = ""
    private var lastEID: String? = null

    private var selectedOtherIdType: IdType? = null
    private var selectedOtherIdColor: IdColor? = null

    private lateinit var binding: ActivityScanAndPrintBinding
    private lateinit var idTypeSelection: ItemSelectionPresenter<IdType>
    private lateinit var idColorSelection: ItemSelectionPresenter<IdColor>
    private lateinit var eidReaderStatePresenter: EIDReaderStatePresenter

    private val bluetoothWatcher = BluetoothWatcher(this)

    private var mService: Messenger? = null
    private var mIsBound = false

    private val mConnection: ServiceConnection = EIDReaderServiceConnection()
    private val mMessenger: Messenger = Messenger(IncomingHandler(Looper.getMainLooper()))

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityScanAndPrintBinding.inflate(layoutInflater)
        setContentView(binding.root)

        idTypeSelection = optionalIdTypeSelectionPresenter(binding.otherTagTypeSpinner) { item ->
            selectedOtherIdType = item
            //TODO: Remove UNIDIRECTIONAL
            idTypeSelection.displaySelectedItem(item)
        }

        idColorSelection = optionalIdColorSelectionPresenter(binding.otherTagColorSpinner) { item ->
            selectedOtherIdColor = item
            //TODO: Remove UNIDIRECTIONAL
            idColorSelection.displaySelectedItem(item)
        }

        dbh = DatabaseHandler.create(this)
        loadPreferences()

        ////////////////////////////////////
        checkIfServiceIsRunning()
        Log.i("Scan & Print", "back from isRunning")
        ////////////////////////////////////

        eidReaderStatePresenter = EIDReaderStatePresenter(this, binding.buttonPanelTop.imageScannerStatus)

        binding.buttonPanelTop.scanEIDButton.setOnClickListener { scanEid() }
        binding.buttonPanelTop.mainActionButton.setOnClickListener { printLabel() }

        binding.buttonPanelTop.show(
            TopButtonBar.UI_SCANNER_STATUS or
            TopButtonBar.UI_SCAN_EID or
            TopButtonBar.UI_ACTION_PRINT_LABEL
        )

        bluetoothWatcher.onActivationChanged = {
            onBluetoothActivationChanged()
        }

        binding.textDataDisclaimer.text = getAnnotatedText(
            R.string.text_scan_and_print_data_disclaimer
        )
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i("Scan & Print", " requestCode ")
        if (requestCode == 899) {
            Log.i("Scan & Print", " 899 xxx $resultCode")
            if (resultCode == 444) {
                Log.i("Scan & Print", " Result OK ")
                readyToPrint = true
            }
        } else if (requestCode == 799) {
            Log.i("Scan & Print", "799 xxx $resultCode")
            if (resultCode == 445) {
                Log.i("Scan & Print", "Result OK ")
            }
        } else if (resultCode == RESULT_OK) {
            Log.i("Scan & Print", " zzz ")
        }
    }

    public override fun onResume() {
        super.onResume()
        Log.i("Scan & Print", " OnResume")
        if (checkRequiredPermissions()) {
            checkIfServiceIsRunning()
            Log.i("Scan & Print", " OnResume after Check ")
            checkForPrint()
            clearData()
        }
        bluetoothWatcher.startWatching()
    }

    override fun onPause() {
        super.onPause()
        bluetoothWatcher.stopWatching()
    }

    override fun onDestroy() {
        super.onDestroy()
        doUnbindService()
        dbh.close()
    }

    private fun loadPreferences() {
        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        try {
            labelText = preferences.getString("label", "text") ?: "text"
            autoPrint = preferences.getBoolean("autop", false)
        } catch (nfe: NumberFormatException) {
            //NO-OP
        }
    }

    private fun scanEid() {
        // Here is where I need to get a tag scanned and put the data into the variable LastEID
        clearData()
        mService?.let { service ->
            try {
                //Request reload preferences
                var msg = Message.obtain(null, EIDReaderService.MSG_RELOAD_PREFERENCES)
                msg.replyTo = mMessenger
                service.send(msg)
                //Start eidService sending tags
                msg = Message.obtain(null, EIDReaderService.MSG_SEND_ME_TAGS)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
    }

    private fun gotEID(scannedEID: String) {
        lastEID = scannedEID
        binding.eidNewTagNumber.setText(lastEID)
        Log.i("Scan & Print", " with LastEID of $lastEID")
        //  Set Print Label button Green
        binding.buttonPanelTop.mainActionButton.isEnabled = true
    }

    private fun printLabel() {

        val otherTagTypeName = selectedOtherIdType?.name
        Log.i("Scan & Print", " type is $otherTagTypeName")

        val otherTagColorName = selectedOtherIdColor?.name
        Log.i("Scan & Print", " Color is $otherTagColorName")

        val otherTagNumber = binding.otherNewTagNumber.text.toString()
            .takeIf { it.isNotBlank() }
        Log.i("Scan & Print", " Number is /$otherTagNumber/")

        val hasOtherTag = otherTagTypeName != null &&
                otherTagColorName != null &&
                otherTagNumber != null

        val hasNoOtherTag = otherTagTypeName == null &&
                otherTagColorName == null &&
                otherTagNumber == null

        if (!hasOtherTag && !hasNoOtherTag) {
            showPartialIdEntryError()
            return
        }

        val contents = lastEID?.takeIf { 16 <= it.length }?.let {
            it.substring(0, 3) + it.substring(4, 16)
        } ?: return
        Log.i("Scan & Print", " contents $contents")

        try {
            // commented out for no other tag version
            // contents = contents + "_" + ThirdTag;
            val encodeIntent = Intent("weyr.LT.ENCODE")
            encodeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            encodeIntent.addCategory(Intent.CATEGORY_DEFAULT)
            encodeIntent.putExtra("ENCODE_FORMAT", "CODE_128")
            encodeIntent.putExtra("ENCODE_SHOW_CONTENTS", false)
            encodeIntent.putExtra("ENCODE_DATA", contents)
            encodeIntent.putExtra("ENCODE_AUTOPRINT", "false")
            if (autoPrint) {
                encodeIntent.putExtra("ENCODE_AUTOPRINT", "true")
                Log.i("Scan & Print", " autoprint is true ")
            }
            Log.i("Scan & Print", "labeltext $labelText")
            encodeIntent.putExtra("ENCODE_DATA1", labelText)
            encodeIntent.putExtra("ENCODE_DATE", Utilities.TodayIs() + "  " + Utilities.TimeIs())
            Log.i("Scan & Print", " before put extra sheepName ")
            if (otherTagNumber != null) {
                encodeIntent.putExtra(
                    "ENCODE_SHEEPNAME",
                    "$otherTagTypeName = $otherTagNumber $otherTagColorName"
                )
            }
            Log.i("Scan & Print", " after put extra sheepName $otherTagNumber $otherTagColorName")
            encodeIntent.setFlags(0)
            startActivityForResult(encodeIntent, 899)
            Log.i("Scan & Print", " after start activity encode ")
        } catch (r: Exception) {
            Log.v("PrintLabel ", " in ScanAndPrint RunTimeException: $r")
        }
        Log.i("Scan & Print", " Ready to print")
    }

    private fun checkForPrint() {
        if (readyToPrint) {
            val print = Intent(this, StartMenu::class.java)
            print.putExtra(Intent.EXTRA_STREAM, StartMenu.tempFileForBarcodePrinting(this))
            print.setType("image/png")
            if (autoPrint) {
                print.addFlags(Intent.FLAG_ACTIVITY_NO_USER_ACTION)
            }
            startActivityForResult(print, 799)
            readyToPrint = false
            binding.buttonPanelTop.mainActionButton.isEnabled = false
        }
    }

    private fun showPartialIdEntryError() {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title_partial_tag_entry_error)
            .setMessage(R.string.dialog_message_partial_tag_entry_error)
            .setPositiveButton(R.string.ok) { _, _ -> /*NO-OP*/ }
            .create()
            .show()
    }

    private fun clearData() {
        // clear out the display of everything
        binding.eidNewTagNumber.setText("")
        binding.otherNewTagNumber.setText("")
    }

    private fun checkRequiredPermissions(): Boolean {
        if (!areFulfilled(this)) {
            MainActivity.returnFrom(this)
            return false
        }
        return true
    }

    private fun onBluetoothActivationChanged() {
        checkRequiredPermissions()
    }

    private fun updateEidServiceConnectionStatus(eidReaderState: String) {
        eidReaderStatePresenter.updateReaderState(eidReaderState)
    }

    private fun checkIfServiceIsRunning() {
        //If the service is running when the activity starts, we want to automatically bind to it.
        Log.i("Scan & Print", "At isRunning?.")
        if (EIDReaderService.isRunning()) {
            doBindService()
        } else {
            startService(Intent(this, EIDReaderService::class.java))
            doBindService()
        }
    }

    private fun doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        bindService(Intent(this, EIDReaderService::class.java), mConnection, BIND_AUTO_CREATE)
        mIsBound = true
        mService?.let { service ->
            try {
                var msg = Message.obtain(null, EIDReaderService.MSG_RELOAD_PREFERENCES)
                msg.replyTo = mMessenger
                service.send(msg)
                //Request status update
                msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_STATUS, 0, 0)
                msg.replyTo = mMessenger
                service.send(msg)
                Log.i("Scan & Print", "At doBind4.")
                //Request full log from service.
                msg = Message.obtain(null, EIDReaderService.MSG_UPDATE_LOG_FULL, 0, 0)
                service.send(msg)
            } catch (e: RemoteException) {
                //NO-OP
            }
        }
    }

    fun doUnbindService() {
        mService?.let { service ->
            try {
                //Stop eidService from sending tags
                val msg = Message.obtain(null, EIDReaderService.MSG_NO_TAGS_PLEASE)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
            if (mIsBound) {
                // If we have received the service, and hence registered with it, then now is the time to unregister.
                try {
                    val msg = Message.obtain(null, EIDReaderService.MSG_UNREGISTER_CLIENT)
                    msg.replyTo = mMessenger
                    service.send(msg)
                } catch (e: RemoteException) {
                    // There is nothing special we need to do if the service has crashed.
                }
                // Detach our existing connection.
                unbindService(mConnection)
                mIsBound = false
            }
        }
    }

    private inner class IncomingHandler(looper: Looper) : Handler(looper) {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                EIDReaderService.MSG_UPDATE_STATUS -> {
                    val b1 = msg.data
                    val state = b1.getString("stat")
                    if (state != null) {
                        updateEidServiceConnectionStatus(state)
                    }
                }

                EIDReaderService.MSG_NEW_EID_FOUND -> {
                    val b2 = msg.data
                    val scannedEID = b2.getString("info1")
                    //We have a good whole EID number
                    Log.i("Scan & Print", "got eid of $lastEID")
                    if (scannedEID != null) {
                        gotEID(scannedEID)
                    }
                }

                EIDReaderService.MSG_UPDATE_LOG_APPEND -> {}
                EIDReaderService.MSG_UPDATE_LOG_FULL -> {}
                EIDReaderService.MSG_THREAD_SUICIDE -> {
                    Log.i("Scan & Print", "Service informed Activity of Suicide.")
                    doUnbindService()
                    stopService(Intent(this@ScanAndPrintActivity, EIDReaderService::class.java))
                }

                else -> super.handleMessage(msg)
            }
        }
    }

    private inner class EIDReaderServiceConnection : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            val localService = Messenger(service).also { mService = it }
            Log.i("Scan & Print", "At Service.")
            try {
                //Register client with service
                val msg = Message.obtain(null, EIDReaderService.MSG_REGISTER_CLIENT)
                msg.replyTo = mMessenger
                localService.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }

        override fun onServiceDisconnected(className: ComponentName) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            mService = null
        }
    }
}
