package com.weyr_associates.animaltrakkerfarmmobile;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.TopButtonBar;
import com.weyr_associates.animaltrakkerfarmmobile.database.Cursors;
import com.weyr_associates.animaltrakkerfarmmobile.database.DefaultSettingsTable;
import com.weyr_associates.animaltrakkerfarmmobile.database.StatesTable;
import com.weyr_associates.animaltrakkerfarmmobile.databinding.AddOwnerBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class AddOwner extends AppCompatActivity {

    private DatabaseHandler dbh;

    public CheckBox mailingsame;

    public String contact_last_name, contact_first_name, contact_middle_name, contact_company ;
    public Integer contact_title;
    public String contact_premise_id, contact_farm_prefix;
    public String contact_physical_address1, contact_mailing_address1;
    public String contact_physical_address2, contact_mailing_address2;
    public String contact_physical_city, contact_mailing_city;
    public Integer contact_physical_state, contact_mailing_state;
    public String contact_physical_postcode, contact_mailing_postcode;
    public Integer contact_physical_country, contact_mailing_country;
    public Integer contact_physical_county, contact_mailing_county;
    public String contact_main_phone, contact_cell_phone, contact_fax;
    public String contact_email, contact_email2, contact_website, country_premise_number;
    public String  premise_latitude, premise_longitude, premise_altitude ;
    public Integer altitude_units, longitude_units, id_contactsid, country_premise_id ;


    public Spinner physical_state_spinner;
    public Spinner mailing_state_spinner;

    public List<String> physical_state, physical_state_id_order, physical_state_display_order;
    public List<String> mailing_state, mailing_state_id_order, mailing_state_display_order;

    public List<String> physical_country, physical_country_id_order, physical_country_display_order;
    public List<String> mailing_country, mailing_country_id_order, mailing_country_display_order;

    public Cursor cursor, stateCursor, countryCursor, contactCursor;
    public Object crsr, statecrsr, countrycrsr, contactcrsr;
    public Map<String,Integer> animaltrakker_defaults;

    ArrayAdapter<String> dataAdapter;

    private AddOwnerBinding binding;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = AddOwnerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        String dbname = getString(R.string.real_database_file);
        Log.i("mobile", "dbname is " + dbname);
        String cmd;
        Button btn;
        TextView TV;
        dbh = new DatabaseHandler(this, dbname);
        // Get the defaults and fill the defaults list for use later.
        animaltrakker_defaults = DefaultSettingsTable.readAsMapFrom(this, dbh);
        //  Fill the state spinner
        physical_state_spinner = binding.physicalStateSpinner;
        physical_state = new ArrayList<String>();
        physical_state_id_order = new ArrayList<String>();
        physical_state_display_order = new ArrayList<String>();
        //  Select the fields we need from the list of states
        cmd = "SELECT * FROM state_table";
        Log.i("fill state spinners", "command is " + cmd);
        statecrsr = dbh.exec(cmd);
        stateCursor = (Cursor) statecrsr;
        dbh.moveToFirstRecord();
        physical_state_id_order.add("state_id");
        physical_state.add("Select a State");
        physical_state_display_order.add("state_display_order");
        for (stateCursor.moveToFirst(); !stateCursor.isAfterLast(); stateCursor.moveToNext()) {
            physical_state_id_order.add(Cursors.getString(stateCursor, StatesTable.Columns.ID));
            physical_state.add(Cursors.getString(stateCursor, StatesTable.Columns.NAME));
            physical_state_display_order.add(Cursors.getString(stateCursor, StatesTable.Columns.ORDER));
        }
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, physical_state);
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
        physical_state_spinner.setAdapter(dataAdapter);

        int defaultStateId = animaltrakker_defaults.get(DefaultSettingsTable.Columns.STATE_ID);
        physical_state_spinner.setSelection(defaultStateId); //Hard code for Colorado is 6
        Log.i("state default is  ", " is " + defaultStateId);

        // Creating adapter for spinner
        mailing_state_spinner = binding.mailingStateSpinner;
        dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, physical_state);
        dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
        mailing_state_spinner.setAdapter(dataAdapter);

        mailing_state_spinner.setSelection(defaultStateId); //Hard code for Colorado is 6
// todo add fields for county and fill from defaults. (requires a database schema update first)
        //  Set update database button green
        binding.buttonPanelTop.show(TopButtonBar.UI_ACTION_UPDATE_DATABASE);
        btn = binding.buttonPanelTop.getMainActionButton();
        btn.setEnabled(true);
        btn.setOnClickListener(view -> updateDatabase());

        binding.buttonPanelTop.show(TopButtonBar.UI_ACTION_UPDATE_DATABASE);
    }

    private void updateDatabase() {
        //todo add the county fields for both physical and mailing addresses requires a new database schema
        // todo this is hard coded for Delta County Colorado as the initial setting but needs to be defaults.

        String cmd, blank_space;
        TextView TV;
        Button btn;
        // Fill all contact fields to prevent any NULL entries
        contact_last_name = "";
        contact_first_name = "";
        contact_middle_name = "";
        contact_title = 6;
        contact_company = "";
        contact_farm_prefix = "";
        contact_physical_address1 = "";
        contact_physical_address2 = "";
        contact_physical_city = "";
        contact_physical_state = 6;
        contact_physical_postcode = "";
        contact_physical_country = 1;
        contact_physical_county = 16;
        contact_mailing_address1 = "";
        contact_mailing_address2 = "";
        contact_mailing_city = "";
        contact_mailing_state = 6;
        contact_mailing_postcode = "";
        contact_mailing_country = 1;
        contact_mailing_county = 16;
        contact_main_phone = "";
        contact_cell_phone = "";
        contact_fax = "";
        contact_email = "";
        contact_email2 = "";
        contact_website = "";
        contact_premise_id = "";
        country_premise_number = "";
        country_premise_id = 0;
        premise_latitude = "";
        altitude_units = 0;
        premise_longitude = "";
        longitude_units = 0;
        premise_altitude = "";
        altitude_units = 17;

        // Disable Update Database button to prevent getting 2 records at one time
        btn = binding.buttonPanelTop.getMainActionButton();
        btn.setEnabled(false);
// todo this needs a total re-work with the new V5 database schema
        //  Start getting all the data into what we need to insert into the contacts database
        TV = binding.firstNameText;
        contact_first_name = TV.getText().toString();
//        if(contact_first_name.isEmpty() || contact_first_name.length() == 0 ||  contact_first_name == null) {
        if( contact_first_name == null) {
            contact_first_name = "";
            Log.i("in iff ", "test for nulls ");
        }
        TV = binding.middleNameText;
        contact_middle_name = TV.getText().toString();
        TV = binding.lastNameText;
        contact_last_name = TV.getText().toString();
        TV = binding.farmNameText;
        contact_company = TV.getText().toString();
        TV = binding.premiseIdText;
        contact_premise_id = TV.getText().toString();
        TV = binding.farmPrefixText;
        contact_farm_prefix = TV.getText().toString();
        TV = binding.physicalAddress1;
        contact_physical_address1 = TV.getText().toString();
        TV = binding.physicalAddress2;
        contact_physical_address2 = TV.getText().toString();
        TV = binding.physicalCityText;
        contact_physical_city = TV.getText().toString();
        TV = binding.mailingCityText;
        contact_mailing_city = TV.getText().toString();
        TV = binding.mailingAddress1;
        contact_mailing_address1 = TV.getText().toString();
        TV = binding.mailingAddress2;
        contact_mailing_address2 = TV.getText().toString();
        physical_state_spinner = binding.physicalStateSpinner;
        contact_physical_state = physical_state_spinner.getSelectedItemPosition();
        cmd = String.format("SELECT id_countryid FROM state_table where id_stateid = '%s' ",contact_physical_state) ;
        statecrsr = dbh.exec(cmd);
        stateCursor = (Cursor) statecrsr;
        stateCursor.moveToFirst();
        contact_physical_country = dbh.getInt(StatesTable.Columns.COUNTRY_ID);
        mailing_state_spinner = binding.mailingStateSpinner;
        contact_mailing_state = mailing_state_spinner.getSelectedItemPosition();
        cmd = String.format("SELECT id_countryid FROM state_table where id_stateid = '%s' ",contact_mailing_state) ;
        statecrsr = dbh.exec(cmd);
        stateCursor = (Cursor) statecrsr;
        stateCursor.moveToFirst();
        contact_mailing_country = dbh.getInt(StatesTable.Columns.COUNTRY_ID);
        TV = binding.physicalZipcode;
        contact_physical_postcode = TV.getText().toString();
        TV = binding.mailingZipcode;
        contact_mailing_postcode = TV.getText().toString();
        TV = binding.mainPhoneText;
        contact_main_phone = TV.getText().toString();
        TV = binding.cellPhoneText;
        contact_cell_phone = TV.getText().toString();
        TV = binding.emailText;
        contact_email = TV.getText().toString();

        //  Got all the data now go update the database
       // todo needs update for version 5 DB schema crashes now must split out the address into the premise table and the company into the company table and
        // all the emais , phones etc into their tables
        cmd = String.format( "INSERT into contact_table (contact_last_name, contact_first_name, contact_middle_name, contact_title, contact_company " +
                ", contact_physical_address1, contact_physical_address2, contact_physical_city, contact_physical_state, contact_physical_postcode , " +
                        "id_contacts_physical_id_countryid, id_contacts_physical_id_countyid" +
                ", contact_mailing_address1, contact_mailing_address2, contact_mailing_city, contact_mailing_state, contact_mailing_postcode, " +
                        "id_contacts_mailing_id_countryid, id_contacts_mailing_id_countyid " +
                ", contact_main_phone, contact_cell_phone, contact_fax, contact_email1, contact_email2, contact_website) values " +
                "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', %s,  %s, '%s', '%s', '%s', %s, '%s', %s, %s, '%s', '%s', '%s', '%s', '%s', '%s' ) "
                , contact_last_name, contact_first_name, contact_middle_name, contact_title, contact_company
                , contact_physical_address1, contact_physical_address2, contact_physical_city, contact_physical_state, contact_physical_postcode, contact_physical_country, contact_physical_county
                , contact_mailing_address1, contact_mailing_address2, contact_mailing_city, contact_mailing_state, contact_mailing_postcode, contact_mailing_country, contact_mailing_county
                , contact_main_phone, contact_cell_phone, contact_fax, contact_email, contact_email2, contact_website);
//        Log.i("before insert ", "contact the command is " + cmd);
        dbh.exec(cmd);
//        Log.i("after insert ", "no info" );
        cmd = String.format("select last_insert_rowid()");
        contactcrsr = dbh.exec(cmd);
        contactCursor = (Cursor) contactcrsr;
        dbh.moveToFirstRecord();
        id_contactsid = dbh.getInt(0);
        Log.i("add an owner ", "the id_contactsid is " + String.valueOf(id_contactsid));
        cmd = String.format("INSERT INTO contacts_premise_table (id_contactsid, id_contactsstateid, state_premise_id, id_contactscountryid, country_premise_id" +
                ", premise_number, premise_latitude, latitude_units, premise_longitude, longitude_units, premise_altitude, altitude_units ) values " +
                "( %s, %s, '%s', %s, %s, '%s', '%s', %s, '%s', %s, '%s', %s ) "
                , id_contactsid ,contact_physical_state, contact_premise_id, contact_physical_country, country_premise_id, country_premise_number, premise_latitude
                , altitude_units, premise_longitude, longitude_units, premise_altitude, altitude_units);
        Log.i("before insert ", "contact the command is " + cmd);
        dbh.exec(cmd);
        Log.i("after insert ", "no info" );
    }

    public void onMailingSame (View v){
        // todo add the county field and set as per the mailing same as physical requires new database schema
        TextView TV;
        mailingsame = binding.checkBoxAddressSame;

        TV = binding.physicalAddress1;
        contact_physical_address1 = TV.getText().toString();
        contact_mailing_address1 = contact_physical_address1;
        TV = binding.mailingAddress1;
        TV.setText(contact_mailing_address1);

        TV = binding.physicalAddress2;
        contact_physical_address2 = TV.getText().toString();
        contact_mailing_address2 = contact_physical_address2;
        TV = binding.mailingAddress2;
        TV.setText(contact_mailing_address2);

        TV = binding.physicalCityText;
        contact_physical_city = TV.getText().toString();
        contact_mailing_city = contact_physical_city;
        TV = binding.mailingCityText;
        TV.setText(contact_mailing_city);

        mailing_state_spinner = binding.mailingStateSpinner;
        mailing_state_spinner.setSelection(physical_state_spinner.getSelectedItemPosition());

        TV = binding.physicalZipcode;
        contact_physical_postcode = TV.getText().toString();
        contact_mailing_postcode = contact_physical_postcode;
        TV = binding.mailingZipcode;
        TV.setText(contact_mailing_postcode);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        dbh.close();
    }
}
