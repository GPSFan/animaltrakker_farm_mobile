package com.weyr_associates.animaltrakkerfarmmobile.preferences

import androidx.activity.result.ActivityResultCaller

interface HasPrerequisites {
    fun registerPrerequisiteFulfillment(caller: ActivityResultCaller)
}
