package com.weyr_associates.animaltrakkerfarmmobile.preferences

import android.os.Bundle
import androidx.preference.CheckBoxPreference
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.Sdk

class BaaCodeSettingsPreferencesFragment : BasePreferencesFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupAutoSwitchBluetooth()
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences_baa_code_settings, rootKey)
    }

    private fun setupAutoSwitchBluetooth() {
        findPreference<CheckBoxPreference>("autoswitchbluetooth")?.let {
            it.isVisible = Sdk.allowsProgrammaticActivationOfBluetooth()
        }
    }
}