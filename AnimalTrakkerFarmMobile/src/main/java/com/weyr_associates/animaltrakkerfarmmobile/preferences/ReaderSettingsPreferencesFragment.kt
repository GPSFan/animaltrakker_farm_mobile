package com.weyr_associates.animaltrakkerfarmmobile.preferences

import android.os.Bundle
import android.text.InputType
import android.widget.EditText
import androidx.preference.CheckBoxPreference
import androidx.preference.EditTextPreference
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.Sdk

class ReaderSettingsPreferencesFragment : BasePreferencesFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupRaceReaderFileNameMod()
        setupAutoSwitchBluetooth()
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences_reader_settings, rootKey)
    }

    private fun setupRaceReaderFileNameMod() {
        val preference = findPreference<EditTextPreference>("filenamemod")
        preference?.setOnBindEditTextListener { editText: EditText ->
            editText.inputType = InputType.TYPE_CLASS_NUMBER
        }
    }

    private fun setupAutoSwitchBluetooth() {
        findPreference<CheckBoxPreference>("autoswitchbluetooth")?.let {
            it.isVisible = Sdk.allowsProgrammaticActivationOfBluetooth()
        }
    }
}
